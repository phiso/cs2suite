create table Trains (
	ID integer primary key autoincrement,
	Address integer not null unique,
	Name varchar(255) not null,
	ArtNr varchar(6) not null default '?',
	Epoch integer not null,
	Type integer not null default 0,
	Description text not null default '?',
	Picture varchar(255) not null default '?',
	Decoder integer not null,
	Vmin integer not null default 0,
	Vmax integer not null default 255,
	BrakeForce integer not null default 0,
	Acceleration integer not null default 0,
	Speedometer integer not null default 180,
	Volume integer not null default 0,	
	FOREIGN KEY(Decoder) REFERENCES Decoders(ID)
);

create table Functions (
	ID integer primary key autoincrement,
	Address integer not null unique,
	Mode integer not null default 0,
	Functionnumber integer not null,
	Name varchar(255) not null,
	Description text not null default '?',	
	FOREIGN KEY(Address) REFERENCES Trains(Address)
);

create table ArticleTypes (
	ID integer primary key autoincrement,
	Name varchar(255) not null,	
	Description text not null default '?',
	States integer not null default 2,
	Picture varchar(255) not null default '?'
);

create table Articles (
	ID integer primary key autoincrement,
	ArticleType integer not null,
	Name varchar(255) not null,
	Description text not null default '?',
	Timing integer not null default 200,
	Address integer not null unique,
	Decoder integer not null,	
	FOREIGN KEY(ArticleType) REFERENCES ArticleTypes(ID) on delete cascade,
	FOREIGN KEY(Decoder) REFERENCES Decoders(ID)
);

create table LayoutObjects (
	ID integer primary key autoincrement,
	Name varchar(255) not null,
	Description text not null default '?',
	States integer not null default 1,
	Icon varchar(255) not null default '?',
	Linkable boolean not null default false,
	Referable boolean not null default false
);

create table Layouts (
	ID integer primary key autoincrement,
	Name varchar(255) not null,
	Description text not null default '?',
	SizeX integer not null,
	SizeY integer not null
);

create table LayoutPositions (
	ID integer primary key autoincrement,
	LayoutID integer not null,
	LayoutObjectID integer not null,
	PosX integer not null,
	PosY integer not null,
	Rotation integer not null default 0,
	LinkedID integer
);

create table Systems (
	ID integer primary key autoincrement,
	Name varchar(255) not null,
	Description text not null default '?',
	Connection varchar(15) not null default '0.0.0.0'
);

create table SystemConnections (	
	System integer not null,
	Connection integer not null,
	ConnectionState boolean not null,
	PRIMARY KEY(System,Connection),
	FOREIGN KEY(System) REFERENCES Systems(ID) on delete cascade,
	FOREIGN KEY(Connection) REFERENCES Connections(ID)
);

create table SystemTrains (	
	System integer not null,
	Train integer not null,
	PRIMARY KEY(System,Train),
	FOREIGN KEY(System) REFERENCES Systems(ID) on delete cascade,
	FOREIGN KEY(Train) REFERENCES Trains(ID)
);

create table SystemArticles (	
	System integer not null,
	Article integer not null,
	lastState integer not null default 0,
	PRIMARY KEY(System,Article),
	FOREIGN KEY(System) REFERENCES Systems(ID) on delete cascade,
	FOREIGN KEY(Article) REFERENCES Articles(ID)
);

create table SystemLayouts (	
	System integer not null,
	Layout integer not null,
	PRIMARY KEY(System,Layout),
	FOREIGN KEY(System) REFERENCES Systems(ID) on delete cascade,
	FOREIGN KEY(Layout) REFERENCES Layouts(ID)
);
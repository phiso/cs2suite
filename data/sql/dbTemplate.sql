create table article_templates (
	id integer primary key autoincrement,
	states integer not null,	
	typ varchar(50) not null,	
	images varchar(255) not null
);

insert into article_templates (typ,states,images) values
("leftSwitch",2,"leftswitch_0.png,leftswitch_1.png"),
("rightSwitch",2,"rightswitch_0.png,rightswitch_1.png"),
("ySwitch",2,"yswitch_0.png,yswitch_1.png"),
("light1",2,"light1_0.png,light1_1.png"),
("light2",2,"light2_0.png,light2_1.png"),
("light0",2,"light0_0.png,light0_1.png"),
("barrier",2,"barrier_0.png,barrier_1.png"),
("threeWaySwitch",3,"threewayswitch_0.png,threewayswitch_1.png,threewayswitch_2.png"),
("crossSwitch1",2,"crossswitch1_0.png,crossswitch1_1.png"),
("crossSwitch2",4,"crossswitch2_0.png,crossswitch2_1.png,crossswitch2_2.png,crossswitch2_3.png"),
("std",2,"std_0.png,std_1.png"),
("andrewsCross",2,"andrewscross_0.png,andrewscross_1.png"),
("declutch",2,"declutch_0.png,declutch_1.png"),
("k84",3,"k84_0.png,k84_1.png,k84_2.png"),
("formSignalHP01",2,"formsignalHP01_0.png,formsignalHP01_1.png"),
("formSignalHP02",2,"formsignalHP02_0.png,formsignalHP02_1.png"),
("formSignalSH01",2,"formsignalSH01_0.png,formsignalSH01_1.png"),
("lightSignalHP01",2,"lightsignalHP01_0.png,lightsignalHP01_1.png"),
("lightSignalHP02",2,"lightsignalHP02_0.png,lightsignalHP02_1.png"),
("urcLightSignalSH01",2,"urclightsignalSH01_0.png,urclightsignalSH01_1.png"),
("urcLightSignalHP012SH01",4,"urclightsignalHP012SH01_0.png,urclightsignalHP012SH01_1.png,urclightsignalHP012SH01_2.png,urclightsignalHP012SH01_3.png"),
("turntable",1,"turntable.png");


create table layoutObjects(
	id integer primary key autoincrement,
	name varchar(255) not null,
	states integer not null default 1,	
	images varchar(255) not null default "",
	correlation boolean not null
);

insert into layoutObjects (name,states,images,correlation) values 
("arrow",1,"arrow.bmp",1),
("bumper",1,"bumper.bmp",0),
("corner",1,"corner.bmp",0),
("cross",1,"cross.bmp",0),
("declutch",2,"declutch_0.bmp,declutch_1.bmp",1),
("doublecorner",1,"doublecorner.bmp",0),
("doubleswitch",4,"doubleswitch_0.bmp,doubleswitch_1.bmp,doubleswitch_2.bmp,doubleswitch_3.bmp",1),
("formsignalhp02",2,"formsignal-HP02_0.bmp,formsignal-HP02_1.bmp",1),
("leftswitch",2,"leftswitch_0.bmp,leftswitch_1.bmp",1),
("rightswitch",2,"rightswitch_0.bmp,rightswitch_1.bmp",1),
("s88corner",2,"s88corner_0.bmp,s88corner_1.bmp",1),
("s88straight",2,"s88straight_0.bmp,s88straight_1.bmp",1),
("signal",4,"signal_0.bmp,signal_1.bmp,signal_2.bmp,signal_3.bmp",1),
("straight",1,"straight.bmp",0),
("subway",1,"subway.bmp",0),
("threewayswitch",3,"threewayswitch_0.bmp,threewayswitch_1.bmp,threewayswitch_2.bmp",1),
("track",3,"track_0.bmp,track_1.bmp,track_2.bmp",1),
("tunnel",1,"tunnel.bmp",0),
("yswitch",2,"yswitch_0.bmp,yswitch_1.bmp",1);

create table articles(
	id integer primary key autoincrement,
	name varchar(50) not null,
	template_id integer not null default 11,
	address integer not null,
	currentState integer not null,
	decoder varchar(5) not null default 'mm2',
	timing integer not null default 200,
	system_id integer
);

create table functions (
	id integer primary key autoincrement,
	name varchar(50) not null,
	picNumber integer not null	
);

create table train_functions (
	train_id integer not null references trains(id) on update cascade on delete cascade,
	function_id integer not null references functions(id) on update cascade on delete cascade,
	function_nr integer not null,
	toggleMode numeric not null default 1,
	timing integer not null default 0
);

create table trains(
	id integer primary key autoincrement,
	name varchar(100) not null,
	address integer not null,
	picture varchar not null,
	description text not null,
	direction integer not null,
	decoder varchar(5) not null default 'mm2',
	vMax integer not null,
	vMin integer not null,
	brakes integer not null,
	acceleration integer not null,
	volume integer not null,
	tacho integer not null,
	hasCV integer not null default 0,
    system varchar(50)
);

/*create table systems(
	id integer primary key autoincrement,
	name varchar(50) not null,
	s88controllers integer not null default 0,
	last_keyboardPage integer not null default 0
);*/
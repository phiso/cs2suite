/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.sound;

import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class SoundPlayer implements LineListener{

    private Sound sound;    
    private int fileNumber;
    private int loopCounter;
    
    public SoundPlayer(Sound sound){        
        this.sound = sound;
        fileNumber = 0;
        loopCounter = 0;
    }
    
    public void play(int loopCounter){
        this.loopCounter = loopCounter;
        sound.getSoundClip(fileNumber).start();
    }
    
    public void play(){
        play(1);
    }        
    
    @Override
    public void update(LineEvent event) {
        LineEvent.Type type = event.getType();
        if (type == LineEvent.Type.START){
            
        }else if (type == LineEvent.Type.STOP){
            if (fileNumber < sound.getFileCount()-1){
                fileNumber++;
                sound.getSoundClip(fileNumber).start();
            }else{                
                fileNumber = 0;
                if (loopCounter > 1){
                    sound.getSoundClip(fileNumber).start();
                    loopCounter--;
                }
            }
        }
    }
    
}

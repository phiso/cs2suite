/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.sound;

import java.util.ArrayList;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class MultitextSound extends Sound{

    private int startIndex;
    private int endIndex;
    private ArrayList<Integer> numberIndexes;
    private int leftIndex;
    private int rightIndex;
    
    public MultitextSound(String name, ArrayList<SoundFile> sounds) {
        super(name, sounds);
        startIndex = -1;
        endIndex = startIndex = leftIndex = rightIndex;        
    }
    
    private int findFile(String ident){
        for (int i = 0; i < getSoundFiles().size(); i++){
            if (getSoundFiles().get(i).getFileName().contains(ident)){
                return i;
            }
        } 
        return -1;
    }
    
    private int findNumberFile(int nr){
        return findFile("0".concat(Integer.toString(nr)));
    }
    
    private int findStartFile(){         
        return findFile("start");
    }
    
    private int findEndFile(){
        return findFile("end");
    }
    
    private int findLeftFile(){
        return findFile("left");
    }
    
    private int findRightFile(){
        return findFile("right");
    }

    //<editor-fold defaultstate="collapsed" desc="Getter/setter">
    /**
     * @return the startIndex
     */
    public int getStartIndex() {
        return startIndex;
    }
    
    /**
     * @return the endIndex
     */
    public int getEndIndex() {
        return endIndex;
    }
    
    /**
     * @return the numberIndexes
     */
    public ArrayList<Integer> getNumberIndexes() {
        return numberIndexes;
    }
    
    /**
     * @return the leftIndex
     */
    public int getLeftIndex() {
        return leftIndex;
    }
    
    /**
     * @return the rightIndex
     */
    public int getRightIndex() {
        return rightIndex;
    }
    //</editor-fold>    
}

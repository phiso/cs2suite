/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.sound;

import cs2suite.Models.Objects.CSObject;
import cs2suite.logic.CSLogger;
import java.io.IOException;
import java.util.ArrayList;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class Sound extends CSObject{
    
    private ArrayList<SoundFile> soundFiles;
    
    public Sound(String name, ArrayList<SoundFile> sounds) {
        super(name, "?");
        soundFiles = new ArrayList<>();
        sounds.stream().forEach((s) ->{
            try {
                soundFiles.add(s);
                s.openAudioStream();
            } catch (LineUnavailableException | IOException ex) {
                CSLogger.logEvent(4, ex, "Error loading audio file "+s.getSourceFile().getAbsolutePath());
            }
        });
    }
    
    public Clip getSoundClip(int index){
        return soundFiles.get(index).getAudioClip();
    }
    
    public ArrayList<SoundFile> getSoundFiles(){
        return soundFiles;
    }
    
    public int getFileCount(){
        return soundFiles.size();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.sound;

import cs2suite.logic.CSLogger;
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public final class SoundFile {
    private final File sourceFile;
    private final AudioInputStream audioInputStream;
    private final AudioFormat audioFormat;
    private final Clip audioClip;
    private final DataLine.Info audioInfo;
    
    public SoundFile(String source) throws UnsupportedAudioFileException, IOException, LineUnavailableException{        
        sourceFile = new File(source);        
        audioInputStream = AudioSystem.getAudioInputStream(sourceFile);
        audioFormat = audioInputStream.getFormat();
        audioInfo = new DataLine.Info(Clip.class, getAudioFormat());
        audioClip = (Clip) AudioSystem.getLine(audioInfo);        
    }        
    
    public void openAudioStream() throws LineUnavailableException, IOException{
        audioClip.open(audioInputStream);
    }

    /**
     * @return the sourceFile
     */
    public File getSourceFile() {
        return sourceFile;
    }
    
    public String getFileName(){
        return sourceFile.getName();
    }

    /**
     * @return the audioInputStream
     */
    public AudioInputStream getAudioInputStream() {
        return audioInputStream;
    }

    /**
     * @return the audioFormat
     */
    public AudioFormat getAudioFormat() {
        return audioFormat;
    }

    /**
     * @return the audioClip
     */
    public Clip getAudioClip() {
        return audioClip;
    }

    /**
     * @return the audioInfo
     */
    public DataLine.Info getAudioInfo() {
        return audioInfo;
    }
}

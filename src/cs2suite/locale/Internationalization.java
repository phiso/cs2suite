/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.locale;

import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class Internationalization {
    
    private static Internationalization instance;
    
    private static String country;
    private static String language;
    private static Locale currentLocale;
    private static ResourceBundle messages;    
    
    private Internationalization(String language, String country){
        super();
        Internationalization.language = language;        
        Internationalization.country = country;
        currentLocale = new Locale(Internationalization.language, Internationalization.country);
        messages = ResourceBundle.getBundle("cs2suite.locale.MessageBundle", currentLocale);
        //CSLogger.logEvent(111, null, "Loaded locale "+language+"_"+country);
    }
    
    public static Internationalization getInstance(String language, String country){
        if (Internationalization.instance == null){
            Internationalization.instance = new Internationalization(language, country);
        }
        return Internationalization.instance;
    }
    
    public static String getString(String key, String... wildcards){
        String rawString = Internationalization.getString(key);
        ArrayList<String> parts = new ArrayList<>();
        if (!rawString.contains("*")){
            return rawString;
        } else {
           int counter = 0;           
           while (rawString.indexOf("*") != -1){
               counter++;
               parts.add(rawString.substring(0, rawString.indexOf("*")));
               rawString = rawString.substring(rawString.indexOf("*")+1, rawString.length());
           }
           if (counter == wildcards.length){
               String result = "";
               for (String str : wildcards){
                   result += parts.get(counter)+str;
               }
               if (parts.size() > counter){
                   result += parts.get(counter+1);
               }
               return result;
           }
        }
        return null;
    }
    
    public static String getString(String key){
        return messages.getString(key);
    }
}

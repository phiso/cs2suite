/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.fxmlControllers;

import cs2suite.CS2Suite;
import cs2suite.csUIController.FXMLBundle;
import cs2suite.csUIController.FXMLController;
import cs2suite.managers.FXMLManager;
import cs2suite.logic.CSLogger;
import cs2suite.logic.Settings;
import cs2suite.locale.Internationalization;
import cs2suite.utils.SqliteDriver;
import cs2suite.utils.HelperUtils;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;

/**
 * FXML Controller class
 *
 * @author Philipp
 */
public class CSSettingsTabController extends FXMLController implements Initializable {

    // <editor-fold defaultstate="collapsed" desc=" FXML ">
    @FXML
    private ChoiceBox mainSettings_languageChoiceBox;
    @FXML
    private ChoiceBox mainSettings_loglevelChoiceBox;
    @FXML
    private CheckBox mainSettings_autoSearchIPCheckBox;
    @FXML
    private CheckBox layoutSettings_showGridCheckBox;
    @FXML
    private CheckBox settings_startMaximizedCheckBox;
    @FXML
    private ChoiceBox settings_trainControlCountChoiceBox;
    @FXML
    private ChoiceBox settings_s88ControllersChoiceBox;
    @FXML
    private CheckBox settings_minimizeToTrayCheckBox;
    @FXML
    private CheckBox settings_consoleLoggingCheckBox;
    @FXML
    private CheckBox settings_controlTrainWithKeys;
    @FXML
    private CheckBox settings_controlTrainWithMouse;
    @FXML
    private Button mainSettings_initDBFunctions;
    @FXML
    private Button settings_acceptButton;
    @FXML
    private Button settings_abortButton;
    @FXML
    private Button mainSettings_openLogFolderButton;
    @FXML
    private Button mainSettings_clearLogsButton;
    @FXML
    private Button mainSettings_langAskButton;
    @FXML
    private Button settings_resetToDefaultButton;
    @FXML
    private AnchorPane settings_gameSettingsAnchor;
    @FXML
    private TitledPane settings_mainSettingsPane;
    @FXML
    private TitledPane settings_gameCtrlSettingsPane;
    @FXML
    private TitledPane settings_layoutSettingsPane;
    @FXML
    private TitledPane settings_turntableSettingsPane;
    @FXML
    private TitledPane settings_craneSettingsPane;
    @FXML
    private TextField mainSettings_csipField;
    @FXML
    private Accordion mainSettingsAccordion;
    @FXML
    private ColorPicker layoutSettings_gridColorPicker;
    @FXML
    private Separator settings_seperator;
    @FXML
    private Slider settings_mouseTrainControlSpeedSlider;
    @FXML
    private GridPane gridPane;
// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Labels ">
    @FXML
    private Label mainSettings_mouseTrainControlSpeedLabel;
    @FXML
    private Label mainSettings_langLabel;
    @FXML
    private Label mainSettings_csipLabel;
    @FXML
    private Label mainSettings_loggingLabel;
    @FXML
    private Label mainSettings_loglevelLabel;
    @FXML
    private Label mainSettings_trainControlCountLabel;
    @FXML
    private Label mainSettings_s88Controllers;
    @FXML
    private Label layoutSettings_layoutColorLabel;
    @FXML
    private Label mainSettings_dbOptionsLabel;
    @FXML        
    private Label mainSettings_generalLabel;
    @FXML
    private Label mainSettings_trainControlLabel;
// </editor-fold>
        
    
    private GamePadSettingsController gamePadSettingsController;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {      
        
        CSLogger.logEvent(111, null, "Initialising GamepadSettings-FXML");
        try {
            FXMLBundle gamepadSettingsBundle = FXMLManager.loadFXML("GamePadSettings.fxml");
            settings_gameSettingsAnchor.getChildren().add(gamepadSettingsBundle.getParent());
            gamePadSettingsController = (GamePadSettingsController) gamepadSettingsBundle.getController();
        } catch (Exception ex) {
            CSLogger.logEvent(2, ex, "Error loading GamepadSettings-FXML");
        }
        CSLogger.logEvent(111, null, "Finished");        
    }
    
    @Override
    public void initAfter(){
        mainSettings_loglevelChoiceBox.getItems().setAll("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
        for (int i = 0; i < Settings.getMaxTrainControlCount(); i++) {
            settings_trainControlCountChoiceBox.getItems().add(String.valueOf(i + 1));
        }

        for (int i = 0; i < 8; i++) {
            settings_s88ControllersChoiceBox.getItems().add(String.valueOf(i));
        }

        settings_trainControlCountChoiceBox.getSelectionModel().select(Settings.getTrainControlCount() - 1);
        mainSettings_languageChoiceBox.getItems().setAll(Settings.getLanguages());
        mainSettings_languageChoiceBox.getSelectionModel().select(Settings.getLangIndex() - 1);
        mainSettings_loglevelChoiceBox.getSelectionModel().select(Settings.getLogLevel() - 1);
        mainSettingsAccordion.getPanes().get(0).setExpanded(true);
        applySettings();
        applyLang();
        gridPane.autosize();        
        //mainSettingsAccordion.setPrefWidth(((AnchorPane) mainSettingsAccordion.getParent()).getWidth());
    }
    
    @FXML
    private void handleResetToDefaultButton(ActionEvent e) throws IOException{
        Settings.loadConfigFile(new File(CS2Suite.CONF_DIR+"default_config.ini"));
    }
    
    @FXML
    private void handleLangAskButton(ActionEvent e) throws IOException {
        HelperUtils.showHelpText(new File(System.getProperty("user.dir") + "\\data\\help\\language_" + Settings.getLanguage().toLowerCase() + ".txt"), cs2SuiteGUIController.getStage());
    }    
    
    @FXML
    private void handleClearLogsButton(ActionEvent e) throws IOException {
        CSLogger.clearLogs();
    }

    @FXML
    private void handleInitDBFunctions(ActionEvent e) {
        try {
            SqliteDriver.initFunctions();
        } catch (SQLException ex) {
            CSLogger.logEvent(3, ex, "Error initialising Functions Table");
        }
    }
       
    @FXML
    private void handleSettingsAbortButton(ActionEvent e) {
        guiControllerWrapper.getController().abortSettings();
    }    
    
    @FXML
    private void handleSettingsAcceeptButton(ActionEvent e) throws IOException {
        guiControllerWrapper.getController().abortSettings();

        //CSSettings.setLanguage((String) mainSettings_languageChoiceBox.getSelectionModel().getSelectedItem());
        Settings.setImplicitExit(settings_minimizeToTrayCheckBox.isSelected());
        Settings.setAutoIPSetting(mainSettings_autoSearchIPCheckBox.isSelected());
        Settings.setConsoleLogging(settings_consoleLoggingCheckBox.isSelected());
        if (mainSettings_csipField.getText() != null && mainSettings_csipField.getText() != "") {
            Settings.setCs2IPAddress(mainSettings_csipField.getText());
        }
        Settings.setLogLevel(mainSettings_loglevelChoiceBox.getSelectionModel().getSelectedIndex() + 1);
        Settings.setS88Controllers(settings_s88ControllersChoiceBox.getSelectionModel().getSelectedIndex());
        if (Settings.getTrainControlCount() != settings_trainControlCountChoiceBox.getSelectionModel().getSelectedIndex() + 1) {
            Settings.setTrainControlCount(settings_trainControlCountChoiceBox.getSelectionModel().getSelectedIndex() + 1);
            cs2SuiteGUIController.setRestartFlag(true);
        }
        
        Settings.setControlTrainsWithKeys(settings_controlTrainWithKeys.isSelected());
        Settings.setControlTrainsWithMouse(settings_controlTrainWithMouse.isSelected());
        Settings.setMouseTrainControlSpeed(100 - Double.valueOf(settings_mouseTrainControlSpeedSlider.getValue()).intValue());
        gamePadSettingsController.saveSettings();
        Settings.save();
    }       
    
    private void applySettings() {
        settings_consoleLoggingCheckBox.setSelected(Settings.getConsoleLogging());
        settings_s88ControllersChoiceBox.getSelectionModel().select(Settings.getS88Controllers());
        mainSettings_autoSearchIPCheckBox.setSelected(Settings.getAutoIPSetting());
        mainSettings_languageChoiceBox.getSelectionModel().select(Settings.getLangIndex());
        mainSettings_loglevelChoiceBox.getSelectionModel().select(Settings.getLogLevel());
        settings_trainControlCountChoiceBox.getSelectionModel().select(Settings.getTrainControlCount());
        settings_minimizeToTrayCheckBox.setSelected(Settings.getImplicitExit());
        settings_controlTrainWithKeys.setSelected(Settings.getControlTrainsWithKeys());
        settings_controlTrainWithMouse.setSelected(Settings.getControlTrainsWithMouse());
        settings_mouseTrainControlSpeedSlider.setValue(100 - Settings.getMouseTrainControlSpeed());        
    }
    
    private void applyLang(){
        settings_abortButton.setText(Internationalization.getString("abort"));
        settings_acceptButton.setText(Internationalization.getString("accept"));
        settings_craneSettingsPane.setText(Internationalization.getString("cranesettings"));
        settings_gameCtrlSettingsPane.setText(Internationalization.getString("controllersettings"));
        settings_layoutSettingsPane.setText(Internationalization.getString("layoutsettings"));
        settings_mainSettingsPane.setText(Internationalization.getString("generalsettings"));
        settings_turntableSettingsPane.setText(Internationalization.getString("turntablesettings"));
        settings_startMaximizedCheckBox.setText(Internationalization.getString("startmaximized"));
        mainSettings_autoSearchIPCheckBox.setText(Internationalization.getString("autoipsearch"));
        mainSettings_clearLogsButton.setText(Internationalization.getString("clearlogs"));
        mainSettings_csipLabel.setText(Internationalization.getString("setCS2Ip"));
        mainSettings_langLabel.setText(Internationalization.getString("language"));
        mainSettings_loggingLabel.setText(Internationalization.getString("logging"));
        mainSettings_loglevelLabel.setText(Internationalization.getString("loglevel"));
        mainSettings_openLogFolderButton.setText(Internationalization.getString("openlogfolder"));
        layoutSettings_layoutColorLabel.setText(Internationalization.getString("layoutsettingsgridcolor"));
        layoutSettings_showGridCheckBox.setText(Internationalization.getString("layoutsettingsshowgrid"));
        mainSettings_trainControlCountLabel.setText(Internationalization.getString("settingstraincontrols"));
        mainSettings_s88Controllers.setText(Internationalization.getString("s88controllers"));
        mainSettings_initDBFunctions.setText(Internationalization.getString("initdbfunctions"));
        mainSettings_dbOptionsLabel.setText(Internationalization.getString("dboptions"));
        mainSettings_mouseTrainControlSpeedLabel.setText(Internationalization.getString("train_mousecontrolspeed"));
        settings_controlTrainWithKeys.setText(Internationalization.getString("train_controlwithkeys"));
        settings_controlTrainWithMouse.setText(Internationalization.getString("train_controlwithmouse"));
        mainSettings_generalLabel.setText(Internationalization.getString("generalsettings"));
        mainSettings_trainControlLabel.setText(Internationalization.getString("traincontrol"));
    }
    
    public void showMainSettingsPane(){
        mainSettingsAccordion.setExpandedPane(settings_mainSettingsPane);
    }

    public GamePadSettingsController getgamePadSettingsController(){
        return gamePadSettingsController;
    }
}

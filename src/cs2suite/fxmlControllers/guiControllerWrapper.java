/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.fxmlControllers;

import cs2suite.fxmlControllers.cs2SuiteGUIController;
import cs2suite.csObjects.CSSystem;

/**
 *------------------------------------------------------------------------------
 * Die gesamte Controller Klasse wird über diesen Singleton Wrapper im
 * ganzen Projekt zugreifbar gemacht.
 * So kann jederzeit ohne die Klasse übergeben zu müssen ein Zugriff auf die
 * haupt- und normalerweise auch einzige Instanz des mainControllers realisiert
 * werden.
 *------------------------------------------------------------------------------
 * @author Philipp
 */
public class guiControllerWrapper {

    private static guiControllerWrapper instance;
    private static cs2SuiteGUIController controller;

    private guiControllerWrapper(cs2SuiteGUIController controller) {
        guiControllerWrapper.controller = controller;
    }

    public static guiControllerWrapper getInstance(cs2SuiteGUIController controller) {
        if (guiControllerWrapper.instance == null) {
            guiControllerWrapper.instance = new guiControllerWrapper(controller);
        }
        return guiControllerWrapper.instance;
    }

    public static cs2SuiteGUIController getController() {        
        return guiControllerWrapper.controller;
    }
    
    /*
    public static CSSystem getCurrentSystem(){
        return guiControllerWrapper.controller.getCurrentSystem();
    }
    */
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.fxmlControllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class SoundManagerController implements Initializable {
    
    @FXML
    private Label soundListLabel;
    @FXML
    private Label soundFilesLabel;
    @FXML
    private Label soundNameLabel;
    
    @FXML
    private ListView soundListView;
    @FXML
    private ListView soundFilesListView;
    
    @FXML
    private TextField soundNameField;
    
    @FXML
    private Button reloadSoundFromLibraryButton;
    @FXML
    private Button updateSelectedSoundButton;
    @FXML
    private Button playSelectedSoundButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void handleReloadSoundsButton(ActionEvent event){
        
    }
    
    @FXML
    private void handleUpdateSoundButton(ActionEvent event){
        
    }
    
    @FXML
    private void handlePlaySoundButton(ActionEvent event){
        
    }
    
}

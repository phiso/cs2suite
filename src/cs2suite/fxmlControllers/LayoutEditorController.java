/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.fxmlControllers;

import cs2suite.logic.CSLogger;
import cs2suite.locale.Internationalization;
import cs2suite.csObjects.CSArticle;
import cs2suite.csObjects.CSLayout;
import cs2suite.csObjects.CSLayoutObject;
import cs2suite.csObjects.CSLayoutSquare;
import cs2suite.csUIController.FXMLController;
import cs2suite.csUIController.CS_UI_MainController;
import cs2suite.guiElements.LayoutObjectTableCell;
import cs2suite.utils.SqliteDriver;
import cs2suite.utils.HelperUtils;
import java.awt.MouseInfo;
import java.awt.Point;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author Philipp
 */
public class LayoutEditorController extends FXMLController implements Initializable {

    @FXML
    private SplitPane splitpane;
    @FXML
    private Button loadButton;
    @FXML
    private Button saveButton;
    @FXML
    private Button newButton;
    @FXML
    private Canvas canvas;
    @FXML
    private VBox vbox;
    @FXML
    private AnchorPane canvasAnchor;
    @FXML
    private Rectangle rectangle;

    private ContextMenu layoutObjectsContextMenu;
    private ContextMenu layoutContextMenu;
    private TableView layoutsTable;
    private TableView layoutObjectsTable;

    private Integer xPos = 0;
    private Integer yPos = 0;
    private String imagePath;
    private ArrayList<CSLayoutObject> layoutObjects;
    private CSLayoutObject selectedLayoutObject;
    private GraphicsContext mainGC;
    private Point lastMarked;
    private Integer xCount;
    private Integer yCount;
    //0=edit mode, 1=control mode
    private IntegerProperty mode;
    private Integer gridXY = 21;

    private ObservableList<CSLayoutObject> tableData;
    private CSLayout curLayout;
    private ObservableList<CSLayout> layouts;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tableData = FXCollections.observableArrayList();
        layoutObjects = new ArrayList<>();
        layouts = FXCollections.observableArrayList();

        mode = new SimpleIntegerProperty(0);
        canvas.setLayoutX(6);
        canvas.setLayoutY(6);
        mainGC = canvas.getGraphicsContext2D();
        lastMarked = new Point(0, 0);
        Double dxCount = canvas.getWidth() / gridXY;
        Double dyCount = canvas.getHeight() / gridXY;
        xCount = dxCount.intValue();
        yCount = dyCount.intValue();
        mainGC.setStroke(Color.BLACK);
        mainGC.setLineWidth(0.2);

        curLayout = new CSLayout(xCount, yCount);
        // create ContextMenus        
        layoutObjectsContextMenu = new ContextMenu();
        MenuItem moveUp = new MenuItem(Internationalization.getString("moveup"));
        MenuItem moveDown = new MenuItem(Internationalization.getString("movedown"));
        MenuItem createNew = new MenuItem(Internationalization.getString("createnew"));
        layoutObjectsContextMenu.getItems().addAll(createNew, moveUp, moveDown);

        layoutContextMenu = new ContextMenu();
        MenuItem delete = new MenuItem(Internationalization.getString("delete"));
        MenuItem link = new MenuItem(Internationalization.getString("changeArticle_MenuItem"));
        MenuItem unlink = new MenuItem(Internationalization.getString("unlinkArticle_MenuItem"));
        layoutContextMenu.getItems().addAll(link, unlink, delete);

        try {
            loadLayoutObjects();
        } catch (SQLException | IOException ex) {
            CSLogger.logEvent(3, ex, "Error loading LayoutObjects from Database");
        }

        splitpane.getDividers().get(0).setPosition(0.2);
        tableData.setAll(layoutObjects);
        layoutObjectsTable = createLayoutObjectsTable();
        layoutsTable = createLayoutsTable();
        vbox.getChildren().add(layoutsTable);
        vbox.getChildren().add(layoutObjectsTable);
        drawInitalGrid();

        // <editor-fold defaultstate="collapsed" desc=" handler ">
        mode.addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            switch (newValue.intValue()){
                case 0: // edit
                    rectangle.setStroke(Paint.valueOf("#e71c1c"));
                    break;
                case 1: // control 
                    rectangle.setStroke(Paint.valueOf("#30d416"));
                    break;
            }
        });
        
        layoutObjectsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<CSLayoutObject>() {
            @Override
            public void changed(ObservableValue<? extends CSLayoutObject> observable, CSLayoutObject oldValue, CSLayoutObject newValue) {
                selectedLayoutObject = newValue;
                guiControllerWrapper.getController().setInfoText3("selected: " + newValue.getName());
            }
        });

        canvas.setOnMouseMoved((MouseEvent event) -> {
            Double dxSquare = event.getX() / gridXY;
            Double dySquare = event.getY() / gridXY;
            xPos = dxSquare.intValue();
            yPos = dySquare.intValue();
            guiControllerWrapper.getController().setInfoText2("Position: x:" + xPos + ",y:" + yPos + " -> " + curLayout.getSquare(xPos, yPos).isOccupied());
            //this.markSquare(xPos, yPos);
        });

        canvas.setOnMouseClicked((MouseEvent event) -> {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                if (mode.get() == 0) {
                    // insert Object
                    if (selectedLayoutObject != null && !curLayout.getSquare(xPos, yPos).isOccupied()) {
                        this.insertIntoSquare(new Point(xPos, yPos), selectedLayoutObject);
                    }
                } else {
                    // Control Mode
                    CSLayoutSquare sq = curLayout.getSquare(xPos, yPos);
                    if (sq.isOccupied()) {
                        sq.getLayoutObject().toNextState();
                    } else {
                        CSLogger.logEvent(9, null, "Tried to Trigger empty square in Control-Mode");
                    }
                }
            } else if (event.getButton().equals(MouseButton.SECONDARY)) {
                Point mousePos = MouseInfo.getPointerInfo().getLocation();
                layoutContextMenu.show(canvas, mousePos.x, mousePos.y);
            } else if (event.getButton().equals(MouseButton.MIDDLE)) {
                if (curLayout.getSquare(xPos, yPos).isOccupied()) {
                    curLayout.getSquare(xPos, yPos).rotate();
                    insertRotatedSquare(new Point(xPos, yPos));
                }
                CSLogger.logEvent(9, null, "tried to rotate an empty position");
            }
        });

        mode.addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            switch (newValue.intValue()) {
                case 0: // edit
                    guiControllerWrapper.getController().setInfoText3(Internationalization.getString("editmode"));
                    break;
                case 1: // control
                    guiControllerWrapper.getController().setInfoText3(Internationalization.getString("controlmode"));
                    break;
            }

        });
        setMode(1); // switch to control-mode

        link.setOnAction((ActionEvent event) -> {

        });

// </editor-fold>
    }

    @Override
    public void initAfter() {
        loadButton.setText(Internationalization.getString("load"));
        saveButton.setText(Internationalization.getString("save"));
        newButton.setText(Internationalization.getString("newLayout"));

        vbox.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            sceneChange(cs2SuiteGUIController.getStage().getWidth(), cs2SuiteGUIController.getStage().getHeight());
        });
    }

    private ArrayList<CSArticle> filterArticles(String type) {
        ArrayList<CSArticle> results = new ArrayList<>();
        
        return results;
    }

    private void rotate(GraphicsContext gc, double angle, double px, double py) {
        Rotate r = new Rotate(angle, px, py);
        gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
    }

    private void insertIntoSquare(Point pos, CSLayoutObject obj) {
        mainGC.save();
        CSLayoutSquare temp = curLayout.getSquare(pos.x, pos.y);
        temp.setLayoutObject(obj);
        Point rp = this.getSquarePosition(pos.x, pos.y);
        mainGC.drawImage(obj.getImage(0), rp.x, rp.y);
        mainGC.restore();
    }

    private void insertRotatedSquare(Point pos) {
        CSLayoutSquare tempSquare = curLayout.getSquare(pos.x, pos.y);
        Integer angle = tempSquare.getRotation();
        Integer addX = null;
        Integer addY = null;
        switch (angle) {
            case 0:
                addX = 0;
                addY = 1;
                break;
            case 90:
                addX = 1;
                addY = -1;
                break;
            case 180:
                addX = -1;
                addY = -2;
                break;
            case 270:
                addX = -2;
                addY = 0;
                break;
        }
        Point rp = this.getSquarePosition(pos.x, pos.y);
        mainGC.save();
        rotate(mainGC, angle, rp.x + 9.5, rp.y + 9.5);
        mainGC.drawImage(tempSquare.getLayoutObject().getStateImage(), rp.x + addX, rp.y + addY);
        mainGC.restore();
    }

    private void drawInitalGrid() {
        for (int i = 0; i <= xCount; i++) {
            mainGC.moveTo(i * gridXY, 0);
            mainGC.lineTo(i * gridXY, canvas.getHeight());
            mainGC.stroke();
        }
        for (int i = 0; i <= yCount; i++) {
            mainGC.moveTo(0, i * gridXY);
            mainGC.lineTo(canvas.getWidth(), i * gridXY);
            mainGC.stroke();
        }
    }

    private void loadLayoutObjects() throws SQLException, IOException {
        //int count = SqliteDriver.getLayoutObjectCount();
        layoutObjects.addAll(SqliteDriver.getLayoutObjects());
    }

    private TableView createLayoutObjectsTable() {
        TableColumn<CSLayoutObject, LayoutObjectTableCell> layoutObjectColumn = new TableColumn<>(Internationalization.getString("layoutobjects"));
        layoutObjectColumn.setCellValueFactory(new PropertyValueFactory<CSLayoutObject, LayoutObjectTableCell>("tableCell"));
        layoutObjectColumn.setCellFactory(new Callback<TableColumn<CSLayoutObject, LayoutObjectTableCell>, TableCell<CSLayoutObject, LayoutObjectTableCell>>() {
            @Override
            public TableCell<CSLayoutObject, LayoutObjectTableCell> call(TableColumn<CSLayoutObject, LayoutObjectTableCell> param) {
                TableCell<CSLayoutObject, LayoutObjectTableCell> cell = new TableCell<CSLayoutObject, LayoutObjectTableCell>() {
                    @Override
                    public void updateItem(LayoutObjectTableCell item, boolean empty) {
                        if (item != null) {
                            VBox vbox = new VBox();
                            HBox hbox = new HBox();
                            hbox.setSpacing(2);
                            vbox.setSpacing(2);
                            ArrayList<ImageView> imageViews = new ArrayList<>();
                            for (int i = 0; i < item.getImages().size(); i++) {
                                imageViews.add(new ImageView(item.getImage(i)));
                                hbox.getChildren().add(imageViews.get(i));
                            }
                            vbox.getChildren().add(hbox);
                            vbox.getChildren().add(new Label(item.getName()));
                            setGraphic(vbox);
                        }
                    }
                };
                return cell;
            }
        });

        TableView tempTable = new TableView<CSLayoutObject>();
        tempTable.setPrefHeight(CS_UI_MainController.getScreenDimension().height / 2.5);
        layoutObjectColumn.setPrefWidth(180);
        tempTable.setItems(tableData);
        tempTable.getColumns().addAll(layoutObjectColumn);
        return tempTable;
    }

    private TableView createLayoutsTable() {
        TableColumn<String, CSLayout> layoutCol = new TableColumn<>(Internationalization.getString("layoutColumn"));
        layoutCol.setCellValueFactory(new PropertyValueFactory<String, CSLayout>("name"));
        TableView tempTable = new TableView<CSLayout>();
        layoutCol.setPrefWidth(180);
        tempTable.setItems(layouts);
        tempTable.setPrefHeight(CS_UI_MainController.getScreenDimension().height / 4.8);
        tempTable.getColumns().addAll(layoutCol);
        return tempTable;
    }

    @FXML
    private void handleNewButton(ActionEvent e) {
        
        Optional<String> response = HelperUtils.showTextInputDialog(Internationalization.getString("dialog_nameLayoutTitle") + "\n", 
                Internationalization.getString("dialog_nameLayoutMessage"));                
        response.ifPresent((String name) -> {
            if (!name.equals(null) && !name.equals("")) {
                curLayout.setName("Layout" + layouts.size());
            } else {
                curLayout.setName(name);
            }
        });
        layouts.add(curLayout); // save current active Layout
        layoutsTable.setItems(layouts);
        drawInitalGrid();
    }

    @FXML
    private void handleLoadButton(ActionEvent e) {

    }

    @FXML
    private void handleSaveButton(ActionEvent e) {
        Optional<String> response = HelperUtils.showTextInputDialog(Internationalization.getString("dialog_saveSystemTitle") + "\n", 
                Internationalization.getString("dialog_saveSystemMessage"));                
        response.ifPresent((String name) -> {
            if (!name.equals(null) && !name.equals("")) {
                try {
                    curLayout.saveToFile(name);
                    SqliteDriver.addLayoutAsTable(curLayout);
                } catch (IOException ex) {
                    CSLogger.logEvent(4, ex, "Error: unable to save Layout to File.");
                } catch (SQLException ex) {
                    CSLogger.logEvent(4, ex, "Error: SQL Error: couldnt write layout to new Table");
                }
            }
        });
    }

    private void markSquare(int x, int y) {
        Point rp = this.getSquarePosition(x, y);
        Point last = this.getSquarePosition(lastMarked.x, lastMarked.y);
        if (!curLayout.getSquare(lastMarked.x, lastMarked.y).isOccupied()) {
            mainGC.setFill(Color.WHITE);
            mainGC.fillRect(last.x + 1, last.y + 1, 18, 18);
            mainGC.fill();
        }
        if (!curLayout.getSquare(x, y).isOccupied()) {
            mainGC.setFill(Color.LIGHTGRAY);
            mainGC.fillRect(rp.x + 1, rp.y + 1, 18, 18);
            lastMarked.x = x;
            lastMarked.y = y;
            mainGC.fill();
        }
    }

    private Point getSquarePosition(int x, int y) {
        Point result = new Point();
        result.x = x * gridXY;
        result.y = y * gridXY;
        return result;
    }

    public int getMode() {
        return mode.get();
    }

    public IntegerProperty getModeProperty() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode.set(mode);
        if (mode == 1) {
            mainGC.setStroke(new Color(0, 0, 0, 0));
            redrawGrid();
        } else {

        }
    }

    public void redrawGrid() {
        drawInitalGrid();
    }

    public void sceneChange(double w, double h) {
        newButton.setPrefWidth(vbox.getWidth() - 12);
        loadButton.setPrefWidth(vbox.getWidth() - 12);
        saveButton.setPrefWidth(vbox.getWidth() - 12);
        layoutObjectsTable.setPrefWidth(vbox.getWidth() - 2);
        ((TableColumn) layoutObjectsTable.getColumns().get(0)).setPrefWidth(vbox.getWidth() - 12);
        ((TableColumn) layoutsTable.getColumns().get(0)).setPrefWidth(vbox.getWidth() - 12);
        layoutsTable.setPrefWidth(vbox.getWidth() - 2);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.fxmlControllers;

import com.sun.javafx.scene.traversal.Direction;
import cs2suite.csUIController.FXMLController;
import cs2suite.utils.GamePadWrapper;
import java.awt.Point;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Animation;
import javafx.animation.Animation.Status;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Point2D;
import javafx.scene.control.Slider;
import javafx.scene.transform.Translate;
import javafx.util.Duration;
import jdk.net.SocketFlow;

/**
 * FXML Controller class
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class PortalCraneController extends FXMLController implements Initializable {

    @FXML
    private ImageView railsImage;
    @FXML
    private ImageView bridgeImage;
    @FXML
    private ImageView craneImage;
    @FXML
    private ImageView cranePreview;
    @FXML
    private ImageView hookArrowsSolid;
    @FXML
    private ImageView hookArrowsAnimated;
    @FXML
    private AnchorPane parentAnchorPane;
    @FXML
    private Rectangle rectangle;
    @FXML
    private Pane animationBackgroundPane;
    @FXML
    private Button hookUpButton;
    @FXML
    private Button hookDownButton;
    @FXML
    private Button bridgeUpButton;
    @FXML
    private Button bridgeDownButton;
    @FXML
    private Button craneMLButton;
    @FXML
    private Button craneMRButton;
    @FXML
    private Button craneTLButton;
    @FXML
    private Button craneTRButton;

    @FXML
    private Label hookLabel;

    @FXML
    private Slider xslider;
    @FXML
    private Slider yslider;

    private Rotate craneRotateLeftTransform;
    private Rotate craneRotateRightTransform;
    private Timeline craneRotationLeftAnimation;
    private Timeline craneRotationRightAnimation;
    private Timeline bridgeTranslationAnimation;
    private Timeline craneTranslationAnimation;
    private Point2D craneImageDefaultPos;
    private Point2D bridgeImageDefaultPos;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }

    public void initAfter() {
        double cranePivotX = craneImage.getBoundsInLocal().getWidth() - (craneImage.getBoundsInLocal().getWidth() / 5);
        double cranePivotY = craneImage.getBoundsInLocal().getHeight() / 2;

        // Rotation Animation for crane housing
        craneRotateLeftTransform = new Rotate(45, cranePivotX, cranePivotY);
        craneImage.getTransforms().add(craneRotateLeftTransform);
        craneRotationLeftAnimation = new Timeline();
        craneRotationLeftAnimation.getKeyFrames()
                .add(new KeyFrame(
                                Duration.seconds(1.5),
                                new KeyValue(
                                        craneRotateLeftTransform.angleProperty(),
                                        0)
                        )
                );
        craneRotationLeftAnimation.setCycleCount(Animation.INDEFINITE);

        craneRotateRightTransform = new Rotate(45, cranePivotX, cranePivotY);
        craneImage.getTransforms().add(craneRotateRightTransform);
        craneRotationRightAnimation = new Timeline();
        craneRotationRightAnimation.getKeyFrames()
                .add(new KeyFrame(
                                Duration.seconds(1.5),
                                new KeyValue(
                                        craneRotateRightTransform.angleProperty(),
                                        100)
                        )
                );
        craneRotationRightAnimation.setCycleCount(Animation.INDEFINITE);

        // Translation Animation for moving the bridge with the crane
        bridgeTranslationAnimation = new Timeline();
        bridgeTranslationAnimation.setCycleCount(Animation.INDEFINITE);
        KeyValue brkv1 = new KeyValue(bridgeImage.yProperty(), -60);
        KeyValue brkv2 = new KeyValue(craneImage.xProperty(), -60);
        KeyFrame brkf = new KeyFrame(Duration.seconds(1.5), brkv1, brkv2);
        bridgeTranslationAnimation.getKeyFrames().add(brkf);

        // Translation Animation for the crane on the bridge
        craneTranslationAnimation = new Timeline();
        craneTranslationAnimation.setCycleCount(Animation.INDEFINITE);
        KeyValue crkv = new KeyValue(craneImage.yProperty(), 60);
        KeyFrame crkf = new KeyFrame(Duration.seconds(1.5), crkv);
        craneTranslationAnimation.getKeyFrames().add(crkf);

        craneImageDefaultPos = new Point2D(craneImage.getX(), craneImage.getY());
        bridgeImageDefaultPos = new Point2D(bridgeImage.getX(), bridgeImage.getY());                        
    }

    @FXML
    private void handleHookUpButton(ActionEvent e) {
        hookArrowsAnimated.setRotate(180);
        if (!hookArrowsAnimated.isVisible()) {
            hookArrowsAnimated.setVisible(true);
        } else {
            hookArrowsAnimated.setVisible(false);
        }
    }

    @FXML
    private void handleHookDownButton(ActionEvent e) {
        hookArrowsAnimated.setRotate(0);
        if (!hookArrowsAnimated.isVisible()) {
            hookArrowsAnimated.setVisible(true);
        } else {
            hookArrowsAnimated.setVisible(false);
        }
    }

    @FXML
    private void handleBridgeUpButton(ActionEvent e) {
        moveBridge(1, Direction.UP);
    }

    @FXML
    private void handleBridgeDownButton(ActionEvent e) {
        moveBridge(1, Direction.DOWN);
    }

    private void moveBridge(double speed, Direction direction) {
        double rate = 0;
        if (direction.equals(Direction.UP)) {
            rate = 1;
        } else if (direction.equals(Direction.DOWN)) {
            rate = -1;
        }
        rate *= speed;
        bridgeTranslationAnimation.setRate(rate);
        if (bridgeTranslationAnimation.getStatus().equals(Status.RUNNING)) {
            bridgeTranslationAnimation.playFrom(Duration.ZERO);
            bridgeTranslationAnimation.stop();
        } else if (bridgeTranslationAnimation.getStatus().equals(Status.STOPPED)) {
            bridgeTranslationAnimation.play();
        }
    }

    @FXML
    private void handleCraneMLButton(ActionEvent e) {
        moveCrane(1, Direction.LEFT);
    }

    @FXML
    private void handleCraneMRButton(ActionEvent e) {
        moveCrane(1, Direction.RIGHT);
    }

    private void moveCrane(double speed, Direction direction) {
        double rate = 0;
        if (direction.equals(Direction.LEFT)) {
            rate = 1;
        } else if (direction.equals(Direction.RIGHT)) {
            rate = -1;
        }
        rate *= speed;
        craneTranslationAnimation.setRate(rate);
        if (craneTranslationAnimation.getStatus().equals(Status.RUNNING)) {
            craneTranslationAnimation.playFrom(Duration.ZERO);
            craneTranslationAnimation.stop();
        } else if (craneTranslationAnimation.getStatus().equals(Status.STOPPED)) {
            craneTranslationAnimation.play();
        }
    }

    @FXML
    private void handleCraneTLButton(ActionEvent e) {
        turnCrane(1, Direction.LEFT);
    }

    @FXML
    private void handleCraneTRButton(ActionEvent e) {
        turnCrane(1, Direction.RIGHT);
    }

    private void turnCrane(double speed, Direction direction) {        
        if (direction.equals(Direction.LEFT)) {                
            craneRotationLeftAnimation.setRate(speed);
            if (craneRotationLeftAnimation.getStatus().equals(Status.RUNNING)) {
                craneRotationLeftAnimation.playFrom(Duration.ZERO);
                craneRotationLeftAnimation.stop();
            } else if (craneRotationLeftAnimation.getStatus().equals(Status.STOPPED)) {
                craneRotationLeftAnimation.play();
            }
        } else if (direction.equals(Direction.RIGHT)) {
            craneRotationRightAnimation.setRate(speed);
            if (craneRotationRightAnimation.getStatus().equals(Status.RUNNING)) {
                craneRotationRightAnimation.playFrom(Duration.ZERO);
                craneRotationRightAnimation.stop();
            } else if (craneRotationRightAnimation.getStatus().equals(Status.STOPPED)) {
                craneRotationRightAnimation.play();
            }
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.fxmlControllers;

import cs2suite.csUIController.FXMLController;
import cs2suite.csUIController.CS_UI_LoadSystemController;
import cs2suite.locale.Internationalization;
import cs2suite.logic.CSLogger;
import cs2suite.managers.SystemsManager;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import org.controlsfx.control.PopOver;

/**
 * FXML Controller class
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class LoadSystemController extends FXMLController implements Initializable {
    
    //<editor-fold defaultstate="collapsed" desc="FXML">
    @FXML
    private Label systemListLabel;
    @FXML
    private Label systemDescriptionLabel;
    
    @FXML
    private ListView systemsListView;
    
    @FXML
    private Button loadSystemButton;
    @FXML
    private Button abortButton;
    @FXML
    private Button importSystemButton;
    
    @FXML
    private TextArea systemDescriptionArea;
    
    @FXML
    private AnchorPane parentPane;
    //</editor-fold>
    
    private CS_UI_LoadSystemController loadSystemController;
    private PopOver thisPopOver;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       loadSystemController = new CS_UI_LoadSystemController(this);
        try {
            SystemsManager.getSystemsFromdatabase();
        } catch (SQLException | UnknownHostException ex) {
            CSLogger.logEvent(3, ex, "Error getting systems from Database!");
        }
    }    
    
    @Override
    public void initAfter(){
        systemListLabel.setText(Internationalization.getString("systems"));
        systemDescriptionLabel.setText(Internationalization.getString("description"));
        importSystemButton.setText(Internationalization.getString("importSystem"));
        abortButton.setText(Internationalization.getString("abort"));
        loadSystemButton.setText(Internationalization.getString("loadSystem"));
    }
    
    public void setPopOver(PopOver po){
        thisPopOver = po;
    }   
    
    @FXML
    public void handleImportSystemButton(ActionEvent event){
        
    }
    
    @FXML
    public void handleLoadSystemButton(ActionEvent event){
        
    }
    
    @FXML
    public void handleAbortButton(ActionEvent event){
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.fxmlControllers;

import cs2suite.Models.Objects.CSConnection;
import cs2suite.Models.Objects.CSSystem;
import cs2suite.csUIController.FXMLController;
import cs2suite.csUIController.CS_UI_NewSystemController;
import cs2suite.locale.Internationalization;
import cs2suite.managers.ConnectionManager;
import cs2suite.managers.SystemsManager;
import java.net.SocketException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.controlsfx.control.PopOver;

/**
 * FXML Controller class
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class NewSystemController extends FXMLController implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="FXML">
    @FXML
    private Label newSystemNameLabel;
    @FXML
    private Label newSystemConnectionsLabel;
    @FXML
    private Label availableConnectionsLabel;
    @FXML
    private Label newSystemDescriptionLabel;

    @FXML
    private TextField newSystemNameField;

    @FXML
    private TextArea newSystemDescriptionArea;

    @FXML
    private ListView availableConnectionsListView;

    @FXML
    private Button abortButton;
    @FXML
    private Button acceptButton;

    @FXML
    private ProgressIndicator availableConnectionsProgressIndicator;

    @FXML
    private CheckBox openSystemOnCreationCheckBox;

    @FXML
    private AnchorPane parentPane;

    private PopOver thisPopOver;
//</editor-fold>

    private static Stage thisStage;
    private CS_UI_NewSystemController newSystemController;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        newSystemController = new CS_UI_NewSystemController(this);
    }

    /**
     *
     */
    @Override
    public void initAfter() {
        newSystemNameLabel.setText(Internationalization.getString("name"));
        newSystemConnectionsLabel.setText(Internationalization.getString("connections"));
        newSystemDescriptionLabel.setText(Internationalization.getString("description"));
        availableConnectionsLabel.setText(Internationalization.getString("availableConnections"));
        abortButton.setText(Internationalization.getString("abort"));
        acceptButton.setText(Internationalization.getString("accept"));
        availableConnectionsProgressIndicator.setVisible(true);

        newSystemController.fillWithConnections(availableConnectionsListView);

        ConnectionManager.lastConnectionProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            if (newValue != null) {
                CSConnection connection = (CSConnection) newValue;
                availableConnectionsListView.getItems().add(new Label(connection.getIPAddressAsString()));
            }
        });
    }

    @FXML
    private void handleAbortButton(ActionEvent event) throws SocketException {
        newSystemController.closePopOver(thisPopOver);
    }

    @FXML
    private void handleAcceptButton(ActionEvent event) throws SocketException {
        int conIndex = availableConnectionsListView.getSelectionModel()
                .getSelectedIndex();
        
        newSystemController.setActiveConnection(conIndex);
        CSSystem system = new CSSystem(newSystemNameField.getText(),
                newSystemDescriptionArea.getText(),
                newSystemController.getConnection(conIndex));
        
        SystemsManager.addNewSystem(system,
                openSystemOnCreationCheckBox.isSelected());
        
        newSystemController.closePopOver(thisPopOver);
    }

    @FXML
    private void handleSelectConnectionButton(ActionEvent event) {

    }

    @FXML
    private void handleDeselectConnectionButton(ActionEvent event) {

    }

    public void setPopOver(PopOver po) {
        thisPopOver = po;
    }

    //@Override
    //public void setStage(Stage stage){
    //  thisStage = stage;
    //}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.fxmlControllers;

import cs2suite.csUIController.FXMLController;
import cs2suite.logic.CSLogger;
import cs2suite.logic.Settings;
import cs2suite.locale.Internationalization;
import cs2suite.utils.GamePadWrapper;
import cs2suite.utils.HelperUtils;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Controller;
import org.lwjgl.input.Controllers;

/**
 * FXML Controller class
 *
 * @author Philipp
 */
public class GamePadSettingsController extends FXMLController implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="FXML">
    @FXML
    private VBox controllerTestAnchor;
    @FXML
    private CheckBox useGameControllerCheckBox;
    @FXML
    private CheckBox trainCheckBox;
    @FXML
    private CheckBox craneCheckBox;
    
    @FXML
    private ChoiceBox controllerChoiceBox;
    @FXML
    private ChoiceBox stopChoiceBox;
    @FXML
    private Label stopLabel;
    @FXML
    private Button modeHelpButton;
    @FXML
    private ChoiceBox train_speedChoiceBox;
    @FXML
    private Label train_speedLabel;
    @FXML
    private ChoiceBox train_modeChoiceBox;
    @FXML
    private Label train_modeLabel;
    @FXML
    private ChoiceBox train_directionChoiceBox;
    @FXML
    private Label train_directionLabel;
    @FXML
    private ChoiceBox train_stopChoiceBox;
    @FXML
    private Label train_stopLabel;
    @FXML
    private ChoiceBox train_lightChoiceBox;
    @FXML
    private Label train_lightLabel;
    @FXML
    private ChoiceBox train_switchforwardChoiceBox;
    @FXML
    private Label train_switchforwardLabel;
    @FXML
    private ChoiceBox train_switchbackwardChoiceBox;
    @FXML
    private Label train_switchbackwardLabel;
    @FXML
    private ChoiceBox crane_moveBridgeChoiceBox;
    @FXML
    private Label crane_moveBridgeLabel;
    @FXML
    private ChoiceBox crane_moveCraneChoiceBox;
    @FXML
    private Label crane_moveCraneLabel;
    @FXML
    private ChoiceBox crane_turnCraneChoiceBox;
    @FXML
    private Label crane_turnCraneLabel;
    @FXML
    private ChoiceBox crane_liftHookChoiceBox;
    @FXML
    private Label crane_liftHookLabel;
    @FXML
    private ChoiceBox crane_lightChoiceBox;
    @FXML
    private Label crane_lightLabel;
    @FXML
    private ChoiceBox crane_lightHouseChoiceBox;
    @FXML
    private Label crane_lightHouseLabel;
    @FXML
    private ChoiceBox crane_hookChoiceBox;
    @FXML
    private Label crane_hookLabel;
    @FXML
    private TitledPane trainTitledPane;
    @FXML
    private TitledPane craneTitledPane;
    @FXML
    private TitledPane testTitledPane;
    @FXML
    private Button train_applyButton;
    @FXML
    private Button crane_applyButton;
    @FXML
    private Button applyButton;
    @FXML
    private Label slowModeFactorLabel;
    @FXML
    private TextField slowModeFactorField;
//</editor-fold>

    private final Label axesLabel = new Label(Internationalization.getString("axis"));
    private final Label buttonsLabel = new Label(Internationalization.getString("buttons"));
    private final Label povLabel = new Label(Internationalization.getString("pov"));

    private ListView axisArea = new ListView();
    private Label buttonCountLabel = new Label();
    private Label activeButtonLabel = new Label();
    private Label povXLabel = new Label();
    private Label povYLabel = new Label();

    private Controller controller;
    private ArrayList<Controller> polledControllers;
    private ArrayList<String> axis;
    private ArrayList<String> buttons;

    private GamePadWrapper gamepadWrapper;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        polledControllers = new ArrayList<>();

        try {
            Controllers.create();
        } catch (LWJGLException ex) {
            CSLogger.logEvent(4, ex, "Error initialising LWJGL Controllers");
        }
        Controllers.poll();
        controllerChoiceBox.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            try {
                handleControllerChoiceBox(newValue.intValue());
            } catch (IOException ex) {
                CSLogger.logEvent(5, ex, "Error initializing Gamecontroller Connection");
            }
        });

        slowModeFactorField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {

        });
    }

    public void initAfter() {
        controllerChoiceBox.getItems().clear();
        for (int i = 0; i < Controllers.getControllerCount(); i++) {
            polledControllers.add(Controllers.getController(i));
            controllerChoiceBox.getItems().add(Controllers.getController(i).getName());
        }
    }

    @FXML
    private void handleControllerChoiceBox(Integer selectedIndex) throws IOException {
        try {                        
            gamepadWrapper = GamePadWrapper.getInstance(controllerChoiceBox.getItems().get(selectedIndex).toString());
            controller = GamePadWrapper.getController();
            
        } catch (LWJGLException ex) {
            CSLogger.logEvent(4, ex, "Failed to create CSGamepadWrapper instance");
        }
        Platform.runLater(() -> {
            initTestPane();
        });

        // init selecte controller controls
        axis = new ArrayList<>();
        buttons = new ArrayList<>();
        for (int i = 0; i < controller.getAxisCount(); i++) {
            axis.add(controller.getAxisName(i));
            axisArea.getItems().add(new Label());
        }
        for (int i = 0; i < controller.getButtonCount(); i++) {
            buttons.add(controller.getButtonName(i));
        }

        initControllerLists();

        stopLabel.setDisable(false);
        stopChoiceBox.setDisable(false);

        train_modeChoiceBox.getItems().addAll("Direct", "Buffered");
        train_modeChoiceBox.getSelectionModel().select(0);

        // <editor-fold defaultstate="collapsed" desc=" handler ">
        buttonCountLabel.setText(String.valueOf(controller.getButtonCount()));
        gamepadWrapper.getButtonProperty().addListener((ObservableValue<? extends Number> observable1, Number oldValue1, Number newValue1) -> {
            Platform.runLater(() -> {
                activeButtonLabel.setText("Active: " + String.valueOf(newValue1.intValue()));
            });
        });

        for (int i = 0; i < axis.size(); i++) {
            final int num = i;
            gamepadWrapper.getAxisProperty(i).addListener((ObservableValue<? extends Number> observable1, Number oldValue1, Number newValue1) -> {
                Platform.runLater(() -> {
                    ((Label) axisArea.getItems().get(num)).setText(axis.get(num) + ": " + HelperUtils.round(newValue1.doubleValue(), 2));
                });
            });
        }

        gamepadWrapper.getPOVXProperty().addListener((ObservableValue<? extends Number> observable1, Number oldValue1, Number newValue1) -> {
            Platform.runLater(() -> {
                povXLabel.setText("X: " + String.valueOf(newValue1.intValue()));
            });
        });

        gamepadWrapper.getPOVYProperty().addListener((ObservableValue<? extends Number> observable1, Number oldValue1, Number newValue1) -> {
            Platform.runLater(() -> {
                povYLabel.setText("Y: " + String.valueOf(newValue1.intValue()));
            });
        });

        train_directionChoiceBox.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable1, Number oldValue1, Number newValue1) -> {

        });
    }

    private void resetControllerLists() {
        axis.clear();
        buttons.clear();
        for (int i = 0; i < controller.getAxisCount(); i++) {
            axis.add(controller.getAxisName(i));
        }
        for (int i = 0; i < controller.getButtonCount(); i++) {
            buttons.add(controller.getButtonName(i));
        }
    }

    private void initControllerLists() {
        train_directionChoiceBox.getItems().addAll(buttons);
        train_lightChoiceBox.getItems().addAll(buttons);
        train_speedChoiceBox.getItems().addAll(axis);
        train_stopChoiceBox.getItems().addAll(buttons);
        train_switchbackwardChoiceBox.getItems().addAll(buttons);
        train_switchforwardChoiceBox.getItems().addAll(buttons);
    }

    @FXML
    private void handleUseControllerCheckBox() {
        Controllers.poll();
        initAfter();
        if (useGameControllerCheckBox.isSelected()) {
            controllerChoiceBox.setDisable(false);
            trainCheckBox.setDisable(false);
            craneCheckBox.setDisable(false);
        } else {
            controllerChoiceBox.setDisable(true);
            trainCheckBox.setDisable(true);
            craneCheckBox.setDisable(true);
        }
    }

    @FXML
    private void handleTrainCheckBox() {
        if (trainCheckBox.isSelected()) {
            trainTitledPane.setDisable(false);
            train_directionChoiceBox.getSelectionModel().select(0);
            train_lightChoiceBox.getSelectionModel().select(0);
            train_modeChoiceBox.getSelectionModel().select(0);
            train_speedChoiceBox.getSelectionModel().select(0);
            train_stopChoiceBox.getSelectionModel().select(0);
            train_switchbackwardChoiceBox.getSelectionModel().select(0);
            train_switchforwardChoiceBox.getSelectionModel().select(0);
        } else {
            trainTitledPane.setDisable(true);
        }
    }

    @FXML
    private void handleCraneCheckBox() {
        if (craneCheckBox.isSelected()) {
            craneTitledPane.setDisable(false);
        } else {
            craneTitledPane.setDisable(true);
        }
    }

    @FXML
    private void handleTrainApplyButton() {

    }

    @FXML
    private void handleCraneApplyButton() {
        
    }

    private void initTestPane() {
        controllerTestAnchor.getChildren().add(axesLabel);
        controllerTestAnchor.getChildren().add(axisArea);
        controllerTestAnchor.getChildren().add(new Separator(Orientation.HORIZONTAL));
        controllerTestAnchor.getChildren().add(povLabel);
        controllerTestAnchor.getChildren().add(povXLabel);
        controllerTestAnchor.getChildren().add(povYLabel);
        controllerTestAnchor.getChildren().add(new Separator(Orientation.HORIZONTAL));
        controllerTestAnchor.getChildren().add(buttonsLabel);
        controllerTestAnchor.getChildren().add(buttonCountLabel);
        controllerTestAnchor.getChildren().add(activeButtonLabel);
    }

    public Controller getController() {
        return controller;
    }

    

    private void checkSettings(Integer gcIndex) throws IOException {
        handleUseControllerCheckBox();
        handleControllerChoiceBox(gcIndex);
        handleCraneCheckBox();
        handleTrainCheckBox();
    }

    public void saveSettings() {
        try {
            Settings.setUsingGamecontroller(useGameControllerCheckBox.isSelected());
            Settings.setGameControllerIndex(controllerChoiceBox.getSelectionModel().getSelectedIndex());
            Settings.setGameControllerName(controllerChoiceBox.getSelectionModel().getSelectedItem().toString());
            Settings.setController_controlTrains(trainCheckBox.isSelected());
            Settings.setController_controlCrane(craneCheckBox.isSelected());            
        } catch (Exception e) {
            CSLogger.logEvent(4, e, "Error saving Gamecontroller settings!");
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.fxmlControllers;

import cs2suite.logic.CSLogger;
import cs2suite.logic.Settings;
import cs2suite.locale.Internationalization;
import cs2suite.Models.Objects.Train;
import cs2suite.csUIController.FXMLController;
import cs2suite.csUIController.CS_UI_MainController;
import cs2suite.network.Package;
import cs2suite.utils.CSIcons;
import eu.hansolo.enzo.gauge.Gauge;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author Philipp
 */
public class TrainControlController extends FXMLController implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="FXML">
    @FXML
    private AnchorPane view;
    @FXML
    private AnchorPane viewAnchor;
    @FXML
    private ImageView trainImage;
    @FXML
    private Label picPath;
    @FXML
    private Label name;
    @FXML
    private Label adress;
    @FXML
    private Label decoder;
    @FXML
    private Label directionLabel;
    @FXML
    private Slider speedSlider;
    @FXML
    private Button directionButton;
    @FXML
    private Button settingsButton;
    @FXML
    private TitledPane pane;
    @FXML
    private VBox tableAnchor;
    @FXML
    private Rectangle rectangle;
//</editor-fold>

    private Gauge speedGauge;
    private Train train;
    private TableView functionTable;
    private MouseButton mouseButtonStatus;
    private Boolean userChanged = false;
    private Boolean isInControl = false;
    private ImageView settingsImageView;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        speedGauge = new Gauge();
        speedGauge.setAnimated(true);
        speedGauge.setDropShadowEnabled(true);
        speedGauge.setMinValue(0);
        speedSlider.setMin(0);
        speedGauge.setPrefSize(180, 180);
        speedGauge.setLayoutX(10);
        speedGauge.setLayoutY(20);

        speedGauge.setOnMouseMoved((MouseEvent event) -> {

        });

        /*
         speedGauge.setOnMousePressed((MouseEvent event) -> {
         if (event.getButton().equals(MouseButton.PRIMARY)) {
         int mx = (int) (speedGauge.getWidth() / 2);
         int my = (int) (speedGauge.getHeight() / 2);
         int x = (int) event.getX();
         int y = (int) event.getY();
         Double alpha = CSHelperUtils.calcAngleInCircle(new Point(mx, my), new Point(x, y));
         Double fac = speedGauge.getMaxValue() / (270);
         if (alpha < 45) {
         speedGauge.setValue((220 + alpha) * fac);
         } else if (alpha > 135) {
         speedGauge.setValue((alpha - 135) * fac);
         }
         }
         });
        
        speedGauge.setOnMouseReleased((MouseEvent event) -> {
            train.setSpeed((int) (speedGauge.getValue() * (1000 / train.getTacho().doubleValue())));
        });
        */

        speedSlider.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
           //TODO
        });

        speedSlider.setOnMouseReleased((MouseEvent event) -> {
          //TODO
        });

        pane.setOnMouseEntered((MouseEvent event) -> {
            if (!getIsInControl()) {
                rectangle.setStroke(Paint.valueOf("#111111"));
            }
        });

        pane.setOnMouseExited((MouseEvent event) -> {
            if (!getIsInControl()) {
                rectangle.setStroke(Paint.valueOf("#5a5858"));
            }
        });

        pane.setOnMouseClicked((MouseEvent event) -> {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                CS_UI_MainController.deselectAllTrainControllers();
                rectangle.setStroke(Paint.valueOf("#20d80f"));
                setIsInControl((Boolean) true);
                guiControllerWrapper.getController().setInfoText4(this.pane.getText());
            }
        });

        viewAnchor.getChildren().add(speedGauge);
        functionTable = new TableView();
    }

    @Override
    public void initAfter() throws IOException {
        picPath.setText("");
        rectangle.setStroke(Paint.valueOf("#5a5858"));
        settingsButton.setGraphic(CSIcons.getIconView("settings", 16, 14));
        directionButton.setText("");
        directionButton.setGraphic(CSIcons.getIconView("default", 16, 16));
    }

    public void setTrain(Train train) throws IOException {        
        this.train = train;
        BufferedImage bufimg = ImageIO.read(new File(train.getProfilePicture()));
        Image img = SwingFXUtils.toFXImage(bufimg, null);
        trainImage.setImage(img);        
        functionTable = null;
        speedGauge.setMaxValue(train.getSpeedometer());
        speedSlider.setMax(train.getSpeedometer());
        speedGauge.setMinorTickSpace(10);
        speedGauge.setMajorTickSpace(50);
        pane.setText(train.getName() + " Adr. " + train.getAddress());        
        tableAnchor.getChildren().clear();
        tableAnchor.getChildren().add(functionTable);
        adress.setText(Internationalization.getString("address"));
        getName().setText(train.getName());
        decoder.setText(train.getDecoder().nameString());                

        /*
         handling mousewheel on TrainController
         Allow control only if the current controlpanel is focused/selected and
         the Setting is activated
         */
        cs2SuiteGUIController.getStage().getScene().addEventFilter(ScrollEvent.SCROLL, (ScrollEvent event) -> {
            
        });

        cs2SuiteGUIController.getStage().getScene().addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            
        });
    }

    @FXML

    private void handleDirectionButton(ActionEvent e) throws IOException {
        
    }

    @FXML
    private void handleSettingsButton(ActionEvent e) throws IOException {
        
    }

    public Node getView() {
        return view;
    }

    public void setTitle(String title) {
        pane.setText(title);
    }
    
    public String getTitle(){
        return pane.getText();
    }

    /**
     * @return the userChanged
     */
    public Boolean getUserChanged() {
        return userChanged;
    }

    /**
     * @param userChanged the userChanged to set
     */
    public void setUserChanged(Boolean userChanged) {
        this.userChanged = userChanged;
    }

    public void setBorderColor(Paint color) {
        rectangle.setStroke(color);
    }

    /**
     * @return the isInControl
     */
    public Boolean isInControl() {
        return getIsInControl();
    }

    /**
     * @param isInControl the isInControl to set
     */
    public void setIsInControl(Boolean isInControl) {
        this.isInControl = isInControl;
    }

    /**
     * @return the name
     */
    public Label getName() {
        return name;
    }

    /**
     * @return the train
     */
    public Train getTrain() {
        return train;
    }

    /**
     * @return the isInControl
     */
    public Boolean getIsInControl() {
        return isInControl;
    }

}

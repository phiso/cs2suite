/*
 * Author: Philipp Sommer <phiso08@aol.de>
 */
package cs2suite.fxmlControllers;

// <editor-fold defaultstate="collapsed" desc=" imports ">
import cs2suite.Models.Objects.Train;
import cs2suite.logic.CSLogger;
import cs2suite.logic.Settings;
import cs2suite.locale.Internationalization;
import cs2suite.csFile.CSFile;
import cs2suite.csFile.CSFileViewer;
import cs2suite.csObjects.CSSystem;
import cs2suite.csUIController.FXMLBundle;
import cs2suite.csUIController.FXMLController;
import cs2suite.managers.FXMLManager;
import cs2suite.csUIController.CS_UI_MainController;
import cs2suite.managers.SystemsManager;
import cs2suite.network.ArticleListener;
import cs2suite.network.CommandParser;
import cs2suite.network.ConfigStreamListener;
import cs2suite.network.CoreListener;
import cs2suite.network.Packet;
import cs2suite.network.S88Polling;
import cs2suite.network.SystemListener;
import cs2suite.network.TrainListener;
import cs2suite.network.UDPSender;
import cs2suite.utils.ArticleTestThread;
import cs2suite.utils.Console;
import cs2suite.utils.SqliteDriver;
import cs2suite.utils.HelperUtils;
import cs2suite.utils.ShadowEffects;
import eu.hansolo.enzo.led.Led;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import java.io.FileNotFoundException;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.zip.DataFormatException;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
// </editor-fold>

/**
 *
 * @author Philipp
 */
public class cs2SuiteGUIController extends FXMLController implements Initializable {

    // <editor-fold defaultstate="collapsed" desc=" FXML variables ">
    // <editor-fold defaultstate="collapsed" desc=" Menu-Items ">
//Menu/MenuBar declarations
    @FXML
    private MenuBar topMenuBar;
    @FXML
    private Menu projectMenu;
    @FXML
    private MenuItem closeApplication;
    @FXML
    private MenuItem importSystemData;
    @FXML
    private MenuItem exportSystem;
    @FXML
    private Menu editMenu1;
    @FXML
    private MenuItem trainsFromFileMenuItem;
    @FXML
    private MenuItem newLok;
    @FXML
    private MenuItem articlesFromFileMenuItem;
    @FXML
    private Menu editMenu;
    @FXML
    private MenuItem showSettings;
    @FXML
    private MenuItem setCS2Ip;
    @FXML
    private Menu extraMenu;
    @FXML
    private CheckMenuItem sendConsole;
    @FXML
    private CheckMenuItem debugModeMenuItem;
    @FXML
    private MenuItem importTrainsFromCS;
    @FXML
    private MenuItem newSystemMenuItem;
    @FXML
    private MenuItem loadSystemMenuItem;
    @FXML
    private Menu helpMenu;
    @FXML
    private MenuItem showLogProtocollMenuItem;
    @FXML
    private MenuItem saveSystemMenuItem;
    @FXML
    private MenuItem showWikiMenuItem;

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Panes ">
//Pane Declarations   
    @FXML
    private TabPane mainViewTabPane;
    @FXML
    private ScrollPane trainControlScrollPane;
    @FXML
    private AnchorPane keyboardScrollAnchor;
    @FXML
    private SplitPane keyboardSplitPane;
    @FXML
    private TitledPane favoriteKeyboard;
    @FXML
    private ScrollPane favoriteKeyboarScrollPane;
    @FXML
    private AnchorPane favoriteKeyboardScrollAnchor;
    @FXML
    private TitledPane allKeyboard;
    @FXML
    private AnchorPane keyboardAllAnchor;
    @FXML
    private ScrollPane fahrstrassenScrollPane;
    @FXML
    private AnchorPane fahrstrassenScrollAnchor;
    @FXML
    private AnchorPane soundAnchorPane;
    @FXML
    private AnchorPane turntableAnchor;
    @FXML
    private TitledPane projektTitlePane;
    @FXML
    private TitledPane csFileTitlePane;
    @FXML
    private TitledPane exportDataPane;
    @FXML
    private TitledPane importPreviewPane;
    @FXML
    private AnchorPane importPreviewAnchor;
    @FXML
    private TitledPane configFileTitlePane;
    @FXML
    private AnchorPane systemMonitoringAnchorPane;
    @FXML
    private TitledPane soundManagementPane;
    @FXML
    private AnchorPane soundManagementAnchorPane;
    @FXML
    private TitledPane favoritesTitlePane;
    @FXML
    private AnchorPane favoritesAnchorPane;
    @FXML
    private AnchorPane infoAnchorPane;
    @FXML
    private AnchorPane importFileScrollAnchor;
    @FXML
    private VBox mainLayoutAnchor;
    @FXML
    private AnchorPane trainTableAnchor;
    @FXML
    private SplitPane trainSplitPane;
    @FXML
    private HBox trainControlHBox;
    @FXML
    private Pane statusLedPane;
    @FXML
    private AnchorPane trainSettingsAnchorPane;
    @FXML
    private AnchorPane settingsTabAnchorPane;
    @FXML
    private AnchorPane portalCraneAnchorPane;
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" tabs ">
//Tab declarations
    @FXML
    private Tab zugTab;
    @FXML
    private Tab layoutTab;
    @FXML
    private Tab keyboardTab;
    @FXML
    private Tab fahrstrassenTab;
    @FXML
    private Tab soundTab;
    @FXML
    private Tab drehscheibenTab;
    @FXML
    private Tab portalkranTab;
    @FXML
    private Tab importExportTab;
    @FXML
    private Tab systemMonitoringTab;
    @FXML
    private Tab newTrainTab;
    @FXML
    private Tab newArticleTab;
    @FXML
    private Tab settingsTab;

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Rectangles ">
//Rectangle and other shapes
    @FXML
    private Rectangle gleisbildRectangle;
    @FXML
    private Rectangle sysInfoRectangle;

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Canvas ">
    @FXML
    private Canvas drehscheibeCanvas;
    @FXML
    private Canvas drehscheibeRailwayCanvas;

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Buttons ">
//Button Declarations    
    @FXML
    private Button importSystemDataButton;
    @FXML
    private Button searchExportDirectoryButton;
    @FXML
    private Button exportCurrentSystemButton;
    @FXML
    private Button searchImportFileButton;
    @FXML
    private Button importButton;
    @FXML
    private Button exportButton;
    @FXML
    private Button debugConsoleSendButton;
    @FXML
    private Button stopGoButton;
    @FXML
    private Button newArticle_acceptButton;
    @FXML
    private Button newArticle_abortButton;
    @FXML
    private Button clearLiveLog;
    @FXML
    private Button saveProtocolButton;
    @FXML
    private ToggleButton layoutModeToggleButton;
    @FXML
    private Button startArticleTestButton;

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Combo/Check/etc Boxes ">
//Combobox declarations
    @FXML
    private ComboBox anfahrAdressComboBox;
    @FXML
    private ComboBox importSystemComboBox;
    @FXML
    private CheckBox setAsDefaultSystemCheckBox;
    @FXML
    private ComboBox exportDataComboBox;
    @FXML
    private CheckBox liveLoggingCheckBox;
    @FXML
    private CheckBox editGleisbildCheckBox;
    @FXML
    private ChoiceBox newArticle_decoderChoiceBox;
    @FXML
    private ChoiceBox newArticle_timingChoiceBox;
    @FXML
    private ComboBox newArticle_articleComboBox;

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Labels ">
//Label declarations   
    @FXML
    private Label importSystemLabel;
    @FXML
    private Label exportSystemLabel;
    @FXML
    private Label infoLabel1;
    @FXML
    private Label infoLabel2;
    @FXML
    private Label infoLabel3;
    @FXML
    private Label infoLabel4;
    @FXML
    private Label infoLabel5;
    @FXML
    private Label newArticle_articleLabel;
    @FXML
    private Label newArticle_addressLabel;
    @FXML
    private Label newArticle_timingLabel;
    @FXML
    private Label newArticle_nameLabel;
    @FXML
    private Label newArticle_decoderLabel;
    @FXML
    private Label articleTestLabel;
    @FXML
    private Label articleTestAddressLabel;
    @FXML
    private Label articleTestDelayLabel;
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" TextFields ">
//TextField declarations
    @FXML
    private TextField exportDirectoryField;
    @FXML
    private TextField importFileTextField;
    @FXML
    private TextArea liveLoggingArea;
    @FXML
    private TextField debugConsoleInputField;
    @FXML
    private TextField newArticle_nameField;
    @FXML
    private TextField newArticle_addressField;
    @FXML
    private TextField testArticleAddressField;
    @FXML
    private TextField testArticleDelayField;

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Accordions ">
//Accordions
    @FXML
    private Accordion importExportAccordion;

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Progess ">
//Progress indicators
    @FXML
    private ProgressBar progressBar;
    @FXML
    private ProgressIndicator progressIndicator;

// </editor-fold>        
    private ContextMenu trainContextMenu;
    private Led statusLed;

    // <editor-fold defaultstate="collapsed" desc=" Tab Labels ">
    private Label trainTabLabel;
    private Label layoutTabLabel;
    private Label keyboardTabLabel;
    private Label tracksTabLabel;
    private Label soundsTabLabel;
    private Label turntableTabLabel;
    private Label craneTabLabel;
    private Label exportTabLabel;
    private Label systemTabLabel;
// </editor-fold>  

    private static Stage thisStage;
    private Train curNewTrain;
    private Boolean layoutEditMode = false;
    private Point mouseLayoutPos;   
    private Integer activeTabIndex = 0;
    private Tab activeTab;
    private PrintStream liveLoggingStream;
    private PrintStream stdOut;
    private Console debugConsole;
    private ArrayList<String> recentDebugInputs;
    private Integer atDebugConsoleRecent = 0;
    private Boolean testThreadon = false;
    private ArticleTestThread testThread = new ArticleTestThread();
    private ArrayList<TrainControlController> trainFXMLController;
    private LayoutEditorController layoutEditorController;
    private KeyboardController keyboardController;
    private CSSettingsTabController settingsFXMLController;
    private TrainSettingsController trainSettingsController;
    private PortalCraneController portalCraneController;
    private ContextMenu tabDetachMenu;           
    private static BooleanProperty needRestart;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        try {
            CS_UI_MainController.getInstance(this);
        } catch (SQLException | UnknownHostException ex) {
            CSLogger.logEvent(3, ex, "Error initializing Main UI Controller!");
        }
        CS_UI_MainController.initialize();

        mouseLayoutPos = new Point();
        recentDebugInputs = new ArrayList<>();
        ShadowEffects.getInstance(); //effect helper             
        setTrainFXMLController(new ArrayList<>());
        statusLed = new Led();
        needRestart = new SimpleBooleanProperty(false);

        trainTabLabel = new Label(Internationalization.getString("zugTab"));
        layoutTabLabel = new Label(Internationalization.getString("gleisbildTab"));
        keyboardTabLabel = new Label(Internationalization.getString("keyboardTab"));
        tracksTabLabel = new Label(Internationalization.getString("fahrstrassenTab"));
        soundsTabLabel = new Label(Internationalization.getString("soundTab"));
        turntableTabLabel = new Label(Internationalization.getString("drehscheibenTab"));
        craneTabLabel = new Label(Internationalization.getString("portalkranTab"));
        exportTabLabel = new Label(Internationalization.getString("importExportTab"));
        systemTabLabel = new Label(Internationalization.getString("systemMonitoringTab"));

        guiControllerWrapper.getInstance(this);
        Settings.getInstance(true);
        CSLogger.setLogLevel(Settings.getLogLevel());

        tabDetachMenu = new ContextMenu();
        MenuItem detach = new MenuItem(Internationalization.getString("detachtab"));
        tabDetachMenu.getItems().addAll(detach);
        getZugTab().setContextMenu(tabDetachMenu);

        //redirecting the Systems standart output to textArea on gui
        debugConsole = new Console(liveLoggingArea);
        liveLoggingStream = new PrintStream(debugConsole, true);
        stdOut = System.out;
        System.setOut(liveLoggingStream);

        CSLogger.logEvent(111, null, "Initialising...");

        mouseLayoutPos = new Point();

        trainContextMenu = new ContextMenu();
        trainContextMenu.setHideOnEscape(true);
        MenuItem newTrain = new MenuItem(Internationalization.getString("newtrain"));
        MenuItem importTrains = new MenuItem(Internationalization.getString("loadTrainsFromFileMenuItem"));
        MenuItem clearTrains = new MenuItem(Internationalization.getString("cleartrains"));
        trainContextMenu.getItems().addAll(newTrain, importTrains, clearTrains);
        //trainControlHBox.setPrefSize(trainControlScrollPane.getWidth(), trainControlScrollPane.getHeight());
        trainControlHBox.setSpacing(1);

        try {
            SqliteDriver.getInstance(System.getProperty("user.dir") + "\\data\\data.db", false);
        } catch (ClassNotFoundException | SQLException ex) {
            CSLogger.logEvent(1, ex, "Error loading the SQLite Driver");
        }

        try {
            // load all icons
            SqliteDriver.populateIcons();
            SqliteDriver.loadIcons();
        } catch (SQLException ex) {
            CSLogger.logEvent(2, ex, "Failed to load Icons");
        } catch (IOException ex) {
            CSLogger.logEvent(1, ex, "Failed to put Icons to database");
        }
        
        CS_UI_MainController.initializeTrainControllerFXML(trainControlHBox);            

        //<editor-fold defaultstate="collapsed" desc="LayoutEditorController">
        CSLogger.logEvent(111, null, "Initialising Layout-Editor");
        try {
            FXMLBundle layoutControlBundle = FXMLManager.loadFXML("LayoutEditor.fxml");
            setLayoutEditorController((LayoutEditorController) layoutControlBundle.getController());
            mainLayoutAnchor.getChildren().add(layoutControlBundle.getParent());
        } catch (Exception ex) {
            CSLogger.logEvent(2, ex, "Failed to load Layout-FXML");
        }
//</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="KeyboardController">
        CSLogger.logEvent(111, null, "Initialising Keyboard UI");
        try {
            FXMLBundle keyboardControlBundle = FXMLManager.loadFXML("Keyboard.fxml");
            setKeyboardController((KeyboardController) keyboardControlBundle.getController());
            keyboardScrollAnchor.getChildren().add(keyboardControlBundle.getParent());
        } catch (Exception ex) {
            CSLogger.logEvent(2, ex, "Failed to load Keyboard-FXML");
        }
//</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="PortalCraneController">
        CSLogger.logEvent(111, null, "Initialising PortalCrane UI");
        try {
            FXMLBundle portalCraneControlBundle = FXMLManager.loadFXML("PortalCrane.fxml");
            setPortalCraneController((PortalCraneController) portalCraneControlBundle.getController());
            portalCraneAnchorPane.getChildren().add(portalCraneControlBundle.getParent());
        } catch (Exception ex) {
            CSLogger.logEvent(2, ex, "Failed to load PortalCrane-FXML");
        }
//</editor-fold>        

        //<editor-fold defaultstate="collapsed" desc="SettingsController">
        CSLogger.logEvent(111, null, "initialising Settings-FXML");
        try {
            FXMLBundle settingsControlBundle = FXMLManager.loadFXML("CSSettingsTab.fxml");
            setSettingsFXMLController((CSSettingsTabController) settingsControlBundle.getController());
            settingsTabAnchorPane.getChildren().add(settingsControlBundle.getParent());
            settingsControlBundle.getParent().autosize();
        } catch (Exception ex) {
            CSLogger.logEvent(2, ex, "Error loading Settings-FXML");
        }
//</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="TrainSettingsController">
        CSLogger.logEvent(111, null, "Initialising Train-Settings-FXML");
        try {
            FXMLBundle trainSettingsControlBundle = FXMLManager.loadFXML("TrainSettings.fxml");
            setTrainSettingsController((TrainSettingsController) trainSettingsControlBundle.getController());
            trainSettingsAnchorPane.getChildren().add(trainSettingsControlBundle.getParent());
        } catch (Exception ex) {
            CSLogger.logEvent(2, ex, "Failed to load TrainSettings-FXML");
        }
//</editor-fold>
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Event Handler ">        
        /*
         listening to the system state provided by CSPacketParser
         */
        // <editor-fold defaultstate="collapsed" desc=" detaching Tabs ">
        detach.setOnAction((ActionEvent event) -> {
            Parent root;
            FXMLLoader loader = new FXMLLoader();
            URL location = null;
            switch (activeTabIndex) {
                case 0:
                    // location = getClass().getResource("Trains.fxml");
                    break;
                case 1:
                    location = getClass().getResource("LayoutEditor.fxml");
                    break;
                case 2:
                    location = getClass().getResource("Keyboard.fxml");
                    break;
                case 3:
                    // location = getClass().getResource("LayoutEditor.fxml");
                    break;
                case 4:
                    // location = getClass().getResource("LayoutEditor.fxml");                    
                    break;
                case 5:
                    // location = getClass().getResource("LayoutEditor.fxml");
                    break;
                case 6:
                    // location = getClass().getResource("LayoutEditor.fxml");
                    break;
                case 7:
                    // location = getClass().getResource("LayoutEditor.fxml");
                    break;
                case 8:
                    // location = getClass().getResource("LayoutEditor.fxml");
                    break;
            }

            loader.setLocation(location);
            try {
                root = (Parent) loader.load();
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setScene(scene);
                stage.show();
            } catch (IOException ex) {
                CSLogger.logEvent(2, ex, "Error loading FXML for detaching Tab " + activeTab.getText());
            }
        });

// </editor-fold>        
        /**
         * provide access to the currently sselected tab via variables
         */
        mainViewTabPane.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            activeTabIndex = newValue.intValue();
            activeTab = mainViewTabPane.getTabs().get(activeTabIndex);
            sceneChange(thisStage.getWidth(), thisStage.getHeight());
            switch (newValue.intValue()) {
                case 8: //systemMonitoringTab
                    debugConsoleInputField.requestFocus();
                    debugConsoleInputField.selectHome();
                    break;
            }
        });
        /*mainViewTabPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
         newValue.setContextMenu(tabDetachMenu);
         oldValue.setContextMenu(null);
         });*/

        newTrain.setOnAction((ActionEvent event) -> {
            handleNewLokMenuItem(event);
        });

        importTrains.setOnAction((ActionEvent event) -> {
            try {
                handleTrainsFromFileMenuItem(event);
            } catch (IOException ex) {
                CSLogger.logEvent(4, ex, "Failed to import trains from cs2File (File Error)");
            } catch (SQLException ex) {
                CSLogger.logEvent(4, ex, "Failed to import trains from cs2File (SQL Error)");
            }
        });

        clearTrains.setOnAction((ActionEvent event) -> {
        });

        /*
         UDPPackageInterpreter.configDataProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
         CSFile csFile = new CSFile(newValue);
         ArrayList<CSTrain> temptrains = csFile.getAllTrains();
         System.out.println("Got Trains: " + temptrains.size());
         });
         */
        needRestart.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue.booleanValue() == true) {
                setInfoText5(Internationalization.getString("needsrestart"), Paint.valueOf("#ff0000"));
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle(Internationalization.getString("dialog_needsrestart") + "\n");
                alert.setContentText(Internationalization.getString("dialog_needsrestartmsg"));
                alert.showAndWait();
            } else {
                setInfoText5("", Paint.valueOf("#000000"));
            }
        });
// </editor-fold>...
    }

    // <editor-fold defaultstate="collapsed" desc=" Later inits ">
/*
     executed after everything is already initialized
     */
    /**
     *
     * @throws IOException
     */
    @Override
    public void initAfter() throws IOException {
        InnerShadow is = ShadowEffects.getInnerShadow(Color.valueOf("#00ab00"));
        stopGoButton.setEffect(is);

        statusLed.setLedType(Led.LedType.ROUND);
        statusLed.setLayoutX(5);
        statusLed.setLayoutY(5);
        statusLed.setFrameVisible(true);
        statusLed.setOn(true);
        statusLed.setLedColor(Paint.valueOf("#00c000"));
        statusLedPane.getChildren().add(statusLed);

        mainViewTabPane.getTabs().removeAll(getNewTrainTab(), getNewArticleTab(), getSettingsTab());

        liveLoggingCheckBox.setSelected(Settings.getConsoleLogging());
        CSLogger.logEvent(111, null, "Application started, no System loaded");

        //<editor-fold defaultstate="collapsed" desc="Internationalization">
        getLayoutTab().setText(Internationalization.getString("gleisbildTab"));
        getKeyboardTab().setText(Internationalization.getString("keyboardTab"));
        getZugTab().setText(Internationalization.getString("zugTab"));
        getFahrstrassenTab().setText(Internationalization.getString("fahrstrassenTab"));
        getDrehscheibenTab().setText(Internationalization.getString("drehscheibenTab"));
        getPortalkranTab().setText(Internationalization.getString("portalkranTab"));
        getImportExportTab().setText(Internationalization.getString("importExportTab"));
        getSystemMonitoringTab().setText(Internationalization.getString("systemMonitoringTab"));
        projectMenu.setText(Internationalization.getString("projectMenu"));
        closeApplication.setText(Internationalization.getString("closeApplication"));
        showSettings.setText(Internationalization.getString("showSettings"));
        setCS2Ip.setText(Internationalization.getString("setCS2Ip"));
        editMenu.setText(Internationalization.getString("editMenu"));
        projectMenu.setText(Internationalization.getString("projectMenu"));
        extraMenu.setText(Internationalization.getString("extraMenu"));
        importSystemDataButton.setText(Internationalization.getString("importButton"));
        exportCurrentSystemButton.setText(Internationalization.getString("exportButton"));
        importSystemLabel.setText(Internationalization.getString("importLabel"));
        exportSystemLabel.setText(Internationalization.getString("exportLabel"));
        exportSystem.setText(Internationalization.getString("exportLabel"));
        importSystemData.setText(Internationalization.getString("importLabel"));
        trainsFromFileMenuItem.setText(Internationalization.getString("loadTrainsFromFileMenuItem"));
        articlesFromFileMenuItem.setText(Internationalization.getString("loadArticlesFromFileMenuItem"));
        getSoundTab().setText(Internationalization.getString("soundTab"));
        getSettingsTab().setText(Internationalization.getString("settings"));
        importTrainsFromCS.setText(Internationalization.getString("importtrainsfromcs"));
        saveProtocolButton.setText(Internationalization.getString("saveprotocol"));
        layoutModeToggleButton.setText(Internationalization.getString("layoutmode"));
        articleTestLabel.setText(Internationalization.getString("articletest"));
        articleTestDelayLabel.setText(Internationalization.getString("delay"));
        articleTestAddressLabel.setText(Internationalization.getString("address"));
        startArticleTestButton.setText(Internationalization.getString("starttest"));
        helpMenu.setText(Internationalization.getString("help"));
        showLogProtocollMenuItem.setText(Internationalization.getString("protocol"));
        newSystemMenuItem.setText(Internationalization.getString("newSystem"));
        loadSystemMenuItem.setText(Internationalization.getString("loadSystem"));
        saveSystemMenuItem.setText(Internationalization.getString("dialog_saveSystemTitle"));
        getNewTrainTab().setText(Internationalization.getString("newtrain"));
        showLogProtocollMenuItem.setText(Internationalization.getString("show_protocol"));
        showWikiMenuItem.setText(Internationalization.getString("show_wiki"));
//</editor-fold>
    }

    /**
     * executed after stage.show is called
     */
    @Override
    public void finalInit() {
        thisStage.setTitle("CS2 - Control Suite Ver. " + Settings.getVersion() + " Build: " + Settings.getBuild() + " --- ");
        liveLoggingArea.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            int num = liveLoggingArea.getText().split("\n").length;
            if (num > Settings.getLiveLoggingBufferLength()) {

            }
        });
        sceneChange(thisStage.getWidth(), thisStage.getHeight());
        
        CoreListener.emptyProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                setInfoText1(Internationalization.getString("noSystemLoaded"));
            } else {
                setInfoText1(Internationalization.getString("systemLoaded", SystemsManager.getActiveSystem().getName()));
            }
        });
        CS_UI_MainController.setPopoverParent(thisStage);
    }
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" Load System ">    
    private void loadSystem(CSSystem system) {        
        SystemListener.systemStateProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            switch (newValue.intValue()) {
                case 0:
                    Platform.runLater(() -> {
                        stopGoButton.setText("GO");
                        InnerShadow is = ShadowEffects.getInnerShadow(Color.valueOf("#cc2b0a"));
                        stopGoButton.setEffect(is);
                        setInfoText2("Status: " + Internationalization.getString("cs_stopped"));
                    });                    
                    break;
                case 1:
                    Platform.runLater(() -> {
                        stopGoButton.setText("STOP");
                        InnerShadow is = ShadowEffects.getInnerShadow(Color.valueOf("#00ab00"));
                        stopGoButton.setEffect(is);
                        setInfoText2("Status: " + Internationalization.getString("cs_running"));
                    });                    
                    break;
                case 2:
                    setInfoText2("Status: " + Internationalization.getString("cs_hold"));                    
                    break;
                default:
                    Platform.runLater(() -> {
                        setInfoText2("Status: " + Internationalization.getString("cs_undefinedstate"));
                    });                    
                    break;
            }
        });        
    }
// </editor-fold>

    public void sceneChange(double w, double h) {
        stopGoButton.setPrefWidth(w);
        infoLabel2.setText(w + "/" + h);
        switch (activeTabIndex) {
            case 0:
                //dynamically handled by traincontrols
                break;
            case 1: // Layout editor
                layoutModeToggleButton.setPrefWidth(mainLayoutAnchor.getWidth() - 12);
                if (getLayoutEditorController() != null) {
                    getLayoutEditorController().sceneChange(w, h);
                }
                break;
            case 2: // keyboard
                if (keyboardController != null) {
                    keyboardController.sceneChange(w, h);
                }
                break;
        }
    }

    private void deactivateNotAllowedFunctionsWithoutLoadedSystem() {
        importTrainsFromCS.setDisable(true);
        trainsFromFileMenuItem.setDisable(true);
        newLok.setDisable(true);
        articlesFromFileMenuItem.setDisable(true);
        exportSystem.setDisable(true);
    }

    public String getConsoleLog() {
        return liveLoggingArea.getText();
    }

    // <editor-fold defaultstate="collapsed" desc=" SetInfoTexts ">
    public void setInfoText2(String text) {
        Platform.runLater(() -> {
            infoLabel2.setText(text);
        });
    }

    public void setInfoText1(String text) {
        Platform.runLater(() -> {
            infoLabel1.setText(text);
        });
    }

    public void setInfoText3(String text) {
        Platform.runLater(() -> {
            infoLabel3.setText(text);
        });
    }

    public void setInfoText5(String text, Paint c) {
        Platform.runLater(() -> {
            infoLabel5.setText(text);
            infoLabel5.setTextFill(c);
        });
    }

    public void setInfoText5(String text) {
        Platform.runLater(() -> {
            infoLabel5.setText(text);
        });
    }

    public void setInfoText4(String text) {
        Platform.runLater(() -> {
            infoLabel4.setText(text);
        });
    }

    public void setInfoText4(String text, Paint c) {
        Platform.runLater(() -> {
            infoLabel4.setText(text);
            infoLabel4.setTextFill(c);
        });
    }

    public void setInfoText3(String text, Paint c) {
        Platform.runLater(() -> {
            infoLabel3.setText(text);
            infoLabel3.setTextFill(c);
        });
    }

    public void setInfoText2(String text, Paint c) {
        Platform.runLater(() -> {
            infoLabel2.setText(text);
            infoLabel2.setTextFill(c);
        });
    }

    public void setInfoText1(String text, Paint c) {
        Platform.runLater(() -> {
            infoLabel1.setText(text);
            infoLabel1.setTextFill(c);
        });
    }
// </editor-fold>

    @FXML
    private void handleShowWikiMenuItem(ActionEvent e) {
        HelperUtils.showWikiEntry("Home", thisStage);
    }

    @FXML
    private void handleSaveSystemMenuItem(ActionEvent e) {
        /*
        Optional<String> response = HelperUtils.showTextInputDialog(Internationalization.getString("dialog_saveSystemTitle") + "\n",
                Internationalization.getString("dialog_saveSystemMessage"));
        response.ifPresent((String name) -> {
            if (!name.equals(null) && !name.equals("")) {
                try {
                    CSLogger.logEvent(111, null, "Saving System: " + name + " to File");
                    curSystem.saveSystem(name, false);
                    CSLogger.logEvent(111, null, "System created");
                } catch (IOException ex) {
                    CSLogger.logEvent(2, ex, "Error saving System (cs2SuiteGUIController)");
                }
            } else {
                CSLogger.logEvent(5, null, "Error creating System: no name given!");
                CSLogger.logEvent(101, null, "Error creating System: no name given!");
            }
        });
        */
        //TODO
    }

    @FXML
    private void handleShowProtocolMenuItem(ActionEvent e) throws IOException {
        HelperUtils.showHelpText(new File(CSLogger.getLogFilePath()), thisStage);
    }

    @FXML
    private void handleArticleTestButton(ActionEvent e) throws InterruptedException {
        if (testThreadon) {
            testThread.stop();
            testThreadon = false;
            startArticleTestButton.setText(Internationalization.getString("starttest"));
        } else {
            testThread = new ArticleTestThread();
            Integer adr = Integer.parseInt(testArticleAddressField.getText());
            Integer delay = Integer.parseInt(testArticleDelayField.getText());
            testThread.setData(adr, delay, "mm2");
            testThread.start();
            testThreadon = true;
            startArticleTestButton.setText(Internationalization.getString("stop"));
        }
    }

    @FXML
    private void handleNewSystemMenuItem(ActionEvent e) throws Exception {
        FXMLBundle bundle = FXMLManager.loadFXML("NewSystem.fxml");
        ((NewSystemController) bundle.getController()).setPopOver(CS_UI_MainController.showPopOver(Internationalization.getString("createNewSystem"), bundle));
    }

    @FXML
    private void handleLoadSystemMenuItem(ActionEvent e) throws Exception {
        FXMLBundle bundle = FXMLManager.loadFXML("LoadSystem.fxml");
        ((LoadSystemController) bundle.getController()).setPopOver(CS_UI_MainController.showPopOver(Internationalization.getString("loadSystem"), bundle));
    }

    @FXML
    private void handleSaveProtocolButton(ActionEvent e) throws FileNotFoundException {
        File file = lookForFile("Textdateien (*.txt)", "*.txt", null);

        HelperUtils.saveStringToFile(file, liveLoggingArea.getText());
    }

    @FXML
    private void handleClearLiveLog(ActionEvent e) {
        liveLoggingArea.clear();
    }

    @FXML
    public void handleApplicationClose() throws SocketException, IOException {       
        CoreListener.getInstance().stop();
        TrainListener.getInstance().stop();
        ArticleListener.getInstance().stop();
        S88Polling.getInstance().stop();
        ConfigStreamListener.getInstance().stop();
        SystemListener.getInstance().stop();
        System.exit(0);
    }

    @FXML
    private void handleLoadSystemData(ActionEvent event) {
    }

    @FXML
    private void handleExportSystem(ActionEvent event) {
    }

    @FXML
    private void handleTrainsFromFileMenuItem(ActionEvent event) throws IOException, SQLException {
        File file = lookForFile("CSFile (*.cs2)", "*.cs2", System.getProperty("user.dir") + "\\data\\imports");
        ArrayList<Train> trains = CS_UI_MainController.importTrainsFromFile(file);
        //loadedTrains.addAll(CS_UI_MainController.getTrainsFromFile(file));
        //CS_UI_MainController.createTrainsTableView(trainTableView, loadedTrains);
    }

    @FXML
    private void handleArticlesFromFileMenuItem(ActionEvent event) throws SQLException, IOException {
        File file = lookForFile(Internationalization.getString("cs2file"), "*.cs2", null);
        CSFile csFile = new CSFile(file.getAbsolutePath());        
        //TODO
    }

    @FXML
    private void handleNewLokMenuItem(ActionEvent event) {
        disableAllTabs(true);
        topMenuBar.setDisable(true);
        mainViewTabPane.getTabs().add(getNewTrainTab());
        mainViewTabPane.getSelectionModel().select(getNewTrainTab());
        getTrainSettingsController().setMode(0); // open in create mode
    }

    @FXML
    private void handleShowGlobalSettingsMenuItem(ActionEvent event) {
        disableAllTabs(true);
        mainViewTabPane.getTabs().add(getSettingsTab());
        mainViewTabPane.getSelectionModel().select(getSettingsTab());
        topMenuBar.setDisable(true);
        getSettingsFXMLController().showMainSettingsPane();
    }

    @FXML
    private void handleSendConsoleMenuItem(ActionEvent event) {
    }

    @FXML
    private void handleDebugMenuItem(ActionEvent event) {
    }

    @FXML
    private void handleLayoutModeToggleButton(ActionEvent e) {
        if (layoutModeToggleButton.isSelected()) {
            layoutEditMode = true;
            getLayoutEditorController().setMode(0);
            layoutModeToggleButton.setText(Internationalization.getString("layoutEditMode"));
        } else {
            layoutEditMode = false;
            getLayoutEditorController().setMode(1);
            layoutModeToggleButton.setText(Internationalization.getString("layoutControlMode"));
        }
    }

    @FXML
    private void handleImportTrainsFromCSMenuItem(ActionEvent e) {

    }

    @FXML
    private void handleSetAsDefaultSystemCheckBox(ActionEvent event) {
    }

    @FXML
    private void handleSearchImportFileButton(ActionEvent event) {
        File importFile = lookForFile(Internationalization.getString("cs2files"), "*.cs2", null);
        importFileTextField.setText(importFile.getAbsolutePath());
        CSFile csFile = new CSFile(importFile.getAbsolutePath());
        CSFileViewer viewer = new CSFileViewer(csFile, importPreviewAnchor);

        //importPreviewPane.setPrefWidth((csFileTitlePane.getWidth() / 2) - 35);
        importFileScrollAnchor.setPrefHeight(viewer.getTableHeight() + 24);
        importPreviewPane.setVisible(true);
        importPreviewPane.setText(importFile.getAbsolutePath());
        importButton.setVisible(true);
    }

    @FXML
    private void handleImportButton(ActionEvent event) {
    }

    @FXML
    private void handleLiveLoggingCheckBox(ActionEvent event) {
        if (liveLoggingCheckBox.isSelected()) {
            Settings.setConsoleLogging((Boolean) true);
        } else {
            Settings.setConsoleLogging((Boolean) false);
        }
    }

    /**
     *
     * @param e
     * @throws IOException
     * @throws DataFormatException
     * @throws SQLException
     * @throws ClassNotFoundException Handles sending command via Commandline in Debug
     */
    @FXML
    public void handleDebugConsoleComboBoxKeyPress(KeyEvent e) throws IOException, DataFormatException, SQLException, ClassNotFoundException {
        if (e.getCode().equals(KeyCode.ENTER)) {
            atDebugConsoleRecent = -1;
            if (recentDebugInputs.size() >= 10) {
                recentDebugInputs.remove(0);
            }
            recentDebugInputs.add(debugConsoleInputField.getText());

            CommandParser.execCommand(debugConsoleInputField.getText());
            //CSCommandParser.checkdebugCmd(debugConsoleInputField.getText());

            debugConsoleInputField.clear();
        } else if (e.getCode().equals(KeyCode.UP)) {
            if (atDebugConsoleRecent != -1) {
                atDebugConsoleRecent++;
            }
            if (atDebugConsoleRecent >= recentDebugInputs.size()) {
                atDebugConsoleRecent = recentDebugInputs.size() - 1;
            }
            debugConsoleInputField.setText(recentDebugInputs.get(atDebugConsoleRecent));
        } else if (e.getCode().equals(KeyCode.DOWN)) {
            if (atDebugConsoleRecent == -1) {
                atDebugConsoleRecent = recentDebugInputs.size() - 1;
            } else {
                atDebugConsoleRecent--;
            }
            debugConsoleInputField.setText(recentDebugInputs.get(atDebugConsoleRecent));
        }
    }

    @FXML
    private void handleDebugConsoleSendButton(ActionEvent event) throws IOException, DataFormatException, SQLException, ClassNotFoundException {
        KeyEvent ke = new KeyEvent(null, null, null, KeyCode.ENTER, false, false, false, false);
        handleDebugConsoleComboBoxKeyPress(ke);
    }

    @FXML
    private void handleMainStopButton(ActionEvent event) throws IOException {
        if (stopGoButton.getText().toLowerCase().equals("go")) {
            UDPSender.sendBytes(Packet.GLOBAL_GO);
        } else if (stopGoButton.getText().toLowerCase().equals("stop")) {
            UDPSender.sendBytes(Packet.GLOBAL_STOP);
        }
    }

    /*
     opens a filechooser at given location to open files of specific extension
     */
    private File lookForFile(String files, String extension, String startdir) {
        FileChooser fc = new FileChooser();
        FileChooser.ExtensionFilter extFilter;
        if (extension.contains("|")) {
            String[] exts = extension.split("|");
            List<String> ext_list = new ArrayList<String>(Arrays.asList(exts));
            extFilter = new FileChooser.ExtensionFilter(files, ext_list);
        } else {
            extFilter = new FileChooser.ExtensionFilter(files, extension);
        }
        fc.getExtensionFilters().add(extFilter);
        File initialDir;
        if (startdir == null) {
            initialDir = new File(System.getProperty("user.dir") + "\\data");
        } else {
            initialDir = new File(startdir);
        }
        if (initialDir.canRead()) {
            fc.setInitialDirectory(initialDir);
        } else {
            fc.setInitialDirectory(new File("c:"));
        }
        File result = fc.showOpenDialog(null);
        return result;
    }

    private void fillImageView(ImageView view, File file) throws IOException {
        BufferedImage bufimg = ImageIO.read(file);
        Image img = SwingFXUtils.toFXImage(bufimg, null);
        view.setImage(img);
    }

    public void addFavoriteTrainControl(Integer nr) {

    }

    public void disableAllTabs(Boolean b) {
        for (int i = 0; i < mainViewTabPane.getTabs().size(); i++) {
            mainViewTabPane.getTabs().get(i).setDisable(b);
        }
    }

    private void setTabVisibility(int tabIndex, boolean visible) {
        mainViewTabPane.getTabs().get(tabIndex).setDisable(visible);
    }    

    public void setProgress(Double progress) {
        Platform.runLater(() -> {
            this.progressBar.setProgress(progress);
        });
    }

    public void addProgress(Double value) {
        Platform.runLater(() -> {
            this.progressBar.setProgress(progressBar.getProgress() + value);
        });
    }

    public final KeyboardController getKeyboardController() {
        return keyboardController;
    }

    public void setStatus(Integer stat) {
        CS_UI_MainController.setLedStatusColor(statusLed, stat);
    }

    public void hideApplication() {
        thisStage.hide();
    }

    public void showTab(Tab tab, boolean exclusive) {
        disableAllTabs(exclusive);
        mainViewTabPane.getTabs().add(tab);
        mainViewTabPane.getSelectionModel().select(tab);
        topMenuBar.setDisable(true);
    }

    public void editTrain(Train train) throws IOException {
        showTab(newTrainTab, true);
    }

    /**
     * closes the Settingstab if open and gets the overall tab-states back to normal
     */
    public void abortSettings() {
        mainViewTabPane.getTabs().remove(getSettingsTab());
        disableAllTabs(false);
        topMenuBar.setDisable(false);
        mainViewTabPane.getSelectionModel().select(getZugTab());
    }

    public void clearConsoleBuffer(int buffer) {
    }

    // <editor-fold defaultstate="collapsed" desc=" Setter/Getter ">
    /**
     * @return the zugTab
     */
    public Tab getZugTab() {
        return zugTab;
    }

    /**
     * @return the layoutTab
     */
    public Tab getLayoutTab() {
        return layoutTab;
    }

    /**
     * @return the keyboardTab
     */
    public Tab getKeyboardTab() {
        return keyboardTab;
    }

    /**
     * @return the fahrstrassenTab
     */
    public Tab getFahrstrassenTab() {
        return fahrstrassenTab;
    }

    /**
     * @return the soundTab
     */
    public Tab getSoundTab() {
        return soundTab;
    }

    /**
     * @return the drehscheibenTab
     */
    public Tab getDrehscheibenTab() {
        return drehscheibenTab;
    }

    /**
     * @return the portalkranTab
     */
    public Tab getPortalkranTab() {
        return portalkranTab;
    }

    public PortalCraneController getPortalCraneController() {
        return portalCraneController;
    }

    /**
     * @return the importExportTab
     */
    public Tab getImportExportTab() {
        return importExportTab;
    }

    /**
     * @param importExportTab the importExportTab to set
     */
    public void setImportExportTab(Tab importExportTab) {
        this.importExportTab = importExportTab;
    }

    /**
     * @return the systemMonitoringTab
     */
    public Tab getSystemMonitoringTab() {
        return systemMonitoringTab;
    }

    /**
     * @return the newTrainTab
     */
    public Tab getNewTrainTab() {
        return newTrainTab;
    }

    /**
     * @return the newArticleTab
     */
    public Tab getNewArticleTab() {
        return newArticleTab;
    }

    /**
     * @return the settingsTab
     */
    public Tab getSettingsTab() {
        return settingsTab;
    }

    public static boolean needsRestart() {
        return needRestart.get();
    }

    public static void setRestartFlag(boolean b) {
        needRestart.set(b);
    }

    public static BooleanProperty getNeedsRestartProperty() {
        return needRestart;
    }

    /**
     * @return the trainFXMLController
     */
    public ArrayList<TrainControlController> getTrainFXMLController() {
        return trainFXMLController;
    }

    /**
     * @param trainFXMLController the trainFXMLController to set
     */
    private void setTrainFXMLController(ArrayList<TrainControlController> trainFXMLController) {
        this.trainFXMLController = trainFXMLController;
    }

    /**
     * @return the layoutEditorController
     */
    public LayoutEditorController getLayoutEditorController() {
        return layoutEditorController;
    }

    /**
     * @param layoutEditorController the layoutEditorController to set
     */
    private void setLayoutEditorController(LayoutEditorController layoutEditorController) {
        this.layoutEditorController = layoutEditorController;
    }

    /**
     * @param keyboardController the keyboardController to set
     */
    private void setKeyboardController(KeyboardController keyboardController) {
        this.keyboardController = keyboardController;
    }

    /**
     * @return the settingsFXMLController
     */
    public CSSettingsTabController getSettingsFXMLController() {
        return settingsFXMLController;
    }

    /**
     * @param settingsFXMLController the settingsFXMLController to set
     */
    private void setSettingsFXMLController(CSSettingsTabController settingsFXMLController) {
        this.settingsFXMLController = settingsFXMLController;
    }

    /**
     * @return the trainSettingsController
     */
    public TrainSettingsController getTrainSettingsController() {
        return trainSettingsController;
    }

    /**
     * @param trainSettingsController the trainSettingsController to set
     */
    private void setTrainSettingsController(TrainSettingsController trainSettingsController) {
        this.trainSettingsController = trainSettingsController;
    }

    /**
     * @param portalCraneController the portalCraneController to set
     */
    private void setPortalCraneController(PortalCraneController portalCraneController) {
        this.portalCraneController = portalCraneController;
    }

    @Override
    public void setStage(Stage stage) {
        thisStage = stage;
    }

    public static Stage getStage() {
        return thisStage;
    }
    // </editor-fold> 
}

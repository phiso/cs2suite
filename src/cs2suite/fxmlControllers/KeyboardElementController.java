/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.fxmlControllers;

import cs2suite.logic.CSLogger;
import cs2suite.fxmlControllers.NewArticleController;
import cs2suite.fxmlControllers.guiControllerWrapper;
import cs2suite.locale.Internationalization;
import cs2suite.csObjects.CSArticle;
import cs2suite.csUIController.FXMLController;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author Philipp
 */
public class KeyboardElementController extends FXMLController implements Initializable {

    @FXML
    private AnchorPane parent;
    @FXML
    private VBox vBox;
    @FXML
    private ImageView upperImageView;
    @FXML
    private ImageView lowerImageView;
    @FXML
    private Separator separator;
    @FXML
    private Label nameLabel;

    private ContextMenu ctxMenu;
    private MenuItem addFav;
    private MenuItem configArticle;
    private MenuItem repairState;

    private ColorAdjust effect;
    private String keyboardImgTrunk;
    private CSArticle article;
    private NewArticleController newArticleController;
    private IntegerProperty uiState;
    private Boolean repairing;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        repairing = false;
        uiState = new SimpleIntegerProperty(-1);
        effect = new ColorAdjust(0, -1, 0, 0);
        keyboardImgTrunk = System.getProperty("user.dir") + "\\data\\img\\keyboard\\";
        ctxMenu = new ContextMenu();
        addFav = new MenuItem(Internationalization.getString("setasfavorite"));
        configArticle = new MenuItem(Internationalization.getString("config"));
        repairState = new MenuItem(Internationalization.getString("repair_article_state"));
        repairState.setDisable(true);
        ctxMenu.getItems().addAll(addFav, configArticle/*, repairState*/);

        /**
         * State Repairing needs to be solved different or maybe its not even needed
         * we'll see
         */
        
        /*uiState.addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue.intValue() == -1) {
                repairState.setDisable(true);
            } else {
                repairState.setDisable(false);
            }
        });*/

        configArticle.setOnAction((ActionEvent event) -> {

            try {
                FXMLLoader loader = new FXMLLoader();
                URL location = getClass().getResource("NewArticle.fxml");
                loader.setLocation(location);
                Parent root = (Parent) loader.load();
                newArticleController = loader.getController();
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.initModality(Modality.WINDOW_MODAL);
                stage.initOwner(guiControllerWrapper.getController().getStage());
                stage.setScene(scene);
                newArticleController.setStage(stage);
                newArticleController.setArticle(article);
                stage.show();
            } catch (IOException ex) {
                CSLogger.logEvent(3, ex, "Error loading NewArticle.fxml as modal Window");
            }
        });

        addFav.setOnAction((ActionEvent event) -> {
        });

        repairState.setOnAction((ActionEvent event) -> {
            repairing = true;
            switch (uiState.get()) {
                case 0:
                    article.setState(0);
                    break;
                case 1:
                    article.setState(1);
                    break;
            }
        });
    }

    @FXML
    private void handleUpperImageClick(MouseEvent e) {
        if (e.getButton().equals(MouseButton.PRIMARY)) {
            //upperImageView.setEffect(null);
            //lowerImageView.setEffect(effect);
            article.setState(1);
        } else if (e.getButton().equals(MouseButton.SECONDARY)) {
            ctxMenu.show(vBox, e.getScreenX(), e.getScreenY());
        }
    }

    @FXML
    private void handleLowerImageClick(MouseEvent e) {
        if (e.getButton().equals(MouseButton.PRIMARY)) {
            //lowerImageView.setEffect(null);
            //upperImageView.setEffect(effect);
            article.setState(0);
        } else if (e.getButton().equals(MouseButton.SECONDARY)) {
            ctxMenu.show(vBox, e.getScreenX(), e.getScreenY());
        }
    }

    private void setLowerImgEffects() {
        upperImageView.setEffect(null);
        lowerImageView.setEffect(effect);
        uiState.set(1);
    }

    private void setUpperImgEffects() {
        lowerImageView.setEffect(null);
        upperImageView.setEffect(effect);
        uiState.set(0);
    }

    private void setState(int state) {
        MouseEvent me = new MouseEvent(state, parent, null, state, state, state, state, MouseButton.PRIMARY, state, true, true, true, true, true, true, true, true, true, true, null);
        switch (state) {
            case 0:
                Platform.runLater(() -> {
                    if (!repairing) {
                        setUpperImgEffects();
                    } else {
                        repairing = false;
                    }
                });
                break;
            case 1:
                Platform.runLater(() -> {
                    if (!repairing) {
                        setLowerImgEffects();
                    } else {
                        repairing = false;
                    }
                });
                break;
        }
    }

    public void setUpperImage(Image img) {
        upperImageView.setImage(img);
    }

    public void setLowerImage(Image img) {
        lowerImageView.setImage(img);
    }

    private void loadImages() throws IOException {
        BufferedImage bufImg = null;
        nameLabel.setText(article.getName() + "_" + article.getAddress());

        bufImg = ImageIO.read(new File(article.getImagePath(0)));
        Image img = SwingFXUtils.toFXImage(bufImg, null);
        upperImageView.setImage(img);
        bufImg = ImageIO.read(new File(article.getImagePath(1)));
        img = SwingFXUtils.toFXImage(bufImg, null);
        lowerImageView.setImage(img);
    }

    public void setArticle(CSArticle article) throws IOException {
        this.article = article;
        loadImages();
        article.getImageRessourceProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            try {
                loadImages();
            } catch (IOException ex) {
                CSLogger.logEvent(4, ex, "Warning: changing template failed!");
            }
        });

        article.getCurStateProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            setState(newValue.intValue());
        });
    }
}

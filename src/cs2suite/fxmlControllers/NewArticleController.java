/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.fxmlControllers;

import cs2suite.locale.Internationalization;
import cs2suite.csObjects.CSArticle;
import cs2suite.csObjects.CSDecoder;
import cs2suite.csObjects.CSKeyboard;
import cs2suite.csObjects.CSArticleTemplate;
import cs2suite.csUIController.FXMLController;
import cs2suite.utils.SqliteDriver;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author Philipp
 */
public class NewArticleController extends FXMLController implements Initializable {

    @FXML
    private AnchorPane parent;
    @FXML
    private TitledPane titledPane;
    @FXML
    private AnchorPane titledPaneAnchor;
    @FXML
    private Label decoderChoiceLabel;
    @FXML
    private Label articleChoiceLabel;
    @FXML
    private Label setNameLabel;
    @FXML
    private Label setTimingLabel;
    @FXML
    private ChoiceBox decoderChoiceBox;
    @FXML
    private ComboBox articleTypeChoiceBox;
    @FXML
    private TextField nameField;
    @FXML
    private TextField timingField;
    @FXML
    private Button incTimingButton;
    @FXML
    private Button decTimingButton;
    @FXML
    private Button abortButton;
    @FXML
    private Button acceptButton;

    private Stage thisStage;
    private CSArticle article;
    private Integer timing;
    private String name;
    private String selectedDecoder;
    private CSArticleTemplate selectedTemplate;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        decoderChoiceBox.getItems().add(CSDecoder.MM2);
        decoderChoiceBox.getItems().add(CSDecoder.DCC);
        decoderChoiceBox.getSelectionModel().select(0);
        selectedDecoder = "mm2";
        titledPane.setText("");
        timing = 200;

        articleTypeChoiceBox.setCellFactory(new Callback<ListView<CSArticleTemplate>, ListCell<CSArticleTemplate>>() {
            @Override
            public ListCell<CSArticleTemplate> call(ListView<CSArticleTemplate> param) {
                return new ListCell<CSArticleTemplate>() {
                    private final ImageView view;

                    {
                        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        view = new ImageView();
                    }

                    @Override
                    protected void updateItem(CSArticleTemplate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            HBox box = new HBox();
                            box.setSpacing(10);
                            view.setFitHeight(30);
                            view.setFitWidth(30);
                            view.setImage(item.getImage(0));
                            box.getChildren().add(view);
                            box.getChildren().add(new Text(Internationalization.getString(item.getName())));
                            setGraphic(box);
                        }
                    }
                };
            }
        });

        articleTypeChoiceBox.getItems().setAll(CSKeyboard.getArticleTemplatesObs());
        
        articleTypeChoiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<CSArticleTemplate>(){
            @Override
            public void changed(ObservableValue<? extends CSArticleTemplate> observable, CSArticleTemplate oldValue, CSArticleTemplate newValue) {
                selectedTemplate = newValue;                
            }
        });
        
        decoderChoiceBox.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            selectedDecoder = (String) decoderChoiceBox.getItems().get(newValue.intValue());            
        });
        
        nameField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            name = newValue;
        });
    }        

    public void setArticle(CSArticle article) {
        this.article = article;
        nameField.setText(article.getName());
        name = article.getName();
        timing = article.getTiming();
        timingField.setText(timing.toString());
    }

    @FXML
    private void handleIncTimingButton(ActionEvent e) {
        timing += 50;
        timingField.setText(timing.toString());
        article.setTiming(timing);
    }

    @FXML
    private void handleDecTimingButton(ActionEvent e) {
        timing -= 50;
        timingField.setText(timing.toString());
        article.setTiming(timing);
    }

    @FXML
    private void handleAbortButton(ActionEvent e) {
        thisStage.close();
    }

    @FXML
    private void handleAcceptButton(ActionEvent e) throws SQLException {
        article.setDecoder(selectedDecoder);
        article.changeTemplate(selectedTemplate);        
        article.setName(name);
        SqliteDriver.updateArticle(article);
        thisStage.close();
    }    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.fxmlControllers;

import cs2suite.logic.CSLogger;
import cs2suite.fxmlControllers.KeyboardElementController;
import cs2suite.fxmlControllers.guiControllerWrapper;
import cs2suite.locale.Internationalization;
import cs2suite.csObjects.CSKeyboard;
import cs2suite.csUIController.FXMLController;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Pagination;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author Philipp
 */
public class KeyboardController extends FXMLController implements Initializable {

    @FXML
    private AnchorPane parent;
    @FXML
    private AnchorPane keyboardAnchor;
    @FXML
    private AnchorPane favAnchor;
    @FXML
    private SplitPane splitPane;
    @FXML
    private Pagination pagination;

    private ArrayList<KeyboardElementController> keyboardElementController;
    private Integer keyboardPage = 0;
    private Integer pages;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        keyboardElementController = new ArrayList<>();
        //keyboardPage = guiControllerWrapper.getController().getCurrentSystem().getKeyboardPage();
        pages = Math.floorDiv(CSKeyboard.MAX_ADDRESS, 40) + 1;

        pagination.setPageCount(pages);
        pagination.setMaxPageIndicatorCount(10);
    }

    public void initAfter() {

    }

    public void initKeyboard() {       
        pagination.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                try {
                    loadKeyboardPage(pageIndex);
                } catch (IOException ex) {
                    CSLogger.logEvent(2, ex, "Error loading elements for Keyboard Page " + pageIndex);
                }                
                return new Pane();
            }
        });
    }

    public void loadKeyboardPage(int page) throws IOException {
        keyboardAnchor.getChildren().subList(1, keyboardAnchor.getChildren().size()).clear();
        int counter = page*40;        
        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 10; x++) {
                FXMLLoader loader = new FXMLLoader();
                AnchorPane controlRoot = (AnchorPane) loader.load(getClass().getResource("KeyboardElement.fxml").openStream());
                controlRoot.setLayoutY(10 + (y * 150));
                controlRoot.setLayoutX(20 + (x * 70));
                keyboardElementController.add(loader.getController());
                keyboardElementController.get(keyboardElementController.size() - 1).setArticle(CSKeyboard.getArticle(counter));
                keyboardAnchor.getChildren().add(controlRoot);
                counter++;
            }
        }
        guiControllerWrapper.getController().setInfoText4(Internationalization.getString("loaded"));        
    }
    
    public Integer getKeyboardPage(){
        return pagination.getCurrentPageIndex();
    }
    
    public void sceneChange(double w, double h){
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.fxmlControllers;

import cs2suite.locale.Internationalization;
import cs2suite.csObjects.CSDecoder;
import cs2suite.csObjects.CSTrain;
import cs2suite.csUIController.FXMLController;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author Philipp
 */
public class TrainSettingsController extends FXMLController implements Initializable {

    // <editor-fold defaultstate="collapsed" desc=" FXML ">
    @FXML
    private AnchorPane parentAnchorPane;
    @FXML
    private Button acceptButton;
    @FXML
    private Button abortButton;
    @FXML
    private ImageView trainImageView;
    @FXML
    private TextField trainImagePathField;
    @FXML
    private Button selectTrainImageButton;
    @FXML
    private TextField trainNameField;
    @FXML
    private TextField trainAddrField;
    @FXML
    private ChoiceBox trainDecoderChoiceBox;
    @FXML
    private Rectangle trainImageRectangle;
    @FXML
    private TextArea trainDescriptionArea;
    @FXML
    private LineChart accelerationChart;
    @FXML
    private Pane mainSettingsPane;
    @FXML
    private Pane sliderSettingsPane;
    @FXML
    private Pane functionSettingsPane;
    @FXML
    private Slider trainAccelSlider;
    @FXML
    private Slider trainVminSlider;
    @FXML
    private Slider trainVmaxSlider;
    @FXML
    private Slider trainBreakesSlider;
    @FXML
    private Slider trainTachoSlider;
    @FXML
    private Slider trainVolumeSlider;
    @FXML
    private TextField trainAccelField;
    @FXML
    private TextField trainVminField;
    @FXML
    private TextField trainVmaxField;
    @FXML
    private TextField trainBreakesField;
    @FXML
    private TextField trainTachoField;
    @FXML
    private TextField trainVolumeField;
    @FXML
    private ImageView functionImage1;
    @FXML
    private ImageView functionImage2;
    @FXML
    private ImageView functionImage3;
    @FXML
    private ImageView functionImage4;
    @FXML
    private ImageView functionImage5;
    @FXML
    private ImageView functionImage6;
    @FXML
    private ImageView functionImage7;
    @FXML
    private ImageView functionImage8;
    @FXML
    private ImageView functionImage9;
    @FXML
    private ImageView functionImage10;
    @FXML
    private ImageView functionImage11;
    @FXML
    private ImageView functionImage12;
    @FXML
    private ImageView functionImage13;
    @FXML
    private ImageView functionImage14;
    @FXML
    private ImageView functionImage15;
    @FXML
    private ImageView functionImage16;
    
    @FXML
    private Label trainImageLabel;
    @FXML
    private Label trainNameLabel;
    @FXML
    private Label trainDecoderLabel;
    @FXML
    private Label trainAddrLabel;
    @FXML
    private Label trainDescriptionLabel;
    @FXML
    private Label trainAccelLabel;
    @FXML
    private Label trainVminLabel;
    @FXML
    private Label trainVmaxLabel;
    @FXML
    private Label trainBreakesLabel;
    @FXML
    private Label trainTachoLabel;
    @FXML
    private Label trainVolumeLabel;
// </editor-fold>

    private CSTrain currentTrain;
    private Integer mode;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mode = 0; // create 1=edit
    }
    
    public void initAfter() {
        loadLang(); // loading multi language fields
        trainDecoderChoiceBox.getItems().setAll("mm2", "dcc", "mfx");
        trainDecoderChoiceBox.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            switch (newValue.intValue()) {
                case 0:
                    currentTrain.setDecodertype(CSDecoder.MM2);
                    break;
                case 1:
                    currentTrain.setDecodertype(CSDecoder.DCC);
                    break;
                case 2:
                    currentTrain.setDecodertype(CSDecoder.MM2);
                    break;
            }
        });        
        
        trainAccelSlider.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            trainAccelField.setText(String.valueOf(newValue.intValue()));
        });
        
        trainBreakesSlider.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            trainBreakesField.setText(String.valueOf(newValue.intValue()));
        });
        
        trainTachoSlider.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            trainTachoField.setText(String.valueOf(newValue.intValue()));
        });
        
        trainVolumeSlider.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            trainVolumeField.setText(String.valueOf(newValue.intValue()));
        });
        
        trainVminSlider.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            trainVminField.setText(String.valueOf(newValue.intValue()));
        });
        
        trainVmaxSlider.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            trainVmaxField.setText(String.valueOf(newValue.intValue()));
        });                
    }
    
    @FXML
    private void handleAbortButton(ActionEvent e) {
        
    }
    
    @FXML
    private void handleAcceptButton(ActionEvent e) {
        
    }
    
    public void setMode(int m) {
        mode = m;
    }
    
    public void loadTrain(CSTrain train) throws IOException {
        setMode(1); // assume edit mode
        this.currentTrain = train;
        trainNameField.setText(train.getName());
        trainAddrField.setText(train.getAddress().toString());
        trainImagePathField.setText(train.getPicturePath());
        BufferedImage bufimg = ImageIO.read(new File(train.getPicturePath()));
        Image img = SwingFXUtils.toFXImage(bufimg, null);
        trainImageView.setImage(img);
        trainDecoderChoiceBox.getSelectionModel().select(train.getDecoderType());
        trainAccelSlider.setValue(train.getAcceleration());
        trainBreakesSlider.setValue(train.getBrakes());
        trainDescriptionArea.setText(train.getDescription());
        trainVolumeSlider.setValue(train.getVolume());
        trainVmaxSlider.setValue(train.getVMax());
        trainVminSlider.setValue(train.getVMin());
        trainTachoSlider.setValue(train.getTacho());
        /*
        functionImage1.setImage(train.getFunction(0).getFunctionOffIcon());
        functionImage1.setImage(train.getFunction(1).getFunctionOffIcon());
        functionImage1.setImage(train.getFunction(2).getFunctionOffIcon());
        functionImage1.setImage(train.getFunction(3).getFunctionOffIcon());
        functionImage1.setImage(train.getFunction(4).getFunctionOffIcon());
        functionImage1.setImage(train.getFunction(5).getFunctionOffIcon());
        functionImage1.setImage(train.getFunction(6).getFunctionOffIcon());
        functionImage1.setImage(train.getFunction(7).getFunctionOffIcon());
        functionImage1.setImage(train.getFunction(8).getFunctionOffIcon());
        functionImage1.setImage(train.getFunction(9).getFunctionOffIcon());
        functionImage1.setImage(train.getFunction(10).getFunctionOffIcon());
        functionImage1.setImage(train.getFunction(11).getFunctionOffIcon());
        functionImage1.setImage(train.getFunction(12).getFunctionOffIcon());
        functionImage1.setImage(train.getFunction(13).getFunctionOffIcon());
        functionImage1.setImage(train.getFunction(14).getFunctionOffIcon());
        functionImage1.setImage(train.getFunction(15).getFunctionOffIcon());        
        */
    }
    
    public void createTrain() {
        setMode(0); // create mode
        this.currentTrain = new CSTrain();
    }
    
    private void clearAllFields() {
        trainAddrField.setText("");
        trainAccelSlider.setValue(0.);
        
    }
    
    private void loadLang() {
        trainImageLabel.setText(Internationalization.getString("train_icon"));
        trainNameLabel.setText(Internationalization.getString("name"));
        trainAccelLabel.setText(Internationalization.getString("accel"));
        trainAddrLabel.setText(Internationalization.getString("address"));
        trainBreakesLabel.setText(Internationalization.getString("breaks"));
        trainDecoderLabel.setText(Internationalization.getString("decoder"));
        trainDescriptionLabel.setText(Internationalization.getString("description"));
        trainVolumeLabel.setText(Internationalization.getString("volume"));
        trainVminLabel.setText(Internationalization.getString("vmin"));
        trainVmaxLabel.setText(Internationalization.getString("vmax"));
        trainTachoLabel.setText(Internationalization.getString("tacho"));
    }
}

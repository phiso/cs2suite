/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.fxmlControllers;

import cs2suite.Models.Objects.Train;
import cs2suite.csUIController.FXMLController;
import cs2suite.utils.HelperUtils;
import eu.hansolo.enzo.gauge.RadialBargraph;
import eu.hansolo.enzo.led.Led;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/**
 * FXML Controller class
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class TrainControl_TableController extends FXMLController implements Initializable {
    
    private static final String UP_ARROW = "\u2191";
    private static final String DOWN_ARROW = "\u2193";
    private static final String ARROWS = "\u21C5";
    private static final Paint LED_GREEN = Paint.valueOf("#00c000");
    private static final Paint LED_RED = Paint.valueOf("#ff0000");
    
    @FXML
    private ImageView trainImageView;
    @FXML
    private Label trainInfoLabel;
    @FXML
    private Pane tachoBackgroundPane;
    @FXML
    private Pane ledBackgroundPane;
    @FXML
    private Label trainDirectionLabel;
    
    private Led trainStatusLed;
    private RadialBargraph trainTachoBargraph;
        
    private Train thisTrain;
            
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        trainStatusLed = new Led();
        trainTachoBargraph = new RadialBargraph();
    } 
    
    @Override
    public void initAfter(){        
        tachoBackgroundPane.setPrefSize(64, 64);
        tachoBackgroundPane.getChildren().addAll(trainTachoBargraph);
        ledBackgroundPane.getChildren().addAll(trainStatusLed);
        trainStatusLed.setLedType(Led.LedType.VERTICAL);
        trainStatusLed.setLedColor(LED_RED);
        trainTachoBargraph.setBarColor(Color.valueOf("#0003c5"));
        trainTachoBargraph.setMinValue(0);
        trainTachoBargraph.setMaxValue(1024);
        trainTachoBargraph.setPrefSize(64, 64);
        trainTachoBargraph.setPlainValue(true);
        trainDirectionLabel.setText(ARROWS);
    }
    
    /**
     * Loads the given Train Object into the Controller
     * @param train 
     */
    public void loadTrain(Train train) throws IOException{
        thisTrain = train;
        trainImageView.setImage(HelperUtils.loadFXImage(thisTrain.getProfilePicture()));
        trainInfoLabel.setText(thisTrain.toString());
        trainDirectionLabel.setText(ARROWS);
    }

    /**
     * @return the thisTrain
     * its the Train Object which is currently controller by this Controller
     */
    public Train getThisTrain() {
        return thisTrain;
    }
    
}

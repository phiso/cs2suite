/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.fxmlControllers;

import cs2suite.csUIController.FXMLController;
import cs2suite.locale.Internationalization;
import cs2suite.network.CommandParser;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.zip.DataFormatException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Philipp
 */
public class TrayMenuController extends FXMLController implements Initializable {

    @FXML
    private AnchorPane parentPane;
    @FXML
    private TitledPane titledPane;
    @FXML
    private Button openButton;
    @FXML
    private Button closeButton;
    @FXML
    private Button sendButton;
    @FXML
    private Button hideButton;
    @FXML
    private TextField sendField;    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        closeButton.setText(Internationalization.getString("closeApplication"));
        openButton.setText(Internationalization.getString("open"));
        hideButton.setText(Internationalization.getString("hideToTray"));
    }                   

    @FXML
    private void handleOpenButton() {
        cs2SuiteGUIController.getStage().show();
        getStage().hide();
    }

    @FXML
    private void handleCloseButton() throws IOException {
        guiControllerWrapper.getController().handleApplicationClose();
    }
    
    @FXML
    private void handleHideButton() {
        guiControllerWrapper.getController().hideApplication();
    }

    @FXML
    private void handleSendButton() throws IOException, DataFormatException, SQLException, ClassNotFoundException {
        KeyEvent ke = new KeyEvent(null, null, null, KeyCode.ENTER, false, false, false, false);
        handleSendFieldKeyPress(ke);
    }

    @FXML
    public void handleSendFieldKeyPress(KeyEvent e) throws IOException, DataFormatException, SQLException, ClassNotFoundException {
        if (e.getCode().equals(KeyCode.ENTER)) {
            CommandParser.execCommand(sendField.getText());
            sendField.clear();
        }/* else if (e.getCode().equals(KeyCode.UP)) {
         if (atDebugConsoleRecent != -1) {
         atDebugConsoleRecent++;
         }
         if (atDebugConsoleRecent >= recentDebugInputs.size()) {
         atDebugConsoleRecent = recentDebugInputs.size() - 1;
         }
         debugConsoleInputField.setText(recentDebugInputs.get(atDebugConsoleRecent));
         } else if (e.getCode().equals(KeyCode.DOWN)) {
         if (atDebugConsoleRecent == -1) {
         atDebugConsoleRecent = recentDebugInputs.size() - 1;
         } else {
         atDebugConsoleRecent--;
         }
         debugConsoleInputField.setText(recentDebugInputs.get(atDebugConsoleRecent));
         }*/

    }        
}

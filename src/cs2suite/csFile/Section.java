/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csFile;

import java.util.ArrayList;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Philipp
 */
public class Section {

    private ArrayList<String> section;
    private ArrayList<String> tempSubSection;
    private ArrayList<SubSection> subSections;
    private ArrayList<String> subSectionIdentifiers;
    private String identifier;
    private Integer subSectionCount;

    private SimpleStringProperty sectionName;
    private SimpleIntegerProperty sectionAttributeCount;

    public Section(String identifier) {
        super();
        this.subSections = new ArrayList<>();
        tempSubSection = new ArrayList<>();
        this.subSections.clear();
        this.subSectionCount = 0;
        this.subSectionIdentifiers = new ArrayList<>();
        this.identifier = identifier;

        switch (this.identifier) {
            case "lok":
                subSectionIdentifiers.clear();
                break;
            case "lokomotive":
                subSectionIdentifiers.add("funktionen");
                subSectionIdentifiers.add("prg");
                subSectionIdentifiers.add("reg");
                break;
            case "artikel":
                subSectionIdentifiers.clear();
                break;
            case "fahrstrasse":
                subSectionIdentifiers.add("item");
                break;
            case "gleisbildseite":
                subSectionIdentifiers.add("element");
                break;
            case "gleisbild":
                subSectionIdentifiers.clear();
                break;
        }
    }

    public Section(ArrayList<String> section, String identifier) {
        this(identifier);
        this.section = (ArrayList<String>) section.clone();

        for (int i = 0; i < subSectionIdentifiers.size(); i++) {
            subSectionCount += getSubSections(subSectionIdentifiers.get(i));
        }

        if (identifier != "gleisbildseite") {
            this.sectionName = new SimpleStringProperty(this.getAttribute("name"));
        } else {
            this.sectionName = new SimpleStringProperty(this.getAttribute("typ"));
        }
        this.sectionAttributeCount = new SimpleIntegerProperty(0);
        this.sectionAttributeCount.set(getAttributeCount());

    }

    private int getSubSections(String ident) {
        int subCounter = 0;
        int resultCount = 0;
        tempSubSection.clear();
        for (int i = 0; i < section.size(); i++) {
            if (section.get(i).equals(ident)) {
                subCounter = i + 1;
                while (section.get(subCounter) != ident) {
                    tempSubSection.add(section.get(subCounter));
                    subCounter++;
                    if (subCounter >= section.size()) {
                        break;
                    }
                }
                subSections.add(new SubSection(tempSubSection, ident));
                tempSubSection.clear();
                i = subCounter - 1;
                resultCount++;
            }
        }
        return resultCount;
    }

    public SubSection addSubSection(String identifier) {
        section.add("."+identifier+"\n");
        subSections.add(new SubSection(identifier));
        return subSections.get(subSections.size() - 1);
    }

    private int getAttributeCount() {
        int result = 0;
        for (int i = 0; i < section.size(); i++) {
            if (isAttributeLine(i)) {
                result++;
            }
        }
        return result;
    }

    private String getLineKey(int index) {
        String result = section.get(index);
        result = result.replace(".", "");
        result = result.substring(1, result.indexOf("="));
        return result;
    }

    private String getLineValue(int index) {
        String result = section.get(index);
        result = result.substring(result.indexOf("=") + 1, result.length());
        return result;
    }

    private Boolean isAttributeLine(int index) {
        if (section.get(index).indexOf("=") == -1 && section.get(index).indexOf("..") == -1) {
            //kein Attribut, da kein "=" vorhanden
            return false;
        } else {
            return true;
        }
    }

    public String getAttribute(String key) {
        for (int i = 0; i < section.size(); i++) {
            if (isAttributeLine(i)) {
                String lineKey = getLineKey(i);
                if (lineKey.equals(key)) {
                    return getLineValue(i);
                }
            }
        }
        return null;
    }
    
    public Attribute getAttribute(Integer index) {
        if (isAttributeLine(index)) {
            String lineKey = getLineKey(index);
            String lineValue = getLineValue(index);
            Attribute temp = new Attribute();
            temp.setKey(lineKey);
            temp.setValue(lineValue);
            return temp;
        }
        return null;
    }
    
    public void addAttribute(String key, String val){
        section.add("."+key+"="+val+"\n");
    }

    public SubSection getSubSectionByAttribute(String attr, String val) {
        for (int i = 0; i < subSections.size(); i++) {
            if (subSections.get(i).getAttribute(attr) == val) {
                //Section gefunden
                return subSections.get(i);
            }
        }
        return null;
    }

    public SubSection getSubSection(int index) {
        return subSections.get(index);
    }

    public String getSectionName() {
        return sectionName.get();
    }

    public SimpleStringProperty getSectionNameProperty() {
        return sectionName;
    }

    public void setSectionName(String sname) {
        this.sectionName.set(sname);
    }

    public Integer getSectionAttributeCount() {
        return this.sectionAttributeCount.get();
    }

    public SimpleIntegerProperty getSectionAttributeCountProperty() {
        return this.sectionAttributeCount;
    }

    public void setSectionAttributeCount(Integer scount) {
        this.sectionAttributeCount.set(scount);
    }
}

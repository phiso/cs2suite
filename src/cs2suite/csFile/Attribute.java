package cs2suite.csFile;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Philipp This Class is needed to build a TableView out of Attributes
 * from a cs2 File
 */
public class Attribute {

    private SimpleStringProperty key;
    private SimpleStringProperty value;
    
    public Attribute() {
        super();
        key = new SimpleStringProperty();
        value = new SimpleStringProperty();
    }

    public SimpleStringProperty keyProperty() {
        return key;
    }

    public SimpleStringProperty valueProperty() {
        return value;
    }

    public String getKey() {
        return key.get();
    }

    public String getValue() {
        return value.get();
    }

    public void setValue(String val) {
        value.set(val);
    }

    public void setKey(String key) {
        this.key.set(key);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csFile;

import java.util.ArrayList;

/**
 *
 * @author Philipp
 */
public class SubSection {

    private ArrayList<String> subSection;
    private String identifier;

    public SubSection(String identifier){
        super();
        this.identifier = identifier;
    }
    
    public SubSection(ArrayList<String> sublist, String identifier) {
        this(identifier);
        subSection = new ArrayList<>();

    }

    private String getLineKey(int index) {
        String result = subSection.get(index);
        result = result.replace("..", "");
        result = result.substring(0, result.indexOf("="));
        return result;
    }

    private String getLineValue(int index) {
        String result = subSection.get(index);
        result = result.substring(result.indexOf("=") + 1, result.length());
        return result;
    }

    public String getAttribute(String key) {
        for (int i = 0; i < subSection.size(); i++) {
            String lineKey = getLineKey(i);
            if (lineKey.equals(key)) {
                return getLineValue(i);
            }
        }
        return null;
    }
    
    public void addAttribute(String key, String val){
        subSection.add(".."+key+"="+val+"\n");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csFile;

import java.awt.MouseInfo;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.controlsfx.control.PopOver;

/**
 *
 * @author Philipp
 */
public class CSFileViewer {

    private AnchorPane previewPane;
    private CSFile csFile;
    private TableView sectionList = new TableView();
    private Label info;
    private final ObservableList<Section> sections = FXCollections.observableArrayList();
    private PopOver popOver;

    public CSFileViewer(CSFile file, AnchorPane preview) {
        super();
        csFile = file;
        previewPane = preview;
        sectionList = createTableView();
        sectionList.setLayoutX(10);
        sectionList.setLayoutY(25);
        //sectionList.setPrefWidth(previewPane.getWidth() - 10);

        info = new Label();
        info.setText("Dateityp: " + file.getFileType());
        info.setLayoutY(5);
        info.setLayoutX(10);

        previewPane.getChildren().add(sectionList);
        previewPane.getChildren().add(info);

        for (int i = 0; i < file.getSectionCount(); i++) {
            sections.add(file.getSection(i));
        }
    }

    private TableView createTableView() {
        TableColumn<Section, String> nameCol = new TableColumn<>("Section - Name");
        nameCol.setCellValueFactory(new PropertyValueFactory<Section, String>("sectionName"));

        TableColumn<Section, Integer> countCol = new TableColumn<>("Attribute");
        countCol.setCellValueFactory(new PropertyValueFactory<Section, Integer>("sectionAttributeCount"));

        TableView<Section> cssections = new TableView<>();
        cssections.setItems(sections);

        cssections.getColumns().addAll(nameCol, countCol);
        cssections.setLayoutX(10);
        cssections.setLayoutY(10);
        cssections.setPrefWidth(previewPane.getWidth() - 50);
        cssections.setPrefHeight(previewPane.getHeight() - 40);

        nameCol.setPrefWidth(cssections.getPrefWidth() * 0.75);
        countCol.setPrefWidth(cssections.getPrefWidth() * 0.25);

        cssections.setOnMousePressed((MouseEvent event) -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                Section temp = cssections.getSelectionModel().getSelectedItem();
                ObservableList<Attribute> attributes = FXCollections.observableArrayList();
                for (int i = 0; i < temp.getSectionAttributeCount(); i++) {
                    attributes.add(temp.getAttribute(i));
                }
                AnchorPane popOverContent = new AnchorPane();
                popOverContent.setPrefWidth(250);
                TableView popupTable = popupTableView(attributes);
                popOverContent.getChildren().add(popupTable);
                popOver = new PopOver(popOverContent);
                popOver.setDetached(true);
                popOver.setHideOnEscape(true);
                popOver.setTitle(temp.getSectionName());
                popOver.show(previewPane, MouseInfo.getPointerInfo().getLocation().x + 5, MouseInfo.getPointerInfo().getLocation().y);
            }
        });

        /*
        close popover when clicking on another entry
        */
        cssections.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            try {
                popOver.hide();
            } catch (Exception e) {
            }
        });

        return cssections;
    }

    private TableView popupTableView(ObservableList<Attribute> attr) {
        TableColumn<Attribute, String> keyCol = new TableColumn<>("Attribute");
        keyCol.setCellValueFactory(new PropertyValueFactory<Attribute, String>("key"));

        TableColumn<Attribute, String> valueCol = new TableColumn<>("Value");
        valueCol.setCellValueFactory(new PropertyValueFactory<Attribute, String>("value"));

        TableView<Attribute> tableView = new TableView<>();
        tableView.setItems(attr);
        tableView.getColumns().addAll(keyCol, valueCol);

        tableView.setLayoutX(0);
        tableView.setLayoutY(0);
        tableView.setPrefWidth(250);
        keyCol.setPrefWidth(125);
        valueCol.setPrefWidth(125);

        return tableView;
    }

    public Double getTableHeight() {
        return sectionList.getHeight();
    }

}

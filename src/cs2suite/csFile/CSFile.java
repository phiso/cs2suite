/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csFile;

import cs2suite.Models.Objects.Train;
import cs2suite.Models.Enums.TrainType;
import cs2suite.logic.CSLogger;
import cs2suite.Models.Objects.Article;
import cs2suite.csObjects.CSDecoder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Philipp
 */
public class CSFile {

    public static final String CSTRAINS = "lokomotive";
    public static final String CSARTICLE = "magnetartikel";
    public static final String CSLAYOUTPAGE = "gleisbildseite";
    public static final String CSLAYOUT = "gleisbild";
    public static final String CSTRACKS = "fahrstrassen";

    private String filePath;
    private String fileType;
    private ArrayList<String> wholeFile;
    private ArrayList<String> sectionNames;
    private ArrayList<Section> sections;
    private String sectionIdentifier;
    private String subAttributeIdentifier;
    private ArrayList<String> tempSection;
    private Integer sectionsCount;
    private String csType;

    public CSFile(String data) {
        super();
        sectionsCount = 0;
        wholeFile = new ArrayList<>();
        sectionNames = new ArrayList<>();
        sections = new ArrayList<>();
        tempSection = new ArrayList<>();
        File file = new File(data);
        filePath = null;
        if (file.canRead()) {
            filePath = data;
            readFile();
            fileType = identifyFile();  //hier werden auch die section Attribute gesetzt
            sectionsCount = getSections(sectionIdentifier);
        } else {
            readString(data);
            fileType = identifyFile();
            sectionsCount = getSections(sectionIdentifier);
        }

    }

    /*
     reads the File given in the Constructor
     every line -> one entry in "wholeFile"
     */
    private void readFile() {
        try {
            BufferedReader in = new BufferedReader(new FileReader(filePath));
            String zeile = null;
            while ((zeile = in.readLine()) != null) {
                wholeFile.add(zeile);
            }
        } catch (IOException e) {
            CSLogger.logEvent(4, e, "Error reading cs2 File (" + filePath + ")");
        }
    }

    private void readString(String str) {
        try {
            BufferedReader in = new BufferedReader(new StringReader(str));
            String zeile = null;
            while ((zeile = in.readLine()) != null) {
                wholeFile.add(zeile);
            }
        } catch (IOException e) {
            CSLogger.logEvent(4, e, "Error reading cs2 File (" + filePath + ")");
        }
    }

    private boolean isSectionIdentifier(String line) {
        char[] chars = line.toCharArray();
        return chars[0] != "[".charAt(0) && chars[0] != " ".charAt(0);
    }

    public Integer parseHexValue(String str) {
        Integer p = str.indexOf("x");
        String temp = str.substring(p + 1, str.length());
        return Integer.parseInt(temp, 16);
    }

    private String identifyFile() {
        String result = wholeFile.get(0).replace("[", "");
        result = result.replace("]", "");
        csType = result;
        switch (result) {
            case "lokomotive":
                Section version = getSection("version");
                if (version != null) {
                    sectionIdentifier = "lokomotive";
                    subAttributeIdentifier = "funktionen,prg,reg";
                } else {
                    sectionIdentifier = "lok";
                }
                break;
            case "magnetartikel":
                sectionIdentifier = "artikel";
                break;
            case "fahrstrassen":
                sectionIdentifier = "fahrstrasse";
                subAttributeIdentifier = "item";
                break;
            case "gleisbildseite":
                sectionIdentifier = "element";
                break;
            case "gleisbild":
                sectionIdentifier = "seite";
                break;
        }
        return result;
    }

    private int getSections(String identifier) {
        int sectionCounter = 0;
        int resultCount = 0;
        tempSection.clear();
        for (int i = 0; i < wholeFile.size(); i++) {
            if (wholeFile.get(i).equals(identifier)) {
                sectionCounter = i + 1;
                while (!wholeFile.get(sectionCounter).equals(identifier)) {
                    tempSection.add(wholeFile.get(sectionCounter));
                    sectionCounter++;
                    if (sectionCounter >= wholeFile.size() - 1) {
                        break;
                    }
                }
                sections.add(new Section(tempSection, fileType));
                tempSection.clear();
                i = sectionCounter - 1;
                resultCount++;
            }
        }
        return resultCount;
    }

    /**
     * @param sectionIdentifier
     * @return returns the first section found with the given identifier
     */
    public Section getSection(String sectionIdentifier) {
        tempSection.clear();
        for (int i = 0; i < wholeFile.size(); i++) {
            if (wholeFile.get(i).equals(sectionIdentifier)) {
                int counter = i + 1;
                while (!isSectionIdentifier(wholeFile.get(counter))) {
                    tempSection.add(wholeFile.get(counter));
                    counter++;
                    if (counter >= wholeFile.size() - 1) {
                        break;
                    }
                }
                return new Section(tempSection, sectionIdentifier);
            }
        }
        return null;
    }

    public void store() {
        PrintWriter printWriter = null;
        File file = new File(filePath);
        try {
            printWriter = new PrintWriter(new FileWriter(file));
            Iterator iter = wholeFile.iterator();
            while (iter.hasNext()) {
                Object o = iter.next();
                printWriter.println(o);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
    }

    public ArrayList<Train> getAllTrains() {
        ArrayList<Train> results = new ArrayList<>();
        Integer sectionCount = this.getSectionCount();
        for (int i = 0; i < sectionCount; i++) {
            Section section = this.getSection(i);
            Integer adr = 0;
            String temp = null;
            String decoder = CSDecoder.parseDecoder(section.getAttribute("typ"));
            if (decoder == "dcc" || decoder == "mfx") {
                temp = section.getAttribute("adresse");
            } else {
                temp = section.getAttribute("uid");
            }
            adr = this.parseHexValue(temp);
            Train tempTrain = new Train(adr, section.getAttribute("name"), TrainType.LOCOMOTIVE);
            tempTrain.setProfilePicture(System.getProperty("user.dir") + "\\data\\img\\trains\\" + section.getAttribute("icon") + ".jpg");
            results.add(tempTrain);
        }
        return results;
    }

    public ArrayList<Article> getAllArticles() throws SQLException, IOException {
        ArrayList<Article> results = new ArrayList<>();
        /*
        Integer sectionCount = this.getSectionCount();
        for (int i = 0; i < sectionCount; i++) {
            Section section = this.getSection(i);
            Integer adr = Integer.parseInt(section.getAttribute("id"));
            String decoder = CSDecoder.parseDecoder(section.getAttribute("dectyp"));
            String name = "Adr. " + adr;
            try {
                name = section.getAttribute("name");
            } catch (Exception e) {
            }
            String articleType = section.getAttribute("typ");
            Integer timing;
            try {
                timing = Integer.parseInt(section.getAttribute("schaltzeit"));
            } catch (Exception e) {
                timing = 200;
            }
            Article tempArticle = new Article(name, adr, );
            Integer state = 0;
            try {
                state = Integer.parseInt(section.getAttribute("stellung"));
            } catch (Exception e) {
            }
            tempArticle.setState(state);
            results.add(tempArticle);
        }
                */
        return results;
    }

    public Section addSection(Section section) {
        sections.add(section);
        return sections.get(sections.size() - 1);
    }

    public String getFilePath() {
        return filePath;
    }

    public ArrayList<String> getSectionNames() {
        return sectionNames;
    }

    public Section getSection(int index) {
        return sections.get(index);
    }

    public Section getSectionByAttribute(String attrib, String value) {
        for (int i = 0; i < sections.size(); i++) {
            if (sections.get(i).getAttribute(attrib) == value) {
                //Section gefunden
                return sections.get(i);
            }
        }
        return null;
    }

    private String parseArticleType(String typ) {
        /*
        switch (typ) {
            case "std_gruen":
            case "std_rot":
            case "std_rot_gruen":
                return "Standard";
            case "entkupplungsgleis":
                return "DeclutchRail";
            case "rechtsweiche":
                return "RailwaySwitchRight";
            case "linksweiche":
                return "RailwaySwitchLeft";
            case "licht_neon":
                
            case "licht_lampe":
            case "licht_mast":
            case "y_weiche":
            case "andreas":
            case "schranke":

            case "lichtsignal_HP01":

            case "lichtsignal_SH01":

            case "formsignal_SH01":

            case "urc_lichtsignal_HP01":

            case "urc_lichtsignal_SH01":

            case "k84_doppelausgang":

            case "formsignal_HP02":

            case "formsignal_HP012":

            case "lichtsignal_HP02":

            case "lichtsignal_HP012":

            case "urc_lichtsignal_HP012":

            case "dkw_2antriebe":

            case "lichtsignal_HP012_SH01":

            case "formsignal_HP012_SH01":
            case "urc_lichtsignal_HP012_SH01":

            case "digitaldrehscheibe":

            default:

        }*/
        return "";
    }

    public int getSectionCount() {
        return this.sectionsCount;
    }

    public String getFileType() {
        return fileType;
    }

    public String getSectionIdentifier() {
        return sectionIdentifier;
    }

    public String getCSType() {
        return csType;
    }
}

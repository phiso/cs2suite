/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite;

//<editor-fold defaultstate="collapsed" desc="imports">
import cs2suite.Models.SqliteDatabase;
import cs2suite.csUIController.CS_UI_MainController;
import cs2suite.logic.Settings;
import cs2suite.logic.CSLogger;
import cs2suite.fxmlControllers.TrayMenuController;
import cs2suite.fxmlControllers.cs2SuiteGUIController;
import cs2suite.locale.Internationalization;
import cs2suite.managers.ConnectionManager;
import cs2suite.managers.SqliteManager;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.MenuItem;
import java.awt.MouseInfo;
import java.awt.PopupMenu;
import java.awt.SplashScreen;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javax.imageio.ImageIO;
//</editor-fold>

/**
 *
 * @author Philipp
 */
public class CS2Suite extends Application {

    private static final String ver = "0.0.5";
    private static final String buildNr = "85";
    
    //<editor-fold defaultstate="collapsed" desc="Filepaths">
    public static final String TEMP_DIR = System.getProperty("user.dir") + "\\data\\temp\\";
    public static final String TRUNK_IMG_DIR = System.getProperty("user.dir") + "\\data\\img\\";
    public static final String UI_IMG_DIR = System.getProperty("user.dir") + "\\data\\img\\ui\\";
    public static final String FUNCTION_IMG_DIR = System.getProperty("user.dir") + "\\data\\img\\functions\\";
    public static final String KEYBOARD_IMG_DIR = System.getProperty("user.dir") + "\\data\\img\\keyboard\\";
    public static final String LAYOUT_IMG_DIR = System.getProperty("user.dir") + "\\data\\img\\layout\\";
    public static final String TRAIN_IMG_DIR = System.getProperty("user.dir") + "\\data\\img\\trains\\";
    public static final String LANG_DIR = System.getProperty("user.dir") + "\\data\\lang\\";
    public static final String HELP_DIR = System.getProperty("user.dir") + "\\data\\help\\";
    public static final String CONF_DIR = System.getProperty("user.dir") + "\\data\\conf\\";
    public static final String SAVES_DIR = System.getProperty("user.dir") + "\\data\\saves\\";
    public static final String LAYOUT_DIR = System.getProperty("user.dir") + "\\data\\layout\\";
    public static final String SOUND_DIR = System.getProperty("user.dir") + "\\data\\sound\\";
    public static final String DATA_DIR = System.getProperty("user.dir") + "\\data\\";
    public static final String TRUNK_DIR = System.getProperty("user.dir");
    public static final String SOUND_LIB_DIR = System.getProperty("user.dir")+"\\data\\sound\\maerklin\\";
    public static final String IMPORT_DIR = System.getProperty("user.dir")+"\\data\\import\\";
    //</editor-fold>

    private FXMLLoader fxmlLoader;
    private cs2SuiteGUIController controller;
    private Stage stage;
    private Stage trayStage;
    private static SplashScreen splashScreen;
    private static Rectangle2D.Double splashTextArea;
    private static Rectangle2D.Double splashVerArea;
    private static Rectangle2D.Double splashBuildArea;
    private static Rectangle2D.Double splashProgressArea;
    private static Graphics2D splashGraphics;
    private Boolean trayShown = false;

    @Override
    public void start(Stage stage) throws Exception {
        Settings.getInstance();
        CSLogger.getInstance();
        Internationalization.getInstance("de", "DE");
        ConnectionManager.getInstance();
        SqliteManager.getInstance(new SqliteDatabase(SqliteDatabase.DEFAULT_DB_LOCATION, false));
        
        splashInit();
        Settings.setVersion(ver);
        Settings.setBuild(buildNr);
        splashText("Loading UI...");
        splashProgress(25);
        URL location = getClass().getResource("CS2Suite.fxml");
        fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        splashText("building UI...");
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        Parent root = (Parent) fxmlLoader.load(location.openStream());        
        splashProgress(75);
        splashText("creating stage...");
        Scene scene = new Scene(root);        
        scene.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            ((cs2SuiteGUIController) fxmlLoader.getController()).sceneChange(scene.getWidth(), scene.getHeight());
        });
        scene.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            ((cs2SuiteGUIController) fxmlLoader.getController()).sceneChange(scene.getWidth(), scene.getHeight());
        });
        stage.setScene(scene);        

        controller = fxmlLoader.getController();
        controller.initAfter();
        controller.setStage(stage);
        splashProgress(90);
        splashText("Ready!");
        splashProgress(150);
        // setting application Icon
        BufferedImage bufImg = ImageIO.read(new File(System.getProperty("user.dir") + "\\data\\img\\ui\\tray_icon.png"));
        Image img = SwingFXUtils.toFXImage(bufImg, null);
        stage.getIcons().add(img);
        if (SystemTray.isSupported()) {
            Platform.setImplicitExit(!Settings.getImplicitExit());
            setTrayIcon(stage);
        }
        stage.show();
        controller.finalInit();

        if (splashScreen != null) {
            splashScreen.close();
        }        
    }

    @Override
    public void stop() throws SocketException, IOException {
        controller.handleApplicationClose();
    }

    private static void splashInit() {
        // the splash screen object is created by the JVM, if it is displaying a splash image

        splashScreen = SplashScreen.getSplashScreen();
        // if there are any problems displaying the splash image
        // the call to getSplashScreen will returned null

        if (splashScreen != null) {
            // get the size of the image now being displayed
            Dimension ssDim = splashScreen.getSize();
            int height = ssDim.height;
            int width = ssDim.width;

            // stake out some area for our status information
            //splashTextArea = new Rectangle2D.Double(15., height * 0.88, width * .45, 32.);
            splashTextArea = new Rectangle2D.Double(width * .105, height * 0.835, width * .7, 18.);
            splashProgressArea = new Rectangle2D.Double(width * .11, height * .90, width * .70, 5.);
            splashVerArea = new Rectangle2D.Double(width * .934, height * 0.912, width, 10.);
            splashBuildArea = new Rectangle2D.Double(width * .934, height * 0.947, width, 10.);

            // create the Graphics environment for drawing status info
            splashGraphics = splashScreen.createGraphics();
            splashGraphics.setFont(new Font("Dialog", Font.PLAIN, 10));

            // initialize the status info
            splashText("Starting");
        }
    }

    /**
     * Display text in status area of Splash. Note: no validation it will fit.
     *
     * @param str - text to be displayed
     */
    public static void splashText(String str) {
        if (splashScreen != null && splashScreen.isVisible()) {   // important to check here so no other methods need to know if there
            // really is a Splash being displayed

            // erase the last status text
            splashGraphics.setPaint(Color.WHITE);
            splashGraphics.fill(splashTextArea);
            // draw the text           
            splashGraphics.setPaint(new Color(0, 0, 129));
            splashGraphics.drawString(str, (int) (splashTextArea.getX() + 10), (int) (splashTextArea.getY() + 15));
            splashGraphics.drawString(ver, (int) (splashVerArea.getX() + 10), (int) (splashVerArea.getY() + 15));
            splashGraphics.drawString(buildNr, (int) (splashBuildArea.getX() + 10), (int) (splashBuildArea.getY() + 15));

            // make sure it's displayed
            splashScreen.update();
        }
    }

    /**
     * Display a (very) basic progress bar
     *
     * @param pct how much of the progress bar to display 0-100
     */
    public static void splashProgress(int pct) {
        if (splashScreen != null && splashScreen.isVisible()) {

            // Note: 3 colors are used here to demonstrate steps
            // erase the old one
            splashGraphics.setPaint(new Color(162, 181, 205, 3));
            splashGraphics.fill(splashProgressArea);

            // draw an outline
            splashGraphics.setPaint(new Color(0, 0, 0, 120));
            splashGraphics.draw(splashProgressArea);

            // Calculate the width corresponding to the correct percentage
            int x = (int) splashProgressArea.getMinX();
            int y = (int) splashProgressArea.getMinY();
            int wid = (int) splashProgressArea.getWidth();
            int hgt = (int) splashProgressArea.getHeight();

            int doneWidth = Math.round(pct * wid / 100.f);
            doneWidth = Math.max(0, Math.min(doneWidth, wid - 1));  // limit 0-width

            // fill the done part one pixel smaller than the outline
            splashGraphics.setPaint(new Color(0, 0, 139, 150));
            splashGraphics.fillRect(x, y + 1, doneWidth, hgt - 1);

            // make sure it's displayed
            splashScreen.update();
        }
    }

    private void setTrayIcon(Stage stage) throws AWTException {
        SystemTray sTray = SystemTray.getSystemTray();
        java.awt.Image image = Toolkit.getDefaultToolkit().getImage(System.getProperty("user.dir") + "\\data\\img\\ui\\tray_icon.png");

        Parent root;
        FXMLLoader trayLoader = new FXMLLoader();
        URL trayLocation = getClass().getResource("TrayMenu.fxml");
        trayLoader.setLocation(trayLocation);
        try {
            root = (Parent) trayLoader.load();
            Scene trayScene = new Scene(root);
            trayStage = new Stage(StageStyle.UNDECORATED);
            trayStage.setScene(trayScene);      
            ((TrayMenuController) trayLoader.getController()).setStage(trayStage);
        } catch (IOException ex) {
            CSLogger.logEvent(3, ex, "Error loading TrayMenu FXML");
        }

        ActionListener listenerShow = (ActionEvent e) -> {
            Platform.runLater(() -> {
                stage.show();                
            });
        };

        ActionListener listenerClose = (ActionEvent e) -> {
            Platform.runLater(() -> {
                try {
                    controller.handleApplicationClose();
                } catch (SocketException ex) {
                    Logger.getLogger(CS2Suite.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(CS2Suite.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        };

        PopupMenu popup = new PopupMenu();
        MenuItem showItem = new MenuItem(Internationalization.getString("tray_show"));
        MenuItem exitItem = new MenuItem(Internationalization.getString("tray_exit"));
        showItem.addActionListener(listenerShow);
        exitItem.addActionListener(listenerClose);
        popup.add(showItem);
        popup.add(exitItem);

        TrayIcon icon = new TrayIcon(image, Internationalization.getString("cs2suite"), popup);
        icon.setToolTip(Internationalization.getString("cs2suite"));
        icon.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                Platform.runLater(() -> {
                    if (!trayShown) {
                        trayStage.show();
                        trayShown = true;
                        trayStage.setX(MouseInfo.getPointerInfo().getLocation().getX() - 10);
                        trayStage.setY(CS_UI_MainController.getScreenDimension().height - trayStage.getHeight() - 45);
                        if (trayStage.getX()+trayStage.getWidth() > CS_UI_MainController.getScreenDimension().getWidth()){
                            int diff = (int) (trayStage.getX()+trayStage.getWidth() - CS_UI_MainController.getScreenDimension().getWidth());
                            trayStage.setX(trayStage.getX()-(diff+10));
                        }
                    }else{
                        trayStage.hide();
                        trayShown = false;
                    }
                });
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        icon.setImageAutoSize(true);
        sTray.add(icon);

        if (Settings.getImplicitExit()) {
            stage.setOnCloseRequest((WindowEvent event) -> {
                stage.hide();
                icon.displayMessage("CS2Suite", Internationalization.getString("tray_stillRunningMessage"), TrayIcon.MessageType.INFO);
            });
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}

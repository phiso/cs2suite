/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.tables;

import cs2suite.Models.Objects.LayoutObject;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class LayoutObjectsTableData {
    
    private LayoutObject layoutObject;
    
    public LayoutObjectsTableData(LayoutObject obj){
        this.layoutObject = obj;
    }
    
    public String getPicturePath(){
        return layoutObject.getIconPath();
    }
}

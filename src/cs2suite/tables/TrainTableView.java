/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.tables;

import cs2suite.Models.Objects.Train;
import cs2suite.csUIController.FXMLBundle;
import cs2suite.csUIController.FXMLController;
import cs2suite.locale.Internationalization;
import cs2suite.logic.CSLogger;
import cs2suite.managers.FXMLManager;
import cs2suite.network.TrainListener;
import cs2suite.utils.HelperUtils;
import eu.hansolo.enzo.gauge.RadialBargraph;
import eu.hansolo.enzo.led.Led;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.util.Callback;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class TrainTableView {
    
    private static final Paint ledGreenPaint = Paint.valueOf("#00c000");
    private static final Paint ledRedPaint = Paint.valueOf("#ff0000");
    
    private TableView tableView;
    private final ObservableList<Train> data;
    
    public TrainTableView() {
        super();
        data = FXCollections.observableArrayList();
    }
    
    public TrainTableView(ArrayList<Train> data) {
        this();
        this.data.setAll(data);
    }

    /**
     * creates the initial Table columns with data from the data holding class
     */
    private void createTable() {
        
        TableColumn<Train, TrainTableData> mainColumn = new TableColumn<>(Internationalization.getString("train"));
        mainColumn.setCellValueFactory(new PropertyValueFactory<>("trainTableData"));
        mainColumn.setCellFactory(new Callback<TableColumn<Train, TrainTableData>, TableCell<Train, TrainTableData>>() {
            @Override
            public TableCell<Train, TrainTableData> call(TableColumn<Train, TrainTableData> param) {
                TableCell<Train, TrainTableData> cell = new TableCell<Train, TrainTableData>() {
                    @Override
                    public void updateItem(TrainTableData item, boolean empty){
                        if (item != null) {                            
                            try {
                                FXMLBundle bundle = FXMLManager.loadFXML("TrainControl_Table.fxml");
                                setGraphic(bundle.getParent());
                            } catch (Exception ex) {
                                CSLogger.logEvent(4, ex, "Error while loading Train-Table-Controller [CSTrainTableView]");
                            }
                        }
                    }
                };
                return cell;
            }
        });

        //<editor-fold defaultstate="collapsed" desc="InfoColumn">
        TableColumn<Train, TrainTableData> infoColumn = new TableColumn<>(
                Internationalization.getString("train"));
        infoColumn.setCellValueFactory(new PropertyValueFactory<>("trainTableData"));
        infoColumn.setPrefWidth(200);
        infoColumn.setCellFactory(new Callback<TableColumn<Train, TrainTableData>, TableCell<Train, TrainTableData>>() {
            @Override
            public TableCell<Train, TrainTableData> call(TableColumn<Train, TrainTableData> param) {
                TableCell<Train, TrainTableData> cell = new TableCell<Train, TrainTableData>() {
                    @Override
                    public void updateItem(TrainTableData item, boolean empty) {
                        if (item != null) {
                            VBox vBox = new VBox();
                            ImageView imageView = new ImageView();
                            try {
                                HelperUtils.loadImageToImageView(imageView, item.getPicture());
                            } catch (IOException ex) {
                                CSLogger.logEvent(5, ex, "Failed to load Train Image to Table.");
                            }
                            Label label = new Label(Internationalization.getString("name")
                                    + ": " + item.getName() + "   " + Internationalization.getString("address")
                                    + ": " + item.getAddress());
                            vBox.getChildren().addAll(imageView, label);
                            setGraphic(vBox);
                        }
                    }
                };
                return cell;
            }
        });
        //</editor-fold>
        
        TableColumn<Train, String> description = new TableColumn<>(Internationalization.getString("description"));
        description.setCellValueFactory(new PropertyValueFactory<>("description"));

        //<editor-fold defaultstate="collapsed" desc="speed/direction">
        TableColumn<Train, TrainTableData> speedAndDirectionColumn
                = new TableColumn<>(Internationalization.getString("speed"));
        speedAndDirectionColumn.setCellValueFactory(new PropertyValueFactory<>("trainTableData"));
        speedAndDirectionColumn.setCellFactory(new Callback<TableColumn<Train, TrainTableData>, TableCell<Train, TrainTableData>>() {
            @Override
            public TableCell<Train, TrainTableData> call(TableColumn<Train, TrainTableData> param) {
                TableCell<Train, TrainTableData> cell = new TableCell<Train, TrainTableData>() {
                    @Override
                    public void updateItem(TrainTableData item, boolean empty) {
                        if (item != null) {
                            HBox hBox = new HBox();
                            hBox.setSpacing(10);
                            Label directionLabel = new Label("\u2191");
                            directionLabel.setStyle("-fx-font-size: 30pt;");
                            directionLabel.setLayoutX(10);
                            directionLabel.setLayoutY(64);
                            RadialBargraph speedGauge = new RadialBargraph();
                            speedGauge.setMinValue(0);
                            speedGauge.setMaxValue(1024);
                            speedGauge.setPrefSize(64, 64);
                            speedGauge.setMaxSize(128, 128);
                            speedGauge.setBarColor(Color.valueOf("#0003c5"));
                            speedGauge.setPlainValue(true);
                            
                            directionLabel.setOnMousePressed((MouseEvent event) -> {
                                //TODO: toggle train direction
                            });

                            //listen for changes in speed of the current train
                            TrainListener.speedProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
                                if (TrainListener.addressProperty().get() == item.getAddress()) {
                                    item.getCurSpeed().set(newValue.intValue());
                                    speedGauge.setValue(newValue.intValue());
                                }
                            });
                            
                            hBox.getChildren().addAll(directionLabel, speedGauge);
                            setGraphic(hBox);
                        }
                    }
                };
                return cell;
            }
        });
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="emergencaStop Led">
        TableColumn<Train, TrainTableData> emergStop = new TableColumn<>(Internationalization.getString("status"));
        emergStop.setCellValueFactory(new PropertyValueFactory<>("trainTableData"));
        emergStop.setCellFactory(new Callback<TableColumn<Train, TrainTableData>, TableCell<Train, TrainTableData>>() {
            @Override
            public TableCell<Train, TrainTableData> call(TableColumn<Train, TrainTableData> param) {
                TableCell<Train, TrainTableData> cell = new TableCell<Train, TrainTableData>() {
                    @Override
                    public void updateItem(TrainTableData item, boolean empty) {
                        if (item != null) {
                            Led led = new Led();
                            led.setOn(true);
                            led.setFrameVisible(true);
                            led.setLedColor(ledGreenPaint);
                            led.setPrefSize(20, 20);
                            led.setMaxSize(40, 40);
                            led.setOnMouseClicked((MouseEvent event)->{
                                //TODO: send emergency stop (hold) to train
                                led.setLedColor(ledRedPaint);
                            });
                            setGraphic(led);
                        }
                    }
                };
                return cell;
            }
        });
        //</editor-fold>    
        
        this.tableView = new TableView<Train>();
        this.tableView.setItems(data);
        description.setPrefWidth(180);
        emergStop.setPrefWidth(45);
        this.tableView.getColumns().addAll(infoColumn, speedAndDirectionColumn, emergStop);        
    }
    
    public void createNewTable(){
        createTable();
    }
    
    public void addTrainToTable(Train train){        
        this.tableView.getItems().add(train);
    }
    
    public void setTrains(ArrayList<Train> data){
        this.data.setAll(data);
    }
    
    public TableView getTableView(){
        return this.tableView;
    }    
}

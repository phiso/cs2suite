/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.tables;

import cs2suite.Models.Objects.Train;
import cs2suite.network.TrainListener;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class TrainTableData {
    private String name;
    private String picture;
    private int address;
    private IntegerProperty curDirection;
    private IntegerProperty curSpeed;
    private BooleanProperty curStatus;
    private Train train;
    
    public TrainTableData(Train train){
        this.train = train;        
        
        name = train.getName();
        train.getNameProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            this.name = newValue;
        });
        
        picture = train.getProfilePicture();
        train.getProfilePictureProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            this.picture = newValue;
        });
        
        address = train.getAddress();
        train.getAddressProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            this.address = newValue.intValue();
        });
        
        this.curDirection = new SimpleIntegerProperty(0);        
        TrainListener.addressProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue.intValue() == this.train.getAddress()){
                this.curDirection.set(TrainListener.directionProperty().get());
            }            
        });
        
        this.curSpeed = new SimpleIntegerProperty(0);
        TrainListener.addressProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue.intValue() == this.train.getAddress()){
                this.curSpeed.set(TrainListener.speedProperty().get());
            }            
        });
        
        this.curStatus = new SimpleBooleanProperty(true);                
    }

    //<editor-fold defaultstate="collapsed" desc="Getter/Setter">
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    
    /**
     * @return the picture
     */
    public String getPicture() {
        return picture;
    }
    
    /**
     * @return the address
     */
    public int getAddress() {
        return address;
    }
    
    /**
     * @return the curDirection
     */
    public IntegerProperty getCurDirection() {
        return curDirection;
    }
    
    /**
     * @return the curSpeed
     */
    public IntegerProperty getCurSpeed() {
        return curSpeed;
    }
    
    /**
     * @return the curStatus
     */
    public BooleanProperty getCurStatus() {
        return curStatus;
    }
    
    /**
     * @return the train
     */
    public Train getTrain() {
        return train;
    }
//</editor-fold>
}

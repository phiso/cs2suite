/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csUIController;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class FXMLBundle {

    private final FXMLController controller;
    private final Parent parent;
    private final Stage stage;
    private final Scene scene;

    /**
     *
     * @param controller
     * @param parentPane
     */
    public FXMLBundle(FXMLController controller, Parent parent) {
        this.controller = controller;
        this.parent = parent;
        this.scene = new Scene(parent);
        this.stage = new Stage();
        this.stage.setScene(scene);
        this.controller.setBundle(this);
    }

    /**
     * @return the controller
     */
    public FXMLController getController() {
        return controller;
    }

    /**
     * @return the parentPane
     */
    public Parent getParent() {
        return parent;
    }

    public Stage getStage() {
        return stage;
    }

    public Scene getScene() {
        return scene;
    }
    
    public void close(){
        stage.close();
    }
}

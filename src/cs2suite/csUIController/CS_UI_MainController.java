/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csUIController;

import cs2suite.Models.Objects.Article;
import cs2suite.Models.Objects.CSSystem;
import cs2suite.Models.Objects.Train;
import cs2suite.csFile.CSFile;
import cs2suite.fxmlControllers.TrainControlController;
import cs2suite.managers.FXMLManager;
import cs2suite.fxmlControllers.cs2SuiteGUIController;
import cs2suite.locale.Internationalization;
import cs2suite.logic.CSLogger;
import cs2suite.logic.Settings;
import cs2suite.managers.ConnectionManager;
import cs2suite.managers.SoundsManager;
import cs2suite.managers.SystemsManager;
import cs2suite.tables.TrainTableView;
import cs2suite.utils.HelperUtils;
import eu.hansolo.enzo.led.Led;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.ArrayList;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.stage.Window;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import org.controlsfx.control.PopOver;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class CS_UI_MainController {

    private static CS_UI_MainController instance;

    private static cs2SuiteGUIController controller;    
    private static TrainTableView trainTableView;
    private static ArrayList<TrainControlController> trainControllers;
    private static ArrayList<FXMLBundle> trainControllerBundles;
    private static Window popoverParent;
    private static Dimension screenDimension;

    //<editor-fold defaultstate="collapsed" desc="Getter">       
    /**
     * @return the screenDimension
     */
    public static Dimension getScreenDimension() {
        return screenDimension;
    }
    //</editor-fold>

    /**
     * @return the popoverParent
     */
    public static Window getPopoverParent() {
        return popoverParent;
    }

    /**
     * @param aPopoverParent the popoverParent to set
     */
    public static void setPopoverParent(Window aPopoverParent) {
        popoverParent = aPopoverParent;
    }

    private CS_UI_MainController(cs2SuiteGUIController controller) throws SQLException, UnknownHostException {
        super();        
        this.popoverParent = null;
        this.controller = controller;       
        SystemsManager.getInstance();
        SystemsManager.getSystemsFromdatabase();
        screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
        try {
            SoundsManager.getInstance();
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
            CSLogger.logEvent(4, ex, "Error initializing SoundManager!");
        }
        
        trainControllers = new ArrayList<>();
        trainControllerBundles = new ArrayList<>();
        
    }

    public static CS_UI_MainController getInstance(cs2SuiteGUIController controller) throws SQLException, UnknownHostException {
        if (CS_UI_MainController.instance == null) {
            CS_UI_MainController.instance = new CS_UI_MainController(controller);
        }
        return CS_UI_MainController.instance;
    }

    public static void initialize() {        
    }
    
    public static void initializeTrainControllerFXML(Pane parent){
        CSLogger.logEvent(111, null, "initialising Train Controllers");
        try {
            //initTrainControlFXML(Settings.getTrainControlCount());
            for (int i = 0; i < Settings.getTrainControlCount(); i++) {
                FXMLBundle trainControlBundle = FXMLManager.loadFXML("TrainControl.fxml");
                trainControllerBundles.add(trainControlBundle);
                trainControllers.add((TrainControlController) trainControlBundle.getController());
                trainControllers.get(trainControllers.size() - 1).setTitle(Internationalization.getString("traincontroller") + " " + i);
                parent.getChildren().add(trainControlBundle.getParent());
            }
        } catch (Exception ex) {
            CSLogger.logEvent(2, ex, "Failed to load traincontrollers-FXML");
        }
    }
    
    public static TrainControlController createFloatingTrainController(int index) throws Exception{
        FXMLBundle bundle = trainControllerBundles.get(index);
        String title = trainControllers.get(index).getTitle();
        showPopOver(title, bundle);
        return (TrainControlController) bundle.getController();
    }

    public static void deselectAllTrainControllers() {
        for (int i = 0; i < controller.getTrainFXMLController().size(); i++) {
            controller.getTrainFXMLController().get(i).setBorderColor(Paint.valueOf("#5a5858"));
            controller.getTrainFXMLController().get(i).setIsInControl(false);
        }
    }

    public static void openNewSystemDialog(Window parent) throws Exception {
        PopOver popOver = new PopOver();
        FXMLBundle bundle = FXMLManager.loadFXML("NewSystem.fxml");
        popOver.setTitle(Internationalization.getString("createNewSystem"));
        popOver.setContentNode(bundle.getParent());
        popOver.show(parent);
    }

    public static void openLoadSystemDialog(Window parent) throws Exception {
        PopOver popOver = new PopOver();
        FXMLBundle bundle = FXMLManager.loadFXML("LoadSystem.fxml");
        popOver.setTitle(Internationalization.getString("LoadSystem"));
        popOver.setContentNode(bundle.getParent());
        popOver.show(parent);
    }   

    public static void createTrainsTableView(TableView tableView, ArrayList<Train> trains) {
        trainTableView = new TrainTableView(trains);
        trainTableView.createNewTable();
        tableView = trainTableView.getTableView();
    }

    public static void setDisabled(Node... nodes) {
        for (Node n : nodes) {
            n.setDisable(true);
        }
    }

    public static void setEnabled(Node... nodes) {
        for (Node n : nodes) {
            n.setDisable(false);
        }
    }

    /**
     *
     * @param led
     * @param status sets the color of given Led Object corresponding to given status id
     */
    public static void setLedStatusColor(Led led, int status) {
        switch (status) {
            case 0:
                Platform.runLater(() -> {
                    led.setLedColor(Paint.valueOf("#ff0000"));
                });
                break;
            case 1:
                Platform.runLater(() -> {
                    led.setLedColor(Paint.valueOf("#00c000"));
                });
                break;
            case 2:
                Platform.runLater(() -> {
                    led.setLedColor(Paint.valueOf("#DB6216"));
                });
                break;
        }
    }

    private static String multWhitesapce(int nr) {
        String result = "";
        for (int i = 0; i < nr; i++) {
            result += " ";
        }
        return result;
    }

    /**
     * @param title Adds the given String to the Title of the Main FXML Window
     */
    public static void setTitleSystem(String title) {
        String curTitle = cs2SuiteGUIController.getStage().getTitle();
        curTitle = curTitle.substring(0, curTitle.indexOf("---") + 4)
                + multWhitesapce(50) + title;
        cs2SuiteGUIController.getStage().setTitle(curTitle);
    }

    public static PopOver showPopOver(String titleString, FXMLBundle fxmlBundle) {
        PopOver popOver = new PopOver();
        popOver.setContentNode(fxmlBundle.getParent());
        popOver.setTitle(titleString);
        popOver.show(getPopoverParent());
        popOver.detach();
        return popOver;
    }

    private static File findCS2File(File file, String name) {
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                String lowercaseName = name.toLowerCase();
                if (lowercaseName.endsWith(".cs2")) {
                    return true;
                } else {
                    return false;
                }
            }
        };
        if (file.isDirectory()) {
            File[] files = file.listFiles(filter);
            for (File f : files){
                if (f.getName().equals(name +".cs2")){
                    return f;
                }
            }
        }
        return null;
    }
    
    public static ArrayList<Train> importTrainsFromFile(File file){
        if (file.isFile() && file.getName().toLowerCase().endsWith(".cs2")){
            CSFile csFile = new CSFile(file.getAbsolutePath());
            return csFile.getAllTrains();
        }
        return new ArrayList<>();
    }
    
    public static ArrayList<Article> importArticlesFromFile(File file) throws SQLException, IOException{
         if (file.isFile() && file.getName().toLowerCase().endsWith(".cs2")){
            CSFile csFile = new CSFile(file.getAbsolutePath());
            return csFile.getAllArticles();
        }
        return new ArrayList<>();
    }

    /**
     * @param path finds the necessery files to create a new System under the given @path. Can be given a directory with
     * the files in i, or a zip Archive containing the Files which can be a complete backup of the Central Station 2.
     */
    public static void importSystemFromFiles(String path) {
        File file = new File(path);
        CSSystem system = new CSSystem(file.getName().substring(0, file.getName().indexOf(".")), "?",
                ConnectionManager.getActiveConnection());
        
        if (file.isDirectory()) {
            File trainsFile = CS_UI_MainController.findCS2File(file, "lokomotive");
            system.setTrains(importTrainsFromFile(file));
            
            File magsFile = CS_UI_MainController.findCS2File(file, "magnetartikel");
            File layoutsFile = CS_UI_MainController.findCS2File(file, "gleisbild");
        } else if (file.isFile()) {
            if (HelperUtils.getFileExtension(file) == "zip") {

            }
        }
    }
}

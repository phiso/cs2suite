/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csUIController;

import javafx.stage.Stage;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public abstract class FXMLController { 
    
    private static Stage thisStage;
    private FXMLBundle bundle;
    
    public void initAfter()throws Exception{
    }  
    
    public void finalInit() throws Exception{
    }                
    
    public void setStage(Stage stage){
        thisStage = stage;
    }
    
    public static Stage getStage(){
        return thisStage;
    }
    
    public void setBundle(FXMLBundle bundle){
        this.bundle = bundle;
    }
    
    public FXMLBundle getBundle(){
        return this.bundle;
    }
}

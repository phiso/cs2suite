/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csUIController;

import cs2suite.Models.Objects.CSConnection;
import cs2suite.fxmlControllers.NewSystemController;
import cs2suite.managers.ConnectionManager;
import java.util.ArrayList;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import org.controlsfx.control.PopOver;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class CS_UI_NewSystemController {
    
    private ArrayList<CSConnection> allConnections;
    private final NewSystemController controller;

    public CS_UI_NewSystemController(NewSystemController controller) {
        super();
        this.controller = controller;
        allConnections = new ArrayList<>();
        allConnections = ConnectionManager.getAllConnections();        
    }

    public void fillWithConnections(ListView listView) {
        allConnections.stream().forEach((conn) -> {
            listView.getItems().add(new Label(conn.getIPAddressAsString()));
        });                
    }    
    
    public void setActiveConnection(int index){
        ConnectionManager.activateConnection(index);
    }
    
    public void closePopOver(PopOver po){
        po.hide();
        po = null;
        controller.getBundle().close();
    }
    
    public CSConnection getConnection(int index){
        return allConnections.get(index);
    }
}

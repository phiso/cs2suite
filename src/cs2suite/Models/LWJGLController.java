/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models;

import cs2suite.logic.CSLogger;
import cs2suite.managers.LWJGLControllerManager;
import java.util.HashMap;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javax.swing.event.EventListenerList;
import org.lwjgl.input.Controller;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class LWJGLController {       
    protected EventListenerList listenerList = new EventListenerList();
    
    private Controller controller;
    private LWJGLControllerThread thread;
    private String controllerName;
    private int controllerID;
    private boolean loaded;        
    
    private HashMap<String, DoubleProperty> axisMap;
    private HashMap<String, BooleanProperty> buttonsMap;
    private IntegerProperty povX;
    private IntegerProperty povY;    
    
    //<editor-fold defaultstate="collapsed" desc="Constructors">
    /**
     * Creates new empty CSLWJGLController Object
     */
    public LWJGLController(){
        super();
        controller = null;
        thread = null;
        controllerName = "";
        controllerID = -1;
        loaded = false;
        axisMap = new HashMap<>();
        buttonsMap = new HashMap<>();
        povX = new SimpleIntegerProperty(-1);
        povY = new SimpleIntegerProperty(-1);        
    }
    
    /**
     * @param controller
     *
     * Creates new CSLWJGLController Object based on given Controller Object
     */
    public LWJGLController(Controller controller){
        this();
        this.controller = controller;
        controllerID = controller.getIndex();
        controllerName = controller.getName();
        thread = new LWJGLControllerThread(this);
        loaded = true;
    }
    
    /**
     * Creates new, mostly empty, CSLWJGLController Object based on given name,
     * the Controller can later be loaded using his Name.
     *
     * @param name
     */
    public LWJGLController(String name){
        this();
        controllerName = name;
    }
    
    /**
     * Creates new, mostly empty, CSLWJGLController object based on given ID,
     * the corresponding Controller can later be loaded using his ID.
     *
     * @param id
     */
    public LWJGLController(int id){
        this();
        controllerID = id;
    }
    
    /**
     * Connects to the actual Controller Device via the LWJGL-Driver using given name and/or ID
     */
    public void loadController(){
        if (controllerName != "" && controllerName != null){
            controller = LWJGLControllerManager.getControllerByName(controllerName);
        }else if (controllerID != -1){
            controller = LWJGLControllerManager.getControllerById(controllerID);            
        }else{
            controller = LWJGLControllerManager.getFirst();
            CSLogger.logEvent(7, null, "Nothing to identify Controller, loaded First found");
        }        
        load();
    }
    
    /**
     * Loads the necessery Data from the Controller into this classes Fields
     */
    private void load(){
        for (int i = 0; i < controller.getButtonCount(); i++){
            buttonsMap.put(controller.getButtonName(i), new SimpleBooleanProperty(false));
        }
        for (int i = 0; i < controller.getAxisCount(); i++){
            axisMap.put(controller.getAxisName(i), new SimpleDoubleProperty(0.0));
        }
        if (thread == null){
            thread = new LWJGLControllerThread(this);
            new Thread(thread, "lwjgl_ControllerThread_"+controller.getName()).start();
        }
        loaded = true;
    }
    //</editor-fold>       
    
    //<editor-fold defaultstate="collapsed" desc="Getter/Setter">
    public Controller getController(){
        return controller;
    }               
    
    public IntegerProperty povXProperty(){
        return povX;
    }
    
    public IntegerProperty povYProperty(){
        return povY;
    }
    
    public DoubleProperty axisProperty(String axisName){
        return axisMap.get(axisName);
    }
    
    public BooleanProperty buttonProperty(String buttonName){
        return buttonsMap.get(buttonName);
    }
    //</editor-fold>
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models;

import cs2suite.logic.CSLogger;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.input.Controller;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class LWJGLControllerThread implements Runnable {

    private Controller controller;
    private LWJGLController lwjglController;

    private int frequency = 10;    

    public LWJGLControllerThread(LWJGLController controller) {
        this.lwjglController = controller;
        this.controller = controller.getController();        
    }

    @Override
    public void run() {
        CSLogger.logEvent(111, null, "Polling Thread for Controller: " + controller.getName());
        while (true) {
            controller.poll();           
            lwjglController.povXProperty().set((int) controller.getPovX());
            lwjglController.povYProperty().set((int) controller.getPovY());
                        
            for (int i = 0; i < controller.getButtonCount(); i++){
                lwjglController.buttonProperty(controller.getButtonName(i)).set(controller.isButtonPressed(i));
            }            
            
            for (int i = 0; i < controller.getAxisCount(); i++){
                lwjglController.axisProperty(controller.getAxisName(i)).set(controller.getAxisValue(i));
            }
            
            try {
                Thread.sleep(frequency);
            } catch (InterruptedException ex) {
                Logger.getLogger(LWJGLControllerThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * @param frequency the frequency to set
     */
    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }
}

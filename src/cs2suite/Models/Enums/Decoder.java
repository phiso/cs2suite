/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models.Enums;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public enum Decoder {

    MM2(0, "MM2"),
    DCC(1, "DCC"),
    MFX(2, "MFX");

    private final int id;
    private final String nameString;

    Decoder(int id, String nameString) {
        this.id = id;
        this.nameString = nameString;
    }

    public int id() {
        return id;
    }
    
    public String nameString(){
        return nameString;
    } 
    
    public static Decoder fromId(int id){
        switch(id){
            case 0: return Decoder.MM2;
            case 1: return Decoder.DCC;
            case 2: return Decoder.MFX;
        }
        return Decoder.MM2;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models.Enums;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public enum Epoch {
    I("I",1),
    II("II",2),
    III("III",3),
    IV("IV",4),
    V("V",5),
    VI("VI",6),
    VII("VII",7);
    
    private final String nameString;
    private final int numericValue;
    
    Epoch(String nameString, int numericValue){
        this.nameString = nameString;
        this.numericValue = numericValue;
    }
    
    public String nameString(){
        return nameString;
    }
    
    public int numericValue(){
        return numericValue;
    }
    
    public static Epoch fromId(int id){
        switch (id){
            case 0:
            case 1: return Epoch.I;                
            case 2: return Epoch.II;
            case 3: return Epoch.III;
            case 4: return Epoch.IV;
            case 5: return Epoch.V;
            case 6: return Epoch.VI;
            case 7: return Epoch.VII;
        }
        return Epoch.I;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models.Enums;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public enum TrainType {
    LOCOMOTIVE("locomotive", 0),
    STEAM_LOCOMOTIVE("steam_locomotive", 1),
    ELECTRIC_LOCOMOTIVE("electric_locomotive", 2),
    DIESEL_LOCOMOTIVE("diesel_locomotive", 3),
    OTHER("other", 4);
    
    private final String nameString;
    private final int id;
    
    TrainType(String name, int id){
        this.nameString = name;
        this.id = id;
    }
        
    public String value(){
        return nameString;
    }
    
    public int Id(){
        return id;
    }
    
    public static TrainType fromId(Integer id){
        switch(id){
            case 0: return TrainType.LOCOMOTIVE;
            case 1: return TrainType.STEAM_LOCOMOTIVE;                            
            case 2: return TrainType.ELECTRIC_LOCOMOTIVE;
            case 3: return TrainType.DIESEL_LOCOMOTIVE;
            case 4: return TrainType.OTHER;
        }         
        return TrainType.OTHER;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models.Objects;

import cs2suite.logic.CSLogger;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class LayoutPosition {

    private int xPos;
    private int yPos;
    private int rotation; // icon rotation in degrees (0 90 180 270)
    private final LayoutObject layoutObject;
    private Article linkedArticle;
    private Layout linkedLayout;

    public LayoutPosition(int x, int y, LayoutObject layoutObject) {
        xPos = x;
        yPos = y;
        this.layoutObject = layoutObject;
        linkedLayout = null;
        linkedArticle = null;
        rotation = 0;
    }

    //<editor-fold defaultstate="collapsed" desc="Getter/Setter">
    /**
     * @return the xPos
     */
    public int getxPos() {
        return xPos;
    }

    /**
     * @param xPos the xPos to set
     */
    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    /**
     * @return the yPos
     */
    public int getyPos() {
        return yPos;
    }

    /**
     * @param yPos the yPos to set
     */
    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    /**
     * @return the rotation
     */
    public int getRotation() {
        return rotation;
    }

    /**
     * @param rotation the rotation to set
     */
    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

    /**
     * @return the layoutObject
     */
    public LayoutObject getLayoutObject() {
        return layoutObject;
    }

    public Article getLinkedArticle() {
        return linkedArticle;
    }

    /**
     * @return the article
     */
    public boolean hasArticle() {
        return linkedArticle != null;
    }

    /**
     * @param article the article to set
     */
    public void setArticle(Article article) {
        if (this.layoutObject.isReferrable()) {
            if (this.linkedLayout == null) {
                this.linkedArticle = article;
            } else {
                CSLogger.logEvent(5, null, "Cannot set linkedLayout AND "
                        + "linkedArticle on one LayoutPosition");
            }
        } else {
            CSLogger.logEvent(5, null, "The LayoutObject on this Position is not"
                    + " refferable to any Articles");
        }
    }

    public void setLayout(Layout layout) {
        if (this.layoutObject.isLink()) {
            if (this.linkedArticle == null) {
                this.linkedLayout = layout;
            } else {
                CSLogger.logEvent(5, null, "Cannot set linkedArticle AND "
                        + "linkedLayout on one LayoutPosition");
            }
        }else{
            CSLogger.logEvent(5, null, "The LayoutObject on this Postion is not"
                    + " linkable to any other Layout.");
        }
    }

    public boolean hasLayout() {
        return linkedLayout != null;
    }

    public Layout getLayout() {
        return linkedLayout;
    }
    //</editor-fold>
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models.Objects;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 * Instances of this Class are created from database Data.
 * Its used as a Enumeration of existing Article types.
 */
public final class ArticleType extends CSObject{
       
    private final String icon;
    private final int states;    
    
    public ArticleType(String name, String icon, int states){
        super(name, "?");    
        this.icon = icon;
        this.states = states;        
    }

    /**
     * @return the icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * @return the states
     */
    public int getStates() {
        return states;
    }   
}

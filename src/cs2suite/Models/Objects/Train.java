/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models.Objects;

import cs2suite.Models.Enums.Epoch;
import cs2suite.Models.Enums.Decoder;
import cs2suite.Models.Enums.TrainType;
import cs2suite.Models.SqliteDatabase;
import cs2suite.locale.Internationalization;
import cs2suite.managers.SqliteInterface;
import cs2suite.tables.TrainTableData;
import java.sql.SQLException;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public final class Train extends ControllableObject implements SqliteInterface{

    private StringProperty profilePicture;
    private StringProperty artNr;
    private Decoder decoder;
    private TrainType type;
    private Epoch epoch;
    private IntegerProperty vMax;
    private IntegerProperty vMin;
    private IntegerProperty acceleration; // Anfahrverzögerung
    private IntegerProperty speedometer;
    private IntegerProperty volume;
    private IntegerProperty brakeForce; // Bremsverzögerung
    private ObjectProperty trainTableData;

    public Train(int adr, String name, String description, String artNr, TrainType trainType, Decoder decoder, Epoch epoch, int vMax, int vMin,
            int acceleration, int speedometer, int volume, int brakeforce, String profilePicture){
        super(name, adr);
        setDescription(description);
        this.artNr = new SimpleStringProperty(artNr);
        this.vMax = new SimpleIntegerProperty(vMax);
        this.vMin = new SimpleIntegerProperty(vMin);
        this.acceleration = new SimpleIntegerProperty(acceleration);
        this.brakeForce = new SimpleIntegerProperty(brakeforce);
        this.volume = new SimpleIntegerProperty(volume);
        this.profilePicture = new SimpleStringProperty(profilePicture);
        this.decoder = decoder;
        this.type = trainType;
        this.epoch = epoch;
        trainTableData = new SimpleObjectProperty();        
        setTrainTableData(new TrainTableData(this));
    }
    
    public Train(int adr, String name, TrainType type) {
        this(adr, name, "?", "000000", type, Decoder.MM2, Epoch.I, 255, 0, 0, 180, 0, 0, 
                System.getProperty("user.dir") + "\\data\\img\\trains\\keinbild.jpg");                
    }

    //<editor-fold defaultstate="collapsed" desc="Getter/Setter">
    /**
     *
     * @return the Decoder
     */
    public Decoder getDecoder() {
        return decoder;
    }

    /**
     * @return the type
     */
    public TrainType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(TrainType type) {
        this.type = type;
    }

    /**
     * @return the vMax
     */
    public int getvMax() {
        return vMax.get();
    }

    /**
     * @param vMax the vMax to set
     */
    public void setvMax(int vMax) {
        this.vMax.set(vMax);
    }

    /**
     * @return the vMin
     */
    public int getvMin() {
        return vMin.get();
    }

    /**
     * @param vMin the vMin to set
     */
    public void setvMin(int vMin) {
        this.vMin.set(vMin);
    }

    /**
     * @return the acceleration
     */
    public int getAcceleration() {
        return acceleration.get();
    }

    /**
     * @param acceleration the acceleration to set
     */
    public void setAcceleration(int acceleration) {
        this.acceleration.set(acceleration);
    }

    /**
     * @return the speedometer
     */
    public int getSpeedometer() {
        return speedometer.get();
    }

    /**
     * @param speedometer the speedometer to set
     */
    public void setSpeedometer(int speedometer) {
        this.speedometer.set(speedometer);
    }

    /**
     * @return the volume
     */
    public int getVolume() {
        return volume.get();
    }

    /**
     * @param volume the volume to set
     */
    public void setVolume(int volume) {
        this.volume.set(volume);
    }

    /**
     * @return the brakeForce
     */
    public int getBrakeForce() {
        return brakeForce.get();
    }        

    /**
     * @param brakeForce the brakeForce to set
     */
    public void setBrakeForce(int brakeForce) {
        this.brakeForce.set(brakeForce);
    }

    /**
     *
     * @return the Path to the Profile Picture
     */
    public String getProfilePicture() {
        return profilePicture.get();
    }
    
    public String getArticleNumber(){
        return artNr.get();
    }

    /**
     *
     * @return Profile Picture Property
     */
    public StringProperty getProfilePictureProperty() {
        return profilePicture;
    }

    /**
     * @param profilePicture the profilePicture to set
     */
    public void setProfilePicture(String profilePicture) {
        this.profilePicture.set(profilePicture);
    }

    /**
     * @param decoder the decoder to set
     */
    public void setDecoder(Decoder decoder) {
        this.decoder = decoder;
    }

    /**
     * @return the epoch
     */
    public String getEpoch() {
        return epoch.nameString();
    }

    /**
     * @param epoch the epoch to set
     */
    public void setEpoch(Epoch epoch) {
        this.epoch = epoch;        
    }    

    public IntegerProperty getVmaxProperty() {
        return this.vMax;
    }

    public IntegerProperty getVminProperty() {
        return this.vMin;
    }

    public IntegerProperty getAccelerationProperty() {
        return this.acceleration;
    }

    public IntegerProperty getSpeedometerProperty() {
        return this.speedometer;
    }

    public IntegerProperty getVolumeProperty() {
        return this.volume;
    }

    public IntegerProperty getBrakeForceProperty() {
        return this.brakeForce;
    }

    public void setTrainTableData(TrainTableData cell) {
        trainTableData.set(cell);
    }

    public Object getTrainTableData() {
        return trainTableData.get();
    }

    public ObjectProperty trainTableDataProperty() {
        return trainTableData;
    }
    //</editor-fold>  

    /**
     * Saves the Object to the given SQLiteDatabase. It is Inserted if not already existing, otherwise
     * it is updated and overriden with the new Values.
     * @param db
     * @throws SQLException 
     */
    @Override
    public void saveToDatabase(SqliteDatabase db) throws SQLException {
        String insertOrIgnore = "INSERT OR IGNORE INTO Trains (Address, Name, ArtNr, Epoch, Type, Description, Picture,"
                + " Decoder, Vmin, Vmax, BrakeForce, Acceleration, Speedometer, Volume)";
        String update = "UPDATE Trains "
                + "SET Address="+this.getAddress()+","
                + "Name=\""+this.getName()+"\","
                + "ArtNr=\""+this.getArticleNumber()+"\","
                + "Epoch="+this.epoch.numericValue()+","
                + "Type="+this.type.Id()+","
                + "Description=\""+this.getDescription()+"\","
                + "Picture=\""+this.getProfilePicture()+"\","
                + "Decoder="+this.decoder.id()+","
                + "Vmin="+this.getvMin()+","
                + "Vmax="+this.getvMax()+","
                + "BrakeForce="+this.getBrakeForce()+","
                + "Acceleration="+this.getAcceleration()+","
                + "Speedometer="+this.getSpeedometer()+","
                + "Volume="+this.getVolume()+" WHERE ";
    }
    
    @Override
    public String toString(){
        return this.getName()+"("+this.getEpoch()+")"
                + ", "+Internationalization.getString("addressShort")+" "+this.getAddress();
    }
}

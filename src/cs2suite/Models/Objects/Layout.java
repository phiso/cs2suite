/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models.Objects;

import cs2suite.logic.CSLogger;
import java.util.ArrayList;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class Layout extends CSObject{
    
    private ArrayList<LayoutPosition> positions;

    public Layout(String name, String descr, int xSize, int ySize) {
        super(name, descr);        
        positions = new ArrayList<>();
        for (int x = 0; x < xSize; x++){
            for (int y = 0; y < ySize; y++){
                positions.add(new LayoutPosition(x, y, null));
            }
        }
    }
    
    public void addPosition(LayoutPosition position){
        this.positions.add(position);
    }
    
    public LayoutPosition getPosition(int x, int y){
        for (LayoutPosition p : positions){
            if (p.getxPos() == x && p.getyPos() == y){
                return p;
            }
        }
        CSLogger.logEvent(6, new Exception("Not in List"), 
                "Requested Postion is not in List of available Positions"
                + " [CSLAyout:37]");
        return null;
    }
    
    public ArrayList<LayoutPosition> getPositions(){
        return positions;
    }   
}

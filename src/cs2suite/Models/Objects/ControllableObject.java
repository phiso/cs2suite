/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models.Objects;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public abstract class ControllableObject extends CSObject{
    
    private IntegerProperty address;
    
    public ControllableObject(String name, String descr, int adr) {
        super(name, descr);
        this.address = new SimpleIntegerProperty(adr);
    }
    
    public ControllableObject(String name, int adr){
        this(name,"None",adr);        
    }
    
    /**
     *
     * @return
     */
    public IntegerProperty getAddressProperty(){
        return address;
    }
    
    /**
     *
     * @return
     */
    public int getAddress(){
        return address.get();
    }    
    
    /**
     *
     * @param adr
     */
    public void setAddress(int adr){
        this.address.set(adr);
    }
}

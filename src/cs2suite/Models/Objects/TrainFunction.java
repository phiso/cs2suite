/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models.Objects;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class TrainFunction extends CSObject{
    
    private IntegerProperty mode;
    private final int index;
    private StringProperty icon;
    
    /**
     *
     * @param name
     * @param descr
     * @param mode
     * @param index
     * @param icon
     */
    public TrainFunction(String name, String descr, int mode, 
            int index, String icon) {
        super(name, descr);
        this.mode = new SimpleIntegerProperty(mode);
        this.index = index;
        this.icon = new SimpleStringProperty(icon);        
    }
    
    /**
     *
     * @param name
     * @param icon
     * @param mode
     * @param index
     */
    public TrainFunction(String name, String icon, int mode, int index){
        this(name, "None", mode, index, icon);        
    } 
    
    /**
     *
     * @return
     */
    public int getIndex(){
        return index;
    }
    
    /**
     *
     * @param mode
     */
    public void setMode(int mode){
        this.mode.set(mode);
    }
    
    /**
     *
     * @return
     */
    public IntegerProperty getModeProperty(){
        return mode;
    }
    
    /**
     *
     * @return
     */
    public int getMode(){
        return mode.get();
    }
   
    /**
     *
     * @param icon
     */
    public void setIcon(String icon){
        this.icon.set(icon);
    }
    
    /**
     *
     * @return
     */
    public StringProperty getIconProperty(){
        return icon;
    }
    
    /**
     *
     * @return
     */
    public String getIcon(){
        return icon.get();
    }
}

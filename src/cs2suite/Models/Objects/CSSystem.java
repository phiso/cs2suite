/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models.Objects;

import java.util.ArrayList;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class CSSystem extends CSObject {

    private ArrayList<Train> trains;
    private ArrayList<Article> articles;
    private ArrayList<Layout> layouts;
    private ArrayList<String> epochs;
    private ArrayList<String> trainTypes;
    private CSConnection connection;

    public CSSystem(String name, String descr, CSConnection connection) {
        super(name, descr);
        this.connection = connection;
    }

    /**
     *
     * @param name
     */
    public CSSystem(String name) {
        this(name, "?", null);
    }

    //<editor-fold defaultstate="collapsed" desc="getter/setter">
    public Train getTrainAtIndex(int index) {
        return trains.get(index);
    }

    public Article getArticleAtIndex(int index) {
        return articles.get(index);
    }

    public Layout getLayoutAtIndex(int index) {
        return layouts.get(index);
    }

    public void setTrains(ArrayList<Train> setTrains) {
        setTrains.stream().forEach((Train t) -> {
            this.trains.add(t);
        });
    }

    public void setArticles(ArrayList<Article> setArticles) {
        setArticles.stream().forEach((Article a) -> {
            this.articles.add(a);
        });
    }

    public void setLayouts(ArrayList<Layout> setLayouts) {
        setLayouts.stream().forEach((Layout l) -> {
            this.layouts.add(l);
        });
    }

    public ArrayList<Train> getTrains() {
        return trains;
    }

    public ArrayList<Article> getArticles() {
        return articles;
    }

    public ArrayList<Layout> getLayouts() {
        return layouts;
    }

    public void addTrain(Train train) {
        this.trains.add(train);
    }

    public void addArticle(Article article) {
        this.articles.add(article);
    }

    public void addLayout(Layout layout) {
        layouts.add(layout);
    }

    public void setConnection(CSConnection con) {
        this.connection = con;
    }

    public CSConnection getConnection() {
        return connection;
    }
    //</editor-fold>
}

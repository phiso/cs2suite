/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models.Objects;

import cs2suite.Models.Enums.TrainType;
import cs2suite.Models.SqliteDatabase;
import cs2suite.managers.SqliteInterface;
import java.sql.SQLException;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class PortalCrane extends ControllableObject implements SqliteInterface{

    private final int secondAddress;
    private final Train driveTrain;
    private final Train functionsTrain;
    
    public PortalCrane(String name, int driveAddr, int funcAddr) {
        super(name, driveAddr);
        this.secondAddress = funcAddr;        
        driveTrain = new Train(driveAddr, name+"_drive", TrainType.OTHER);
        functionsTrain = new Train(funcAddr, name+"_functions", TrainType.OTHER);
    }

    @Override
    public void saveToDatabase(SqliteDatabase db) throws SQLException {
        //TODO
        //save both addresses as trains (which they actually are)
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public int getSecondAddress(){
        return secondAddress;
    }
    
    public Train getDriveTrain(){
        return driveTrain;
    }
    
    public Train getFunctionsTrain(){
        return functionsTrain;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models.Objects;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class Article extends ControllableObject{
    
    private ObjectProperty articleType;   
    private IntegerProperty timing;
    
    /**
     *
     * @param name
     * @param adr
     * @param type
     */
    public Article(String name, int adr, ArticleType type) {
        super(name, adr);        
        this.articleType = new SimpleObjectProperty(articleType);
        this.timing = new SimpleIntegerProperty(200);        
    }
    
    /**
     *
     * @return The ArticleType as ObjectProperty
     */
    public ObjectProperty getArticleTypeProperty(){
        return articleType;
    }
    
    /**
     *
     * @return The ArticleType as Object
     */
    public ArticleType getArticleType(){
        return (ArticleType) articleType.get();
    }
    
    /**
     *
     * @param type
     * set a new articleType
     */
    public void setArticleType(ArticleType type){
        this.articleType.set(type);
    }
    
    public int getTiming(){
        return timing.get();
    }
    
    public IntegerProperty getTimingProperty(){
        return timing;
    }
    
    public void setTiming(int timing){
        this.timing.set(timing);
    }
}

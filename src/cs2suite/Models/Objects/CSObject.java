/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models.Objects;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public abstract class CSObject {    
    private StringProperty name;
    private StringProperty description;
    
    public CSObject(String name, String descr){
        super();        
        this.name = new SimpleStringProperty(name);
        this.description = new SimpleStringProperty(descr);       
    }
    
    public CSObject(String name){
        this(name,"None");
    }   

    /**
     * @return the name Property
     */
    public StringProperty getNameProperty() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name.set(name);
    }
    
    /**
     * 
     * @return the name
     */
    public String getName(){
        return name.get();
    }

    /**
     * @return the description Property
     */
    public StringProperty getDescriptionProperty() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description.set(description);
    }
    
    /**
     * 
     * @return the Description
     */
    public String getDescription(){
        return description.get();
    }

   
}

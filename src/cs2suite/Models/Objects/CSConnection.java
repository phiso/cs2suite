/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models.Objects;

import java.net.InetAddress;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class CSConnection {
    private final InetAddress cs2Address;
    private boolean active;
    
    public CSConnection(InetAddress adr){
        super();
        this.active = false;        
        this.cs2Address = adr;
    }
    
    public String getIPAddressAsString(){
        return cs2Address.getHostAddress();
    }
    
    public InetAddress getIPAddress(){
        return cs2Address;
    }
    
    public boolean isActive(){
        return this.active;
    }
    
    public void deactivate(){
        this.active = false;
    }
    
    public void activate(){
        this.active = true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models.Objects;

import cs2suite.logic.CSLogger;
import cs2suite.tables.LayoutObjectsTableData;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class LayoutObject extends CSObject {

    private final StringProperty iconPath;
    private int maxStates;
    private boolean referrable;
    private boolean link;
    private ObjectProperty tableCell;

    public LayoutObject(String name, String iconPath, int states) {
        super(name, "?");
        this.iconPath = new SimpleStringProperty(iconPath);
        referrable = false;
        link = false;
        maxStates = 1;
        tableCell = new SimpleObjectProperty(new LayoutObjectsTableData(this));
    }

    public LayoutObject(String name, String iconPath, int states,
            boolean ref, boolean link) {
        this(name, iconPath, states);
        referrable = ref;
        this.link = link;
    }

    //<editor-fold defaultstate="collapsed" desc="Getter/Setter">
    public void setReferrable(boolean b) {
        if (b && isLink()) {
            CSLogger.logEvent(5, null, "LayoutObject cannot be Link and "
                    + "refferable to Articles");
        } else {
            referrable = b;
        }
    }
    
    public void setLink(boolean b) {
        if (b && isReferrable()) {
            CSLogger.logEvent(5, null, "LayoutObject cannot be Link and "
                    + "refferable to Articles");
        } else {
            link = b;
        }
    }
    
    public String getIconPath() {
        return iconPath.get();
    }
    
    public StringProperty getIconPathProperty() {
        return iconPath;
    }
    
    /**
     * @return the referrable
     */
    public boolean isReferrable() {
        return referrable;
    }
    
    /**
     * @return the link
     */
    public boolean isLink() {
        return link;
    }
    
    /**
     * @return the maxStates
     */
    public int getMaxStates() {
        return maxStates;
    }
    
    /**
     * @param maxStates the maxStates to set
     */
    public void setMaxStates(int maxStates) {
        this.maxStates = maxStates;
    }
    
    public void setTableCell(LayoutObjectsTableData cell){
        this.tableCell.set(cell);
    }
    
    public ObjectProperty getTableCellProperty(){
        return tableCell;
    }
    
    public LayoutObjectsTableData getTableCell(){
        return (LayoutObjectsTableData) tableCell.get();
    }
    //</editor-fold>
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.Models;

import cs2suite.logic.CSLogger;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class SqliteDatabase {
    
    public static final String DEFAULT_DB_LOCATION=System.getProperty("user.dir")+"\\data\\cs2suite.db";                
    
    private final String dbPath;
    private final File dbFile;
    private Connection dbConnection;        
    
    public SqliteDatabase(String path, boolean autoCommit) throws ClassNotFoundException, SQLException{
        dbPath = path;
        dbFile = new File(dbPath);
        Class.forName("org.sqlite.JDBC");
        if (dbFile.exists() && !dbFile.isDirectory()){
            dbConnection = DriverManager.getConnection("jdbc:sqlite:"+dbPath);
            dbConnection.setAutoCommit(autoCommit);
            CSLogger.logEvent(111, null, "Database: \""+dbPath+"\" is connected.");
        }else{
            CSLogger.logEvent(3, new IOException("DB File invalid"), "Error loading Database-file");
        }        
    }
    
    public SqliteDatabase(boolean autocommit) throws ClassNotFoundException, SQLException{
        this(SqliteDatabase.DEFAULT_DB_LOCATION, autocommit);
    }
    
    public ResultSet simpleSQLQuery(String query) throws SQLException{                
        //query = query.toLowerCase();
        Statement stmt = dbConnection.createStatement();                
        ResultSet result = stmt.executeQuery(query);
        stmt.close();
        CSLogger.logEvent(7, null, "Executed SQL-Query: "+query);
        return result;
    }
    
    public void simpleSQLUpdate(String query) throws SQLException{
        Statement stmt = dbConnection.createStatement();
        int result = stmt.executeUpdate(query);
        stmt.close();
        CSLogger.logEvent(7, null, "Executed SQL-Update-Query: "+query+" -> returned: "+result);
    }
    
    public String getDBPath(){
        return dbPath;
    }
    
    public File getDBFile(){
        return dbFile;
    }
    
    public Connection getDBConnection(){
        return dbConnection;
    }
}

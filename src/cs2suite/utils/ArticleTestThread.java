/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.utils;

import cs2suite.network.Package;
import cs2suite.network.UDPSender;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Philipp
 */
public class ArticleTestThread extends Thread {

    private Integer address;
    private String decoder;
    private Integer delay;
    private Integer state;
    private boolean paused;

    public ArticleTestThread() {
        super();
        delay = 1000;
        decoder = "mm2";
        state = 0;
        paused = true;
    }

    public void setData(Integer adr, Integer delay, String decoder) {
        this.decoder = decoder;
        this.address = adr;
        this.delay = delay;
    }

    @Override
    public void run() {
        while (true) {
            if (state == 0) {
                try {                    
                    UDPSender.forceSend(Package.getArticlePackage(address, state, decoder));
                } catch (IOException ex) {
                    Logger.getLogger(ArticleTestThread.class.getName()).log(Level.SEVERE, null, ex);
                }
                state++;
            } else {
                try {
                    UDPSender.forceSend(Package.getArticlePackage(address, state, decoder));
                } catch (IOException ex) {
                    Logger.getLogger(ArticleTestThread.class.getName()).log(Level.SEVERE, null, ex);
                }
                state--;
            }
            synchronized (this) {
                try {
                    wait(delay);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ArticleTestThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    public void pause() {
        paused = true;
    }

    public void proceed() {
        paused = false;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.utils;

import cs2suite.logic.Settings;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Window;
import javax.imageio.ImageIO;
import org.controlsfx.control.PopOver;

/**
 *
 * @author Philipp
 */
public class HelperUtils {

    public static String getFileExtension(File file) {
        String fileName = file.getName();
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    public static double round(double zahl, int stellen) {
        return (double) ((int) zahl + (Math.round(Math.pow(10, stellen) * (zahl - (int) zahl))) / (Math.pow(10, stellen)));
    }

    public Button createImageButton(String imgPath) {
        Button temp = new Button();
        temp.setStyle("-fx-graphic: url(" + imgPath + ");");
        return temp;
    }

    public static void saveStringToFile(File file, String str) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(file);
        pw.println(str);
        pw.close();
    }

    public static String langCodeTranslation(String langCode) {
        switch (langCode) {
            case "DE":
                return "Deutsch";
            case "EN":
                return "English";
            case "FR":
                return "Francais";
            case "IT":
                return "italiano";//TODO
            default:
                return null;
        }
    }

    public static Double calcAngleInCircle(Point mp, Point p) {
        Double a;
        Double r = Math.sqrt(Math.pow(mp.x - p.x, 2) + Math.pow(mp.y - p.y, 2));
        Double ax = Math.acos((p.x - mp.x) / r) * (180 / Math.PI);
        Double ay = Math.asin((p.y - mp.y) / r) * (180 / Math.PI);
        if (ay >= 0) {
            a = ax;
        } else if ((ax >= 0 && ax <= 90) && (ay >= -90 && ay <= 0)) {
            a = 270 + (90 + ay);
        } else {
            a = 180 + Math.abs(ay);
        }
        return a;
    }

    public static String getHelpTextFile(String fileName) throws FileNotFoundException, IOException {
        File file = new File(System.getProperty("user.dir") + "\\data\\help\\" + fileName + "_" + Settings.getLanguage().toLowerCase() + ".txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line = null;
        String result = null;
        int count = 1;
        while ((line = br.readLine()) != null) {
            result += line + "\n";
        }
        return result;
    }

    public static void showHelpText(File helpFile, Window window) throws FileNotFoundException, IOException {
        if (!helpFile.isFile()) {
            throw new FileNotFoundException("Help File seems not to exit");
        }

        PopOver helpPopOver = new PopOver();
        Text text = new Text();

        FileReader fr = new FileReader(helpFile);
        BufferedReader br = new BufferedReader(fr);
        String line = null;
        String result = null;
        int count = 1;
        while ((line = br.readLine()) != null) {
            result += line + "\n";
        }
        text.setText(result);
        helpPopOver.setContentNode(text);
        helpPopOver.setDetached(true);
        helpPopOver.setTitle(helpFile.getName());
        helpPopOver.show(window, MouseInfo.getPointerInfo().getLocation().x, MouseInfo.getPointerInfo().getLocation().y);
    }

    public static void showWikiEntry(String entry, Window window) {
        String wiki = "https://bitbucket.org/phiso/cs2suite/wiki/"; // wiki address
        String wikiPage = wiki + entry;
        PopOver webPopOver = new PopOver();
        webPopOver.setTitle("Wiki");
        WebView webView = new WebView();
        webView.setContextMenuEnabled(false);
        webView.getEngine().load(wikiPage);
        webView.setMaxSize(1280, 1024);
        webView.autosize();
        webPopOver.setContentNode(webView);
        webPopOver.setDetached(true);
        webPopOver.setMaxWidth(Screen.getPrimary().getBounds().getMaxX());
        webPopOver.show(window, MouseInfo.getPointerInfo().getLocation().x, MouseInfo.getPointerInfo().getLocation().y);
    }

    public static void showHelpNode(Node node, String title, Window window) {
        PopOver popOver = new PopOver();
        popOver.setContentNode(node);
        popOver.setDetached(true);
        popOver.setTitle(title);
    }

    /*
     create off_state images with grey coloring instead off yellow or else
     */
    public void convertImage(File img) throws IOException, InterruptedException {
        File f = new File(System.getProperty("user.dir") + "\\img\\function_icons");
        File[] files = f.listFiles();

        for (int i = 0; i < files.length; i++) {
            if (files[i].isFile()) {
                BufferedImage bufimg = ImageIO.read(files[i]);
                WritableImage wimg = SwingFXUtils.toFXImage(bufimg, null);
                PixelReader pr = wimg.getPixelReader();
                PixelWriter pw = wimg.getPixelWriter();
                for (int y = 0; y < (int) wimg.getHeight(); y++) {
                    for (int x = 0; x < (int) wimg.getWidth(); x++) {
                        if (pr.getArgb(x, y) != 0) {
                            pw.setColor(x, y, Color.GREY);
                        }
                    }
                }
                RenderedImage rimg = SwingFXUtils.fromFXImage(wimg, null);
                String temp = files[i].getName();
                temp = temp.substring(0, temp.indexOf("."));
                File sf = new File(System.getProperty("user.dir") + "\\img\\function_icons\\" + temp + "_off.png");
                ImageIO.write(rimg, "png", sf);
            }
        }
    }

    public static Optional<String> showTextInputDialog(String title, String header, String msg, String def) {
        TextInputDialog dialog = new TextInputDialog(def);
        dialog.setTitle(title);
        dialog.setHeaderText(header);
        dialog.setContentText(msg);
        return dialog.showAndWait();
    }

    public static Optional<String> showTextInputDialog(String title, String msg) {
        return HelperUtils.showTextInputDialog(title, "", msg, "");
    }

    public static Optional<ButtonType> showAlertDialog(AlertType type, String title, String msg) {
        Alert dialog = new Alert(type);
        dialog.setTitle(title);
        dialog.setContentText(msg);
        return dialog.showAndWait();
    }
    
    public static void loadImageToImageView(ImageView view, String pathToImage) throws IOException{
        BufferedImage bufImg = ImageIO.read(new File(pathToImage));
        Image img = SwingFXUtils.toFXImage(bufImg, null);
        view.setImage(img);
    }
    
    public static Image loadFXImage(String file) throws IOException{
        BufferedImage bufImg = ImageIO.read(new File(file));
        return SwingFXUtils.toFXImage(bufImg, null);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.utils;

import cs2suite.logic.CSLogger;
import cs2suite.logic.Settings;
import java.io.IOException;
import java.util.ArrayList;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Controller;
import org.lwjgl.input.Controllers;

/**
 *
 * @author Philipp
 */
public class GamePadWrapper {

    private static GamePadWrapper instance;
    private final Integer controllerIndex;
    private static Controller controller;
    private static ControllerPollingThread pollingThread;
    private static DoubleProperty controllerPOVX;
    private static DoubleProperty controllerPOVY;
    private static IntegerProperty controllerButton;
    private static ArrayList<DoubleProperty> axis;

    private GamePadWrapper(Integer controllerId) throws LWJGLException, IOException {
        axis = new ArrayList<>();
        this.controllerIndex = controllerId;
        try {
            controller = Controllers.getController(controllerIndex);
            for (int i = 0; i < controller.getAxisCount(); i++) {
                axis.add(new SimpleDoubleProperty(0.0));
            }

            controllerButton = new SimpleIntegerProperty(-1);
            controllerPOVX = new SimpleDoubleProperty(0.0);
            controllerPOVY = new SimpleDoubleProperty(0.0);

            Controllers.create();
            pollingThread = new ControllerPollingThread(controllerIndex,
                    axis,
                    controllerButton,
                    controllerPOVX,
                    controllerPOVY);
            pollingThread.start();
        } catch (Exception e) {
            CSLogger.logEvent(8, e, "Failed to initialize Controller: reseted Controller Settings");
            Settings.resetSetting(Settings.CONTROLLER_SETTINGS);
        }
    }

    public static GamePadWrapper getInstance(Integer id) throws LWJGLException, IOException {
        if (GamePadWrapper.instance == null) {
            GamePadWrapper.instance = new GamePadWrapper(id);
        }
        return GamePadWrapper.instance;
    }

    public static GamePadWrapper getInstance(String name) throws LWJGLException, IOException {
        for (int i = 0; i < Controllers.getControllerCount(); i++) {
            if (name.equals(Controllers.getController(i).getName())) {
                if (GamePadWrapper.instance == null) {
                    GamePadWrapper.instance = new GamePadWrapper(i);
                }
            }
        }
        return GamePadWrapper.instance;
    }

    public static Controller getController() {
        return controller;
    }

    public static void initialize(Integer id) throws LWJGLException, IOException {
        GamePadWrapper.instance = new GamePadWrapper(id);
    }
    
    public static void initialize(String name) throws LWJGLException, IOException {
        for (int i = 0; i < Controllers.getControllerCount(); i++) {
            if (name.equals(Controllers.getController(i).getName())) {
                if (GamePadWrapper.instance == null) {
                    GamePadWrapper.instance = new GamePadWrapper(i);
                }
            }
        }
    }

    public static DoubleProperty getAxisProperty(Integer index) {
        return axis.get(index);
    }

    public static Double getAxisValue(Integer index) {
        return axis.get(index).get();
    }

    public static Double getAxisByName(String name) {
        for (int i = 0; i < controller.getAxisCount(); i++) {
            if (name.equals(controller.getAxisName(i))) {
                return axis.get(i).get();
            }
        }
        return 0.;
    }

    public static IntegerProperty getButtonProperty() {
        return controllerButton;
    }

    public static Integer getButtonValue() {
        return controllerButton.get();
    }

    public static DoubleProperty getPOVXProperty() {
        return controllerPOVX;
    }

    public static Double getPOVXValue() {
        return controllerPOVX.get();
    }

    public static DoubleProperty getPOVYProperty() {
        return controllerPOVY;
    }

    public static Double getPOVYValue() {
        return controllerPOVY.get();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.utils;

import cs2suite.logic.CSLogger;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import org.lwjgl.input.Controller;
import org.lwjgl.input.Controllers;

/**
 *
 * @author Philipp
 */
public class ControllerPollingThread extends Thread {

    private final Integer controller;
    private final Integer timing;
    private DoubleProperty povX;
    private DoubleProperty povY;
    private IntegerProperty button;
    private ArrayList<DoubleProperty> axisProperties;
    private final Controller thisController;

    
    public ControllerPollingThread(Integer ctrl,
            ArrayList<DoubleProperty> properties,
            IntegerProperty button,
            DoubleProperty povX,
            DoubleProperty povY,
            Integer... timing){       
        axisProperties = new ArrayList<>();
        Integer pause = timing.length == 1 ? timing[0] : 50;
        controller = ctrl;
        thisController = Controllers.getController(controller);
        this.timing = pause;
        for (int i = 0; i < properties.size(); i++){
            axisProperties.add(properties.get(i));
        }
        this.povX = povX;
        this.povY = povY;
        this.button = button;
        CSLogger.logEvent(9, null, "--PollingThread Initialized--");
        CSLogger.logEvent(111, null, "--PollingThread Initialized--");
    }       

    @Override
    public void run() {
        System.out.println("Polling Thread running...");
        while (true) {
            //System.out.println("still running...");
            Controllers.poll();
            //controller.poll();
            try {
                Thread.sleep(timing);
            } catch (InterruptedException ex) {
                CSLogger.logEvent(5, null, "Controller POlling Thread Error: sleep-Error");                
            }
            povX.set(thisController.getPovX());
            povY.set(thisController.getPovY());
            for (int i = 0; i < axisProperties.size(); i++){
                axisProperties.get(i).set(thisController.getAxisValue(i));
            }                        
            for (int i = 0; i < thisController.getButtonCount(); i++) {
                if (thisController.isButtonPressed(i)) {                   
                    button.set(i);
                    break;
                }
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(ControllerPollingThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            button.set(-1);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.utils;

import cs2suite.CS2Suite;
import cs2suite.csObjects.CSArticleTemplate;
import cs2suite.logic.CSLogger;
import cs2suite.fxmlControllers.cs2SuiteGUIController;
import cs2suite.csObjects.CSArticle;
import cs2suite.csObjects.CSLayout;
import cs2suite.csObjects.CSLayoutObject;
import cs2suite.csObjects.CSLayoutSquare;
import cs2suite.csObjects.CSTrain;
import cs2suite.fxmlControllers.guiControllerWrapper;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Philipp
 */
public class SqliteDriver {

    private static SqliteDriver instance;

    private static String driverName;
    private static String dbPath;
    private static String jdbc;
    private static int timeout = 30;
    private static Connection connection;

    private SqliteDriver(String path) throws ClassNotFoundException, SQLException {
        dbPath = path;
        Class.forName("org.sqlite.JDBC");
        File f = new File(path);
        if (f.exists() && !f.isDirectory()) {
            connection = DriverManager.getConnection("jdbc:sqlite:" + path);
            connection.setAutoCommit(false);
            CSLogger.logEvent(8, null, "Database under \"" + path + "\" connected");
        } else {
            CSLogger.logEvent(timeout, new IOException("DB File invalid"), "Error loading Database-file");
        }
    }

    public static SqliteDriver getInstance(String path, boolean force) throws ClassNotFoundException, SQLException {
        if (SqliteDriver.instance == null || force) {
            SqliteDriver.instance = new SqliteDriver(path);
        }
        return SqliteDriver.instance;
    }

    private static ResultSet sqlQuery(String queryString) {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.createStatement();
            if (queryString.contains("select")) {
                rs = stmt.executeQuery(queryString);
            } else {
                stmt.executeUpdate(queryString);
                stmt.close();
            }
        } catch (Exception e) {
            CSLogger.logEvent(5, e, "Error executing sql-query: " + queryString);
        }
        return rs;
    }

    public static int getLayoutObjectCount() throws SQLException {
        ResultSet rs = sqlQuery("select count(*) from layoutObjects;");
        return rs.getInt(1);
    }

    public static ArrayList<CSLayoutObject> getLayoutObjects() throws SQLException, IOException {
        ResultSet rs = sqlQuery("select * from layoutObjects");
        CSLayoutObject tempObj = null;
        ArrayList<CSLayoutObject> results = new ArrayList<>();
        while (rs.next()) {
            String tempStr = rs.getString("images");
            String[] tempList = tempStr.split(",");
            ArrayList<String> imgs = new ArrayList<String>(Arrays.asList(tempList));
            tempObj = new CSLayoutObject(rs.getString("name"), rs.getInt("states"), rs.getBoolean("correlation"), imgs);
            tempObj.setDb_id(rs.getInt("id"));
            results.add(tempObj);
        }
        return results;
    }

    public static CSLayoutObject getLayoutObject(Integer id) throws SQLException, IOException {
        ResultSet rs = sqlQuery("select * from layoutObjects where id=" + id + " limit 1");
        String tempStr = rs.getString("images");
        String[] tempList = tempStr.split(",");
        ArrayList<String> imgs = new ArrayList<String>(Arrays.asList(tempList));
        CSLayoutObject tempObj = new CSLayoutObject(rs.getString("name"), rs.getInt("states"), rs.getBoolean("correlation"), imgs);
        return tempObj;
    }

    public static void addLayoutAsTable(CSLayout layout) throws SQLException {
        sqlQuery("create table " + layout.getName() + "(id integer primary key autoincrement,"
                + "posx integer not null,"
                + "posy integer not null,"
                + "obj_id integer not null,"
                + "article_id integer not null default -1)");
        String insertQuery = "insert into " + layout.getName() + " (posx,posy,obj_id,article_id) values (?,?,?,?)";
        PreparedStatement insertStmt = connection.prepareStatement(insertQuery);
        for (int x = 0; x < layout.getxCount(); x++) {
            for (int y = 0; y < layout.getyCount(); y++) {
                CSLayoutSquare square = layout.getSquare(x, y);
                CSLayoutObject layoutObj = square.getLayoutObject();
                if (layoutObj != null) {
                    insertStmt.setInt(1, x);
                    insertStmt.setInt(2, y);
                    insertStmt.setInt(3, layoutObj.getDb_id());
                    insertStmt.setInt(4, layoutObj.getArticle_id());
                    insertStmt.addBatch();
                    CSLogger.logEvent(10, null, "inserted object into layout_table: " + layout.getName() + " -> " + x + "_" + y + ":" + layoutObj.getDb_id() + ";" + layoutObj.getArticle_id());
                }
            }
        }
        insertStmt.executeBatch();
        connection.commit();
    }

    public static ArrayList<CSArticleTemplate> getArticleTemplates() throws SQLException, IOException {
        ResultSet rs = sqlQuery("select * from article_templates");
        CSArticleTemplate template = null;
        ArrayList<CSArticleTemplate> results = new ArrayList<>();
        while (rs.next()) {
            template = new CSArticleTemplate(rs.getString("typ"), rs.getInt("states"), rs.getString("images"));
            template.setSqlId(rs.getInt("id"));
            results.add(template);
        }
        return results;
    }

    public static CSArticleTemplate getArticleTemplate(String type) throws SQLException, IOException {
        ResultSet rs = sqlQuery("select * from article_templates where typ=\"" + type + "\"");
        CSArticleTemplate result = new CSArticleTemplate(rs.getString("typ"), rs.getInt("states"), rs.getString("images"));
        result.setSqlId(rs.getInt("id"));
        return result;
    }

    public static CSArticleTemplate getArticleTemplate(Integer id) throws SQLException, IOException {
        ResultSet rs = sqlQuery("select * from article_templates where id=" + id);
        CSArticleTemplate result = new CSArticleTemplate(rs.getString("typ"), rs.getInt("states"), rs.getString("images"));
        result.setSqlId(rs.getInt("id"));
        return result;
    }

    public static void addNewLayoutObject(CSLayoutObject obj) {
        String imgStr = "";
        for (int i = 0; i < obj.getImages().size(); i++) {
            imgStr += obj.getImagePath(i) + ",";
        }
        imgStr = imgStr.substring(0, imgStr.length() - 1);
        Integer states = obj.getMaxState() + 1;
        Integer correlation = obj.canCorrelate() ? 1 : 0;
        String query = "insert into layoutObject (name, states, images, correlation) values (\"" + obj.getName() + "\"," + states.toString() + ",\"" + imgStr + "\"," + correlation.toString() + ")";
        sqlQuery(query);
        CSLogger.logEvent(10, null, "Inserted Query: " + query);
    }

    public static void addTrain(CSTrain train, boolean systemFlag) {
        /*
        if (systemFlag) {
            String systemName = guiControllerWrapper.getController().getCurrentSystem().getSystemName();
            String query = "insert into trains (name, address, picture, description, direction, decoder, vMax, vMin, brakes, acceleration, volume, tacho, hasCV, system_id) values ("
                    + "\"" + train.getName() + "\", " + train.getAddress() + ", \"" + train.getPicturePath() + "\","
                    + "\"" + train.getDescription() + "\", " + train.getDirection() + ", \"" + train.getDecoderType() + "\","
                    + "" + train.getVMax() + ", " + train.getVMin() + ", " + train.getBrakes() + ", " + train.getAcceleration() + ","
                    + "" + train.getVolume() + ", " + train.getTacho() + ", 0, \"" + systemName + "\")";
        } else {
            String query = "insert into trains (name, address, picture, description, direction, decoder, vMax, vMin, brakes, acceleration, volume, tacho, hasCV) values ("
                    + "\"" + train.getName() + "\", " + train.getAddress() + ", \"" + train.getPicturePath() + "\","
                    + "\"" + train.getDescription() + "\", " + train.getDirection() + ", \"" + train.getDecoderType() + "\","
                    + "" + train.getVMax() + ", " + train.getVMin() + ", " + train.getBrakes() + ", " + train.getAcceleration() + ","
                    + "" + train.getVolume() + ", " + train.getTacho() + ", 0)";
        }
        */
    }

    public static void updateTrains(ArrayList<CSTrain> trains) throws SQLException {
        PreparedStatement insertStmt = connection.prepareStatement("insert into trains (name,address,picture,description,direction,"
                + "decoder,vMax,vMin,brakes,acceleration,volume,tacho) values (?,?,?,?,?,?,?,?,?,?,?,?)");
        PreparedStatement updateStmt = connection.prepareStatement("update trains set name=?, picture=?, description=?,"
                + "direction=?, decoder=?, vMax=?, vMin=?, brakes=?, acceleration=?, volume=?, tacho=? where address=?");
        String query = "";
        for (CSTrain train : trains) {
            query = "select count(*) from trains where address = " + train.getAddress();
            ResultSet rs = sqlQuery(query);
            if (rs.getInt(1) == 0) { //not yet in databse
                insertStmt.setString(1, train.getName());
                insertStmt.setInt(2, train.getAddress());
                insertStmt.setString(3, train.getPicturePath());
                insertStmt.setString(4, train.getDescription());
                insertStmt.setInt(5, train.getDirection());
                insertStmt.setString(6, train.getDecoderType());
                insertStmt.setInt(7, train.getVMax());
                insertStmt.setInt(8, train.getVMin());
                insertStmt.setInt(9, train.getBrakes());
                insertStmt.setInt(10, train.getAcceleration());
                insertStmt.setInt(11, train.getVolume());
                insertStmt.setInt(12, train.getTacho());
                insertStmt.addBatch();
            } else { // already in database
                updateStmt.setString(1, train.getName());
                updateStmt.setString(2, train.getPicturePath());
                updateStmt.setString(3, train.getDescription());
                updateStmt.setInt(4, train.getDirection());
                updateStmt.setString(5, train.getDecoderType());
                updateStmt.setInt(6, train.getVMax());
                updateStmt.setInt(7, train.getVMin());
                updateStmt.setInt(8, train.getBrakes());
                updateStmt.setInt(9, train.getAcceleration());
                updateStmt.setInt(10, train.getVolume());
                updateStmt.setInt(11, train.getTacho());
                updateStmt.setInt(12, train.getAddress());
                updateStmt.addBatch();
            }
        }
        insertStmt.executeBatch();
        updateStmt.executeBatch();
        connection.commit();
        CSLogger.logEvent(9, null, "updated trains, inserted " + insertStmt.getMaxRows() + " rows and updated " + updateStmt.getMaxRows() + " rows");
    }

    /*
     inserts all standart functions into the functions table baased on the picture folder
     for functions
     */
    public static void initFunctions() throws SQLException {
        File picFolder = new File(System.getProperty("user.dir") + "\\data\\img\\functions\\on\\");
        PreparedStatement stmt = null;
        File[] files = picFolder.listFiles();
        Integer nr = 0;
        String queryTemplate = "insert into functions (name, picNumber) values (?,?)";
        stmt = connection.prepareStatement(queryTemplate);
        for (File f : files) {
            stmt.setString(1, "function_" + nr);
            stmt.setInt(2, nr);
            nr++;
            stmt.addBatch();
        }
        stmt.executeBatch();
        connection.commit();
        CSLogger.logEvent(9, null, "created initial functions Table");
    }

    public static ArrayList<CSArticle> getArticles() throws SQLException, IOException {
        ArrayList<CSArticle> results = new ArrayList<>();
        String query = "select * from articles";
        ResultSet rs = sqlQuery(query);
        while (rs.next()) {
            CSArticle tempArticle = new CSArticle(getArticleTemplate(rs.getInt("template_id")), rs.getString("name"), rs.getInt("address"), rs.getInt("timing"));
            results.add(tempArticle);
        }
        return results;
    }

    public static void updateArticle(CSArticle article) throws SQLException {
        /*
        String query = "select count(*) from articles where address = " + article.getAddress();
        ResultSet rs = sqlQuery(query);
        if (rs.getInt(1) == 0) { // article not yet created
            query = "insert into articles (name, address, template_id, currentState, decoder, timing, system) values ("
                    + "\"" + article.getName() + "\", " + article.getAddress() + ", " + article.getTemplate().getSqlId() + ","
                    + "" + article.getCurState() + ",\"" + article.getDecoder() + "\", " + article.getTiming() + ","
                    + "\"" + guiControllerWrapper.getController().getCurrentSystem().getSystemName() + "\")";
            sqlQuery(query);
        } else if (rs.getInt(1) > 0) { // article already exists
            query = "update articles set name=\"" + article.getName() + "\", template_id=" + article.getTemplate().getSqlId() + ","
                    + "currentState=" + article.getCurState() + ", decoder=\"" + article.getDecoder() + "\","
                    + "timing=" + article.getTiming() + " where address = " + article.getAddress();
            sqlQuery(query);
        }
        connection.commit();
        */
    }

    public static void updateArticles(ArrayList<CSArticle> articles) throws SQLException {
        String query;
        PreparedStatement insertStmt = connection.prepareStatement("insert into articles (name, address, template_id, currentState, decoder, timing, system) values "
                + "(?,?,?,?,?,?,?)");
        PreparedStatement updateStmt = connection.prepareStatement("update articles set name=?, template_id=?, currentState=?, decoder=?, timing=? where address=?");
        for (int i = 0; i < articles.size(); i++) {
            CSArticle tempArticle = articles.get(i);
            query = "select count(*) from articles where address=" + tempArticle.getAddress();
            ResultSet rs = sqlQuery(query);
            if (rs.getInt(1) == 0) { // article not yet created
                if (tempArticle.getName() == null) {
                    insertStmt.setString(1, "Adr. " + tempArticle.getAddress());
                } else {
                    insertStmt.setString(1, tempArticle.getName());
                }
                insertStmt.setInt(2, tempArticle.getAddress());
                insertStmt.setInt(3, tempArticle.getTemplate().getSqlId());
                insertStmt.setInt(4, tempArticle.getCurState());
                insertStmt.setString(5, tempArticle.getDecoder());
                insertStmt.setInt(6, tempArticle.getTiming());
                //insertStmt.setString(7, guiControllerWrapper.getController().getCurrentSystem().getSystemName());

                insertStmt.addBatch();
            } else if (rs.getInt(1) > 0) { // article already exists
                updateStmt.setString(1, tempArticle.getName());
                updateStmt.setInt(2, tempArticle.getTemplate().getSqlId());
                updateStmt.setInt(3, tempArticle.getCurState());
                updateStmt.setString(4, tempArticle.getDecoder());
                updateStmt.setInt(5, tempArticle.getTiming());
                updateStmt.setInt(6, tempArticle.getAddress());

                updateStmt.addBatch();
            }
        }
        insertStmt.executeBatch();
        updateStmt.executeBatch();
        connection.commit();
        CSLogger.logEvent(9, null, "inserted articles from file to DB: " + insertStmt.getMaxRows() + " rows affected, " + updateStmt.getMaxRows() + " rows updated");
    }

    public static ArrayList<CSTrain> getTrains() throws SQLException {
        ResultSet rs = sqlQuery("select * from trains");
        ArrayList<CSTrain> results = new ArrayList<>();
        CSTrain tempTrain = null;
        while (rs.next()) {
            tempTrain = new CSTrain();
            tempTrain.setAddress(rs.getInt("address"));
            tempTrain.setName(rs.getString("name"));
            tempTrain.setDescription(rs.getString("description"));
            tempTrain.setDirection(rs.getInt("direction"));
            tempTrain.setDecodertype(rs.getString("decoder"));
            tempTrain.setAcceleration(rs.getInt("acceleration"));
            tempTrain.setBrakes(rs.getInt("brakes"));
            tempTrain.setPicturePath(rs.getString("picture"));
            tempTrain.setTacho(rs.getInt("tacho"));
            tempTrain.setVMax(rs.getInt("vMax"));
            tempTrain.setVMin(rs.getInt("vMin"));
            tempTrain.setVolume(rs.getInt("volume"));
            tempTrain.initFunctions();
            /*Integer trainID = rs.getInt("id");            
             ResultSet subRS = sqlQuery("select * from train_functions where train_id = "+trainID);
             int functionCounter = 0;
             while (subRS.next()){
             ResultSet frs = sqlQuery("select * from functions where id = "+rs.getInt("function_id")+" limit 1");
             String iconPath = cs2SuiteGUIController.FUNCTION_IMG_DIR+"off\\FktIcon_a_ge_"+frs.getInt("picNumber")+".png";
             tempTrain.getFunction(functionCounter).setIconPath(iconPath);
             tempTrain.getFunction(functionCounter).setName(frs.getString("name"));
             tempTrain.getFunction(functionCounter).set
             }*/
            results.add(tempTrain);
        }

        return results;
    }

    /**
     * Helper Function to initialize the icons Table in the DB
     * @throws SQLException
     * @throws IOException
     */
    public static void populateIcons() throws SQLException, IOException {
        sqlQuery("delete from icons"); // previously delete all entries
        PreparedStatement stmt = connection.prepareStatement("insert into icons (name,path) values (?,?)");
        Collection<File> files = FileUtils.listFiles(new File(CS2Suite.TRUNK_IMG_DIR), new String[]{"png", "jpg", "bmp"}, true);
        ArrayList<File> fileArr = new ArrayList(files);
        for (File f : fileArr){
            stmt.setString(1, f.getName().substring(0, f.getName().indexOf(".")));
            stmt.setString(2, f.getAbsolutePath());
            stmt.addBatch();
        }        
        System.out.println("Executing DB Populate in \"icons\"");
        stmt.executeBatch();
        connection.commit();
        System.out.println("Successfully added "+fileArr.size()+" icons to Databse. \n");
    }
    
    public static void loadIcons() throws SQLException{     
        CSIcons.getInstance(); // initialise Icon Class Singleton
        ResultSet rs = sqlQuery("select * from icons");
        CSLogger.logEvent(101, null, "loading icons");
        while (rs.next()){
            CSIcons.put(rs.getString("name"),rs.getString("path"));
        }        
        // CSIcons.processIcons(); experimental state will cause errors in some ocasions
        CSLogger.logEvent(101, null, CSIcons.size()+" icons loaded");
    }
}

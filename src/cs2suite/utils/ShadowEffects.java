/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.utils;

import javafx.scene.effect.BlurType;
import javafx.scene.effect.InnerShadow;
import javafx.scene.paint.Color;

/**
 *
 * @author Philipp
 */
public class ShadowEffects {
    
    private static ShadowEffects instance;
    private static InnerShadow innerShadow;
    
    private ShadowEffects(){
        super();
        innerShadow = new InnerShadow();
        innerShadow.setBlurType(BlurType.GAUSSIAN);
    }   
    
    public static ShadowEffects getInstance() {
        if (ShadowEffects.instance == null) {
            ShadowEffects.instance = new ShadowEffects();
        }
        return ShadowEffects.instance;
    }
    
    public static InnerShadow getInnerShadow(Color color) {
        innerShadow.setRadius(50);
        innerShadow.setHeight(80);
        innerShadow.setWidth(100);
        innerShadow.setChoke(0.24);
        innerShadow.setColor(color);
        return innerShadow;
    }
    
}

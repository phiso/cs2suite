/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.utils;

import cs2suite.CS2Suite;
import cs2suite.logic.CSLogger;
import cs2suite.fxmlControllers.cs2SuiteGUIController;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javax.imageio.ImageIO;

/**
 * handles icon database
 *
 * @author Philipp
 */
public class CSIcons {

    public static CSIcons instance;

    private static HashMap<String, String> icons;
    private static HashMap<String, ArrayList<String>> iconStates;
    private static String defaultIcon;
    private static ArrayList<String> emptyDummy;

    private CSIcons() {
        icons = new HashMap<>();
        iconStates = new HashMap<>();
        emptyDummy = new ArrayList<>();
        defaultIcon = CS2Suite.UI_IMG_DIR + "default.png";
    }

    public static CSIcons getInstance() {
        if (CSIcons.instance == null) {
            CSIcons.instance = new CSIcons();
        }
        return CSIcons.instance;
    }

    /**
     * EXPERIMENTAL
     */
    public static void processIcons() {
        iconStates = new HashMap<>();
        iconStates.clear();
        for (String n : icons.keySet()) {
            String name = "";
            if (n.indexOf("_") == -1) {
                name = n;
            } else {
                name = n.substring(0, n.indexOf("_"));
            }
            if (!iconStates.containsKey(name)) {
                iconStates.put(name, (ArrayList<String>) emptyDummy.clone());
                iconStates.get(name).add(icons.get(n));
            } else {
                iconStates.get(name).add(icons.get(n));
            }
        }
    }

    public static String getIconState(String key, int nr) {
        return iconStates.get(key).get(nr);
    }

    public static Image getIconStateImage(String key, int nr) throws IOException {
        BufferedImage bufimg = ImageIO.read(new File(getIconState(key, nr)));
        return SwingFXUtils.toFXImage(bufimg, null);
    }

    public static void put(String name, String path) {
        icons.put(name, path);
    }

    public static String getIconPath(String key) {
        return icons.getOrDefault(key, defaultIcon);
    }

    public static Image getIcon(String key) {
        try {
            String test = icons.get(key);
            BufferedImage bufimg = ImageIO.read(new File(icons.get(key)));
            Image img = SwingFXUtils.toFXImage(bufimg, null);            
            return img;
        } catch (IOException ex) {
            CSLogger.logEvent(4, ex, "Error loading icon: " + key + " -> " + icons.get(key));
        }
        return null;
    }
    
    public static ImageView getIconView(String key, int h, int w){
        ImageView view = new ImageView(getIcon(key));
        view.setFitHeight(h);
        view.setFitWidth(w);
        view.setPreserveRatio(true);
        return view;
    }

    public static int size() {
        return icons.size();
    }
}

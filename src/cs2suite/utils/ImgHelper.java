/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javax.imageio.ImageIO;
import org.ini4j.Ini;

/**
 *
 * @author Philipp
 */
public class ImgHelper {

    private static ImgHelper instance;
    private static ObservableList<Pane> keyboardIcons;

    private File fkt_file;
    private File keyboard_file;
    private File lok_file;

    private ImgHelper() throws IOException {
        fkt_file = new File(System.getProperty("user.dir") + "\\img\\function_icons\\on");
        keyboard_file = new File(System.getProperty("user.dir") + "\\img\\keyboard_icons\\mapping.ini");
        lok_file = new File(System.getProperty("user.dir") + "\\img\\train_icons");
        keyboardIcons = FXCollections.observableArrayList();

        Ini ini = new Ini();
        ini.load(keyboard_file);
        Ini.Section section = ini.get("names");
        for (String key : section.keySet()){
            File imgFile = new File(System.getProperty("user.dir")+"\\img\\keyboard_icons\\"+key);
            AnchorPane pane = new AnchorPane();
            pane.setPrefSize(128, 50);
            ImageView imgView = new ImageView();
            Label label = new Label();
            imgView.setLayoutX(1);
            imgView.setLayoutY(1);
            imgView.setFitHeight(32);
            imgView.setFitWidth(32);
            label.setLayoutX(35);
            label.setLayoutY(15);
            BufferedImage bufimg = ImageIO.read(imgFile);
            Image img = SwingFXUtils.toFXImage(bufimg, null);
            label.setText(section.get(key));
            imgView.setImage(img);
            imgView.setVisible(true);
            pane.getChildren().add(imgView);
            pane.getChildren().add(label);
            pane.setVisible(true);
            keyboardIcons.add(pane);
        }
    }

    public static ImgHelper getInstance() throws IOException {
        if (ImgHelper.instance == null) {
            ImgHelper.instance = new ImgHelper();
        }
        return ImgHelper.instance;
    }
    
    public static ObservableList<Pane> getKeyboardPanes(){
        return keyboardIcons;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.utils;

import static java.lang.Math.cos;
import static java.lang.Math.floor;
import static java.lang.Math.round;
import static java.lang.Math.sin;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author Philipp
 */
public class Graphics {

    private final Integer GRIDXY = 20;
    private GraphicsContext gc;
    private Canvas destCanvas;
    private final Double STEP = Math.PI / 180;
    private Integer centerX;
    private Integer centerY;

    public Graphics(Canvas dest) {
        super();
        destCanvas = dest;
        gc = dest.getGraphicsContext2D();
        centerX = round((float) destCanvas.getWidth() / 2);
        centerY = round((float) destCanvas.getHeight() / 2);
    }

    public void drawTurntable(Canvas layer2) {
        GraphicsContext gc_l2 = layer2.getGraphicsContext2D();
        //Outer Circle
        drawCircle(round((float) (destCanvas.getWidth() / 2)), centerX, 200);
        //Inner Circle
        drawCircle(round((float) (destCanvas.getWidth() / 2)), centerY, 180);
        gc.stroke();
        Integer s1d_x = round((float) cos(STEP * 273.75) * 175) + centerX;
        Integer s1d_y = round((float) sin(STEP * 273.75) * 175) + centerY;
        Integer s1t_x = round((float) cos(STEP * 93.75) * 175) + centerX;
        Integer s1t_y = round((float) sin(STEP * 93.75) * 175) + centerY;

        Integer s2t_x = round((float) cos(STEP * 86.25) * 175) + centerX;
        Integer s2t_y = round((float) sin(STEP * 86.25) * 175) + centerY;
        Integer s2d_x = round((float) cos(STEP * 266.25) * 175) + centerX;
        Integer s2d_y = round((float) sin(STEP * 266.25) * 175) + centerY;

        gc_l2.moveTo(s1d_x, s1d_y);
        gc_l2.lineTo(s2t_x, s2t_y);
        gc_l2.lineTo(s1t_x, s1t_y);
        gc_l2.lineTo(s2d_x, s2d_y);
        gc_l2.lineTo(s1d_x, s1d_y);
        gc_l2.stroke();

        Double w = 0.0;
        Integer upX;
        Integer upY;
        Integer downX;
        Integer downY;

        for (int i = 0; i < 48; i++) {
            w = 270 + 3.75 + (i * 7.5);
            upX = round((float) cos(STEP * w) * 200) + centerX;
            upY = round((float) sin(STEP * w) * 200) + centerY;
            downX = round((float) cos(STEP * w) * 180) + centerX;
            downY = round((float) sin(STEP * w) * 180) + centerY;

            gc.moveTo(upX, upY);
            gc.lineTo(downX, downY);
        }
        gc.stroke();
    }

    private void drawCircle(Integer cx, Integer cy, Integer r) {
        Integer x = round((float) (cos(0) * r)) + cx;
        Integer y = round((float) (sin(0) * r)) + cy;
        for (int i = 0; i <= 360; i++) {
            gc.moveTo(x, y);
            x = round((float) (cos(STEP * i) * r)) + cx;
            y = round((float) (sin(STEP * i) * r)) + cy;
            gc.lineTo(x, y);
        }
    }

    public GraphicsContext getGraphicsContext() {
        return gc;
    }

}

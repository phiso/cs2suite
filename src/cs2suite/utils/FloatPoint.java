/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.utils;

/**
 *
 * @author Philipp
 */
public class FloatPoint {

    private float xPoint;
    private float yPoint;

    public FloatPoint(float x, float y) {
        super();
        this.xPoint = x;
        this.yPoint = y;
    }

    public void setX(float x) {
        this.xPoint = x;
    }

    public void setY(float y) {
        this.yPoint = y;
    }

    public float getX() {
        return this.xPoint;
    }

    public float getY() {
        return this.yPoint;
    }       

    @Override
    public String toString() {
        return xPoint + ":" + yPoint;
    }
}

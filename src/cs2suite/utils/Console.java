/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.utils;

import java.io.IOException;
import java.io.OutputStream;
import javafx.application.Platform;
import javafx.scene.control.TextArea;

/**
 *
 * @author Philipp
 */
public class Console extends OutputStream {

    private TextArea txtArea;

    public Console(TextArea txtArea) {
        this.txtArea = txtArea;
    }

    @Override
    public void write(int b) throws IOException {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                getTxtArea().appendText(String.valueOf((char) b));
            }
        });

    }        

    /**
     * @return the txtArea
     */
    public TextArea getTxtArea() {
        return txtArea;
    }
}

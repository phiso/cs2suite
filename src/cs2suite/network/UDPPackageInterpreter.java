package cs2suite.network;

import cs2suite.logic.CSLogger;
import cs2suite.logic.Settings;
import cs2suite.csObjects.CSDecoder;
import cs2suite.fxmlControllers.guiControllerWrapper;
import cs2suite.utils.HelperUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.zip.DataFormatException;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import static org.apache.commons.lang3.ArrayUtils.toPrimitive;

/**
 * ------------------------------------------------------------------------------
 * Der PackageInterpreter implementiert funktionen zum übersetzen gesendeter bzw
 * empfangener Datensätze, um diese für das Logging sowie die LogFiles
 * aufzubereiten
 * ------------------------------------------------------------------------------
 *
 * @author Philipp
 */
@Deprecated
public class UDPPackageInterpreter {

    private static UDPPackageInterpreter instance;
    private static byte[] parseData;
    private static IntegerProperty speedValue;
    private static IntegerProperty adressValue;
    private static byte[] crc = new byte[2];
    private static byte[] configDataLength = new byte[4];
    private static ArrayList<Byte> configData;
    private static byte[] configDataPrimitive;
    private static Boolean dataStreamEnd = false;
    private static SimpleStringProperty plainTextConfigData= new SimpleStringProperty();
    
    //private static ArrayIntegerProperty 

    private UDPPackageInterpreter() {
        super();
        configData = new ArrayList<>();
        parseData = Packet.NULL_BYTE;
        speedValue = new SimpleIntegerProperty(0);
        adressValue = new SimpleIntegerProperty(0);
    }

    public static UDPPackageInterpreter getInstance() {
        if (UDPPackageInterpreter.instance == null) {
            UDPPackageInterpreter.instance = new UDPPackageInterpreter();
        }
        return UDPPackageInterpreter.instance;
    }

    private static String parseIsResponse() {
        return null;
    }

    private static String parseCmd() {
        Integer cmdByte = Integer.parseInt(String.valueOf(parseData[1]));
        String result = "";        
        switch (cmdByte) {
            case 1://response to cmd:0 -> System
                result = "<System> ";
                
                switch (parseData[9]){
                    case 0:
                        if (parseData[5] == 0 && parseData[6] == 0){
                            result += "Stop ";
                        }
                        break;
                    case 1:
                        if (parseData[5] == 0 && parseData[6] == 0){
                            result += "Go ";
                        }
                        break;
                    
                }
                if (parseData[9] == 0 && (parseData[5] == 0 && parseData[6] == 0)) {
                    result += "Stop ";
                } else if (parseData[9] == 1 && (parseData[5] == 0 && parseData[6] == 0)) {
                    result += "Go ";
                } else if (parseData[9] == 1 && (parseData[5] == 0 && parseData[6] == 0)) {
                    result += "Hold ";
                }
                break;
            case 9:
                result = "<Speed changed> ";
                byte[] adrBytes = new byte[]{parseData[7], parseData[8]};
                byte[] speedBytes = new byte[]{parseData[9], parseData[10]};
                Integer adr = Package.bytesToAddress(adrBytes);
                Integer speed = Package.bytesToSpeed(speedBytes);
                speedValue.set(speed);
                adressValue.set(adr);
                result += "Adr. " + adr + " set to: " + speed;
                break;
            case 48:
                result = "<Ping request> ";
                break;
            case 49:
                result = "<Ping Success> ";
                break;
            case 66:  // Config-Data-Stream
                switch (parseData[4]) {
                    case 6: // start of transfer
                        crc[0] = parseData[9];
                        crc[1] = parseData[10];
                        configDataLength[0] = parseData[5];
                        configDataLength[1] = parseData[6];
                        configDataLength[2] = parseData[7];
                        configDataLength[3] = parseData[8];
                        configData.clear();
                        result = "<Config-Data-Stream starting...>";
                        break;
                    case 7:
                        break;
                    case 8: // running transfer
                        dataStreamEnd = false;                      
                        for (int i = 5; i < 13; i++) {                            
                            if (parseData[i] != 0x00) {
                                configData.add(parseData[i]);                                                                
                            }
                        }                        
                        configDataPrimitive = toPrimitive(configData.toArray(new Byte[configData.size()]));
                        if (parseData[12] == 0x00){
                           dataStreamEnd = true;
                           plainTextConfigData.set(getConfigData());
                        }
                        result = "<Config-Data-Stream>";
                        break;
                    default:
                        break;

                }
                break;

        }
        return result;
    }

    public static String parse(byte[] data) {
        parseData = data;
        String result = parseCmd();

        return result;
    }

    public static IntegerProperty getSpeedProperty() {
        return speedValue;
    }

    public static Integer getSpeed() {
        return speedValue.get();
    }

    public static IntegerProperty getAdressProperty() {
        return adressValue;
    }

    public static Integer getAdress() {
        return adressValue.get();
    }

    public static StringProperty getSpeedString() {
        StringProperty result = new SimpleStringProperty(speedValue.getValue().toString());
        return result;
    }
    /*
     ----------------------------------------------------------------------------
     Extrahiert aus dem übergebenen String das resultierende 13 byte lange
     Datenpaket und sendet dies.
     Nähere Informtionen über die möglichen Befehle und die Syntax sind im 
     entsprechenden Help-File im Ordner 'help' zu finden.
     ----------------------------------------------------------------------------
     */

    public static void checkdebugCmd(String cmd) throws IOException, DataFormatException {
        ArrayList<Byte> buff = new ArrayList<>();
        byte[] finalBuff = new byte[13];
        if (cmd.indexOf("\\") != -1) {
            //is Command  
            Integer first_wsp = cmd.indexOf(" ");
            String rest = "";
            if (first_wsp == -1) {
                first_wsp = cmd.length();
            } else {
                rest = cmd.substring(first_wsp + 1, cmd.length());
            }
            String command = cmd.substring(1, first_wsp);
            //System.out.println(command);

            switch (command) {
                case "dumpStream":
                    String dat = getConfigData();
                    File file = new File(System.getProperty("user.dir")+"\\data\\import\\cs2\\"+rest+".cs2");
                    HelperUtils.saveStringToFile(file, dat);
                    System.out.println("DataStream saved: "+file.getAbsolutePath());
                    break;
                case "decompress":                   
                    System.out.println(getConfigData());
                    break;
                case "configdata":
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < configDataPrimitive.length; i++) {
                        sb.append(String.format("%02X ", configDataPrimitive[i]));
                    }
                    System.out.println(sb.toString());
                    break;
                case "send":
                    checkdebugCmd(rest);
                    break;
                case "calc":
                    Integer val = Package.convertNegByteValue(Byte.parseByte(rest));
                    System.out.println(val);
                    break;
                case "show":
                    switch (rest) {
                        case "mem":
                            Double temp = Runtime.getRuntime().totalMemory() / 1000000.0;
                            System.out.println("---------------------------------\n"
                                    + "Total Mem: " + temp + "MB");
                            temp = Runtime.getRuntime().freeMemory() / 1000000.0;
                            System.out.println("Free Mem: " + temp + "MB");
                            temp = Runtime.getRuntime().maxMemory() / 1000000.0;
                            System.out.println("Max Mem: " + temp + "MB"
                                    + "\n---------------------------------");
                            break;
                        case "ip":
                            try {
                                System.out.println("------------------------------------------\n"
                                        + "Connected to CS2 at: " + UDPReceiver.getCS2IP().toString()
                                        + "\n------------------------------------------");
                            } catch (Exception e) {
                                System.out.println("No Connection is active!");
                            }
                            break;
                        case "threads":
                            break;
                        default:
                            if (rest.contains("channel")) {
                                String nr = rest.substring(rest.indexOf(" ") + 1, rest.length());
                                switch (nr) {
                                    case "1":
                                        UDPSender.sendBytes(Packet.ASK_SYSTEM_STATUS_CH);
                                        break;
                                }
                            }
                            break;
                    }
                    break;
                case "connection":
                    //TODO
                    break;
                case "exit":
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                guiControllerWrapper.getController().handleApplicationClose();
                            } catch (IOException ex) {
                                CSLogger.logEvent(2, ex, "Stopping Network Services failed");
                            }
                        }
                    });
                    break;
                case "cmd":
                    String subCmd = null;
                    String adr = null;
                    String rest2 = null;
                    String decoder = null;
                    String speedStr = null;
                    ArrayList<Integer> speeds = new ArrayList<>();
                    ArrayList<Integer> adresses = new ArrayList<>();

                    if (rest.contains("[") || rest.contains("]")) {
                        subCmd = rest.substring(0, rest.indexOf("["));
                    } else {
                        subCmd = rest;
                        if (subCmd.contains("lokinfo")) {
                            rest2 = subCmd.substring(subCmd.indexOf(" ") + 1, subCmd.length());
                            subCmd = subCmd.substring(0, subCmd.indexOf(" "));
                        }
                    }
                    subCmd = subCmd.toLowerCase();

                    switch (subCmd) {
                        case "setspeed":
                            adr = rest.substring(rest.indexOf("[") + 1, rest.indexOf("]"));
                            rest2 = rest.substring(rest.indexOf(" ") + 1, rest.length());
                            speedStr = rest2.substring(rest2.indexOf("[") + 1, rest2.indexOf("]"));
                            if (rest2.indexOf(" ") != -1) {
                                decoder = rest2.substring(rest2.indexOf(" ") + 1, rest2.length());
                            }
                            if (adr.indexOf(",") != -1) {
                                for (String a : adr.split(",")) {
                                    adresses.add(Integer.parseInt(a));
                                }
                            } else {
                                adresses.add(Integer.parseInt(adr));
                            }

                            if (speedStr.indexOf(",") != -1) {
                                for (String s : rest2.split(",")) {
                                    speeds.add(Integer.parseInt(s));
                                }
                            } else {
                                for (int i = 0; i < adresses.size(); i++) {
                                    speeds.add(Integer.parseInt(speedStr));
                                }
                            }

                            if (speeds.size() > 1 && speeds.size() != adresses.size()) {
                                System.out.println("Adresses and speed values not matching!");
                                return;
                            }
                            for (int i = 0; i < adresses.size(); i++) {
                                if (decoder != null) {
                                    switch (decoder) {
                                        case "dcc":
                                            Package.setSpeed(adresses.get(i), CSDecoder.DCC, speeds.get(i));
                                            break;
                                        case "mfx":
                                            Package.setSpeed(adresses.get(i), CSDecoder.MFX, speeds.get(i));
                                            break;
                                        case "mm2":
                                            Package.setSpeed(adresses.get(i), CSDecoder.MM2, speeds.get(i));
                                            break;
                                    }
                                } else {
                                    Package.setSpeed(adresses.get(i), CSDecoder.DCC, speeds.get(i));
                                    Package.setSpeed(adresses.get(i), CSDecoder.MM2, speeds.get(i));
                                    Package.setSpeed(adresses.get(i), CSDecoder.MFX, speeds.get(i));
                                }
                            }
                            break;
                        case "setfunction":
                            break;
                        case "setdirection":
                            break;
                        case "setarticle":
                            break;
                        case "stop":
                            if (rest2 == null) {
                                UDPSender.sendBytes(Packet.GLOBAL_STOP);
                            }
                            break;
                        case "go":
                            if (rest2 == null) {
                                UDPSender.sendBytes(Packet.GLOBAL_GO);
                            }
                            break;
                        case "hold":
                            if (rest2 == null) {
                                UDPSender.sendBytes(Packet.GLOBAL_HOLD);
                            }
                            break;
                        case "checkspeed":
                            break;
                        case "lokinfo":
                            Package.requestLokInfo(rest2);
                            break;
                    }
                    break;
                case "save":
                    String path = System.getProperty("user.dir") + "\\saves";
                    if (rest.contains("\\")) {
                        path = rest;
                    } else {
                        path += "\\" + rest;
                    }
                    try (PrintStream out = new PrintStream(new FileOutputStream(path))) {
                        String saveStr = "Console-Log " + Calendar.getInstance().getTime().toString() + "\n\n"
                                + guiControllerWrapper.getController().getConsoleLog();
                        out.print(saveStr);
                    }
                    System.out.println("Console-Log was saved to: " + path);
                    break;
                case "help":
                    switch (rest) {
                        case "cmd":

                            break;
                        default:
                            System.out.println(getHelpFile("console"));
                            break;
                    }
                    break;
            }
        } else {
            String[] temp = cmd.split(" ");
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != " " && temp[i] != "") {
                    if (temp[i].length() == 1 && !temp[i].matches("[0-9]")) {
                        buff.add((byte) temp[i].toCharArray()[0]);
                    } else {
                        Integer val = Integer.parseInt(temp[i], 16);
                        buff.add(val.byteValue());
                    }
                }
            }
            if (buff.size() < 13) {
                for (int i = buff.size(); i < 13; i++) {
                    buff.add((byte) 0);
                }
            }
            for (int i = 0; i < 13; i++) {
                finalBuff[i] = buff.get(i);
            }
            UDPSender.sendBytes(finalBuff);
        }
    }

    private static String getHelpFile(String fileName) throws FileNotFoundException, IOException {
        File file = new File(System.getProperty("user.dir") + "\\data\\help\\" + fileName + "_" + Settings.getLanguage().toLowerCase() + ".txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line = null;
        String result = null;
        int count = 1;
        while ((line = br.readLine()) != null) {
            result += line + "\n";
        }
        return result;
    }

    public static byte[] getConfigDataStream() {
        return configDataPrimitive;
    }

    public static String getConfigData() {
        char[] chars = new char[configDataPrimitive.length];
        for (int i = 0; i < configDataPrimitive.length; i++) {
            chars[i] = (char) configDataPrimitive[i];
        }
        return new String(chars);
    }
    
    public static Boolean configDataExists(){
        return configDataPrimitive != null;
    }
    
    public static void resetDataStreamRecognition(){
        configData.clear();
        configDataPrimitive = null;
        dataStreamEnd = false;
        plainTextConfigData.set("");
    }
    
    public static Boolean isDataStreamEnd(){
        return dataStreamEnd;
    }
    
    public static SimpleStringProperty configDataProperty(){
        return plainTextConfigData;
    }
}

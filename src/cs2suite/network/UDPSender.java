package cs2suite.network;

import cs2suite.logic.CSLogger;
import cs2suite.fxmlControllers.guiControllerWrapper;
import cs2suite.utils.HelperUtils;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Philipp uses Singleton schematic
 */
public class UDPSender extends Thread {

    private static UDPSender instance;

    private static DatagramSocket socket;
    private static byte[] lastBuff;
    private static DatagramPacket packet;
    private static Integer port;
    private static InetAddress address;
    private static ArrayList<byte[]> buffStack;

    private UDPSender(InetAddress addr) throws SocketException {
        super("UDPSender");
        buffStack = new ArrayList<>();
        socket = new DatagramSocket();
        lastBuff = Packet.NULL_BYTE;
        port = Packet.CS2_SEND_PORT;
        address = addr;
        socket.connect(address, port);
        System.out.println("Sender Ready (" + addr.getHostAddress() + ")\n");
        CSLogger.logEvent(10, null, "Sender Ready (" + addr.getHostAddress() + ")");
        CSLogger.logEvent(111, null, "Initialized Sender with IP: " + addr.getHostAddress());
        start();
        CSLogger.logEvent(111, null, "Sender Running on IP: " + addr.getHostAddress());
    }

    public static UDPSender getInstance() {
        return UDPSender.instance;
    }

    public static void initinstance(InetAddress addr) throws SocketException {
        if (UDPSender.instance == null) {
            UDPSender.instance = new UDPSender(addr);
        }
    }

    public static String getAddressAsString() {
        return address.getHostAddress();
    }

    public static void sendBytes(byte[] bytes) throws IOException {
        buffStack.add(bytes);
        guiControllerWrapper.getController().setInfoText1(Integer.toString(buffStack.size()));
    }
    
    public static void forceSend(byte[] bytes) throws IOException {
        socket.send(new DatagramPacket(bytes, bytes.length));
        CSLogger.logEvent(8, null, "Forced sending: "+Package.bufferToString(bytes));
    }

    public static byte[] getLastBuff() {
        return lastBuff;
    }
    
    public static void killQueue(){
        buffStack.clear();
    }

    @Override
    public void run() {
        while (true) {
            if (buffStack.size() > 0) {
                byte[] bytes = buffStack.remove(0);
                guiControllerWrapper.getController().setInfoText1(Integer.toString(buffStack.size()) + " - " + bytes.toString());
                try {
                    socket.send(new DatagramPacket(bytes, bytes.length));
                } catch (IOException ex) {
                    Logger.getLogger(UDPSender.class.getName()).log(Level.SEVERE, null, ex);
                }
                CSLogger.logEvent(10, null, "Sent Bytes: " + Package.bufferToString(bytes));
                lastBuff = bytes.clone();                
            }
            try{
            Thread.sleep(50);
            }catch (Exception e){
                
            }
        }
    }
}

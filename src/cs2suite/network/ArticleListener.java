/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.network;

import cs2suite.fxmlControllers.guiControllerWrapper;
import java.net.SocketException;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Philipp
 */
public class ArticleListener extends Thread {

    private static ArticleListener instance;

    private static byte[] buffer;
    private static IntegerProperty address;
    private static IntegerProperty state;

    private ArticleListener() throws SocketException {
        super();
        CoreListener.getInstance();
        buffer = new byte[13];
        state = new SimpleIntegerProperty(0);
        address = new SimpleIntegerProperty(0);
    }

    public static ArticleListener getInstance() throws SocketException {
        if (ArticleListener.instance == null) {
            ArticleListener.instance = new ArticleListener();
        }
        return ArticleListener.instance;
    }

    @Override
    public void run() {
        CoreListener.bufferStringProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            buffer = CoreListener.getBuffer();
            if (buffer[1] == 0x17) {
                byte[] adrBytes = new byte[2];
                adrBytes[0] = buffer[7];
                adrBytes[1] = buffer[8];
                address.set(Package.parseArticleAddress(adrBytes));
                state.set(buffer[9]);
            }
        });
    }
    
    public static IntegerProperty getStateProperty(){
        return state;
    }
    
    public static IntegerProperty getAddressProperty(){
        return address;
    }
    
    public static Integer getAddress(){
        return address.get();
    }
    
    public static Integer getArticleState(){
        return state.get();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.network;

import cs2suite.logic.CSLogger;
import cs2suite.logic.Settings;
import cs2suite.csObjects.CSDecoder;
import cs2suite.csObjects.CSSystem;
import cs2suite.fxmlControllers.guiControllerWrapper;
import cs2suite.utils.SqliteDriver;
import cs2suite.utils.HelperUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Set;
import java.util.zip.DataFormatException;
import javafx.application.Platform;

/**
 *
 * @author Philipp
 */
public class CommandParser {

    private static CommandParser instance;

    private CommandParser() {
        super();
    }

    public static CommandParser getInstance() {
        if (CommandParser.instance == null) {
            CommandParser.instance = new CommandParser();
        }
        return CommandParser.instance;
    }

    public static void execCommand(String commandStr) throws SQLException, IOException, ClassNotFoundException {
        Boolean send = false;
        byte[] finalBuff = new byte[13];
        commandStr = commandStr.toLowerCase();
        ArrayList<Byte> bufferArray = new ArrayList<>();
        ArrayList<Character> flagsSet = new ArrayList<>();
        ArrayList<String> commandList = new ArrayList<>();
        HashMap<String, String> params = new HashMap<>();

        String[] cmd_list = commandStr.split(" ");
        for (int i = 0; i < cmd_list.length; i++) {
            commandList.add(cmd_list[i]);
        }
        int n = 0;
        while (n < commandList.size()) {
            if (commandList.get(n).indexOf("-") == 0) {
                if (commandList.get(n).length() <= 2) {
                    // flag setting -[a..z]                             
                    flagsSet.add(commandList.get(n).charAt(1));
                    commandList.remove(n);
                } else {
                    for (int i = 1; i < commandList.get(n).length(); i++) {
                        flagsSet.add(commandList.get(n).charAt(i));
                    }
                }
            }
            n++;
        }
        n = 0;
        while (n < commandList.size()) {
            if (commandList.get(n).contains("=")) {
                String cmd = commandList.get(n);
                String key = cmd.substring(0, cmd.indexOf("="));
                String val = cmd.substring(cmd.indexOf("=") + 1, cmd.length());
                params.put(key, val);
                commandList.remove(n);
            }
            n++;
        }

        switch (cmd_list[0]) {
            case "version":
                System.out.println("-------------------------------------------");
                System.out.print("VersionInfo:\n"
                        + "CSSuite: " + Settings.getVersion() + "_" + Settings.getBuild() + "\n"
                        + "Java: " + System.getProperty("java.version") + "\n"
                        + "JavaFX: " + com.sun.javafx.runtime.VersionInfo.getRuntimeVersion()+"\n");
                System.out.println("-------------------------------------------");
                break;
            case "populatedb":
                switch (cmd_list[1]) {
                    case "icons":
                        SqliteDriver.populateIcons();
                        break;
                }
                break;            
            case "dumpstream":
                send = false;
                String fileName = commandList.get(1);
                if (commandList.size() > 2) {
                    CSLogger.logEvent(8, null, commandStr + " : Too many parameters, dumped after \"" + commandList.get(1) + "\".");
                }
                ConfigStreamListener.dump(new File(System.getProperty("user.dir") + "\\data\\import\\cs2\\" + fileName + ".cs2"));
                System.out.println("Stream Dumped with: " + ConfigStreamListener.getDataStreamLength() + " bytes");
                break;
            case "send":
                send = true;
                if (flagsSet.contains('r')) {
                    // send raw data
                    for (int i = 1; i < commandList.size(); i++) {
                        if (commandList.get(i) != " " && commandList.get(i) != "") {
                            if (commandList.get(i).length() == 1 && !commandList.get(i).matches("[0-9]")) {
                                bufferArray.add((byte) commandList.get(i).toCharArray()[0]);
                            } else {
                                Integer val = Integer.parseInt(commandList.get(i), 16);
                                bufferArray.add(val.byteValue());
                            }
                        }
                    }
                } else {
                    switch (commandList.get(1)) {
                        case "stop":
                            UDPSender.sendBytes(Packet.GLOBAL_STOP);
                            break;
                        case "go":
                            UDPSender.sendBytes(Packet.GLOBAL_GO);
                            break;
                        case "hold":
                            UDPSender.sendBytes(Packet.GLOBAL_HOLD);
                            break;
                    }
                }
                break;
            case "set":
                send = true;
                Integer adr = -1;
                String decoder = "mm2";
                switch (commandList.get(1)) {
                    case "trainspeed":
                        Integer speed = Integer.parseInt(params.get("speed"));
                        adr = Integer.parseInt(params.get("address"));
                        if (params.containsKey("decoder")) {
                            decoder = params.get("decoder");
                        }
                        switch (decoder) {
                            case "mm2":
                                Package.setSpeed(adr, CSDecoder.MM2, speed);
                                break;
                            case "dcc":
                                Package.setSpeed(adr, CSDecoder.DCC, speed);
                                break;
                            case "mfx":
                                Package.setSpeed(adr, CSDecoder.MFX, speed);
                                break;
                            case "all":
                                Package.setSpeed(adr, CSDecoder.DCC, speed);
                                Package.setSpeed(adr, CSDecoder.MM2, speed);
                                Package.setSpeed(adr, CSDecoder.MFX, speed);
                                break;
                        }
                        break;
                    case "direction":
                        System.out.println("---Not yet Implemented---");
                        break;
                    case "function":
                        System.out.println("---Not yet Implemented---");
                        break;
                    case "article":
                        System.out.println("---Not yet Implemented---");
                        break;
                }
                break;
            case "get":
                switch (commandList.get(1)) {
                    case "trainspeed":
                        System.out.println("---Not yet Implemented---");
                        break;
                    case "lokinfo":
                        Package.requestLokInfo(commandList.get(2));
                        break;
                }
                break;
            case "show":
                switch (commandList.get(1)) {
                    case "mem":
                    case "memory":
                        send = false;
                        Double temp = Runtime.getRuntime().totalMemory() / 1000000.0;
                        System.out.println("---------------------------------\n"
                                + "Total Mem: " + temp + "MB");
                        temp = Runtime.getRuntime().freeMemory() / 1000000.0;
                        System.out.println("Free Mem: " + temp + "MB");
                        temp = Runtime.getRuntime().maxMemory() / 1000000.0;
                        System.out.println("Max Mem: " + temp + "MB"
                                + "\n---------------------------------");
                        break;
                    case "stream":
                        send = false;
                        System.out.println(ConfigStreamListener.getDataStreamAsHex());
                        break;
                    case "ip":
                    case "address":
                        send = false;
                        try {
                            System.out.println("------------------------------------------\n"
                                    + "Connected to CS2 at: " + CoreListener.getHostAdress()
                                    + "\n------------------------------------------");
                        } catch (Exception e) {
                            System.out.println("No Connection is active!");
                        }
                        break;
                    case "threads":
                        send = false;
                        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
                        Thread[] threadArray = threadSet.toArray(new Thread[threadSet.size()]);
                        if (flagsSet.contains('a')) {
                            // show all
                            System.out.println("------------------------------------------");
                            System.out.println("Threads running: " + threadArray.length);
                            for (int i = 0; i < threadArray.length; i++) {
                                System.out.println("---");
                                System.out.println("Thread " + i + ": " + threadArray[i].getName());
                                System.out.println("State: " + threadArray[i].getState().toString());
                            }
                            System.out.println("------------------------------------------");
                        } else if (flagsSet.contains('s')) {
                            // single thread
                            Thread thread = threadArray[Integer.parseInt(params.get("nr"))];
                            System.out.println("------------------------------------------");
                            System.out.println("Thread" + Integer.parseInt(params.get("nr")) + ": " + thread.getName());
                            System.out.println("ID: " + thread.getId());
                            System.out.println("State: " + thread.getState());
                            System.out.println("Priority: " + thread.getPriority());
                            System.out.println("Stacktrace: " + thread.getStackTrace());
                            System.out.println("isAlive: " + thread.isAlive());

                            if (flagsSet.contains('t')) {
                                System.out.println("\n--Stacktrace--");
                                for (int i = 0; i < thread.getStackTrace().length; i++) {
                                    System.out.println(i + thread.getStackTrace()[i].toString());
                                }
                            }
                            System.out.println("------------------------------------------");
                        }
                        break;

                    case "channel":
                        send = true;
                        finalBuff = Packet.ASK_SYSTEM_STATUS_CH;
                        finalBuff[10] = (byte) Integer.parseInt(commandList.get(2), 8);
                        break;
                }
                break;
            case "exit":
                send = false;
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            guiControllerWrapper.getController().handleApplicationClose();
                        } catch (IOException ex) {
                            CSLogger.logEvent(2, ex, "Stopping Network Services failed");
                        }
                    }
                });
                break;
            case "help":
                send = false;
                System.out.println(HelperUtils.getHelpTextFile(commandList.get(1)));
                break;
            case "calcnegbyte":
                send = false;
                Integer val = Package.convertNegByteValue(Byte.parseByte(commandList.get(1)));
                System.out.println(val);
                break;
        }
        if (send) {
            if (bufferArray.size() < 13) {
                // if buffer is smaller than 13 fill it up with 0's
                for (int i = bufferArray.size(); i < 13; i++) {
                    bufferArray.add((byte) 0);
                }
            }
            for (int i = 0; i < 13; i++) {
                finalBuff[i] = bufferArray.get(i);
            }
            // send previous build buffer package
            UDPSender.sendBytes(finalBuff);
        }
    }

    @Deprecated
    public static void checkdebugCmd(String cmd) throws IOException, DataFormatException, SQLException, ClassNotFoundException {
        ArrayList<Byte> buff = new ArrayList<>();
        byte[] finalBuff = new byte[13];
        if (cmd.indexOf("\\") != -1) {
            //is Command  
            Integer first_wsp = cmd.indexOf(" ");
            String rest = "";
            if (first_wsp == -1) {
                first_wsp = cmd.length();
            } else {
                rest = cmd.substring(first_wsp + 1, cmd.length());
            }
            String command = cmd.substring(1, first_wsp);
            //System.out.println(command);

            switch (command) {
                case "createNewDefaultSave":
                    CSSystem newDefault = new CSSystem("Default", false);
                    newDefault.saveSystem("default_system", false);
                    break;
                case "dumpStream":
                    ConfigStreamListener.dump(new File(System.getProperty("user.dir") + "\\data\\import\\cs2\\" + rest + ".cs2"));
                    System.out.println("Stream Dumped with: " + ConfigStreamListener.getDataStreamLength() + " bytes");
                    break;
                case "send":
                    checkdebugCmd(rest);
                    break;
                case "calc":
                    Integer val = Package.convertNegByteValue(Byte.parseByte(rest));
                    System.out.println(val);
                    break;
                case "show":
                    switch (rest) {
                        case "stream":
                            System.out.println(ConfigStreamListener.getDataStreamAsHex());
                            break;
                        case "mem":
                            Double temp = Runtime.getRuntime().totalMemory() / 1000000.0;
                            System.out.println("---------------------------------\n"
                                    + "Total Mem: " + temp + "MB");
                            temp = Runtime.getRuntime().freeMemory() / 1000000.0;
                            System.out.println("Free Mem: " + temp + "MB");
                            temp = Runtime.getRuntime().maxMemory() / 1000000.0;
                            System.out.println("Max Mem: " + temp + "MB"
                                    + "\n---------------------------------");
                            break;
                        case "ip":
                            try {
                                System.out.println("------------------------------------------\n"
                                        + "Connected to CS2 at: " + UDPReceiver.getCS2IP().toString()
                                        + "\n------------------------------------------");
                            } catch (Exception e) {
                                System.out.println("No Connection is active!");
                            }
                            break;
                        case "threads":
                            break;
                        default:
                            if (rest.contains("channel")) {
                                String nr = rest.substring(rest.indexOf(" ") + 1, rest.length());
                                switch (nr) {
                                    case "1":
                                        UDPSender.sendBytes(Packet.ASK_SYSTEM_STATUS_CH);
                                        break;
                                }
                            }
                            break;
                    }
                    break;
                case "connection":
                    //TODO
                    break;
                case "exit":
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                guiControllerWrapper.getController().handleApplicationClose();
                            } catch (IOException ex) {
                                CSLogger.logEvent(2, ex, "Stopping Network Services failed");
                            }
                        }
                    });
                    break;
                case "cmd":
                    String subCmd = null;
                    String adr = null;
                    String rest2 = null;
                    String decoder = null;
                    String speedStr = null;
                    ArrayList<Integer> speeds = new ArrayList<>();
                    ArrayList<Integer> adresses = new ArrayList<>();

                    if (rest.contains("[") || rest.contains("]")) {
                        subCmd = rest.substring(0, rest.indexOf("["));
                    } else {
                        subCmd = rest;
                        if (subCmd.contains("lokinfo")) {
                            rest2 = subCmd.substring(subCmd.indexOf(" ") + 1, subCmd.length());
                            subCmd = subCmd.substring(0, subCmd.indexOf(" "));
                        }
                    }
                    subCmd = subCmd.toLowerCase();

                    switch (subCmd) {
                        case "setspeed":
                            adr = rest.substring(rest.indexOf("[") + 1, rest.indexOf("]"));
                            rest2 = rest.substring(rest.indexOf(" ") + 1, rest.length());
                            speedStr = rest2.substring(rest2.indexOf("[") + 1, rest2.indexOf("]"));
                            if (rest2.indexOf(" ") != -1) {
                                decoder = rest2.substring(rest2.indexOf(" ") + 1, rest2.length());
                            }
                            if (adr.indexOf(",") != -1) {
                                for (String a : adr.split(",")) {
                                    adresses.add(Integer.parseInt(a));
                                }
                            } else {
                                adresses.add(Integer.parseInt(adr));
                            }

                            if (speedStr.indexOf(",") != -1) {
                                for (String s : rest2.split(",")) {
                                    speeds.add(Integer.parseInt(s));
                                }
                            } else {
                                for (int i = 0; i < adresses.size(); i++) {
                                    speeds.add(Integer.parseInt(speedStr));
                                }
                            }

                            if (speeds.size() > 1 && speeds.size() != adresses.size()) {
                                System.out.println("Adresses and speed values not matching!");
                                return;
                            }
                            for (int i = 0; i < adresses.size(); i++) {
                                if (decoder != null) {
                                    switch (decoder) {
                                        case "dcc":
                                            Package.setSpeed(adresses.get(i), CSDecoder.DCC, speeds.get(i));
                                            break;
                                        case "mfx":
                                            Package.setSpeed(adresses.get(i), CSDecoder.MFX, speeds.get(i));
                                            break;
                                        case "mm2":
                                            Package.setSpeed(adresses.get(i), CSDecoder.MM2, speeds.get(i));
                                            break;
                                    }
                                } else {
                                    Package.setSpeed(adresses.get(i), CSDecoder.DCC, speeds.get(i));
                                    Package.setSpeed(adresses.get(i), CSDecoder.MM2, speeds.get(i));
                                    Package.setSpeed(adresses.get(i), CSDecoder.MFX, speeds.get(i));
                                }
                            }
                            break;
                        case "setfunction":
                            break;
                        case "setdirection":
                            break;
                        case "setarticle":
                            break;
                        case "stop":
                            if (rest2 == null) {
                                UDPSender.sendBytes(Packet.GLOBAL_STOP);
                            }
                            break;
                        case "go":
                            if (rest2 == null) {
                                UDPSender.sendBytes(Packet.GLOBAL_GO);
                            }
                            break;
                        case "hold":
                            if (rest2 == null) {
                                UDPSender.sendBytes(Packet.GLOBAL_HOLD);
                            }
                            break;
                        case "checkspeed":
                            break;
                        case "lokinfo":
                            Package.requestLokInfo(rest2);
                            break;
                    }
                    break;
                case "save":
                    String path = System.getProperty("user.dir") + "\\saves";
                    if (rest.contains("\\")) {
                        path = rest;
                    } else {
                        path += "\\" + rest;
                    }
                    try (PrintStream out = new PrintStream(new FileOutputStream(path))) {
                        String saveStr = "Console-Log " + Calendar.getInstance().getTime().toString() + "\n\n"
                                + guiControllerWrapper.getController().getConsoleLog();
                        out.print(saveStr);
                    }
                    System.out.println("Console-Log was saved to: " + path);
                    break;
                case "help":
                    switch (rest) {
                        case "cmd":

                            break;
                        default:
                            System.out.println(HelperUtils.getHelpTextFile("console"));
                            break;
                    }
                    break;
            }
        } else {
            String[] temp = cmd.split(" ");
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != " " && temp[i] != "") {
                    if (temp[i].length() == 1 && !temp[i].matches("[0-9]")) {
                        buff.add((byte) temp[i].toCharArray()[0]);
                    } else {
                        Integer val = Integer.parseInt(temp[i], 16);
                        buff.add(val.byteValue());
                    }
                }
            }
            if (buff.size() < 13) {
                for (int i = buff.size(); i < 13; i++) {
                    buff.add((byte) 0);
                }
            }
            for (int i = 0; i < 13; i++) {
                finalBuff[i] = buff.get(i);
            }
            UDPSender.sendBytes(finalBuff);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.network;

import java.net.SocketException;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Philipp
 */
public class SystemListener extends Thread {

    private static SystemListener instance;
    
    private static byte[] buffer;
    private static IntegerProperty state;

    private SystemListener() throws SocketException {
        super();
        CoreListener.getInstance();
        buffer = new byte[13];
        state = new SimpleIntegerProperty(-1);
    }

    public static SystemListener getInstance() throws SocketException {
        if (SystemListener.instance == null) {
            SystemListener.instance = new SystemListener();
        }
        return SystemListener.instance;
    }

    @Override
    public void run() {
        CoreListener.bufferStringProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            buffer = CoreListener.getBuffer();
            if (buffer[1] == 1){
                if (buffer[9] == 0 && (buffer[5] == 0 && buffer[6] == 0)) {                    
                    state.set(0);
                } else if (buffer[9] == 1 && (buffer[5] == 0 && buffer[6] == 0)) {                    
                    state.set(1);
                } else if (buffer[9] == 1 && (buffer[5] == 0 && buffer[6] == 0)) {                    
                    state.set(2);
                }
            }
        });
    }
    
    public static IntegerProperty systemStateProperty() {
        return state;
    }

}

package cs2suite.network;

import cs2suite.logic.CSLogger;
import cs2suite.csObjects.CSArticle;
import cs2suite.csObjects.CSDecoder;
import cs2suite.csObjects.CSTrain;
import java.io.IOException;
import java.net.DatagramPacket;

/**
 *
 * @author Philipp
 *
 * ---------------------------------------------------------------------------
 * Diese Klasse stellt alle normalen und statischen Buffer-Pakete zum senden zur
 * Verfügung und implementiert alle nötigen Berechnungsmethoden um Adressen und
 * andere Integer-Werte in die entsprechenden byte-Werte umzuwandeln.
 *
 * Es ist außerdem die Klasse in der alle informationen über ein gesendetes bzw.
 * empfangenes Paket entahlten sind. Die Klasse kann auch vom 'UDPSender'
 * gesendet werden.
 * ---------------------------------------------------------------------------
 */
public class Package {

    private static Package instance;

    private static byte[] buffer;
    private static Boolean state;
    private static Integer address;
    private static Integer value;
    private static Integer type;
    private static Integer functionNumber;
    private static Integer functionValue;
    private static String stateName;

    /**
     * Type mapping: 1 = System 9 = speed
     *
     */
    private Package() {
        super();
        buffer = new byte[13];
        state = false;
        address = 0;
        value = -1;
        functionNumber = -1;
        functionValue = -1;
        stateName = "";
    }

    public static Package getInstance() {
        if (Package.instance == null) {
            Package.instance = new Package();
        }
        return Package.instance;
    }

    private static byte[] setFixedBytes() {
        byte[] buffer = new byte[13];
        buffer = Packet.NULL_BYTE.clone();
        buffer[2] = 47;
        buffer[3] = 11;
        return buffer;
    }

    public static String bufferToString(byte[] buff) {
        String result = "";
        for (byte b : buff) {
            StringBuilder sb = new StringBuilder();
            sb.append(String.format("%02X ", b));
            result += sb.toString();
        }
        return result;
    }
    
    public static void setTrainHold(CSTrain train) throws IOException{
        byte[] buffer = setFixedBytes();
        buffer[1] = 0x00;
        buffer[4] = 0x05;
        byte[] adrBytes = calc2BitAdress(train.getAddress(), train.getDecoderType());
        buffer[7] = adrBytes[0];
        buffer[8] = adrBytes[1];
        UDPSender.sendBytes(buffer);
    }
    
    public static void sendDirectionRequest(CSTrain train) throws IOException{
        byte[] buffer = setFixedBytes();
        buffer[1] = 0x0a;
        buffer[4] = 0x04;
        byte[] adrBytes = calc2BitAdress(train.getAddress(), train.getDecoderType());
        buffer[7] = adrBytes[0];
        buffer[8] = adrBytes[1];
        UDPSender.sendBytes(buffer);
    }
    
    public static byte[] getDirectionPackage(Integer adr, Integer dir, String decoder){
        byte[] tempBuffer = setFixedBytes();
        tempBuffer[1] = 0x0a;
        tempBuffer[4] = 0x05;
        byte[] adrBytes = calc2BitAdress(adr, decoder);
        tempBuffer[7] = adrBytes[0];
        tempBuffer[8] = adrBytes[1];
        tempBuffer[9] = dir.byteValue(); // direction
        return tempBuffer;
    }
    
    public static void setDirection(CSTrain train) throws IOException{
        byte[] buffer = getDirectionPackage(train.getAddress(), train.getDirection(), train.getDecoderType());
        UDPSender.sendBytes(buffer);
    }
    
    public static void toggleDirection(CSTrain train) throws IOException{
        byte[] buffer = getDirectionPackage(train.getAddress(), 0x03, train.getDecoderType());
        UDPSender.sendBytes(buffer);
        sendDirectionRequest(train);
    }
    
    public static byte[] getFunctionPackage(Integer adr, Integer funcNr, Integer state){
        byte[] tempBuffer = setFixedBytes();
        tempBuffer[1] = 0x0c;
        tempBuffer[4] = 0x06;
        byte[] adrBytes = calc2BitAdress(adr, "mm2");
        tempBuffer[7] = adrBytes[0];
        tempBuffer[8] = adrBytes[1];
        tempBuffer[9] = funcNr.byteValue();
        tempBuffer[10] = state.byteValue();
        return tempBuffer;        
    }
    
    public static void setFunction(Integer adr, Integer state, Integer funcNr) throws IOException{
        byte[] buffer = getFunctionPackage(adr, funcNr, state);
        UDPSender.sendBytes(buffer);
    }

    public static byte[] getArticlePackage(Integer adr, Integer state, String decoder) {
        byte[] tempBuffer = setFixedBytes();
        tempBuffer[1] = 0x16;
        tempBuffer[4] = 0x06;
        byte[] adrBytes = calcArticleAddressBytes(adr, decoder);
        tempBuffer[7] = adrBytes[0];
        tempBuffer[8] = adrBytes[1];
        tempBuffer[9] = state.byteValue();
        tempBuffer[10] = 0x01;
        return tempBuffer;
    }

    public static byte[] getArticlePackage(CSArticle article) {
        byte[] tempBuffer = getArticlePackage(article.getAddress(), article.getCurState(), article.getDecoder());
        return tempBuffer;
    }

    public static void setArticle(CSArticle article) throws IOException {
        byte[] buffer = getArticlePackage(article);
        UDPSender.sendBytes(buffer);
    }

    public static void setArticle(Integer adr, Integer state, String decoder) throws IOException {
        byte[] buffer = getArticlePackage(adr, state, decoder);
        UDPSender.sendBytes(buffer);
    }

    private static byte[] calcArticleAddressBytes(Integer adr, String decoder) {
        byte[] result = new byte[2];
        Integer adrVal = null;
        if (decoder == CSDecoder.MM2) {
            if (adr <= 256) {
                result[0] = 0x30;
                adrVal = adr - 1;
            } else {
                Integer temp = Math.floorDiv(adr, 256);
                temp--;
                result[0] = (byte) (0x39 + temp.byteValue());
                adrVal = adr - ((temp + 1) * 256) - 1;
            }
            result[1] = adrVal.byteValue();
        } else {

        }
        return result;
    }

    @Deprecated
    public static Integer parseArticleAddress(byte[] bytes) {
        Integer result = null;
        String byteString = bufferToString(bytes);
        String[] hexValues = byteString.split(" ");
        if (bytes[0] == 0x30) {
            result = Integer.parseInt(hexValues[1], 16) + 1;
        } else if (bytes[0] == 0x31) {
            result = 257 + Integer.parseInt(hexValues[1], 16);
        } else {
            int fac = (bytes[0] - 0x39) + 0x01;
            int pre = fac * 257;
            result = pre + Integer.parseInt(hexValues[1], 16) - 1;
        }
        return result;
    }

    /*
     triggers a config-data-stream with the train informations
     as part auf the lokomotive.cs2.
     receiving should be handled via changelisteners
     */
    public static void requestLokInfo(String lok) throws IOException {
        byte[] tempBuffer = setFixedBytes();
        char[] lokinfo = "lokinfo".toCharArray();
        char[] lokname = lok.toCharArray();
        if (lokname.length > 16) {
            char[] temp = new char[16];
            System.arraycopy(lokname, 0, temp, 0, 16);
            lokname = new char[16];
            lokname = temp.clone();
        }

        tempBuffer[0] = 0x20;
        tempBuffer[1] = 0x40;
        tempBuffer[4] = 0x08;
        for (int i = 5; i < (5 + lokinfo.length); i++) {
            tempBuffer[i] = (byte) lokinfo[i - 5];
        }
        UDPSender.sendBytes(tempBuffer);

        byte[] buffer2 = setFixedBytes();
        buffer2[0] = 0x20;
        buffer2[1] = 0x40;
        buffer2[4] = 0x08;
        tempBuffer = buffer2.clone();

        for (int i = 0; i < lokname.length; i++) {
            if (i < 8) {
                tempBuffer[5 + i] = (byte) lokname[i];
            } else {
                buffer2[5 + (i - 8)] = (byte) lokname[i];
            }
        }

        UDPSender.sendBytes(tempBuffer);
        UDPSender.sendBytes(buffer2);
    }

    public static byte[] speedPackage(Integer adr, String decoder, Integer speed) {
        byte[] buffer = new byte[13];
        buffer = setFixedBytes();
        buffer[1] = 8;
        buffer[4] = 6;

        if (decoder != CSDecoder.DCC && decoder != CSDecoder.MFX && decoder != CSDecoder.MM2) {
            CSLogger.logEvent(4, null, "Unknown Decoder Protocol: " + decoder);
            System.out.println("Unknown Decoder Protocol: " + decoder);
        }
        byte[] adrBytes = calc2BitAdress(adr, decoder);
        byte[] speedBytes = calcSpeedBytes(speed);
        buffer[7] = adrBytes[0];
        buffer[8] = adrBytes[1];
        buffer[9] = speedBytes[0];
        buffer[10] = speedBytes[1];
        return buffer;
    }

    public static void setSpeed(Integer adr, String decoder, Integer speed) throws IOException {
        byte[] tempBuffer = speedPackage(adr, decoder, speed);
        UDPSender.sendBytes(tempBuffer);
    }

    public static void setSpeed(CSTrain train) throws IOException {
        setSpeed(train.getAddress(), train.getDecoderType(), train.getSpeed());
    }

    public static byte[] calcSpeedBytes(Integer speed) {
        String temp = "";
        String hex1 = "";
        String hex2 = "";
        byte[] result = new byte[2];
        result[0] = 0x00;
        result[1] = 0x00;
        if (speed > 255) {
            temp = Integer.toHexString(speed);
            if (temp.length() < 4) {
                temp = "0".concat(temp);
            }
            hex1 = temp.substring(0, 2);
            hex2 = temp.substring(2, 4);
            result[0] = Byte.valueOf(hex1);
            Integer l = Integer.parseInt(hex2, 16);
            result[1] = l.byteValue();
        } else {
            result[1] = speed.byteValue();
        }
        return result;
    }

    public static Integer convertNegByteValue(byte byteVal) {
        //make byte unsigned
        if (byteVal < 0) {
            return byteVal & 0xFF;
        } else {
            return null;
        }
    }

    public static Integer bytesToSpeed(byte b1, byte b2) {
        return bytesToSpeed(new byte[]{b1, b2});
    }

    public static Integer bytesToSpeed(byte[] bytes) {
        String byteString = bufferToString(bytes);
        String[] hexValues = byteString.split(" ");
        if (hexValues[0] == "00") {
            return Integer.parseInt(hexValues[1], 16);
        } else {
            byteString = byteString.replace(" ", "");
            return Integer.parseInt(byteString, 16);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Berechnet die Integer Adresse des Artikels oder Zugs anhand der
     * entsprechenden Bytes. Dabei werden nur Addressen von DCC oder MFX
     * Dekodern übersetzt, da MM2- dekoder generell nur ein Byte als Adresse
     * belegen, weshalb diese dann direkt ausgelesen werden kann ohne das zwite
     * Byte zu beachten
     * -------------------------------------------------------------------------
     *
     * @param bytes
     * @return
     */
    public static Integer bytesToAddress(byte[] bytes) {
        Integer result = 0;
        String byteString = bufferToString(bytes);
        String[] hexValues = byteString.split(" ");
        Integer val1 = Integer.parseInt(hexValues[0], 16);
        Integer val2 = Integer.parseInt(hexValues[1], 16);

        if (val1 >= 64 && val1 < 192) { // address is bigger thaan 255 -> must be calculated
            result = val1 - 64;
            result = (result * 253) + val2;
            result -= 2;
        } else if (val1 >= 192) {
            result = val1 - 192;
            result = (result * 193) + val2;
        } else {
            result = val2;
        }
        return result;
    }

    public static Integer bytesToAddress(byte b1, byte b2) {
        return bytesToAddress(new byte[]{b1, b2});
    }

    public static byte[] calc2BitAdress(Integer addr, String decoder) {
        byte[] result = new byte[2];
        switch (decoder) {
            case CSDecoder.MM2:
                if (addr <= 256) {
                    result[0] = 0;
                    //addr--;
                    result[1] = addr.byteValue();
                } else if (addr <= 320) {
                    result[0] = 49;
                    addr -= 257;
                    result[1] = addr.byteValue();
                }
                break;
            case CSDecoder.DCC:
                if (addr <= 256) {
                    result[0] = (byte) 192;
                    //addr--;
                    result[1] = addr.byteValue();
                } else {
                    Integer temp = Math.floorDiv(addr, 255);
                    temp += 192;
                    result[0] = temp.byteValue();
                    addr = addr - (temp * 257);
                    result[1] = addr.byteValue();
                }
                break;
            case CSDecoder.MFX:
                if (addr <= 253) {
                    result[0] = 64;
                    addr += 2;
                    result[1] = addr.byteValue();
                } else {
                    Integer temp = Math.floorDiv(addr, 253);
                    temp += 64;
                    result[0] = temp.byteValue();
                    addr = addr - (temp * 253);
                    result[1] = addr.byteValue();
                }
                break;
        }
        return result;
    }

    public void setRawBuffer(byte[] buf) {
        if (buf.length == 13) {
            buffer = buf.clone();
        }
    }

    public void setStateName(String name) {
        stateName = name;
    }

    public void setFunctionValue(Integer fval) {
        this.functionValue = fval;
    }

    public void setFunctionNumber(Integer num) {
        this.functionNumber = num;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public void setAddress(Integer addr) {
        this.address = addr;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public byte[] getRawBuffer() {
        return buffer;
    }

    public Boolean getState() {
        return state;
    }

    public Integer getFunctionValue() {
        return functionValue;
    }

    public Integer getFunctionNumber() {
        return functionNumber;
    }

    public Integer getType() {
        return type;
    }

    public Integer getValue() {
        return value;
    }

    public Integer getAddress() {
        return address;
    }

    public String getStateName() {
        return this.stateName;
    }
}

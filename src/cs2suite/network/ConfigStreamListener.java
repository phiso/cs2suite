/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.network;

import cs2suite.logic.CSLogger;
import cs2suite.csFile.CSFile;
import cs2suite.utils.HelperUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import static org.apache.commons.lang3.ArrayUtils.toPrimitive;

/**
 *
 * @author Philipp
 */
public class ConfigStreamListener extends Thread {

    private static ConfigStreamListener instance;

    private static byte[] primitiveDataStream;
    private static ArrayList<Byte> dataStream;
    private static IntegerProperty dataStreamLength;
    private static byte[] buffer;
    private static Integer expectedStreamLength;
    private static BooleanProperty streamDone;
    private static StringProperty streamIdentifier;

    private ConfigStreamListener() throws SocketException {
        super();
        dataStreamLength = new SimpleIntegerProperty();
        primitiveDataStream = null;
        buffer = new byte[13];
        dataStream = new ArrayList<>();
        expectedStreamLength = 0;
        streamDone = new SimpleBooleanProperty(false);
        streamIdentifier = new SimpleStringProperty("");
        CoreListener.getInstance();
    }

    public static ConfigStreamListener getInstance() throws SocketException {
        if (ConfigStreamListener.instance == null) {
            ConfigStreamListener.instance = new ConfigStreamListener();
        }
        return ConfigStreamListener.instance;
    }

    @Override
    public void run() {
        CoreListener.bufferStringProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            buffer = CoreListener.getBuffer();
            if (buffer[1] == PacketParser.CS_CONFIG_DATA_STREAM) {
                switch (buffer[4]) {
                    case 0x06: //starting config-data-stream  
                        byte[] lengthBytes = new byte[4];
                        for (int i = 0; i < 4; i++) {
                            lengthBytes[i] = buffer[5 + i];
                        }
                        String total = Package.bufferToString(lengthBytes);
                        String[] hexLength = total.split(" ");
                        total = "";
                        for (String s : hexLength) {
                            if (s != "00") {
                                total = total.concat(s);
                            }
                        }
                        expectedStreamLength = Integer.parseInt(total, 16);
                        dataStream.clear();
                        streamDone.set(false);
                        primitiveDataStream = null;
                        CSLogger.logEvent(10, null, "Stream Receiving starting");
                        System.out.println("Receiving Stream...");
                        break;
                    case 0x07:
                        break;
                    case 0x08: //running data-stream
                        for (int i = 5; i < 13; i++) {
                            if (dataStream.size() < expectedStreamLength) {
                                dataStream.add(buffer[i]);
                            } else if (buffer[i] != 0) {
                                CSLogger.logEvent(4, null, "Unknown Config-Data-Stream Byte (" + buffer[i] + ") on index " + (dataStream.size() + 1) + " expected Length was " + expectedStreamLength);
                            }
                        }
                        primitiveDataStream = toPrimitive(dataStream.toArray(new Byte[dataStream.size()]));
                        if (expectedStreamLength == primitiveDataStream.length) {
                            streamDone.set(true);
                            CSLogger.logEvent(10, null, "Stream Receiving complete");
                            System.out.println("Stream Received: " + getDataStreamLength() + "/" + expectedStreamLength);
                            //primitiveDataStream[0] = (byte) Integer.parseInt("78", 16);
                            //primitiveDataStream[1] = (byte) Integer.parseInt("01", 16);
                        }
                        break;
                }
            } else if (buffer[1] == PacketParser.CS_CONFIG_DATA_STREAM_RESP) {                
                char[] temp = new char[8];
                for (int i = 5; i < buffer.length; i++) {
                    if (buffer[i] != 0) {
                        temp[i - 5] = (char) buffer[i];
                    }
                }
                streamIdentifier.set(new String(temp));
                CSLogger.logEvent(10, null, "Receiving");
            }
        });
    }

    public static String getDataStreamAsText() {
        char[] chars = new char[primitiveDataStream.length];
        for (int i = 0; i < primitiveDataStream.length; i++) {
            chars[i] = (char) primitiveDataStream[i];
        }
        return new String(chars);
    }

    public static byte[] getDataStream() {
        return primitiveDataStream;
    }

    public static String getDataStreamAsHex() {
        return Package.bufferToString(primitiveDataStream);
    }

    public static Integer getDataStreamLength() {
        return primitiveDataStream.length;
    }

    public static BooleanProperty streamCompleteProperty() {
        return streamDone;
    }

    public static CSFile getStreamAsCSFile() {
        return new CSFile(getDataStreamAsText());
    }
    
    public static StringProperty streamIdentifierProperty(){
        return streamIdentifier;
    }

    /*
     saves the actual datat Stream in plaintext
     */
    public static void dump(File file) throws IOException {
        HelperUtils.saveStringToFile(file, getDataStreamAsText());
    }

    public static void dumpBytes(File file, byte[] bytes) throws IOException {
        FileOutputStream fos = new FileOutputStream(file.getAbsolutePath());
        fos.write(bytes);
        fos.close();
    }

}

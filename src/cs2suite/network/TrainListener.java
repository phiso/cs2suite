/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.network;

import cs2suite.csObjects.CSTrain;
import java.net.SocketException;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Philipp
 */
public class TrainListener extends Thread{
    
    private static TrainListener instance;
    
    private static CSTrain train;
    private static byte[] buffer;
    
    private static IntegerProperty address;
    private static IntegerProperty speed;
    private static IntegerProperty direction;
    private static IntegerProperty function;
    private static IntegerProperty functionValue;        
    
    private TrainListener() throws SocketException{
        super();
        PacketParser.getInstance();        
        buffer = new byte[13];
        address = new SimpleIntegerProperty();
        speed = new SimpleIntegerProperty(0);
        direction = new SimpleIntegerProperty();
        function = new SimpleIntegerProperty();
        functionValue = new SimpleIntegerProperty();
        train = new CSTrain();
    }
    
    public static TrainListener getInstance() throws SocketException{
        if (TrainListener.instance == null){
            TrainListener.instance = new TrainListener();
        }
        return TrainListener.instance;
    }
    
    @Override
    public void run(){
        CoreListener.bufferStringProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            buffer = CoreListener.getBuffer();
            switch (buffer[1]){
                case 9: //Speed
                    address.set(Package.bytesToAddress(buffer[7], buffer[8]));  
                    speed.set(Package.bytesToSpeed(buffer[9], buffer[10]));
                    break;
                case 11: //Direction
                    address.set(Package.bytesToAddress(buffer[7], buffer[8])); 
                    direction.set(buffer[9]);
                    break;
                case 13: //Function
                    address.set(Package.bytesToAddress(buffer[7], buffer[8])); 
                    function.set(buffer[9]);
                    functionValue.set(buffer[10]);
                    break;
            }
        });        
    }
    
    /**
     * @return the address
     */
    public static IntegerProperty addressProperty() {
        return address;
    }

    /**
     * @return the speed
     */
    public static IntegerProperty speedProperty() {
        return speed;
    }

    /**
     * @return the direction
     */
    public static IntegerProperty directionProperty() {
        return direction;
    }

    /**
     * @return the function
     */
    public static IntegerProperty functionProperty() {
        return function;
    }

    /**
     * @return the functionValue
     */
    public static IntegerProperty functionValueProperty() {
        return functionValue;
    }
}

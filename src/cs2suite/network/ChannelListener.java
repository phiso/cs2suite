/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.network;

import java.net.SocketException;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Philipp
 */
public class ChannelListener extends Thread{
    
    private static ChannelListener instance;
    
    private static byte[] buffer;
    
    private static DoubleProperty channel1;
    private static DoubleProperty channel2;
    private static DoubleProperty channel3;
    private static DoubleProperty channel4;
   
    private ChannelListener() throws SocketException{
        super();
        CoreListener.getInstance();
        buffer = new byte[13];        
        channel1 = new SimpleDoubleProperty(0.);
        channel2 = new SimpleDoubleProperty(0.);
        channel3 = new SimpleDoubleProperty(0.);
        channel4 = new SimpleDoubleProperty(0.);
    }
    
    public static ChannelListener getInstance() throws SocketException {
        if (ChannelListener.instance == null){
            ChannelListener.instance = new ChannelListener();
        }
        return ChannelListener.instance;
    }
    
    @Override
    public void run(){
        CoreListener.bufferStringProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            buffer = CoreListener.getBuffer();
            if (buffer[1] == 0x01 && buffer[9] == 0x0B){
                switch (buffer[10]){
                    case 1:
                        channel1.set(0.0);
                        break;
                    case 2:
                        channel2.set(0.0);
                        break;
                    case 3:
                        channel3.set(0.0);
                        break;
                    case 4:
                        channel4.set(0.0);
                        break;
                }
            }
        });
    }

    /**
     * @return the channel1
     */
    public static DoubleProperty getChannel1Property() {
        return channel1;
    }

    /**
     * @return the channel2
     */
    public static DoubleProperty getChannel2Property() {
        return channel2;
    }

    /**
     * @return the channel3
     */
    public static DoubleProperty getChannel3Property() {
        return channel3;
    }

    /**
     * @return the channel4
     */
    public static DoubleProperty getChannel4Property() {
        return channel4;
    }
}

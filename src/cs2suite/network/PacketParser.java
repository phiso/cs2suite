/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.network;

import cs2suite.logic.Settings;
import java.net.SocketException;
import java.util.Calendar;
import java.util.Date;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Philipp
 */
public class PacketParser extends Thread {

    public static final byte CS_SYSTEM = (byte) 0x01;
    public static final byte CS_SPEEDCHANGE = (byte) 0x09;
    public static final byte CS_S88POLLING = (byte) 0x23;
    public static final byte CS_PING_REQUEST = (byte) 0x30;
    public static final byte CS_PING_SUCCESS = (byte) 0x31;
    public static final byte CS_CONFIG_DATA_STREAM = (byte) 0x42;
    public static final byte CS_CONFIG_DATA_STREAM_RESP = (byte) 0x41;

    private static PacketParser instance;
    private static Calendar calendar;
    private static Date now;
    private static byte[] buffer;
    private static IntegerProperty csState;
    private static IntegerProperty articleState;
    private static IntegerProperty articleAddress;
    private static IntegerProperty trainAddress;
    private static IntegerProperty trainSpeed;
    private static IntegerProperty trainDirection;
    private static IntegerProperty trainFunctionNr;
    private static IntegerProperty trainFunctionValue;

    private PacketParser() throws SocketException {
        super();
        buffer = new byte[13];
        CoreListener.getInstance();
        csState = new SimpleIntegerProperty(-1);
        trainAddress = new SimpleIntegerProperty();
        trainSpeed = new SimpleIntegerProperty();
        trainDirection = new SimpleIntegerProperty();
        trainFunctionNr = new SimpleIntegerProperty();
        trainFunctionValue = new SimpleIntegerProperty();
        articleAddress = new SimpleIntegerProperty();
        articleState = new SimpleIntegerProperty();
    }

    public static PacketParser getInstance() throws SocketException {
        if (PacketParser.instance == null) {
            PacketParser.instance = new PacketParser();
        }
        return PacketParser.instance;
    }

    @Override
    public void run() {
        CoreListener.hostAdressProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            System.out.println("New Host: " + newValue);
        });

        CoreListener.bufferStringProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            buffer = CoreListener.getBuffer();
            if (Settings.getConsoleLogging()) {
                calendar = Calendar.getInstance();
                now = calendar.getTime();
                System.out.println(now.toString() + " - @{" + newValue + "}   " + parseCmd(buffer));
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////
    /*
     * Parsing Funtions
     */
    //////////////////////////////////////////////////////////////////////        
    private static String parseCmd(byte[] parseData) {
        Integer cmdByte = Integer.parseInt(String.valueOf(parseData[1]));
        String result = "";
        switch (cmdByte) {
            case 1://response to cmd:0 -> System
                result = "<System>";
                result += parseUID(parseData);
                switch (parseData[9]) {
                    case 0:
                        if (parseData[5] == 0 && parseData[6] == 0) {
                            result += " Stop";
                            csState.set(0);
                        }
                        break;

                    case 1:
                        if (parseData[5] == 0 && parseData[6] == 0) {
                            result += " Go";
                            csState.set(1);
                        }
                        break;
                    case 0x0b:
                        result += "<System-Status>";
                        result += "<Channel " + parseData[10] + "> ";
                        result += Package.bytesToSpeed(parseData[11], parseData[12]);
                        break;
                }
                break;
            case 9:
                result = "<Speed changed>";
                trainAddress.set(Package.bytesToAddress(parseData[7], parseData[8]));
                byte[] speedBytes = new byte[]{parseData[9], parseData[10]};
                trainSpeed.set(Package.bytesToSpeed(speedBytes));
                result += " Adr. " + trainAddress.get() + " set to: " + trainSpeed.get();
                break;
            case 11: // Train Direction
                result = "<Direction Changed>";
                trainAddress.set(Package.bytesToAddress(parseData[7], parseData[8]));
                trainDirection.set(parseData[9]);
                result += "<Adr. " + trainAddress.get() + ">";
                switch (trainDirection.get()) {
                    case 0:
                        result += "<Backwards>";
                        break;
                    case 1:
                        result += "<Forward>";
                        break;
                    case 2:
                        result += "<Toggled>";
                        break;
                }
                break;
            case 13: // Train Function      
                result = "<Function>";
                trainAddress.set(Package.bytesToAddress(parseData[7], parseData[8]));
                trainFunctionNr.set(parseData[9]); // max. 0-31 no conversion needed
                trainFunctionValue.set(parseData[10]); // boolean 0/1
                result += "<Adr. " + trainAddress.get() + "><Function:" + trainFunctionNr.get() + ">";
                switch (trainFunctionValue.get()) {
                    case 0:
                        result += "<Off>";
                        break;
                    case 1:
                        result += "<On>";
                        break;
                }
                break;
            case 23: // Article Switching                
                articleAddress.set(Package.convertNegByteValue(parseData[8])+1);
                result = "<Article><Adr. "+articleAddress.get()+">";
                switch (parseData[9]){
                    case 0x00:
                        result += "<Aus, Rund, Rot, Rechts, HP0>";
                        break;
                    case 0x01:
                        result += "<Ein, Grün Gerade, HP1>";
                        break;
                    case 0x10:
                        result += "<Gelb, Links, HP2>";
                        break;
                    case 0x11:
                        result += "<Weiss, SH0>";
                        break;
                }   
                break;
            case 41:
                result = "<Config-Data-Stream>";
                char[] temp = new char[8];
                for (int i = 5; i < parseData.length; i++) {
                    if (parseData[i] != 0) {
                        temp[i - 5] = (char) parseData[i];
                    }
                }
                result += " " + new String(temp);
                break;
            case 48:
                result = parseUID(parseData);
                result += "<Ping request>";
                break;
            case 49:
                result = parseUID(parseData);
                result += "<Ping Success>";
                break;
            case 66:  // Config-Data-Stream
                switch (parseData[4]) {
                    case 6: // start of transfer                        
                        result = "<Config-Data-Stream starting...>";
                        break;
                    case 7:
                        break;
                    case 8: // running transfer                        
                        result = "<Config-Data-Stream>";
                        break;
                    default:
                        break;
                }
                break;
        }
        return result;
    }
    /*
     parses known UIDs
     input: complete 13byte package    
     */

    private static String parseUID(byte[] data) {
        if (data[5] == 67 && data[6] == 83 && data[7] == -49 && data[8] == 74) {
            return "<Central-Station>";
        }
        return "";
    }

    public static Integer getCSState() {
        return csState.get();
    }

    public static IntegerProperty csStateProperty() {
        return csState;
    }
}

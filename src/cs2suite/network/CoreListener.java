/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.network;

import cs2suite.logic.CSLogger;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Philipp
 */
public class CoreListener extends Thread {

    private static CoreListener instance;

    private static DatagramSocket socket;
    private static DatagramPacket packet;
    private static byte[] buffer;
    private static InetAddress inetAddress;

    private static BooleanProperty empty;   
    private static ObjectProperty addressObject;
    private static StringProperty address;
    private static StringProperty bufferString;
    

    private CoreListener() throws SocketException {
        super();
        buffer = new byte[13];        
        empty = new SimpleBooleanProperty(true);
        address = new SimpleStringProperty();
        addressObject = new SimpleObjectProperty();
        socket = new DatagramSocket(Packet.CS2_RECEIVE_PORT);
        packet = new DatagramPacket(buffer, buffer.length);        
        bufferString = new SimpleStringProperty();
        CoreListener.hostAdressProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            empty.set(false);
        });
    }

    public static CoreListener getInstance() throws SocketException {
        if (CoreListener.instance == null) {
            CoreListener.instance = new CoreListener();
        }
        return CoreListener.instance;
        
    }

    @Override
    public void run() {
        while (true) {
            try {
                socket.receive(packet);                                                                 
                inetAddress = packet.getAddress();
                addressObject.set(inetAddress);                
                address.set(packet.getAddress().getHostAddress());                
                bufferString.set(Package.bufferToString(buffer));
            } catch (IOException ex) {
                CSLogger.logEvent(2, ex, "Error in CoreReceiver (receiving failed)");
            }
        }
    }

    public static Boolean isEmpty(){
        return empty.get();
    }
    
    public static BooleanProperty emptyProperty(){
        return empty;
    }
    
    public static String getHostAdress(){
        return address.get();
    }
    
    public static StringProperty hostAdressProperty(){
        return address;
    }    
    
    public static InetAddress getInetAddress(){
        return inetAddress;
    }
    
    public static Object getInetAddressObject(){
        return addressObject.get();
    }
    
    public static ObjectProperty inetAddressProperty(){
        return addressObject;
    }
    
    public static StringProperty bufferStringProperty(){
        return bufferString;
    }
    
    public static byte[] getBuffer(){
        return buffer;
    }
}

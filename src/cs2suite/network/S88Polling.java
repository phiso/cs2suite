/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.network;

import java.net.SocketException;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Philipp
 * S88 Polling
 */
public class S88Polling extends Thread{
    
    private static S88Polling instance;
    private static IntegerProperty s88Index;
    private static IntegerProperty newState;
    private static IntegerProperty oldState;
    private static byte[] buffer;    
    
    private S88Polling() throws SocketException{
        s88Index = new SimpleIntegerProperty(0);
        newState = new SimpleIntegerProperty(0);
        oldState = new SimpleIntegerProperty(0);
        CoreListener.getInstance();
    }
    
    public static S88Polling getInstance() throws SocketException{
        if (S88Polling.instance == null){
            S88Polling.instance = new S88Polling();
        }
        return S88Polling.instance;
    }
    
    @Override
    public void run(){
        CoreListener.bufferStringProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            buffer = CoreListener.getBuffer();
            if (buffer[1] == PacketParser.CS_S88POLLING){
                switch (buffer[4]){
                    case 0x08:
                        s88Index.set(buffer[8]); //TODO handle bigger numbers than 255
                        oldState.set(buffer[9]);
                        newState.set(buffer[10]);
                        break;
                }
            }
        });
    }
    
    /**
     * @return the s88Index
     */
    public static IntegerProperty getS88IndexProperty() {
        return s88Index;
    }
    
    public static int getS88Index(){
        return s88Index.get();
    }

    /**
     * @return the newState
     */
    public static IntegerProperty getNewStateProperty() {
        return newState;
    }
    
    public static int getNewState(){
        return newState.get();
    }

    /**
     * @return the oldState
     */
    public static IntegerProperty getOldStateProperty() {
        return oldState;
    }
    
    public static int getOldState(){
        return oldState.get();
    }
}

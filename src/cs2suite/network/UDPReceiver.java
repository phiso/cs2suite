/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.network;

import cs2suite.logic.CSLogger;
import cs2suite.logic.Settings;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Calendar;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Philipp uses Singleton schematic
 * -----------------------------------------------------------------------------
 * Diese Klasse implementiert einen Statischen TimerTask der ständig die
 * Broadcasts einer entsprechend konfigurierten Central-Station 2 empfängt und
 * auswertet. Die Klasse wird im Singleton -Schema implementiert um ständigen
 * Zugriff von aussen zu gewährleisten. Außerdem kann ohnehin immer nur eine
 * Insatnz dieser Klasse existieren, da nicht mehrere Listener auf einen Port
 * gebunden werden können
 * -----------------------------------------------------------------------------
 */
@Deprecated
public class UDPReceiver extends Thread {

    private static UDPReceiver instance;

    private static DatagramSocket socket;
    private static DatagramPacket dgPacket;
    private static byte[] buffer;
    private static Package packet;
    private static Integer port;
    private static byte[] lastReceived;
    private static byte[] response;
    private static byte[] checkResponse;
    private static Boolean listenForResponse;
    private static Boolean consoleLog;
    private static Boolean firstRec;
    private static Calendar calendar;
    private static java.util.Date now;
    private static String log;
    private static Boolean isResponse;
    private static Boolean isConfigDataStream;
    private static SimpleIntegerProperty state;
    private static SimpleIntegerProperty foundCS;

    private UDPReceiver() throws SocketException {
        calendar = Calendar.getInstance();
        state = new SimpleIntegerProperty();
        foundCS = new SimpleIntegerProperty(-1);

        state.addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            switch (newValue.intValue()) {
                case -1:
                    if (isResponse) {
                        log += " -RESP- ";
                    }
                    String temp_log = UDPPackageInterpreter.parse(lastReceived);
                    log += " -> " + temp_log;
                    if (temp_log.contains(" Stop ")) {
                        state.set(0);
                    }
                    if (temp_log.contains(" Go ")) {
                        state.set(1);
                    }
                    break;
                case 0:
                    log += " - System is Stopped";                    
                    break;
                case 1:
                    log += " - System is Running";                    
                    break;
            }
        });
        UDPReceiver.firstRec = true;
        UDPReceiver.response = null;
        UDPReceiver.isResponse = false;
        UDPReceiver.port = Packet.CS2_RECEIVE_PORT;
        UDPReceiver.buffer = new byte[13];
        UDPReceiver.lastReceived = new byte[13];
        UDPReceiver.socket = new DatagramSocket(UDPReceiver.port);
        UDPReceiver.dgPacket = new DatagramPacket(buffer, buffer.length);
        UDPReceiver.consoleLog = true;
        UDPReceiver.listenForResponse = false;
        UDPReceiver.consoleLog = true;
        UDPReceiver.firstRec = true;
        UDPSender.getInstance();
    }

    public static synchronized UDPReceiver getInstance() throws SocketException {
        if (UDPReceiver.instance == null) {
            UDPReceiver.instance = new UDPReceiver();
        }
        return UDPReceiver.instance;
    }

    private static Boolean compareRecTo(byte[] b) {
        for (int i = 0; i < lastReceived.length; i++) {
            if (lastReceived[i] != b[i]) {
                return false;
            }
        }
        return true;
    }

    private static Integer isStopped() { //TODO: korrekte werte in cspacket einsetzen
        if (compareRecTo(Packet.SYSTEM_RUNNING_STATUS_0) || compareRecTo(Packet.SYSTEM_RUNNING_STATUS_1)) {
            return 1;
        } else if (compareRecTo(Packet.SYSTEM_STOPPED_STATUS_0) || compareRecTo(Packet.SYSTEM_STOPPED_STATUS_1)) {
            return 0;
        }
        return -1;
    }

    private static String getStringFromData(DatagramPacket dgp) {
        String result = "";
        for (byte b : dgp.getData()) {
            StringBuilder sb = new StringBuilder();
            sb.append(String.format("%02X ", b));
            result += sb.toString();
        }
        return result;
    }

    public static String getLog() {
        return log;
    }

    public void listenForResponse() {
        listenForResponse = true;
    }

    public static Boolean isResponded() {
        return isResponse;
    }

    public static void stopLogging() {
        consoleLog = false;
    }

    public static void startLogging() {
        consoleLog = true;
    }

    public static Integer getSystemState() {
        return state.get();
    }

    public static SimpleIntegerProperty getStateProperty() {
        return state;
    }
    
    public static SimpleIntegerProperty getFoundCSProperty() {
        return foundCS;
    }
    
    public static InetAddress getCS2IP() {
        return dgPacket.getAddress();
    }

    @Override
    public void run() {
        while (true) {
            try {
                socket.receive(dgPacket);
                lastReceived = buffer;
                calendar = Calendar.getInstance();
                now = calendar.getTime();
                log = "";
                log = now.toString() + " >>> ";
                log += getStringFromData(dgPacket);
                String temp_log = UDPPackageInterpreter.parse(lastReceived);
                log += " -> " + temp_log;
            } catch (IOException ex) {
                CSLogger.logEvent(3, ex, "Error receiving Package");
            }

            if (listenForResponse) {
                response = lastReceived;
                listenForResponse = false;
            }

            if (firstRec && Settings.getAutoIPSetting()) {
                InetAddress temp = dgPacket.getAddress();
                try {
                    UDPSender.initinstance(temp);
                    CSLogger.logEvent(111, null, "Initialised UDPSender");
                    foundCS.set(1);
                } catch (SocketException ex) {
                    CSLogger.logEvent(4, ex, "Auto IP-Setting failes due to problems in Sender Class");
                    foundCS.set(0);
                }
                firstRec = false;
            }

            if (lastReceived[1] == UDPSender.getLastBuff()[1] + 1) {
                isResponse = true;
            } else {
                isResponse = false;
            }

            state.set(isStopped());

            if (Settings.getConsoleLogging()) {
                System.out.println(log);
            }

            CSLogger.logEvent(10, null, getStringFromData(dgPacket));
        }
    }

}

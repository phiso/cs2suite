/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.network;

/**
 *
 * @author Philipp
 */
public class Packet {
    public static final byte[] NULL_BYTE = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    public static final byte[] GLOBAL_STOP = new byte[]{0x00, 0x00, 0x47, 0x11, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    public static final byte[] GLOBAL_GO = new byte[]{0x00, 0x00, 0x47, 0x11, 0x05, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00};
    public static final byte[] GLOBAL_HOLD = new byte[]{0x00, 0x00, 0x47, 0x11, 0x05, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00};
    public static final byte[] DEVICE_RECOGNITION = new byte[]{0x00, 0x00, 0x47, 0x11, 0x05, 0x00, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00};
    
    //Config-Data-anfragen (ZLib komprimiert)
    public static final byte[] CONFIG_DATA_LOKS = new byte[]{0x20, 0x40, 0x47, 0x11, 0x08, 0x6c, 0x6f, 0x6b, 0x73, 0x00, 0x00, 0x00, 0x00};
    public static final byte[] CONFIG_DATA_MAGS = new byte[]{0x20, 0x40, 0x47, 0x11, 0x08, 0x6d, 0x61, 0x67, 0x73, 0x00, 0x00, 0x00, 0x00};
    public static final byte[] CONFIG_DATA_GBS = new byte[]{0x20, 0x40, 0x47, 0x11, 0x08, 0x67, 0x62, 0x73, 0x00, 0x00, 0x00, 0x00, 0x00};
    public static final byte[] CONFIG_DATA_FS = new byte[]{0x20, 0x40, 0x47, 0x11, 0x08, 0x66, 0x73, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    public static final byte[] CONFIG_DATA_LOKSTAT = new byte[]{0x20, 0x40, 0x47, 0x11, 0x08, 0x6c, 0x6f, 0x6b, 0x73, 0x74, 0x61, 0x74, 0x00};
    public static final byte[] CONFIG_DATA_MAGSTAT = new byte[]{0x20, 0x40, 0x47, 0x11, 0x08, 0x6d, 0x61, 0x67, 0x73, 0x74, 0x61, 0x74, 0x00};
    public static final byte[] CONFIG_DATA_GBSSTAT = new byte[]{0x20, 0x40, 0x47, 0x11, 0x08, 0x67, 0x62, 0x73, 0x73, 0x74, 0x61, 0x74, 0x00};       
    
    //public static final byte[] SYSTEM_RUNNING_STATUS_0 = new byte[]{(byte) 0, (byte) 0, (byte) 3, (byte) 0, (byte) 6, (byte) 67, (byte) 83, (byte) -49, (byte) 74, (byte) 48, (byte) 1, (byte) 0, (byte) 0};
    public static final byte[] SYSTEM_RUNNING_STATUS_0 = new byte[]{0, 0, 3, 0, 6, 67, 83, -49, 74, 48, 1, 0, 0};
    public static final byte[] SYSTEM_RUNNING_STATUS_1 = new byte[]{0, 0, 3, 0, 6, 67, 83, -49, 74, 48, 0, 0, 0};
    //public static final byte[] SYSTEM_RUNNING_STATUS_1 = new byte[]{(byte) 0, (byte) 0, (byte) 3, (byte) 0, (byte) 6, (byte) 67, (byte) 83, (byte) -49, (byte) 74, (byte) 48, (byte) 0, (byte) 0, (byte) 0};

    //public static final byte[] SYSTEM_STOPPED_STATUS_0 = new byte[]{(byte) 0, (byte) 54, (byte) 99, (byte) 24, (byte) 0, (byte) 67, (byte) 83, (byte) -49, (byte) 74, (byte) 48, (byte) 0, (byte) 0, (byte) 0};
    public static final byte[] SYSTEM_STOPPED_STATUS_0 = new byte[]{0, 54, 99, 24, 0, 67, 83, -49, 74, 2, 31, 0, 0};
    public static final byte[] SYSTEM_STOPPED_STATUS_1 = new byte[]{0, 48, 99, 24, 0, 67, 83, -49, 74, 2, 31, 0, 0};
    //public static final byte[] SYSTEM_STOPPED_STATUS_2 = new byte[]{0, 49, 27, 12, 8, 67, 83, -49, 74, 2, 31, 0, 0};
    //public static final byte[] SYSTEM_STOPPED_STATUS_1 = new byte[]{(byte) 0, (byte) 48, (byte) 99, (byte) 24, (byte) 0, (byte) 67, (byte) 83, (byte) -49, (byte) 74, (byte) 48, (byte) 0, (byte) 0, (byte) 0};
    //public static final byte[] SYSTEM_STOPPED_STATUS_2 = new byte[]{(byte) 0, (byte) 49, (byte) 27, (byte) 12, (byte) 8, (byte) 67, (byte) 83, (byte) -49, (byte) 74, (byte) 2, (byte) 31, (byte) 0, (byte) 0};

    public static final byte[] ASK_SYSTEM_STATUS_CH = new byte[]{0x00, 0x00, 0x47, 0x11, 0x06, 0x43, 0x53, -49 /*0xcf*/, 0x4a, 0x0B, 0x01, 0x00, 0x00};    
    public static final Integer CS2_RECEIVE_PORT = 15730;
    public static final Integer CS2_SEND_PORT = 15731;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.guiElements;

import cs2suite.csObjects.CSLayoutObject;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;

/**
 *
 * @author Philipp
 */
public class LayoutObjectTableCell {
      
    private String name;
    private ArrayList<Image> images;
    private ArrayList<String> imagePaths;
    private CSLayoutObject parentLayoutObject;
        
    public LayoutObjectTableCell(CSLayoutObject obj){
        this.name = obj.getName();       
        imagePaths = new ArrayList<>();
        images = new ArrayList<>();
        for (int i = 0; i < obj.getImages().size(); i++){
            images.add(obj.getImage(i));
            imagePaths.add(obj.getImagePath(i));
        }
        parentLayoutObject = obj;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the images
     */
    public ArrayList<Image> getImages() {
        return images;
    }
    
    public String getImagePath(int index){
        return imagePaths.get(index);
    }

    /**
     * @param images the images to set
     */
    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }
    
    public Image getImage(int index){
        return images.get(index);
    }

    /**
     * @return the parentLayoutObject
     */
    public CSLayoutObject getParentLayoutObject() {
        return parentLayoutObject;
    }    
}
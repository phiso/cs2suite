/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.guiElements;

import cs2suite.csObjects.CSFunction;
import cs2suite.csObjects.CSTrain;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Philipp
 */
public class FunctionCell {

    private String name;
    private String picPath;
    private String filename;
    private Integer nr;
    private Boolean on;
    // private CSTrain train;
    private final CSFunction function;

    public FunctionCell(CSFunction function) {
        this.function = function;
        this.name = function.getName();
        this.picPath = function.getIconPath();
        this.filename = function.getIconFilename();
        this.nr = function.getNr();
        this.on = function.getONState();
        
        function.iconPathProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            this.picPath = newValue;
            this.filename = function.getIconFilename();
        });
        function.nameProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            this.name = newValue;
        });
        function.nrProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            this.nr = newValue.intValue();
        });
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the picPath
     */
    public String getPicPath() {
        return picPath;
    }

    /**
     * @param picPath the picPath to set
     */
    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    /**
     * @return the nr
     */
    public Integer getNr() {
        return nr;
    }

    /**
     * @param nr the nr to set
     */
    public void setNr(Integer nr) {
        this.nr = nr;
    }

    /**
     * @return the function
     */
    public CSFunction getFunction() {
        return function;
    }
    
    public void setOn(Boolean on){
        this.on = on;
    }
    
    public Boolean getOnState(){
        return on;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csObjects;

import java.io.IOException;

/**
 *
 * @author Philipp
 */
public class CSArticleTemplate extends CSKeyboardObject{
    
    public static final int TEMPLATE_ID_STD = 10;
    
    private String images;
    private Integer sql_id;

    public CSArticleTemplate(String type, int states, String images) throws IOException {
        super(type);       
        setStates(states);        
        this.images = images;
        setImages(images);
    }
    
    public String getRawImageString(){
        return images;
    }
    
    public void setSqlId(Integer id){
        sql_id = id;
    }
    
    public Integer getSqlId(){
        return sql_id;
    }
}

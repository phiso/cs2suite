/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csObjects;

import cs2suite.locale.Internationalization;
import cs2suite.fxmlControllers.cs2SuiteGUIController;
import cs2suite.fxmlControllers.guiControllerWrapper;
import cs2suite.utils.SqliteDriver;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Philipp
 */
public class CSKeyboard {

    public static final Integer MAX_ADDRESS = 2048;

    private static CSKeyboard instance;
    private static ArrayList<CSArticle> allArticles;
    private static ArrayList<CSArticleTemplate> articleTemplates;

    private CSKeyboard() throws SQLException, IOException {
        allArticles = new ArrayList<>();
        articleTemplates = SqliteDriver.getArticleTemplates();        
        guiControllerWrapper.getController().setProgress(0.0);                
        guiControllerWrapper.getController().setInfoText3(Internationalization.getString("keyboardready"));
    }

    public static CSKeyboard getInstance() throws SQLException, IOException {
        if (CSKeyboard.instance == null) {
            CSKeyboard.instance = new CSKeyboard();
        }
        return CSKeyboard.instance;
    }
    
    public static void initArticles(){
        Double progressAddValue = 1/CSKeyboard.MAX_ADDRESS.doubleValue();
        for (int i = 1; i <= 2048; i++) {  // init all articles as standart
            allArticles.add(new CSArticle(articleTemplates.get(CSArticleTemplate.TEMPLATE_ID_STD), "Adr. "+i, i, 200));
            guiControllerWrapper.getController().addProgress(progressAddValue);
        }        
    }
    
    public static void setArticles(ArrayList<CSArticle> articles){
        for (CSArticle art : articles){
            int index = art.getAddress()-1;
            allArticles.set(index, art);
        }
    }
    
    public static CSArticle getArticle(int index){
        return allArticles.get(index);
    }
    
    public static Integer getArticleSize(){
        return allArticles.size();
    }
    
    public static ObservableList<CSArticleTemplate> getArticleTemplatesObs(){
        ObservableList<CSArticleTemplate> results = FXCollections.observableArrayList();
        results.setAll(articleTemplates);
        return results;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csObjects;

import cs2suite.CS2Suite;
import cs2suite.locale.Internationalization;
import cs2suite.logic.CSLogger;
import cs2suite.logic.Settings;
import cs2suite.fxmlControllers.cs2SuiteGUIController;
import cs2suite.fxmlControllers.guiControllerWrapper;
import cs2suite.network.ArticleListener;
import cs2suite.network.ConfigStreamListener;
import cs2suite.network.CoreListener;
import cs2suite.network.Package;
import cs2suite.network.PacketParser;
import cs2suite.network.S88Polling;
import cs2suite.network.SystemListener;
import cs2suite.network.TrainListener;
import cs2suite.network.UDPSender;
import cs2suite.utils.SqliteDriver;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import org.ini4j.Ini;

/**
 *
 * @author Philipp
 */
public class CSSystem {

    private ArrayList<CSLayout> layouts;
    private ArrayList<CSArticle> articles;
    private ArrayList<CSLinkedAction> linkedActions;
    private ArrayList<CSTrain> trains;
    private IntegerProperty systemState;
    private Integer s88Controllers;
    private ArrayList<CSS88Pin> s88Pins;
    private String name;
    private Integer keyboardPage = 0;

    public static final String DEFAULT_SYSTEM = "default_system";

    public CSSystem(String name, boolean createFromDefault) throws SQLException, IOException, ClassNotFoundException {
        super();
        initStatics();
        if (createFromDefault) {
            loadSystem(CSSystem.DEFAULT_SYSTEM);
        }
        this.name = name;
        layouts = new ArrayList<>();
        articles = new ArrayList<>();
        linkedActions = new ArrayList<>();
        trains = new ArrayList<>();
        s88Pins = new ArrayList<>();
        systemState = new SimpleIntegerProperty(1);
        s88Controllers = Settings.getS88Controllers();
        for (int i = 0; i < s88Controllers * 16; i++) {
            s88Pins.add(new CSS88Pin("Pin" + i, i));
        }
        articles = SqliteDriver.getArticles();
    }

    public CSSystem(String name) throws SQLException, IOException, ClassNotFoundException {
        this(name, true);
    }

    private void initStatics() {
        Package.getInstance();
        try {
            CoreListener.getInstance().start();
        } catch (SocketException ex) {
            CSLogger.logEvent(2, ex, "Failed initializing CoreReceiver");
        }

        CoreListener.inetAddressProperty().addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                InetAddress address = (InetAddress) newValue;
                try {
                    UDPSender.initinstance(address);
                } catch (SocketException ex) {
                    CSLogger.logEvent(2, ex, "Failed to initialize Sender");
                }
            }

        });

        try {
            TrainListener.getInstance().start();
        } catch (SocketException ex) {
            CSLogger.logEvent(2, ex, "Failed to start TrainListener");
        }

        try {
            SystemListener.getInstance().start();
        } catch (SocketException ex) {
            CSLogger.logEvent(3, ex, "Failed to start SystemListener");
        }

        try {
            PacketParser.getInstance().start();
        } catch (SocketException ex) {
            CSLogger.logEvent(3, ex, "Failed to start PacketParser");
        }

        try {
            S88Polling.getInstance().start();
        } catch (SocketException ex) {
            CSLogger.logEvent(4, ex, "Failed to start s88-Polling Thread");
        }

        try {
            ConfigStreamListener.getInstance().start();
        } catch (SocketException ex) {
            CSLogger.logEvent(3, ex, "Failed to start ConfigStreamListene");
        }

        try {
            CSKeyboard.getInstance();
        } catch (SQLException | IOException ex) {
            CSLogger.logEvent(2, ex, "Error initialising Keyboard");
        }

        try {
            ArticleListener.getInstance().start();
        } catch (SocketException ex) {
            CSLogger.logEvent(3, ex, "Error starting ArticleListener");
        }

        CSKeyboard.initArticles();

        guiControllerWrapper.getController().getKeyboardController().initKeyboard();
    }

    public void resestControllerStates() {
        for (CSTrain t : trains) {
            t.setControlledByController(false);
        }
    }

    private void createSystemIni(File file) throws IOException {
        Ini ini = new Ini();
        Ini.Section mainSection = ini.add("System");
        mainSection.add("Name", name);
        mainSection.add("keyboardPage", keyboardPage);
        mainSection.add("s88Controller", s88Controllers);
        //TODO
        ini.store(file);
    }

    public void saveSystem(String filename, boolean tempFlag) throws FileNotFoundException, IOException {
        createSystemIni(new File(CS2Suite.TEMP_DIR + "tempSystem.ini"));
        FileOutputStream fos;
        if (tempFlag) {
            fos = new FileOutputStream(CS2Suite.TEMP_DIR + filename + "_temp.csdb");
        } else {
            fos = new FileOutputStream(CS2Suite.SAVES_DIR + filename + ".csdb");
        }
        ZipOutputStream zos = new ZipOutputStream(fos);
        ArrayList<String> files = new ArrayList<>();

        files.add(CS2Suite.TEMP_DIR + "tempSystem.ini");
        files.add(CS2Suite.CONF_DIR + "config.ini");
        files.add(CS2Suite.DATA_DIR + "data.db");

        for (int i = 0; i < files.size(); i++) {
            try {
                addToZipFile(files.get(i), zos);
            } catch (IOException ex) {
                CSLogger.logEvent(2, ex, "Error while saving System/Project");
            }
        }

        zos.close();
        fos.close();
        guiControllerWrapper.getController().setInfoText1(Internationalization.getString("savedSystem"));
    }

    public void loadSystem(String filename) throws IOException, SQLException, ClassNotFoundException {
        File saveFile = new File(CS2Suite.SAVES_DIR + filename + ".csdb");
        ZipFile zipFile = new ZipFile(saveFile);
        Enumeration<?> enu = zipFile.entries();
        while (enu.hasMoreElements()) {
            ZipEntry zipEntry = (ZipEntry) enu.nextElement();
            String name = zipEntry.getName();
            long size = zipEntry.getSize();
            long compSize = zipEntry.getCompressedSize();
            CSLogger.logEvent(101, null, String.format("name: %-20s | size: %6d | compressed size: %6d\n",
                    name, size, compSize));

            File file = new File(name);
            if (name.endsWith("/")) {
                file.mkdirs();
                continue;
            }

            File parent = file.getParentFile();
            if (parent != null) {
                parent.mkdirs();
            }

            InputStream is = zipFile.getInputStream(zipEntry);
            FileOutputStream fos = new FileOutputStream(file);
            byte[] bytes = new byte[1024];
            int length;
            while ((length = is.read(bytes)) >= 0) {
                fos.write(bytes, 0, length);
            }
            is.close();
            fos.close();
        }
        createSystemFromTemp();
        updateArticles();
        updateTrains();
    }

    private void createSystemFromTemp() throws IOException, ClassNotFoundException, SQLException {
        Settings.resetInstance();
        SqliteDriver.getInstance(CS2Suite.DATA_DIR + "data.db", true);
        Ini ini = new Ini();
        ini.load(new File(CS2Suite.TEMP_DIR + "tempSystem.ini"));
        Ini.Section mainSection = ini.get("System");
        name = mainSection.get("Name");
        keyboardPage = Integer.parseInt(mainSection.get("keyboardPage"));
        s88Controllers = Integer.parseInt(mainSection.get("s88Controller"));
        //guiControllerWrapper.getController().getGamePadSettingsController().applySettings();
        //TODO
    }

    private void addToZipFile(String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {

        CSLogger.logEvent(8, null, "Writing '" + fileName + "' to csdb save");

        File file = new File(fileName);
        FileInputStream fis = new FileInputStream(file);
        ZipEntry zipEntry = new ZipEntry(fileName);
        zos.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }

        zos.closeEntry();
        fis.close();
    }

    public void updateArticles() throws SQLException, IOException {
        articles = SqliteDriver.getArticles();
        CSKeyboard.setArticles(articles);
    }

    public void updateTrains() throws SQLException {
        trains = SqliteDriver.getTrains();
        //guiControllerWrapper.getController().loadTrainTable(trains);
    }

    public void setState(int state) {
        systemState.set(state);
    }

    public IntegerProperty getSystemStateProperty() {
        return systemState;
    }

    public Integer getSystemState() {
        return systemState.get();
    }

    public String getSystemName() {
        return name;
    }

    /**
     * @return the keyboardPage
     */
    public Integer getKeyboardPage() {
        return keyboardPage;
    }

    /**
     * @param keyboardPage the keyboardPage to set
     */
    public void setKeyboardPage(Integer keyboardPage) {
        this.keyboardPage = keyboardPage;
    }

    /**
     * @return the articles
     */
    public ArrayList<CSArticle> getArticles() {
        return articles;
    }

    /**
     * @return the linkedActions
     */
    public ArrayList<CSLinkedAction> getLinkedActions() {
        return linkedActions;
    }

    /**
     * @return the trains
     */
    public ArrayList<CSTrain> getTrains() {
        return trains;
    }

}

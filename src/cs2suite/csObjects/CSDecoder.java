/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csObjects;

/**
 *
 * @author Philipp
 */
public class CSDecoder {

    public static final String MM2 = "mm2";
    public static final String DCC = "dcc";
    public static final String MFX = "mfx";

    private static CSDecoder instance;

    private CSDecoder() {
        super();
    }

    public static CSDecoder getInstance() {
        if (CSDecoder.instance == null) {
            CSDecoder.instance = new CSDecoder();
        }
        return CSDecoder.instance;
    }

    public static String parseDecoder(String dec) {
        if (dec == "mm2" || dec == "mm2_dil8" || dec == "mm2_prg") {
            return CSDecoder.MM2;
        } else if (dec == "dcc") {
            return CSDecoder.DCC;
        } else if (dec == "mfx") {
            return CSDecoder.MFX;
        }else{
            return CSDecoder.MM2;
        }
    }
}

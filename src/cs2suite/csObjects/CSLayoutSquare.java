/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csObjects;

import java.awt.Point;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Philipp
 */
public class CSLayoutSquare {

    private IntegerProperty rotationValue;
    private BooleanProperty occupied;
    private Point position;
    private CSLayoutObject layoutObject;    

    public CSLayoutSquare() {
        super();
        occupied = new SimpleBooleanProperty(false);
        rotationValue = new SimpleIntegerProperty(0);
        layoutObject = null;
    }

    public CSLayoutSquare(CSLayoutObject layoutObj) {
        this();
        this.setLayoutObject(layoutObj);
    }

    public boolean isOccupied() {
        return occupied.get();
    }

    public BooleanProperty getOccupiedProperty() {
        return occupied;
    }

    public void setOccupied(boolean bool) {
        this.occupied.set(bool);
    }

    public int getRotation() {
        return rotationValue.get();
    }
    
    public void setRotation(int angle){
        rotationValue.set(angle);
    }
    
    public void rotate(){
        switch (rotationValue.get()){
            case 0:
                rotationValue.set(90);
                break;
            case 90:
                rotationValue.set(180);
                break;
            case 180:
                rotationValue.set(270);
                break;
            case 270:
                rotationValue.set(0);
                break;
        }
    }

    public IntegerProperty getRotationProperty() {
        return rotationValue;
    }

    public void setLayoutObject(CSLayoutObject layoutObj) {
        layoutObject = layoutObj;
        if (layoutObj != null) {
            occupied.set(true);
        }
    }
    
    public CSLayoutObject getLayoutObject(){
        return layoutObject;
    }
}

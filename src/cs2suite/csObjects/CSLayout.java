/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csObjects;

import cs2suite.CS2Suite;
import cs2suite.fxmlControllers.cs2SuiteGUIController;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.ini4j.Ini;

/**
 *
 * @author Philipp
 */
public class CSLayout {

    private ArrayList<ArrayList<CSLayoutSquare>> layoutSquares;
    private Integer xCount;
    private Integer yCount;
    private StringProperty name;

    public CSLayout(int xCount, int yCount) {
        super();
        layoutSquares = new ArrayList<>();
        this.xCount = xCount;
        this.yCount = yCount;
        name = new SimpleStringProperty("");
        initSquareObjects();        
    }

    public CSLayout(File sourceFile) {
        //    this();
    }

    private void initSquareObjects() {
        for (int ix = 0; ix <= getxCount(); ix++) {
            ArrayList<CSLayoutSquare> yList = new ArrayList<>();
            for (int iy = 0; iy <= getyCount(); iy++) {
                yList.add(new CSLayoutSquare());
            }
            getLayoutSquares().add(yList);
        }
    }

    private void saveToIni(File file) throws IOException {
        Ini ini = new Ini();
        Ini.Section info = ini.add("Information");
        Ini.Section data = ini.add("Data");
        info.add("date", new Date().toString());
        info.add("name", name);
        for (int x = 0; x < getLayoutSquares().size(); x++) {
            ArrayList<CSLayoutSquare> tempList = getLayoutSquares().get(x);
            for (int y = 0; y < tempList.size(); y++) {
                if (tempList.get(y).getLayoutObject() != null) {
                    CSLayoutObject tempObj = tempList.get(y).getLayoutObject();
                    data.add(x + "_" + y, tempObj.getDb_id());
                }
            }
        }
        ini.store(file);
    }
    
    public void saveToFile(String name) throws IOException{
        this.name.set(name);
        File file = new File(CS2Suite.LAYOUT_DIR+name+".ini");
        saveToIni(file);
    }

    public CSLayoutSquare getSquare(int x, int y) {
        return getLayoutSquares().get(x).get(y);
    }

    /**
     * @return the layoutSquares
     */
    public ArrayList<ArrayList<CSLayoutSquare>> getLayoutSquares() {
        return layoutSquares;
    }

    /**
     * @return the xCount
     */
    public Integer getxCount() {
        return xCount;
    }

    /**
     * @return the yCount
     */
    public Integer getyCount() {
        return yCount;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name.get();
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name.set(name);
    }
    
    public StringProperty getNameProperty(){
        return name;
    }

    /**
     * @param xCount the xCount to set
     */
    public void setxCount(Integer xCount) {
        this.xCount = xCount;
        initSquareObjects();
    }

    /**
     * @param yCount the yCount to set
     */
    public void setyCount(Integer yCount) {
        this.yCount = yCount;
        initSquareObjects();
    }
}

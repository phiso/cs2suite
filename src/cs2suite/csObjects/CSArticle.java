/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csObjects;

import cs2suite.logic.CSLogger;
import cs2suite.network.ArticleListener;
import cs2suite.network.Package;
import cs2suite.utils.SqliteDriver;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Philipp
 */
public class CSArticle extends CSKeyboardObject{   
        
    public static final String STANDART = "std";
    public static final String LEFT_SWITCH = "leftSwitch";
    public static final String RIGHT_SWITCH = "rightSwitch";
    public static final String Y_SWITCH = "ySwitch";
    public static final String LIGHT0 = "light0";
    public static final String LIGHT1 = "light1";
    public static final String LIGHT2 = "light2";
    public static final String BARRIER = "barrier"; // DE:Bahnübergang
    public static final String THREE_WAY_SWITCH = "threeWaySwitch";
    public static final String CROSS_SWITCH_SIMPLE = "crossSwitch1";
    public static final String CROSS_SWITCH_DOUBLE = "crossSwitch2";
    public static final String ANDREWS_CROSS = "andrewsCross";
    public static final String DECLUTCH = "declutch"; // DE:Entkupplungsgleis
    public static final String K84 = "k84";
    public static final String FORMSIGNAL_HP01 = "formSignalHP01";
    public static final String FORMSIGNAL_HP02 = "formSignalHP02";
    public static final String FORMSIGNAL_SH01 = "formSignalSH01";
    public static final String URC_SIGNAL_HP01 = "lightSignalHP01";
    public static final String URC_SIGNAL_HP02 = "lightSignalHP02";
    public static final String URC_SIGNAL_SH01 = "urcLightSignalSH01";
    public static final String URC_SIGNAL_HP012_SH01 = "urcLightSignalHP012SH01";
    public static final String TURNTABLE = "turntable";
   
    
    private Integer timing; // timing in ms
    private Integer type;
    private Integer address;
    private Boolean fromTemplate;
    private String decoder;
    private CSArticleTemplate template;    
    
    public CSArticle(CSArticleTemplate template, String name, int adr, int timing){
        super(name);
        fromTemplate = true;
        this.template = template;
        address = adr;
        this.timing = timing;
        setStates(template.getStates());
        setImages(template.getRawImageString());
        decoder = CSDecoder.MM2;
        
        this.getCurStateProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            try {
                Package.setArticle(this);
            } catch (IOException ex) {
                CSLogger.logEvent(4, ex, "Error switching Article "+this.address);
                CSLogger.logEvent(110, ex, "Error switching Article "+this.address); // console out
            }
        });
        
        ArticleListener.getStateProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (ArticleListener.getAddress() == this.address){
                this.setState(newValue.intValue());
            }
        });
    }
    
    public void changeTemplate(CSArticleTemplate template){
        fromTemplate = true;
        this.template = template;
        setStates(template.getStates());
        setImages(template.getRawImageString());
    }
    
    public CSArticle(Integer adr) throws SQLException, IOException{                
        this(SqliteDriver.getArticleTemplate(CSArticle.STANDART),adr.toString(),adr,200);                
    }
    
    public Integer getAddress(){
        return address;
    }
    
    public String getDecoder(){
        return decoder;
    }

    /**
     * @return the timing
     */
    public Integer getTiming() {
        return timing;
    }

    /**
     * @param timing the timing to set
     */
    public void setTiming(Integer timing) {
        this.timing = timing;
    }

    /**
     * @return the template
     */
    public CSArticleTemplate getTemplate() {
        return template;
    }

    /**
     * @param decoder the decoder to set
     */
    public void setDecoder(String decoder) {
        this.decoder = decoder;
    }
    
    
}

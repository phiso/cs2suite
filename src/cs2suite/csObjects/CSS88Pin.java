/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csObjects;

import cs2suite.network.S88Polling;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Philipp
 */
public class CSS88Pin extends CSKeyboardObject{
    
    private Integer s88Index;
    private Integer pin;
    private IntegerProperty oldState;
    private IntegerProperty newState;

    public CSS88Pin(String name, int pin) {
        super(name);
        this.pin = pin;
        setState(0);
        oldState = new SimpleIntegerProperty(0);
        newState = new SimpleIntegerProperty(0);
        
        S88Polling.getNewStateProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (S88Polling.getS88Index() == pin){
                setState(newValue.intValue());
                newState.set(newValue.intValue());
                oldState.set(S88Polling.getOldState());
            }
        });
    }
    
    public IntegerProperty getOldStateProperty(){
        return oldState;
    }
    
    public IntegerProperty getNewStateProperty(){
        return newState;
    }
    
    public int getOldState(){
        return oldState.get();       
    }
    
    public int getNewState(){
        return newState.get();
    }
    
}

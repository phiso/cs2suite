/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csObjects;

import cs2suite.fxmlControllers.cs2SuiteGUIController;
import cs2suite.guiElements.LayoutObjectTableCell;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;
import org.ini4j.Ini;

/**
 *
 * @author Philipp
 */
public class CSLayoutObject extends CSKeyboardObject{

    public static final String CSLAYOUT_STRAIGHT = "straight";
    public static final String CSLAYOUT_CORNER = "corner";
    public static final String CSLAYOUT_ARROW = "arrow"; //Connect to other layouts
    public static final String CSLAYOUT_BUMPER = "bumper";
    public static final String CSLAYOUT_CROSS = "cross";
    public static final String CSLAYOUT_DECLUTCH = "declutch";
    public static final String CSLAYOUT_DOUBLE_CORNER = "doublecorner";
    public static final String CSLAYOUT_DOUBLE_SWITCH = "doubleswitch";
    public static final String CSLAYOUT_FORMSIGNAL_HP02 = "formsignalhp02";
    public static final String CSLAYOUT_LEFTSWITCH = "leftswitch";
    public static final String CSLAYOUT_RIGHTSWITCH = "rightswitch";
    public static final String CSLAYOUT_S88_CORNER = "s88corner";
    public static final String CSLAYOUT_S88_STRAIGHT = "s88straight";
    public static final String CSLAYOUT_SIGNAL = "signal";
    public static final String CSLAYOUT_SUBWAY = "subway";
    public static final String CSLAYOUT_THREEWAYSWITCH = "threewayswitch";
    public static final String CSLAYOUT_TRACK = "track";
    public static final String CSLAYOUT_TUNNEL = "tunnel";
    public static final String CSLAYOUT_YSWITCH = "yswitch";

    private StringProperty name;
    private ArrayList<String> imagePaths;
    private ArrayList<Image> images;
    private IntegerProperty state;
    private Integer maxState;
    private String type;
    private Boolean correlation;
    private Integer db_id;
    private Integer article_id;
    private ObjectProperty tableCell = new SimpleObjectProperty();

    private Object correlatedObject;

    public CSLayoutObject(String name, int maxState, boolean corr, ArrayList<String> imgList) throws IOException {
        super(name);
        Ini ini = new Ini();
        imagePaths = new ArrayList<>();
        String imageTrunk = System.getProperty("user.dir") + "\\data\\img\\layout\\";                
                
        this.name = new SimpleStringProperty(name);
        state = new SimpleIntegerProperty(0);
        this.maxState = maxState-1;
        correlation = corr;
        db_id = -1; // default not in database
        article_id = -1; // default no article linked

        BufferedImage bufImg;
        images = new ArrayList<>();
        if (maxState > 0) {
            for (int i = 0; i < maxState; i++) {
                File tempFile = new File(imageTrunk + imgList.get(i));
                imagePaths.add(tempFile.getAbsolutePath());
                bufImg = ImageIO.read(tempFile);
                Image img = SwingFXUtils.toFXImage(bufImg, null);
                images.add(img);
            }
        } else if (maxState == 0) {
            File tempFile = new File(imageTrunk + imgList.get(0));
            imagePaths.add(tempFile.getAbsolutePath());
            bufImg = ImageIO.read(tempFile);
            Image img = SwingFXUtils.toFXImage(bufImg, null);
            images.add(img);
        }
        setTableCell(new LayoutObjectTableCell(this));
    }

    public void toNextState() {
        if (state.get() < maxState) {
            state.set(state.get() + 1);
        } else if (state.get() == maxState) {
            state.set(0);
        }
        //TODO
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty getNameProperty() {
        return name;
    }

    public int getState() {
        return state.get();
    }

    public Image getImage(int index) {
        return getImages().get(index);
    }

    public String getImagePath(int index) {
        return imagePaths.get(index);
    }

    public boolean canCorrelate() {
        return correlation;
    }

    public int getMaxState() {
        return maxState;
    }

    public void setTableCell(LayoutObjectTableCell cell) {
        tableCell.set(cell);
    }

    public Object getTableCell() {
        return tableCell.get();
    }

    public ObjectProperty getTableCellProperty() {
        return tableCell;
    }

    /**
     * @return the images
     */
    public ArrayList<Image> getImages() {
        return images;
    }
    
    public Image getStateImage(){
        return this.getImage(state.get());
    }

    /**
     * @return the db_id
     */
    public Integer getDb_id() {
        return db_id;
    }

    /**
     * @param db_id the db_id to set
     */
    public void setDb_id(Integer db_id) {
        this.db_id = db_id;
    }

    /**
     * @return the article_id
     */
    public Integer getArticle_id() {
        return article_id;
    }

    /**
     * @param article_id the article_id to set
     */
    public void setArticle_id(Integer article_id) {
        this.article_id = article_id;
    }
}

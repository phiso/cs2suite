/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csObjects;

import cs2suite.logic.CSLogger;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;

/**
 *
 * @author Philipp
 */
public abstract class CSKeyboardObject {

    private Integer states;
    private IntegerProperty curState;
    private String name;
    private ArrayList<Image> stateImages;
    private ArrayList<String> stateImagePaths;
    private ObjectProperty tableCell;
    private StringProperty imageRessourceString;

    public CSKeyboardObject(String name) {
        super();
        this.tableCell = new SimpleObjectProperty();
        this.states = 1;
        curState = new SimpleIntegerProperty(0);
        this.name = name;
        stateImages = new ArrayList<>();
        stateImagePaths = new ArrayList<>();
        imageRessourceString = new SimpleStringProperty();
    }

    /**
     *
     * @param paths contains all images to add in one string (paths) seperated
     * by commas
     */
    public void setImages(String paths) {        
        String[] splits = paths.split(",");
        stateImagePaths.clear();
        for (String s : splits) {
            stateImagePaths.add(System.getProperty("user.dir") + "\\data\\img\\keyboard\\" + s);
            BufferedImage bufimg = null;
            try {
                bufimg = ImageIO.read(new File(System.getProperty("user.dir") + "\\data\\img\\keyboard\\" + s));
            } catch (IOException ex) {
                CSLogger.logEvent(2, ex, "Error loading Keyboard Image: " + s);
            }
            Image tempImg = SwingFXUtils.toFXImage(bufimg, null);
            stateImages.add(tempImg);
        }
        imageRessourceString.set(paths);
    }
    
    public String getImageRessourceString(){
        return imageRessourceString.get();
    }
    
    public StringProperty getImageRessourceProperty(){
        return imageRessourceString;
    }

    public Integer getStates() {
        return states;
    }

    public void setState(int state) {
        this.curState.set(state);
    }

    public void setStates(int states) {
        this.states = states;
    }

    public Integer getCurState() {
        return curState.get();
    }

    public IntegerProperty getCurStateProperty() {
        return curState;
    }

    public Image getImage(int index) {
        return stateImages.get(index);
    }

    public ArrayList<Image> getImages() {
        return stateImages;
    }

    public ObjectProperty getTableCellProperty() {
        return tableCell;
    }

    public void setTableCell(CSKeyboardObjectTableCell cell) {
        this.tableCell.set(cell);
    }

    public Object getTableCell() {
        return tableCell.get();
    }

    public String getName() {
        return this.name;
    }

    public String getImagePath(int index) {
        return stateImagePaths.get(index);
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}

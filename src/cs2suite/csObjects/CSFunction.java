/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csObjects;

import cs2suite.guiElements.FunctionCell;
import cs2suite.utils.CSIcons;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import org.ini4j.Ini;

/**
 *
 * @author Philipp
 */
public class CSFunction extends CSKeyboardObject{

    private SimpleIntegerProperty nr;
    private SimpleStringProperty name;
    private SimpleStringProperty iconPath;
    private SimpleStringProperty iconFilename;
    private Integer mode;
    private Integer timing;    
    private Integer trainAddress;
    private Image functionOffIcon;
    private Image functionOnIcon;
    private SimpleBooleanProperty isOn;
    private ObjectProperty tableCell = new SimpleObjectProperty();

    public Ini.Section makeIniSection(Ini.Section section) {
        section.add("Name", name);
        section.add("IconPath", iconPath);
        section.add("mode", mode);
        section.add("timing", timing);
        section.add("isOn", isOn);
        return section;
    }
    
    public CSFunction(Integer nr, Integer adr){
        super(nr.toString());        
        this.name = new SimpleStringProperty("none_"+nr);
        this.iconPath = new SimpleStringProperty(System.getProperty("user.dir")+"\\data\\img\\functions\\");
        this.iconFilename = new SimpleStringProperty("FktIcon_a_ge_"+(50+nr));
        functionOffIcon = CSIcons.getIcon(iconFilename.get()+"_off");
        functionOnIcon = CSIcons.getIcon(iconFilename.get()+"_on");
        this.nr = new SimpleIntegerProperty(nr);
        this.trainAddress = adr;
        isOn = new SimpleBooleanProperty(false);
        setFunctionCell(new FunctionCell(this));
    }

    public CSFunction(String name, String icon, Integer nr, Integer adr) {
        this(nr,adr);
        this.name.set(name);
        this.iconPath.set(icon);   
        this.nr.set(nr);
    }
    
    public Button getFunctionButton(){
        Button tempButton = new Button();
        tempButton.setStyle("-fx-graphic: url("+getIconPath()+");");
        
        return tempButton;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public void setIconPath(String path) {
        iconPath.set(path);
    }
  
    public void switchON() {
        isOn.set(true);
        setState(1);
    }

    public void switchOFF() {
        isOn.set(false);
        setState(0);
    }

    public void setMode(Integer mode) {
        this.mode = mode;
    }

    public void setTiming(Integer timing) {
        this.timing = timing;
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public String getIconPath() {
        return iconPath.get();
    }

    public SimpleStringProperty iconPathProperty() {
        return iconPath;
    }

    public Integer getNr() {
        return nr.get();
    }

    public SimpleIntegerProperty nrProperty() {
        return nr;
    }

    public void setNr(Integer nr) {
        this.nr.set(nr);
    }

    public Boolean getONState() {
        return isOn.get();
    }
    
    public SimpleBooleanProperty onProperty() {
        return isOn;
    }

    public String getName() {
        return name.get();
    }  

    public Integer getMode() {
        return mode;
    }

    public Integer getTiming() {
        return timing;
    }
    
    public void setFunctionCell(FunctionCell cell){
        this.tableCell.set(cell);
    }
    
    public ObjectProperty tableCellProperty() {
        return tableCell;
    }
    
    public Object getTableCell(){
        return tableCell.get();
    }

    /**
     * @return the trainAddress
     */
    public Integer getTrainAddress() {
        return trainAddress;
    }

    /**
     * @param trainAddress the trainAddress to set
     */
    public void setTrainAddress(Integer trainAddress) {
        this.trainAddress = trainAddress;
    }
    
    public String getIconFilename(){
        return iconFilename.get();
    }
    
    public SimpleStringProperty getIconFilenameProperty(){
        return iconFilename;
    }

    /**
     * @return the functionOffIcon
     */
    public Image getFunctionOffIcon() {
        return functionOffIcon;
    }

    /**
     * @return the functionOnIcon
     */
    public Image getFunctionOnIcon() {
        return functionOnIcon;
    }
}

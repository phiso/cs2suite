/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.csObjects;

import cs2suite.locale.Internationalization;
import cs2suite.logic.CSLogger;
import cs2suite.guiElements.FunctionCell;
import cs2suite.network.Package;
import cs2suite.network.TrainListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javax.imageio.ImageIO;
import org.ini4j.Ini;

/**
 *
 * @author Philipp
 */
public class CSTrain {

    private SimpleStringProperty profilePicture;
    private SimpleStringProperty decoderType;
    private SimpleStringProperty name;
    private SimpleStringProperty description;
    private SimpleIntegerProperty address;
    private SimpleIntegerProperty direction;
    private ObjectProperty trainTableCell = new SimpleObjectProperty();
    private byte[] hexAddress;
    private ArrayList<CSFunction> functions;
    private SimpleIntegerProperty speed;  //aktuelle Geschwindigkeit
    private Integer vMax;
    private Integer vMin;
    private Integer brakes; //bremsverzögerung
    private Integer acceleration;   //anfahrverzögerung
    private Integer volume; //lautstärke (0-100%)
    private SimpleIntegerProperty tacho;
    private Boolean hasCV;
    private Boolean changing;
    private BooleanProperty controlledByController;
    private Boolean activeOnController;
    private Double gamePadSlowMode = 1.;

    private ChangeListener listenForSpeed;
    private ChangeListener listenForDirection;
    private ChangeListener listenForStop;
    private ChangeListener listenForLight;

    public void exportToFile(File file) throws IOException {
        Ini ini = new Ini(file);
        Ini.Section section = ini.add(name.get());
        section.add("Descr", description);
        section.add("address", address);
        section.add("direction", direction);
        section.add("vMax", vMax);
        section.add("vMin", vMin);
        section.add("brakes", brakes);
        section.add("acceleration", acceleration);
        section.add("volume", volume);
        section.add("tacho", tacho);
        section.add("hasCV", hasCV);
        for (int i = 0; i < functions.size(); i++) {
            Ini.Section temp = section.addChild("Function " + i);
            temp = functions.get(i).makeIniSection(temp);
        }
    }

    public CSTrain(String name, String decoderType, int adr) {
        this();
        this.name = new SimpleStringProperty(name);
        address.set(adr);
        this.decoderType = new SimpleStringProperty(decoderType);
        initFunctions();       
    }

    public CSTrain() {
        super();
        hexAddress = new byte[4];
        this.name = new SimpleStringProperty("none");
        address = new SimpleIntegerProperty(-1);
        description = new SimpleStringProperty("no description");
        this.decoderType = new SimpleStringProperty("mm2");
        functions = new ArrayList<>();
        profilePicture = new SimpleStringProperty(System.getProperty("user.dir") + "\\data\\img\\trains\\keinbild.jpg");
        speed = new SimpleIntegerProperty(0);;
        vMax = 255;
        vMin = 0;
        brakes = 15;
        acceleration = 15;
        volume = 0;
        tacho = new SimpleIntegerProperty(180);  //maximaler Tachowert        
        hasCV = false;
        controlledByController = new SimpleBooleanProperty(false);
        direction = new SimpleIntegerProperty(0);        

        TrainListener.speedProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (TrainListener.addressProperty().get() == CSTrain.this.address.get()) {
                CSTrain.this.speed.set(newValue.intValue());
            }
        });

        TrainListener.directionProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (TrainListener.addressProperty().get() == CSTrain.this.address.get()) {
                CSTrain.this.direction.set(newValue.intValue());
            }
        });

        TrainListener.functionValueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (TrainListener.addressProperty().get() == CSTrain.this.address.get()) {
                int functionNr = TrainListener.functionProperty().get();
                int functionVal = TrainListener.functionValueProperty().get();
                if (functionVal == 0) {
                    functions.get(functionNr).switchOFF();
                } else {
                    functions.get(functionNr).switchON();
                }
            }
        });

    }

    public void initFunctions() {
        for (int i = 0; i < 16; i++) {
            functions.add(new CSFunction(i, this.address.get()));
        }
    }

    private byte[] addressToHex() {
        byte[] result = new byte[4];
        byte newAdr = 0;
        result[0] = 0x00;
        result[1] = 0x00;
        byte praefix = 0;
        switch (decoderType.get()) {
            case "DCC":
                praefix = (byte) (192 & 0xFF); //Hex: C0
                break;
            case "MFX":
                praefix = 64;
                break;
            case "MM2_dil8":
            case "MM2_prg":
                praefix = 8;
                break;
        }
        if (this.address.get() > 255) {
            Integer add = address.get() / 255;
            Integer temp = address.get() % 255;
            newAdr = (byte) (temp & 0xFF);
            praefix += add;
        } else {
            newAdr = address.getValue().byteValue();
        }
        result[2] = praefix;
        result[3] = newAdr;
        return result;
    }

    public TableView createFunctionsTable() {
        ObservableList<CSFunction> functions = FXCollections.observableArrayList();
        functions.setAll(this.getFunctions());
        TableColumn column = new TableColumn(Internationalization.getString("function"));
        column.setCellValueFactory(new PropertyValueFactory<CSFunction, FunctionCell>("tableCell"));
        column.setCellFactory(new Callback<TableColumn<CSFunction, FunctionCell>, TableCell<CSFunction, FunctionCell>>() {
            @Override
            public TableCell<CSFunction, FunctionCell> call(TableColumn<CSFunction, FunctionCell> param) {
                TableCell<CSFunction, FunctionCell> cell = new TableCell<CSFunction, FunctionCell>() {
                    @Override
                    public void updateItem(FunctionCell item, boolean empty) {
                        if (item != null) {
                            VBox box = new VBox();
                            box.setSpacing(2);
                            ImageView imageView = new ImageView();
                            BufferedImage bufimg;
                            try {
                                bufimg = ImageIO.read(new File(item.getPicPath() + item.getFilename() + "_off.png"));
                                Image img = SwingFXUtils.toFXImage(bufimg, null);
                                imageView.setImage(img);
                                imageView.setFitHeight(50);
                                imageView.setFitWidth(50);
                            } catch (IOException ex) {
                                CSLogger.logEvent(3, ex, "Error loading Function Image in TableCell");
                            }

                            imageView.setOnMousePressed((MouseEvent event) -> {
                                BufferedImage buf = null;
                                Image newImg = null;
                                if (item.getFunction().getONState()) {
                                    try {
                                        item.getFunction().switchOFF();
                                        buf = ImageIO.read(new File(item.getPicPath() + item.getFilename() + "_off.png"));
                                        newImg = SwingFXUtils.toFXImage(buf, null);
                                        imageView.setImage(newImg);
                                    } catch (IOException ex) {
                                        CSLogger.logEvent(3, ex, "Error changing Function Image in TableCell to OFF");
                                    }
                                } else {
                                    try {
                                        item.getFunction().switchON();
                                        buf = ImageIO.read(new File(item.getPicPath() + item.getFilename() + "_on.png"));
                                        newImg = SwingFXUtils.toFXImage(buf, null);
                                        imageView.setImage(newImg);
                                    } catch (IOException ex) {
                                        CSLogger.logEvent(3, ex, "Error changing Function Image in TableCell to ON");
                                    }
                                }
                                CSFunction temp = item.getFunction();
                                try {
                                    Package.setFunction(temp.getTrainAddress(), temp.getCurState(), temp.getNr() - 1);
                                } catch (IOException ex) {
                                    Logger.getLogger(CSTrain.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            });

                            item.getFunction().getCurStateProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
                                Platform.runLater(() -> {
                                    BufferedImage buf = null;
                                    Image newImg = null;
                                    if (newValue.intValue() == 0) {
                                        try {
                                            buf = ImageIO.read(new File(item.getPicPath() + item.getFilename() + "_off.png"));
                                            newImg = SwingFXUtils.toFXImage(buf, null);
                                            imageView.setImage(newImg);
                                        } catch (IOException ex) {
                                            CSLogger.logEvent(3, ex, "Error changing Function Image in TableCell to OFF (response)");
                                        }
                                    } else {
                                        try {
                                            buf = ImageIO.read(new File(item.getPicPath() + item.getFilename() + "_on.png"));
                                            newImg = SwingFXUtils.toFXImage(buf, null);
                                            imageView.setImage(newImg);
                                        } catch (IOException ex) {
                                            CSLogger.logEvent(3, ex, "Error changing Function Image in TableCell to ON (response)");
                                        }
                                    }
                                });
                            });

                            box.getChildren().add(imageView);
                            setGraphic(box);
                        }
                    }
                };
                return cell;
            }
        });

        TableColumn<CSFunction, String> nameCol = new TableColumn<>(Internationalization.getString("name"));
        nameCol.setCellValueFactory(new PropertyValueFactory<CSFunction, String>("name"));

        TableView tempFunctionTable = new TableView<CSFunction>();
        tempFunctionTable.setItems(functions);
        column.setMinWidth(80);
        tempFunctionTable.getColumns().setAll(column, nameCol);
        return tempFunctionTable;
    }

    public void toggleDirection() {
        if (direction.get() == 1) {
            direction.set(2);
        } else if (direction.get() == 2) {
            direction.set(1);
        }
    }

    public void loadInControl(int nr) throws IOException {
       // guiControllerWrapper.getController().loadTrainIntoControl(this, nr);
    }

    public void setAddress(Integer address) {
        this.address.set(address);
        hexAddress = addressToHex();
    }

    public Boolean changeDecoderType(String decoder) {
        decoderType.set(decoder);
        return decoder == CSDecoder.DCC
                || decoder == CSDecoder.MFX
                || decoder == CSDecoder.MM2;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public void setVMax(int v) {
        if (v >= 0 && v <= 255) {
            this.vMax = v;
        } else {
            //TODO: Logger-aufruf
        }
    }

    public void setVMin(int v) {
        if (v >= 0 && v <= 255) {
            this.vMin = v;
        } else {
            //TODO: Logger-aufruf
        }
    }

    public void setBrakes(int brake) {
        if (brake >= 0 && brake <= 255) {
            this.brakes = brake;
        } else {
            //TODO: Logger-aufruf
        }
    }

    public void setAcceleration(int accel) {
        if (accel >= 0 && accel <= 255) {
            this.acceleration = accel;
        } else {
            //TODO: Logger-aufruf
        }
    }

    public void setVolume(int vol) {
        if (vol >= 0 && vol <= 100) {
            this.volume = vol;
        } else {
            //TODO: Loggeraufruf
        }
    }

    public void setPicturePath(String path) {
        profilePicture.set(path);
    }

    public void setTacho(int value) {
        this.tacho.set(value);
        //TODO: Maximalwert herausfinden
    }

    public void setDescription(String desc) {
        description.set(desc);
    }

    public String getName() {
        return name.get();
    }

    public Integer getIntAdress() {
        return address.get();
    }

    public SimpleIntegerProperty getAdressProperty() {
        return address;
    }

    public int getSpeed() {
        return speed.get();
    }

    public SimpleIntegerProperty getSpeedProperty() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        if (speed < 0) {
            speed = 0;
        } else if (speed > 1024) {
            speed = 1024;
        }
        this.speed.set(speed);
        try {
            Package.setSpeed(CSTrain.this);
        } catch (IOException ex) {
            CSLogger.logEvent(3, ex, "Error setting speed.");
        }
    }

    public void incSpeed(Integer val) throws IOException {
        Integer newSpeed = this.speed.get() + val;
        this.setSpeed(newSpeed);
    }

    public void decSpeed(Integer val) throws IOException {
        Integer newSpeed = this.speed.get() - val;
        this.setSpeed(newSpeed);
    }

    public String getPicturePath() {
        return profilePicture.get();
    }

    public SimpleStringProperty getPicturePathProperty() {
        return profilePicture;
    }

    public int getVMax() {
        return vMax;
    }

    public int getVMin() {
        return vMin;
    }

    public Integer getTacho() {
        return tacho.getValue();
    }

    public SimpleIntegerProperty getTachoProperty() {
        return tacho;
    }

    public int getVolume() {
        return volume;
    }

    public int getBrakes() {
        return brakes;
    }

    public int getAcceleration() {
        return acceleration;
    }

    public String getDescription() {
        return description.get();
    }

    public SimpleStringProperty getDescriptionProperty() {
        return description;
    }

    public Integer getAddress() {
        return this.address.get();
    }

    public String getDecoderType() {
        return this.decoderType.get();
    }

    public SimpleStringProperty getDecoderTypeProperty() {
        return decoderType;
    }

    public SimpleStringProperty getNameProperty() {
        return name;
    }

    public SimpleIntegerProperty getDirectionProperty() {
        return direction;
    }

    public Integer getDirection() {
        return direction.get();
    }

    public void setDirection(int dir) {
        direction.set(dir);
    }    

    public Object getTrainTableCell() {
        return trainTableCell.get();
    }

    public ObjectProperty trainTableCellProperty() {
        return trainTableCell;
    }

    public CSFunction getFunction(Integer index) {
        return functions.get(index);
    }

    public ArrayList<CSFunction> getFunctions() {
        return functions;
    }

    public void setDecodertype(String dec) {
        decoderType.set(dec);
    }

    /**
     * @return the controlledByController
     */
    public Boolean isControlledByController() {
        return controlledByController.get();
    }

    /**
     * @param controlledByController the controlledByController to set
     */
    public void setControlledByController(Boolean controlledByController) {
        this.controlledByController.set(controlledByController);        
    }

    public BooleanProperty getControlledByControllerProperty() {
        return controlledByController;
    }

    /**
     * @return the activeOnController
     */
    public Boolean getActiveOnController() {
        return activeOnController;
    }

    /**
     * @param activeOnController the activeOnController to set
     */
    public void setActiveOnController(Boolean activeOnController) {
        this.activeOnController = activeOnController;
        /* if (activeOnController && CSSettings.getUsingGamecontroller()){
         Integer boundAxis = CSSettings.getController_train_speed();
         CSGamePadWrapper.getAxisProperty(boundAxis).addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
         this.speed.set((int) (newValue.doubleValue()*1000));
         });
         }*/
    }

    /**
     * @return the gamePadSlowMode
     */
    public Double getGamePadSlowMode() {
        return gamePadSlowMode;
    }

    /**
     * @param gamePadSlowMode the gamePadSlowMode to set
     */
    public void setGamePadSlowMode(Double gamePadSlowMode) {
        this.gamePadSlowMode = gamePadSlowMode;
    }
}

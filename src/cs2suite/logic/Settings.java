package cs2suite.logic;

import cs2suite.utils.HelperUtils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.scene.paint.Color;
import org.ini4j.Ini;

/**
 *
 * @author Philipp uses Singleton schematic
 *
 * ----------------------------------------------------------------------------
 * Diese Klasse enthält bei geladenenm Zustand alle Einstellungen die über die
 * Settings-Form gemacht werden können. Durch das verwendete Singleton-Schema
 * kann jede Klasse im System einfach auf die entsprechenden nötigen
 * Einstellungswerte zugreifen.
 *
 * Die Funktion zum laden der Einstellungen, welche auch diese Klasse erstellt
 * und initialisiert befindet sich im 'mainController' und wird dort beim
 * Programmstart aufgerufen, oder wenn die Einstellungen in der entsprechenden
 * Form geändert wurden.
 * ----------------------------------------------------------------------------
 */
public class Settings {

    public static final int GENERAL_SETTINGS = 0;
    public static final int LAYOUT_SETTINGS = 1;
    public static final int CONTROLLER_SETTINGS = 2;
    public static final int SYSTEM_SETTINGS = 3;

    private static Settings instance;

    private static Boolean autoIPSetting;
    private static Boolean controller_controlTrains;
    private static Boolean controller_controlCrane;
    private static Boolean controller_controlTurntable;
    private static Boolean consoleLogging;
    private static Boolean startMaximized;
    private static Boolean showSplashScreen;
    private static Boolean implicitExit;
    private static Boolean usingGamecontroller;
    private static Boolean controlTrainsWithMouse;
    private static Boolean controlTrainsWithKeys;
    private static Boolean autoLoadSystemOnStartup;
    
    private static String cs2IPAddress;
    private static String language;    
    private static String version;            
    private static String defaultSettingsPath;
    private static String[] availableLanguages;    
    private static String build;    
    private static String lastSettingsPath;
    private static String gameController;
    private static String defaultProject;
    
    private static Integer liveLoggingBufferLength;
    private static Integer mouseTrainControlSpeed;
    private static Integer logLevel;
    private static Integer trainControlCount;
    private static Integer maxTrainControlCount;
    private static Integer langIndex;
    private static Integer s88Controllers;
    private static Integer gameControllerIndex;
    
    private static Color layoutGridColor;
    private static Double slowMode = 1.;

    /**
     * @return the maxTrainControlCount
     */
    public static Integer getMaxTrainControlCount() {
        return maxTrainControlCount;
    }

    /**
     * @return the liveLoggingBufferLength
     */
    public static Integer getLiveLoggingBufferLength() {
        return liveLoggingBufferLength;
    }

    /**
     * @param aLiveLoggingBufferLength the liveLoggingBufferLength to set
     */
    public static void setLiveLoggingBufferLength(Integer aLiveLoggingBufferLength) {
        liveLoggingBufferLength = aLiveLoggingBufferLength;
    }

    /**
     * @return the autoLoadSystemOnStartup
     */
    public static Boolean getAutoLoadSystemOnStartup() {
        return autoLoadSystemOnStartup;
    }

    /**
     * @param aAutoLoadSystemOnStartup the autoLoadSystemOnStartup to set
     */
    public static void setAutoLoadSystemOnStartup(Boolean aAutoLoadSystemOnStartup) {
        autoLoadSystemOnStartup = aAutoLoadSystemOnStartup;
    }

    private Settings(Boolean start) {
        //create Default Settings        
        autoIPSetting = true;
        version = "0";
        logLevel = 5;
        language = "DE";
        usingGamecontroller = false;
        gameControllerIndex = -1;
        gameController = "";
        layoutGridColor = Color.BLACK;
        gameControllerIndex = -1;
        startMaximized = false;
        implicitExit = true;
        showSplashScreen = true;
        defaultSettingsPath = System.getProperty("user.dir") + "\\data\\conf\\default_config.ini";
        lastSettingsPath = System.getProperty("user.dir") + "\\data\\conf\\config.ini";
        defaultProject = "";
        controlTrainsWithKeys = false;
        controlTrainsWithMouse = true;
        mouseTrainControlSpeed = 10;
        liveLoggingBufferLength = 1000;
        try {
            if (start) {
                loadDefaultConfig();
            } else {
                loadConfig();
            }
        } catch (IOException ex) {
            CSLogger.logEvent(4, ex, "Error loading DefaultConfigFile");
        }
        maxTrainControlCount = 6;
    }

    private void loadDefaultConfig() throws IOException {
        File file = new File(defaultSettingsPath);
        loadConfigFile(file);
    }

    private void loadConfig() throws IOException {
        File file = new File(lastSettingsPath);
        loadConfigFile(file);
    }

    public static void save() throws IOException {
        File file = new File(lastSettingsPath);
        if (file.exists() && file.isFile()) {
            file.delete();
        }
        saveToConfigFile(new File(lastSettingsPath));
    }

    public static void saveToDefault() throws IOException {
        File file = new File(defaultSettingsPath);
        if (file.exists() && file.isFile()) {
            file.delete();
        }
        saveToConfigFile(new File(defaultSettingsPath));
    }

    public static void saveToConfigFile(File file) throws IOException {
        file.createNewFile();
        Ini ini = new Ini(file);
        Ini.Section mainSection = ini.add("General_Settings");
        Ini.Section layoutSection = ini.add("Layout_Settings");
        Ini.Section turntableSection = ini.add("Turntable_Settings");
        Ini.Section portalcraneSection = ini.add("Portalcrane_Settings");
        Ini.Section versioning = ini.add("Version");
        Ini.Section restricted = ini.add("Restricted");
        Ini.Section gameController = ini.add("GameController");
        Ini.Section systemSection = ini.add("System");

        //versioning.add("version", version);
        versioning.add("langCode", getLanguage());
        String tempLang = "";
        for (String s : availableLanguages) {
            tempLang += s + ",";
        }
        tempLang = tempLang.substring(0, tempLang.length() - 1);
        versioning.add("av_lang", tempLang);
        versioning.add("version", version);
        versioning.add("build", getBuild());
        //main Section
        mainSection.add("csip", getCs2IPAddress());
        mainSection.add("autoIP", getAutoIPSetting());
        mainSection.add("language", HelperUtils.langCodeTranslation(getLanguage()));
        mainSection.add("loglevel", getLogLevel());
        mainSection.add("consoleLogging", getConsoleLogging());
        mainSection.add("startmaximized", getStartMaximized());
        mainSection.add("showSplashScreen", getShowSplashScreen());
        mainSection.add("implicitExit", implicitExit);
        mainSection.add("defaultProject", getDefaultProject());
        mainSection.add("controlTrainsWithKeys", getControlTrainsWithKeys());
        mainSection.add("controlTrainsWithMouse", getControlTrainsWithMouse());
        mainSection.add("trainControlSpeed", getMouseTrainControlSpeed());
        mainSection.add("liveLoggingBufferLength", getLiveLoggingBufferLength());
        mainSection.add("autoLoadSystemOnStartup", getAutoLoadSystemOnStartup());
        //layout Section
        layoutSection.add("gridColor_r", getLayoutGridColor().getRed());
        layoutSection.add("gridColor_g", getLayoutGridColor().getGreen());
        layoutSection.add("gridColor_b", getLayoutGridColor().getBlue());
        layoutSection.add("trainControllerCount", getTrainControlCount());
        //System Section
        systemSection.add("s88Controllers", s88Controllers);
        //turntable Section
        //portalcrane Section
        //Restriced Section
        //GameController Section
        gameController.add("use_gameController", usingGamecontroller);        
        gameController.add("controller", gameController);
        gameController.add("control_trains", getController_controlTrains());
        gameController.add("control_crane", getController_controlCrane());
        gameController.add("control_turntable", getController_controlTurntable());        
        gameController.add("slowMode", getSlowMode());
        //Restricted Section
        restricted.add("maxTrainControllers", maxTrainControlCount);
        ini.store();
    }

    public static void loadConfigFile(File file) throws IOException {
        Ini ini = new Ini();
        ini.load(file);
        Ini.Section mainSection = ini.get("General_Settings");
        Ini.Section layoutSection = ini.get("Layout_Settings");
        Ini.Section turntableSection = ini.get("Turntable_Settings");
        Ini.Section portalcraneSection = ini.get("Portalcrane_Settings");
        Ini.Section versioning = ini.get("Version");
        Ini.Section restricted = ini.get("Restricted");
        Ini.Section gameController = ini.get("GameController");
        Ini.Section systemSection = ini.get("System");
        // Versioning
        loadVersioningSettings(versioning);
        //main Section                
        loadMainSettings(mainSection);
        //layout Section
        loadLayoutSettings(layoutSection);
        //System Section
        loadSystemSettings(systemSection);
        //turntable Section        
        //portalcrane Section
        //gameController Section
        loadGameControllerSettings(gameController);
        //restricted Section   
        loadRestrictedSettings(restricted);
    }

    private static void loadVersioningSettings(Ini.Section section) {
        try {
            setVersion(section.get("version"));
            setBuild(section.get("build"));
            setLanguage(section.get("langCode"));
            String tempLang = section.get("av_lang");
            availableLanguages = tempLang.split(",");
            int count = 0;
            for (String s : availableLanguages) {
                count++;
                if (s.equals(language)) {
                    langIndex = count;
                    break;
                }
            }
        } catch (Exception e) {
            CSLogger.logEvent(5, e, "Error loading Version Settings");
        }
    }

    private static void loadRestrictedSettings(Ini.Section section) {
        try {
            maxTrainControlCount = Integer.parseInt(section.get("maxTrainControllers"));
        } catch (Exception e) {
            CSLogger.logEvent(5, e, "Error loading restricted Settings");
        }
    }

    private static void loadMainSettings(Ini.Section section) {
        try {
            setCs2IPAddress(section.get("csip"));
            setAutoIPSetting(Boolean.parseBoolean(section.get("autoIP")));
            setLogLevel(Integer.parseInt(section.get("loglevel")));
            setConsoleLogging(Boolean.parseBoolean(section.get("consoleLogging")));
            setStartMaximized(Boolean.parseBoolean(section.get("startmaximized")));
            setShowSplashScreen(Boolean.parseBoolean(section.get("showSplashScreen")));
            setImplicitExit(Boolean.parseBoolean(section.get("implicitExit")));
            setDefaultProject(section.get("defaultProject"));
            setControlTrainsWithKeys(Boolean.parseBoolean(section.get("controlTrainsWithKeys")));
            setControlTrainsWithMouse(Boolean.parseBoolean(section.get("controlTrainsWithMouse")));
            setMouseTrainControlSpeed(Integer.parseInt(section.get("trainControlSpeed")));
            setLiveLoggingBufferLength((Integer) Integer.parseInt(section.get("liveLoggingBufferLength")));
            setAutoLoadSystemOnStartup(Boolean.parseBoolean(section.get("autoLoadSystemOnStartup")));
        } catch (Exception e) {
            e.printStackTrace();
            CSLogger.logEvent(5, e, "Error loading general Settings");
        }
    }

    private static void loadLayoutSettings(Ini.Section section) {
        try {
            setLayoutGridColor(Color.rgb((int) Double.parseDouble(section.get("gridColor_r")),
                    (int) Double.parseDouble(section.get("gridColor_g")),
                    (int) Double.parseDouble(section.get("gridColor_b"))));
            setTrainControlCount(Integer.parseInt(section.get("trainControllerCount")));
        } catch (Exception e) {
            CSLogger.logEvent(5, e, "Error loading Layout Settings");
        }
    }

    private static void loadSystemSettings(Ini.Section section) {
        try {
            setS88Controllers(Integer.parseInt(section.get("s88Controllers")));
        } catch (Exception e) {
            CSLogger.logEvent(5, e, "Error loading System Settings");
        }
    }

    private static void loadGameControllerSettings(Ini.Section section) {
        try {
            setUsingGamecontroller(Boolean.parseBoolean(section.get("use_section")));
            setGameControllerIndex(Integer.parseInt(section.get("controller_index")));
            setGameControllerName(section.get("controller_name"));
            setController_controlTrains(Boolean.parseBoolean(section.get("control_trains")));
            setController_controlCrane(Boolean.parseBoolean(section.get("control_crane")));
            setController_controlTurntable(Boolean.parseBoolean(section.get("control_turntable")));            
            //setSlowMode(Double.parseDouble(section.get("slowMode")));
        } catch (Exception e) {
            CSLogger.logEvent(5, e, "Error loading Gamecontroller Settings");
        }
    }

    /**
     * Resets the Settings in one Section specified by the given Parameter. This
 Class provides a set of Constants to pass to this Method, e.g.:
 Settings.GENERAL_SETTINGS
     *
     * @param settingsID The Settings which should be reseted.
     * @throws IOException
     */
    public static void resetSetting(int settingsID) throws IOException {
        Ini ini = new Ini();
        ini.load(new File(defaultSettingsPath));
        switch (settingsID) {
            case 0: // General Settings
                Ini.Section generalSettings = ini.get("General_Settings");
                loadMainSettings(generalSettings);
                break;
            case 1: // Layout Settings
                Ini.Section layoutSection = ini.get("Layout_Settings");
                loadLayoutSettings(layoutSection);
                break;
            case 2: // GameController Settings
                Ini.Section controllerSection = ini.get("GameController");
                loadGameControllerSettings(controllerSection);
                break;
            case 3: // System Settings
                Ini.Section systemSection = ini.get("System");
                loadSystemSettings(systemSection);
                break;
            default:
                loadConfigFile(new File(defaultSettingsPath));
                break;
        }
    }

    /**
     * resets the whole settings singleton by loading all Settings from the
     * default settings
     *
     * @throws IOException
     */
    public static void resetSettings() throws IOException {
        resetSetting(-1);
    }

    public static Settings getInstance(Boolean start) {
        if (Settings.instance == null) {
            Settings.instance = new Settings(start);
        }
        return Settings.instance;
    }

    public static Settings getInstance() {
        if (Settings.instance == null) {
            Settings.instance = new Settings(false);
        }
        return Settings.instance;
    }

    public static Settings resetInstance() {
        Settings.instance = new Settings(false);
        return Settings.instance;
    }

    public static String getLanguage() {
        return language;
    }

    /**
     * @param aInstance the instance to set
     */
    public static void setInstance(Settings aInstance) {
        instance = aInstance;
    }

    /**
     * @return the autoIPSetting
     */
    public static Boolean getAutoIPSetting() {
        return autoIPSetting;
    }

    /**
     * @param aAutoIPSetting the autoIPSetting to set
     */
    public static void setAutoIPSetting(Boolean aAutoIPSetting) {
        autoIPSetting = aAutoIPSetting;
    }

    /**
     * @return the cs2IPAddress
     */
    public static String getCs2IPAddress() {
        return cs2IPAddress;
    }

    /**
     * @param aCs2IPAddress the cs2IPAddress to set
     */
    public static void setCs2IPAddress(String aCs2IPAddress) {
        cs2IPAddress = aCs2IPAddress;
    }

    /**
     * @param aLanguage the language to set
     */
    public static void setLanguage(String aLanguage) {
        language = aLanguage;
    }

    /**
     * @return the logLevel
     */
    public static Integer getLogLevel() {
        return logLevel;
    }

    /**
     * @param aLogLevel the logLevel to set
     */
    public static void setLogLevel(Integer aLogLevel) {
        logLevel = aLogLevel;
    }

    /**
     * @return the version
     */
    public static String getVersion() {
        return version;
    }

    /**
     * @param aVersion the version to set
     */
    public static void setVersion(String aVersion) {
        version = aVersion;
    }

    /**
     * @return the consoleLogging
     */
    public static Boolean getConsoleLogging() {
        return consoleLogging;
    }

    /**
     * @param aConsoleLogging the consoleLogging to set
     */
    public static void setConsoleLogging(Boolean aConsoleLogging) {
        consoleLogging = aConsoleLogging;
    }

    /**
     * @return the gameControllerName
     */
    public static String getGameController() {
        return gameController;
    }

    /**
     * @param aGameControllerName the gameControllerName to set
     */
    public static void setGameControllerName(String aGameController) {
        gameController = aGameController;
    }

    /**
     * @return the gameControllerIndex
     */
    public static Integer getGameControllerIndex() {
        return gameControllerIndex;
    }

    /**
     * @param aGameControllerIndex the gameControllerIndex to set
     */
    public static void setGameControllerIndex(Integer aGameControllerIndex) {
        gameControllerIndex = aGameControllerIndex;
    }

    /**
     * @return the usingGamecontroller
     */
    public static Boolean getUsingGamecontroller() {
        return usingGamecontroller;
    }

    /**
     * @param aUsingGamecontroller the usingGamecontroller to set
     */
    public static void setUsingGamecontroller(Boolean aUsingGamecontroller) {
        usingGamecontroller = aUsingGamecontroller;
    }

    /**
     * @return the layoutGridColor
     */
    public static Color getLayoutGridColor() {
        return layoutGridColor;
    }

    /**
     * @param aLayoutGridColor the layoutGridColor to set
     */
    public static void setLayoutGridColor(Color aLayoutGridColor) {
        layoutGridColor = aLayoutGridColor;
    }

    /**
     * @return the startMaximized
     */
    public static Boolean getStartMaximized() {
        return startMaximized;
    }

    /**
     * @param aStartMaximized the startMaximized to set
     */
    public static void setStartMaximized(Boolean aStartMaximized) {
        startMaximized = aStartMaximized;
    }

    /**
     * @return the showSplashScreen
     */
    public static Boolean getShowSplashScreen() {
        return showSplashScreen;
    }

    /**
     * @param aShowSplashScreen the showSplashScreen to set
     */
    public static void setShowSplashScreen(Boolean aShowSplashScreen) {
        showSplashScreen = aShowSplashScreen;
    }

    /**
     * @return the trainControlCount
     */
    public static Integer getTrainControlCount() {
        return trainControlCount;
    }

    /**
     * @param aTrainControlCount the trainControlCount to set
     */
    public static void setTrainControlCount(Integer aTrainControlCount) {
        trainControlCount = aTrainControlCount;
    }

    public static String[] getLanguages() {
        return availableLanguages;
    }

    /**
     * @return the langIndex
     */
    public static Integer getLangIndex() {
        return langIndex;
    }

    public static Integer getS88Controllers() {
        return s88Controllers;
    }

    public static void setS88Controllers(int count) {
        s88Controllers = count;
    }

    /**
     * @return the build
     */
    public static String getBuild() {
        return build;
    }

    /**
     * @param aBuild the build to set
     */
    public static void setBuild(String aBuild) {
        build = aBuild;
    }

    /**
     * @return the implicitExit
     */
    public static Boolean getImplicitExit() {
        return implicitExit;
    }

    /**
     * @param aImplicitExit the implicitExit to set
     */
    public static void setImplicitExit(Boolean aImplicitExit) {
        implicitExit = aImplicitExit;
    }

    /**
     * @return the controller_controlTrains
     */
    public static Boolean getController_controlTrains() {
        return controller_controlTrains;
    }

    /**
     * @param aController_controlTrains the controller_controlTrains to set
     */
    public static void setController_controlTrains(Boolean aController_controlTrains) {
        controller_controlTrains = aController_controlTrains;
    }

    /**
     * @return the controller_controlCrane
     */
    public static Boolean getController_controlCrane() {
        return controller_controlCrane;
    }

    /**
     * @param aController_controlCrane the controller_controlCrane to set
     */
    public static void setController_controlCrane(Boolean aController_controlCrane) {
        controller_controlCrane = aController_controlCrane;
    }

    /**
     * @return the controller_controlTurntable
     */
    public static Boolean getController_controlTurntable() {
        return controller_controlTurntable;
    }

    /**
     * @param aController_controlTurntable the controller_controlTurntable to
     * set
     */
    public static void setController_controlTurntable(Boolean aController_controlTurntable) {
        controller_controlTurntable = aController_controlTurntable;
    }

    /**
     * @return the slowMode
     */
    public static Double getSlowMode() {
        return slowMode;
    }

    /**
     * @param aSlowMode the slowMode to set
     */
    public static void setSlowMode(Double aSlowMode) {
        slowMode = aSlowMode;
    }

    /**
     * @return the defaultProject
     */
    public static String getDefaultProject() {
        return defaultProject;
    }

    /**
     * @param aDefaultProject the defaultProject to set
     */
    public static void setDefaultProject(String aDefaultProject) {
        defaultProject = aDefaultProject;
    }

    /**
     * @return the controlTrainsWithMouse
     */
    public static Boolean getControlTrainsWithMouse() {
        return controlTrainsWithMouse;
    }

    /**
     * @param aControlTrainsWithMouse the controlTrainsWithMouse to set
     */
    public static void setControlTrainsWithMouse(Boolean aControlTrainsWithMouse) {
        controlTrainsWithMouse = aControlTrainsWithMouse;
    }

    /**
     * @return the controlTrainsWithKeys
     */
    public static Boolean getControlTrainsWithKeys() {
        return controlTrainsWithKeys;
    }

    /**
     * @param aControlTrainsWithKeys the controlTrainsWithKeys to set
     */
    public static void setControlTrainsWithKeys(Boolean aControlTrainsWithKeys) {
        controlTrainsWithKeys = aControlTrainsWithKeys;
    }

    /**
     * @return the mouseTrainControlSpeed
     */
    public static Integer getMouseTrainControlSpeed() {
        return mouseTrainControlSpeed;
    }

    /**
     * @param aMouseTrainControlSpeed the mouseTrainControlSpeed to set
     */
    public static void setMouseTrainControlSpeed(Integer aMouseTrainControlSpeed) {
        mouseTrainControlSpeed = aMouseTrainControlSpeed;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.logic;

import cs2suite.fxmlControllers.guiControllerWrapper;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

/**
 *
 ** @author Philipp Log-Level übersicht: 0: Kritischer Fehler 1,2,3:
 * Systemfehler * -1- : z.B. Schwerer Fehler im System, führt normalerweise zum
 * absturz des * Programms * -2- : z.B. Unerwünschtes Verhalten der Software, *
 * höchstwahrscheinlich neustart nötig * -3- : -"- -"-, kein neustart nötig
 * 4,5,6: * Programmfehler * -4- : z.B. Fehler in der KOmmunikation im Netzwerk
 * und/oder * mit der central Station * -5- : z.B. Fehler bei der grafischen
 * Darstellung oder * ungenauigkeiten in der Darstellung * -6- : z.B.
 * Fehlerhaftes übernehmen von * Daten 7,8: Warnung * -7- : z.B. Fehlerhafte
 * Benutzereingaben, die falsches * verhalten zur Folge haben könnten * -8- :
 * z.B. Fehlerhafte Benutzereingaben * 9,10: Information/Debug * -9- : z.B.
 * Einstellungen an bestimmten punkten oder * Informationen zu erstllten
 * Objekten etc. * -10- : z.B. empfangenen Nachrichten * von der Central Station
 */
public class CSLogger {

    private static CSLogger instance;

    public static final String LOG_PATH = System.getProperty("user.dir") + "\\log";

    private static File logFile;
    private static Integer logLevel;
    private static PrintWriter pw;
    private static Calendar calendar;
    private static String dateStr;
    private static java.util.Date now;
    private static Boolean outToConsole;

    private CSLogger() throws IOException {
        super();
        outToConsole = false;
        calendar = Calendar.getInstance();
        now = calendar.getTime();
        dateStr = now.toString();
        this.logFile = new File(System.getProperty("user.dir") + "\\data\\log\\log_" + String.valueOf(now.getDate())
                + "_" + String.valueOf(now.getMonth())
                + "_" + String.valueOf(now.getYear()) + " - " + System.currentTimeMillis() + ".txt");
        this.logLevel = 5;
        /* if (!logFile.canRead()){
         logFile.createNewFile();
         }*/
        pw = new PrintWriter(new FileWriter(logFile.getAbsolutePath()));
        logEvent(111, null, "VersionInfo: \n"
                + "CSSuite: " + Settings.getVersion() + "_" + Settings.getBuild() + "\n"
                + "Java: " + System.getProperty("java.version") + "\n"
                + "JavaFX: " + com.sun.javafx.runtime.VersionInfo.getRuntimeVersion() + "\n");
    }

    public static CSLogger getInstance() throws IOException {
        if (CSLogger.instance == null) {
            CSLogger.instance = new CSLogger();
        }
        return CSLogger.instance;
    }

    private static String logPrioToString(Integer prio) {
        switch (prio) {
            case 0:
                return "Critical Error:";
            case 1:
            case 2:
            case 3:
                return "System Error:";
            case 4:
            case 5:
                return "Program Error:";
            case 6:
            case 7:
                return "Warning:";
            case 8:
            case 9:
                return "Debug/Info:";
            case 10:
                return "Special:";
        }
        return "Error:";
    }

    public static void logEvent(Integer prio, Exception ex, String ex_str) {
        now = calendar.getTime();
        if (prio <= logLevel) {
            String level = logPrioToString(prio);
            pw.println(now.toString() + " >>> (" + String.valueOf(prio) + ")" + level + " " + ex_str + ";");
            if (ex != null) {
                String temp_log = "    ->" + ex.getMessage();
                ex.printStackTrace(pw);
                ex.printStackTrace();                
                guiControllerWrapper.getController().setStatus(0);
                if (prio < 4) {
                    temp_log += " >>>> " + ex.getLocalizedMessage();
                }
                pw.println(temp_log);
            } else {
                if (prio <= 4) {
                    guiControllerWrapper.getController().setStatus(0);
                } else if (prio > 4 && prio < 7) {
                    guiControllerWrapper.getController().setStatus(2);
                }
            }
        } else if (prio == 111) { //special logcodes for logging informations which otherwise wont get logged
            //111 -> forced information logging
            pw.println(now.toString() + " >>> " + "--INFO-- >> " + ex_str);
            System.out.println("--------" + ex_str + "--------");
        } else if (prio == 101) { //show log in CommandLine
            System.out.println(now.toString() + " >>> " + ex_str);
        }
        if (outToConsole) {
            System.out.println("--" + ex_str + "--\n " + ex.getMessage());
        }
        pw.flush();
    }

    public static void setLogLevel(Integer lvl) {
        if (lvl >= 0 && lvl <= 10) {
            CSLogger.logLevel = lvl;
        } else {
            //TODO LOgger ?hinweis?
        }
    }

    private static void clearFiles(String path) throws IOException {
        File file = new File(path);
        File[] files = file.listFiles();
        for (File f : files) {
            if (f.isFile() && f.exists() && f.getName() != logFile.getName()) {
                f.delete();
            } else {
                CSLogger.getInstance().logEvent(6, null, "Clear Log-Files: could not delete logfile: " + f.getAbsolutePath());
            }
        }
    }

    /**
     * deletes all Logfiles in the log Folder except for the one which is
     * currently active
     *
     * @throws java.io.IOException
     */
    public static void clearLogs() throws IOException {
        CSLogger.clearFiles(logFile.getPath());
    }

    public static Integer getLogLevel() {
        return logLevel;
    }

    public static String getLogFilePath() {
        return logFile.getAbsolutePath();
    }

    public void setLogToConsole(boolean b) {
        outToConsole = b;
    }
}

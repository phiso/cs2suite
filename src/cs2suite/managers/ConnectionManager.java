/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.managers;

import cs2suite.Models.Objects.CSConnection;
import cs2suite.logic.CSLogger;
import cs2suite.network.ArticleListener;
import cs2suite.network.ChannelListener;
import cs2suite.network.CommandParser;
import cs2suite.network.ConfigStreamListener;
import cs2suite.network.CoreListener;
import cs2suite.network.PacketParser;
import cs2suite.network.S88Polling;
import cs2suite.network.SystemListener;
import cs2suite.network.TrainListener;
import cs2suite.network.UDPReceiver;
import cs2suite.network.UDPSender;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class ConnectionManager {

    private static ConnectionManager instance;

    private static ArrayList<CSConnection> connections;
    private static ObjectProperty lastConnectionProperty;
    private static IntegerProperty activeConnectionIndex;

    private ConnectionManager() {
        super();
        activeConnectionIndex = new SimpleIntegerProperty(-1);
        connections = new ArrayList<>();
        lastConnectionProperty = new SimpleObjectProperty();
        initSingletons();
        CSLogger.logEvent(111, null, "Connection-Manager successfully loaded.");
    }

    public static ConnectionManager getInstance() {
        if (ConnectionManager.instance == null) {
            ConnectionManager.instance = new ConnectionManager();
        }
        return ConnectionManager.instance;
    }

    private static void initSingletons() {
        try {
            CoreListener.getInstance().start();
        } catch (SocketException ex) {
            CSLogger.logEvent(2, ex, "Failed initializing CoreReceiver");
        }
        CSLogger.logEvent(111, null, "Initialized Core Listener");

        CoreListener.inetAddressProperty().addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                InetAddress adrObject = (InetAddress) newValue;
                CSConnection connection = new CSConnection(adrObject);
                if (!checkIfIPExists(adrObject.getHostAddress())) {
                    addConnection(connection);
                    lastConnectionProperty.set(connection);
                }
            }
        });                
    }

    /*
    checks if a CSConnection Object exists, which matches the given InetAddress
    */
    private static boolean checkIfIPExists(String address) {
        for (CSConnection connect : connections) {
            if (connect.getIPAddress().equals(address)) {
                return true;
            }
        }
        return false;
    }
    
    private static void initListeners() throws SocketException{        
        ArticleListener.getInstance().start();
        ChannelListener.getInstance().start();
        PacketParser.getInstance().start();
        S88Polling.getInstance().start();
        SystemListener.getInstance().start();
        TrainListener.getInstance().start();        
        ConfigStreamListener.getInstance().start();        
        CommandParser.getInstance();
    }
    
    public static void activateConnection(int index) {
        activeConnectionIndex.set(index);
        CSLogger.logEvent(111, null, "Activating Connection ("+index+") at "
                +connections.get(index).getIPAddressAsString());
        try {
            initListeners();
        } catch (SocketException ex) {
            CSLogger.logEvent(3, ex, "Error initializing Listeners!");
        }
        try {
            UDPSender.initinstance(connections.get(index).getIPAddress());
        } catch (SocketException ex) {
            CSLogger.logEvent(2, ex, "Error starting UDP Sender");
        }
        CSLogger.logEvent(111, null, "---------Connection activated----------");
    }    

    //<editor-fold defaultstate="collapsed" desc="Getter/Setter">
    /**
     *
     * @param connection
     */
    public static void addConnection(CSConnection connection) {
        connections.add(new CSConnection(connection.getIPAddress()));
    }

    /**
     *
     * @param index
     * @return
     */
    public static CSConnection getConnection(int index) {
        return connections.get(index);
    }
    
    public static CSConnection getConnection(String ip){
        for (CSConnection con : connections){
            if (con.getIPAddressAsString().equals(ip)) return con;
        }
        return null;
    }

    /**
     *
     * @return
     */
    public static ArrayList<CSConnection> getAllConnections() {
        return connections;
    }

    /**
     *
     * @return
     */
    public static ObjectProperty lastConnectionProperty() {
        return lastConnectionProperty;
    }
    
    public static CSConnection getActiveConnection(){
        return connections.get(activeConnectionIndex.get());
    }

    public static CSConnection getLastconnection() {
        return (CSConnection) lastConnectionProperty.get();
    }
    
    public static int getActiveConnectionIndex(){
        return activeConnectionIndex.get();
    }
    
    public static IntegerProperty activeConnectionProperty(){
        return activeConnectionIndex;
    }
    //</editor-fold>    
}

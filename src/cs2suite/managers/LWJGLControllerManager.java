/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.managers;

import cs2suite.logic.CSLogger;
import java.util.ArrayList;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Controller;
import org.lwjgl.input.Controllers;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class LWJGLControllerManager {
    // lwjgl ver 2.9.1
    private static LWJGLControllerManager instance;
    
    private static int controllersCount;
    private static ArrayList<Controller> allControllers;
    
    private LWJGLControllerManager() throws LWJGLException{
        Controllers.create();
        Controllers.poll();       
        allControllers = new ArrayList<>();                       
        controllersCount = Controllers.getControllerCount();
        for(int i = 0; i < controllersCount; i++){
            allControllers.add(Controllers.getController(i));
        }
        CSLogger.logEvent(111, null, "LWJGL-Controller-Manager successfully loaded.");
    }
    
    public static LWJGLControllerManager getInstance() throws LWJGLException{
        if (LWJGLControllerManager.instance == null){
            LWJGLControllerManager.instance = new LWJGLControllerManager();
        }
        return LWJGLControllerManager.instance;
    }
    
    /**
     * refreshes the Controllers by calling Controllers.poll()
     */
    public static void pollControllers(){
        Controllers.poll();
        controllersCount = Controllers.getControllerCount();
        for(int i = 0; i < controllersCount; i++){
            allControllers.add(Controllers.getController(i));
        }
    }
    
    /**
     * 
     * @param name the Name of the Controller
     * @return the Controller with the given Name
     */
    public static Controller getControllerByName(String name) throws NullPointerException{        
        for(Controller c : allControllers){
            if (c.getName().equals(name)){
                return c;
            }
        }
        throw new NullPointerException("No Controller found!");
    }
    
    /**
     * 
     * @param id Controllers index
     * @return the Controller at the given index
     */
    public static Controller getControllerById(int id){
        return Controllers.getController(id);
    }
    
    /**
     * 
     * @return returns the number of recognized COntrollers
     */
    public static int getControllersCount(){
        return controllersCount;
    }        
    
    public static Controller getFirst(){
        return allControllers.get(0);
    }
}

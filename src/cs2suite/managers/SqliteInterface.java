/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.managers;

import cs2suite.Models.SqliteDatabase;
import java.sql.SQLException;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public interface SqliteInterface {

    public void saveToDatabase(SqliteDatabase db) throws SQLException;
}

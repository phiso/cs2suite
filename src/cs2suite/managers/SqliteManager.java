/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.managers;

import cs2suite.Models.Objects.Article;
import cs2suite.Models.Objects.ArticleType;
import cs2suite.Models.Objects.CSConnection;
import cs2suite.Models.Objects.Layout;
import cs2suite.Models.Objects.LayoutObject;
import cs2suite.Models.Objects.LayoutPosition;
import cs2suite.Models.SqliteDatabase;
import cs2suite.Models.Objects.CSSystem;
import cs2suite.Models.Objects.Train;
import cs2suite.Models.Enums.Decoder;
import cs2suite.Models.Enums.Epoch;
import cs2suite.Models.Enums.TrainType;
import cs2suite.logic.CSLogger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class SqliteManager {

    private static SqliteManager instance;
    private static SqliteDatabase database;
    private static boolean instantiated = false;

    private SqliteManager() {
        super();
        instantiated = true;
        CSLogger.logEvent(111, null, "SQLite Manager successfully loaded.");
    }

    private SqliteManager(SqliteDatabase db) {
        this();
        database = db;
    }

    public static SqliteManager getInstance() {
        if (SqliteManager.instance == null) {
            SqliteManager.instance = new SqliteManager();
        }
        return SqliteManager.instance;
    }

    public static SqliteManager getInstance(SqliteDatabase db) {
        if (SqliteManager.instance == null) {
            SqliteManager.instance = new SqliteManager(db);
        }
        return SqliteManager.instance;
    }

    public static void initDatabase(SqliteDatabase database) {
        SqliteManager.database = database;
    }  

    private static int getIdByName(String name, String table) throws SQLException {
        ResultSet resultSet = database.simpleSQLQuery("SELECT ID FROM "
                + table + "WHERE Name = \"" + name + "\" LIMIT 1");
        return resultSet.getInt("id");
    }

    //<editor-fold defaultstate="collapsed" desc="Trains">
    public static ArrayList<Train> getTrainsForSystem(CSSystem system) throws SQLException {
        String query = "SELECT Train FROM SystemTrains WHERE System = "
                + "(SELECT id FROM Systems WHERE Name = \"" + system.getName() + "\")";
        ResultSet resultSet = getDatabase().simpleSQLQuery(query);
        ArrayList<Train> trains = new ArrayList<>();
        while (resultSet.next()) {
            trains.add(getTrainById(resultSet.getInt("Train")));
        }
        return trains;
    }

    public static Train getTrainById(int id) throws SQLException {
        String query = "SELECT * FROM Trains WHERE ID = " + id + " LIMIT 1";
        ResultSet resultSet = getDatabase().simpleSQLQuery(query);                
        Train result = new Train(resultSet.getInt("address"), resultSet.getString("name"), resultSet.getString("Description"),
                resultSet.getString("ArtNr"), TrainType.fromId(resultSet.getInt("Type")), Decoder.fromId(resultSet.getInt("Deoder")), 
                Epoch.fromId(resultSet.getInt("Epoch")), resultSet.getInt("Vmax"), resultSet.getInt("Vmin"),
                resultSet.getInt("Acceleration"), resultSet.getInt("Speedometer"), resultSet.getInt("Volume"),
                resultSet.getInt("brakeForce"), resultSet.getString("Picture"));                                                   
        return result;
    }

    public static Train getTrainByName(String name) throws SQLException {
        String query = "SELECT ID FROM Trains WHERE Name = \"" + name + "\" LIMIT 1";
        ResultSet resultSet = getDatabase().simpleSQLQuery(query);
        return getTrainById(resultSet.getInt("id"));
    }

    public static void insertIntoTrains(Train train) throws SQLException {
        String query = "INSERT INTO TRAINS (Name, Description, Address, Type, "
                + "Decoder, Vmin, Vmax, Acceleration, BrakeForce, Picture, "
                + "Volume, Speedometer, Epoch) values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement stmt = database.getDBConnection().prepareStatement(query);
        stmt.setString(1, train.getName());
        stmt.setString(2, train.getDescription());
        stmt.setInt(3, train.getAddress());
        int trainTypeId = train.getType().Id();
        stmt.setInt(4, trainTypeId);
        int decoderId = train.getDecoder().id();
        stmt.setInt(5, decoderId);
        stmt.setInt(6, train.getvMin());
        stmt.setInt(7, train.getvMax());
        stmt.setInt(8, train.getAcceleration());
        stmt.setInt(9, train.getBrakeForce());
        stmt.setString(10, train.getProfilePicture());
        stmt.setInt(11, train.getVolume());
        stmt.setInt(12, train.getSpeedometer());
        stmt.setInt(13, getIdByName(train.getEpoch(), "TrainEpochs"));
        stmt.executeBatch();
        database.getDBConnection().commit();
    }
//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="LAYOUTS">      
    public static Layout getLayoutByName(String name) throws SQLException {
        ResultSet resultSet = getDatabase().simpleSQLQuery("SELECT id FROM Layouts "
                + "WHERE Name = \"" + name + "\" LIMIT 1");
        return getLayoutById(resultSet.getInt("id"));
    }

    public static LayoutObject getLayoutObjectById(int id) throws SQLException {
        ResultSet resultSet = database.simpleSQLQuery("SELECT * FROM "
                + "LayoutObjects Where id =" + id + " LIMIT 1");
        LayoutObject result = new LayoutObject(resultSet.getString("Name"),
                resultSet.getString("Icon"), resultSet.getInt("States"),
                resultSet.getBoolean("Referable"),
                resultSet.getBoolean("Linkable"));
        return result;
    }

    public static Layout getLayoutById(int id) throws SQLException {
        ResultSet resultSet = database.simpleSQLQuery("SELECT * FROM Layouts"
                + " WHERE id = " + id);
        Layout result = new Layout(resultSet.getString("Name"),
                resultSet.getString("Description"), resultSet.getInt("SizeX"),
                resultSet.getInt("SizeY"));
        ResultSet layoutPositions = database.simpleSQLQuery("SELECT * FROM "
                + "LayoutPositions WHERE LayoutID = " + id);
        while (layoutPositions.next()) {
            LayoutObject tempObject = getLayoutObjectById(
                    layoutPositions.getInt("LayoutObjectID"));

            LayoutPosition position = new LayoutPosition(
                    layoutPositions.getInt("PosX"), layoutPositions.getInt("PosY"),
                    tempObject);
            position.setRotation(layoutPositions.getInt("Rotation"));
            int linkedID = layoutPositions.getInt("LinkedID");
            if (tempObject.isLink()) {
                position.setLayout(getLayoutById(linkedID));
            } else if (tempObject.isReferrable()) {
                position.setArticle(getArticleById(linkedID));
            }
            result.addPosition(position);
        }
        return result;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ARTICLES">
    public static ArticleType getArticleType(int id) throws SQLException {
        ResultSet resultSet = database.simpleSQLQuery("SELECT * FROM "
                + "ArticleTypes WHERE ID=" + id + " LIMIT 1");
        ArticleType tempType = new ArticleType(resultSet.getString("Name"),
                resultSet.getString("Picture"), resultSet.getInt("States"));
        tempType.setDescription(resultSet.getString("Description"));
        return tempType;
    }

    public static Article getArticleById(int id) throws SQLException {
        ResultSet resultSet = database.simpleSQLQuery("SELECT * FROM Articles"
                + " WHERE ID=" + id + " LIMIT 1");

        Article tempArticle = new Article(resultSet.getString("Name"),
                resultSet.getInt("Address"),
                getArticleType(resultSet.getInt("ArticleType")));
        tempArticle.setDescription(resultSet.getString("Description"));
        tempArticle.setTiming(resultSet.getInt("Timing"));
        return tempArticle;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="SYSTEMS">
    private static ArrayList<Train> getTrainsBySystemID(int id) throws SQLException {
        ResultSet resultSet = database.simpleSQLQuery("SELECT Train FROM"
                + " SystemTrains WHERE System = " + id);
        ArrayList<Train> results = new ArrayList<>();
        while (resultSet.next()) {
            results.add(getTrainById(resultSet.getInt("Train")));
        }
        return results;
    }

    private static ArrayList<Article> getArticlesBySystem(int id) throws SQLException {
        ResultSet resultSet = database.simpleSQLQuery("SELECT Article FROM "
                + "SystemArticles WHERE System = " + id);
        ArrayList<Article> results = new ArrayList<>();
        while (resultSet.next()) {
            results.add(getArticleById(resultSet.getInt("Article")));
        }
        return results;
    }

    private static ArrayList<Layout> getLayoutsBySystem(int id) throws SQLException {
        ResultSet resultSet = database.simpleSQLQuery("SELECT Layout FROM"
                + " SystemLayouts WHERE System = " + id);
        ArrayList<Layout> results = new ArrayList<>();
        while (resultSet.next()) {
            results.add(getLayoutById(id));
        }
        return results;
    }

    public static ArrayList<CSSystem> getSystems()
            throws SQLException, UnknownHostException {
        ResultSet resultSet = database.simpleSQLQuery("SELECT * FROM Systems");
        ArrayList<CSSystem> results = new ArrayList<>();
        while (resultSet.next()) {
            int sysID = resultSet.getInt("Id");
            CSConnection con = new CSConnection(InetAddress.getByName(
                    resultSet.getString("Connections")));
            CSSystem tempSystem = new CSSystem(resultSet.getString("Name"),
                    resultSet.getString("Description"), con);
            tempSystem.setTrains(getTrainsBySystemID(sysID));
            tempSystem.setArticles(getArticlesBySystem(sysID));
        }
        return results;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Getter/Setter">
    /**
     * @return the database
     */
    public static SqliteDatabase getDatabase() {
        return database;
    }

    /**
     * @param aDatabase the database to set
     */
    public static void setDatabase(SqliteDatabase aDatabase) {
        database = aDatabase;
    }

    public static boolean isInstantiated() {
        return instantiated;
    }
    //</editor-fold>
}

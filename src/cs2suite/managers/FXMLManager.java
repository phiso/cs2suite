/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.managers;

import cs2suite.csUIController.FXMLBundle;
import cs2suite.csUIController.FXMLController;
import cs2suite.logic.CSLogger;
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class FXMLManager {
    
    private static FXMLManager instance;
    
    private FXMLManager(){
        super();
        CSLogger.logEvent(111, null, "FXML Manager successfully loaded.");
    }
    
    public static FXMLManager getInstance(){
        if (FXMLManager.instance == null){
            FXMLManager.instance = new FXMLManager();
        }
        return FXMLManager.instance;
    }
    
    private Parent loadRoot(FXMLLoader loader, String fxmlName) throws IOException{
        return (Parent) loader.load(getClass().getResource("/cs2suite/"+fxmlName).openStream());
    }
    
    public static FXMLBundle loadFXML(String fxmlName) throws IOException, Exception{
        FXMLLoader loader = new FXMLLoader();        
        //AnchorPane parentPane = (AnchorPane) loader.load(getClass().getResource("/cs2suite/"+fxmlName).openStream());        
        Parent root = FXMLManager.getInstance().loadRoot(loader, fxmlName);
        FXMLController controller = loader.getController();
        controller.initAfter();
        return new FXMLBundle(controller, root);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.managers;

import cs2suite.CS2Suite;
import cs2suite.logic.CSLogger;
import cs2suite.sound.Sound;
import cs2suite.sound.SoundFile;
import cs2suite.sound.SoundPlayer;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.zip.InflaterInputStream;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class SoundsManager {

    private static SoundsManager instance;
    private static ArrayList<Sound> curLoadedSounds;

    private SoundsManager() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        super();
        curLoadedSounds = new ArrayList<>();
        loadMultiAnsagenFromLibrary();        
    }

    public static SoundsManager getInstance() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        if (SoundsManager.instance == null) {
            SoundsManager.instance = new SoundsManager();
        }
        return SoundsManager.instance;
    }

    public static void loadSoundList(ArrayList<Sound> sounds) {
        curLoadedSounds.clear();
        sounds.stream().forEach((sound) -> {
            curLoadedSounds.add(sound);
        });
    }

    private void loadMultiAnsagenFromLibrary() throws UnsupportedAudioFileException, IOException, LineUnavailableException {        
        String path = CS2Suite.SOUND_LIB_DIR + "Ansagen\\Multitext";
        String name = "";
        File root = new File(path);
        int count = 0;
        for (File soundDir : root.listFiles()) {
            if (soundDir.isDirectory()) {
                name = soundDir.getName();
                ArrayList<SoundFile> soundFiles = new ArrayList<>();
                for (File wavFile : soundDir.listFiles()) {
                    if (wavFile.isFile()) {
                        soundFiles.add(new SoundFile(wavFile.getAbsolutePath()));
                    }
                }
                curLoadedSounds.add(new Sound(name, soundFiles));
                count++;
            }
        }
        CSLogger.logEvent(111, null, count+" Ansagen Sounds loaded.");                        
    }        

    private static void playSound(Sound sound) {
        SoundPlayer player = new SoundPlayer(sound);
        player.play();
    }
}

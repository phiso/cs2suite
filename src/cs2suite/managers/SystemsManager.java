/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.managers;

import cs2suite.Models.Objects.CSSystem;
import cs2suite.logic.CSLogger;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.ArrayList;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class SystemsManager {
    
    private static SystemsManager instance;
    
    private static ArrayList<CSSystem> systems;
    private static IntegerProperty activeSystemIndex;
    
    private SystemsManager(){
        super();
        systems = new ArrayList<>();
        activeSystemIndex = new SimpleIntegerProperty(-1);
        CSLogger.logEvent(111, null, "System-Manager successfully loaded.");
    }
    
    public static SystemsManager getInstance(){
        if (SystemsManager.instance == null){
            SystemsManager.instance = new SystemsManager();
        }
        return SystemsManager.instance;
    }
    
    public static int getSystemsFromdatabase() throws SQLException, UnknownHostException{
        if (SqliteManager.isInstantiated()){
            systems = SqliteManager.getSystems();
            return systems.size();
        }
        return -1;        
    }
    
    public static CSSystem getActiveSystem(){
        return systems.get(activeSystemIndex.get());
    }
    
    public static CSSystem getSystem(int index){
        return systems.get(index);
    }
    
    public static ArrayList<CSSystem> getAllSystems(){
        return systems;
    }
    
    public static void addNewSystem(CSSystem system, boolean setActive){
        systems.add(system);
        if (setActive){
            activeSystemIndex.set(systems.indexOf(system));
        }
    }
    
    public static IntegerProperty activeSystemIndexProperty(){
        return activeSystemIndex;
    }
    
    public static int getActiveSystemIndex(){
        return activeSystemIndex.get();
    }
}

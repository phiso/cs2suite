/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2suite.managers;

import cs2suite.Models.Objects.Train;
import cs2suite.csUIController.FXMLBundle;
import cs2suite.csUIController.CS_UI_MainController;
import cs2suite.fxmlControllers.TrainControlController;
import cs2suite.logic.CSLogger;
import cs2suite.logic.Settings;
import java.util.ArrayList;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class TrainsManager {

    private static TrainsManager instance;

    private static ArrayList<FXMLBundle> trainControllerBundles;
    private static int currentExistingStaticControllers;

    private TrainsManager() {
        trainControllerBundles = new ArrayList<>();
        currentExistingStaticControllers = 0;
        CSLogger.logEvent(111, null, "Train-Manager successfully loaded.");
    }

    public static TrainsManager getInstance() {
        if (TrainsManager.instance == null) {
            TrainsManager.instance = new TrainsManager();
        }
        return TrainsManager.instance;
    }

    public static TrainControlController createTrainController(Train train, boolean floating) throws Exception {
        FXMLBundle bundle = FXMLManager.loadFXML("TrainControl.fxml");
        TrainControlController controller = (TrainControlController) bundle.getController();
        TrainsManager.trainControllerBundles.add(bundle);
        controller.setTrain(train);
        if (floating) {
            CS_UI_MainController.showPopOver(train.getName(), bundle);
        } else {
            if (currentExistingStaticControllers < Settings.getMaxTrainControlCount()) {
                currentExistingStaticControllers++;
                //TODO
            }else{
                CSLogger.logEvent(6, null, "New static Controller exeeds Current Controller Limit.");
            }
        }
        return controller;
    }

    public static TrainControlController getTrainController(int index){
        return (TrainControlController) trainControllerBundles.get(index).getController();
    }
}

# README #

### Die CS2Suite ###

Die [CS2Suite]() ist eine open-source Software-Platform zur Kontrolle von Modellbahnanlagen die über eine Märklin Central-Station 2 gesteuert werden.
Das Ziel der Platform soll es sein eine kostenlose Alternative zu den wenigen, guten  Rechner gestützten Steuersystemen zu bieten und dabei einen ständig wachsenden Funktionsumfang anhand von User-Feedback zu erhalten.

Da durch die Verwendung einer Central Station bereits ein gewaltiger Funktionsumfang besteht, liegt der Fokus dieser Software auf allem was die Central Station nicht bietet und vorallem auf einer intuitiven Bedienung und automatisierungsfunktionen

### Installation ###

sobald es eine lauffähige beta gibt wird ein Setup zur verfügung gestellt.

##### Verwendete Java Bibliotheken ####

 * ini4j ver. 0.5.2
 * jfxmessagebox ver. 1.1.0
 * lwjgl (GamePad Unterstützung)
 * org.json
 * sqlite-jdbc4 ver. 3.8.2
 * controlsfx ver. 8.20.8

### Anpassungen zum ursprünglichen Projekt [CS2 Control Suite](https://bitbucket.org/phiso/cs2-control-suite) ###

* Bessere Struktur des vorhandenen Codes
* Anpassungen der Benutzeroberfläche
* Bessere Dokumentation und Hilfe
* Erweitertes JavaFX

### Ansprechpartner ###

* phiso2009@gmail.com